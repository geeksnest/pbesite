<?php

// $uri = explode("/", $_SERVER['REQUEST_URI']);

// if($uri[2]=="power-brain-school"){
// 	echo  "power-brain-school";
// }elseif($uri[2]=="benefits"){
// 	echo  "benefits";
// }elseif($uri[2]=="power-brain-training"){
// 	echo  "power-brain-training";
// }elseif($uri[2]=="power-brain-school"){
// 	echo  "power-brain-school";
// }elseif($uri[2]=="get-started"){
// 	echo  "power-brain-training";
// }
// else{
// 	// ...others
// }


error_reporting(E_ALL);

try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();
	/**
	 * Registering a router
	 */
	$di['router'] = function() {
		$router = new \Phalcon\Mvc\Router(false);

		//Main LINKS
		$router->add('/what-is-brain-education/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'slug' => 'what-is-brain-education',
			'params' => 1,
		));
		$router->add('/benefits/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'slug' => 'benefits',
			'params' => 1,
		));
		$router->add('/power-brain-training/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'slug' => 'power-brain-training',
			'params' => 1,
		));
		$router->add('/power-brain-school/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'slug' => 'power-brain-school',
			'params' => 1,
		));
		$router->add('/get-started/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'slug' => 'get-started',
			'params' => 1,
		));
		//PAGE



		$router->add('/page/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'page',
			'params' => 1,
			));

		//PAGE
		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/index/rss', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'rss'
		));
		

		//BLOG
		$router->add('/blogs', array(
			'module' => 'frontend',
			'controller' =>'blog',
			'action' => 'page',
			));

		$router->add('/blog/:params', array(
			'module' => 'frontend',
			'controller' =>'blog',
			'action' => 'read',
			'params' => 1,
			));

		$router->add('/blog/preview', array(
			'module' => 'frontend',
			'controller' =>'blog',
			'action' => 'preview',
			'params' => 1,
		));
		$router->add('/blogs/:action/:params', array(
			'module' => 'frontend',
			'controller' =>'blog',
			'action' => 1,
			'params' => 2,
			));
	$router->add('/blog/tags/:params', array(
			'module' => 'frontend',
			'controller' =>'blog',
			'action' => 'tag',
			'params' => 1,
		));
		

		//E Learning
		$router->add('/elearning', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 'verify',
			));
		$router->add('/elearning/:action', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 1,
			));
		$router->add('/elearning/:action/:params', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 1,
			'params' => 2,
			));

		$router->add('/elearning/account/activation/:params', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => "register",
			'params' => 1,
		));

		$router->add('/elearning/profile/update', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 'updateprofile'
		));

		$router->add('/elearning/profile/password', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 'password'
		));

		$router->add('/elearning/session', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 'session'
		));

		$router->add('/elearning/logout', array(
			'module' => 'frontend',
			'controller' =>'elearning',
			'action' => 'logout',
		));

		//PBE Forum
		$router->add('/elearning/forum', array(
			'module' => 'frontend',
			'controller' =>'forum',
			'action' => 'index',
		));





		//center
		$router->add('/center/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'center',
			'slug' => 'power-brain-training',
			'page' => 'find-center',
			'params' => 1,
		));


		//EVENT
		$router->add('/event/:params', array(
			'module' => 'frontend',
			'controller' =>'page',
			'action' => 'event',
			'slug' => 'power-brain-training',
			'page' => 'event',
			'params' => 1,
		));


		//Center NEws
		$router->add('/centernews/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'centernews',
			'slug' => 'power-brain-training',
			'page' => 'find-center',
			'params' => 1,
		));

		$router->add('/category/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'category',
			'slug' => 'power-brain-training',
			'page' => 'find-center',
			'params' => 1,
		));

		$router->add('/centernews/preview', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'centernewspreview',
			'slug' => 'power-brain-training',
			'page' => 'find-center',
			'params' => 1,
		));


		//Center NEws
		$router->add('/aboutauthor/:params', array(
			'module' => 'frontend',
			'controller' =>'view',
			'action' => 'aboutauthor',
			'slug' => 'power-brain-training',
			'page' => 'find-center',
			'params' => 1,
		));

		$router->add('/pbeadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
			));

		$router->add('/pbeadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1
			));

		$router->add('/pbeadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
			));
		

		$router->removeExtraSlashes(true);

		return $router;
	};

	
	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
		return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
    $di->set('modelsManager', function() {
    	return new Phalcon\Mvc\Model\Manager();
    });
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
    	return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
			),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
			)
		));

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
