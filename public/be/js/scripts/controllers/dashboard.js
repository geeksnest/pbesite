/**
 * Created by Apple on 5/14/15.
 */

// Flot Chart controller

app.controller('FlotChartDemoCtrl',function($scope, Config,$cookies, $cookieStore,$http) {
    $scope.d = [ [1,6.5],[2,6.5],[3,7],[4,8],[5,7.5],[6,7],[7,6.8],[8,7],[9,7.2],[10,7],[11,6.8],[12,7] ];

    $scope.d0_1 = [ [0,7],[1,6.5],[2,12.5],[3,7],[4,9],[5,6],[6,11],[7,6.5],[8,8],[9,7] ];

    $scope.d0_2 = [ [0,4],[1,4.5],[2,7],[3,4.5],[4,3],[5,3.5],[6,6],[7,3],[8,4],[9,3] ];

    $scope.d1_1 = [ [10, 120], [20, 70], [30, 70], [40, 60] ];

    $scope.d1_2 = [ [10, 50],  [20, 60], [30, 90],  [40, 35] ];

    $scope.d1_3 = [ [10, 80],  [20, 40], [30, 30],  [40, 20] ];

    $scope.d2 = [];

    for (var i = 0; i < 20; ++i) {
        $scope.d2.push([i, Math.sin(i)]);
    }

    $scope.d3 = [
    { label: "iPhone5S", data: 40 },
    { label: "iPad Mini", data: 10 },
    { label: "iPad Mini Retina", data: 20 },
    { label: "iPhone4S", data: 12 },
    { label: "iPad Air", data: 18 }
    ];

    $scope.getRandomData = function() {
        var data = [],
        totalPoints = 150;
        if (data.length > 0)
            data = data.slice(1);
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        // Zip the generated y values with the x values
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }

    $scope.d4 = $scope.getRandomData();
    var loadlist = function(){
        $http({
        url:  Config.ApiURL+"/dashboard/api",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.countevent = data.countevent;
            $scope.countnews = data.countnews;
            $scope.countvideo = data.countvideo;
        })
    }
    loadlist();       
});
app.controller('NewmessageCtrl',function($scope, Config,$cookies, $cookieStore,$http,$modal) {
    
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.hideme = true;
    $scope.hideme1 = true;
    var loadlist = function(){
        $http({
        url:  Config.ApiURL+"/dashboard/api",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.newdata = data.data;
            $scope.newdata1 = data.data1;
            $scope.count = data.count;
            $scope.count1 = data.count1;
            $scope.countevent = data.countevent;
            if(data.count == 0){
                $scope.hideme = false;
            }
            if(data.count1 == 0){
                $scope.hideme1 = false;
            }
            


        })
    }
    loadlist();


    var messsagesent = function(){
            $scope.alerts.push({ type: 'success', msg: 'MESSAGE SENT!' });
    }

    ///MODAL REVIEW REQUEST

        $scope.reviewmodal = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'requestReview.html',
                controller: reviewrequestCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewrequestCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/request/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/request/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.email = data.email;
            $scope.reply.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })

        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/request/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                messsagesent();
                $modalInstance.dismiss('cancel');
                loadlist();
            }).error(function (data, status, headers, config) {

            });


        };

    };



    ///////////////////////

    ///MODAL appointment

        $scope.reviewmodal1 = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'appointmentReview.html',
                controller: reviewappointmentCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewappointmentCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/appointment/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/appointment/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.timesched = data.timesched;
            $scope.datesched = data.datesched;
            $scope.reply.email = data.email;
            $scope.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })
        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/appointment/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                loadlist();
                messsagesent();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
            }); 

        };

    };
});
app.controller('Uniquevisitorctrl',function($scope, Config,$cookies, $cookieStore,$http,$modal) {
   $http({
        url:  Config.ApiURL+"/visitor/count",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        $scope.count = data;
    })
});