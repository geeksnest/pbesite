'use strict';
app.controller('Createcenter', function($scope, $state ,$q, $http, Config, $modal, CenterPage, $log, $timeout){

  $scope.bt1=true;
  $scope.bt2=false;
  $scope.userid = userid;
  $scope.onpagetitle = function convertToSlug(Text){
    if(Text=="" || Text==null){
      $scope.center.slugs = " ";
    }else{
     CenterPage.convertslug(Text, function(data){
       $scope.center.slugs = data;
     },function(datavalidate){
      if(datavalidate==1){
        $scope.notification = true;
        $scope.bt1=false;
        $scope.bt2=true;
      }else{
        $scope.notification = false;
        $scope.bt1=true;
        $scope.bt2=false;
      }
    }


    );
   }



 }



 CenterPage.userlist( function(data){
   $scope.centerManager = data;
 });

 CenterPage.statelist( function(data){
  $scope.statelist = data;
});
 $scope.selectstate =function(state){
  CenterPage.citylist(state, function(data){
    $scope.citylist = data;
  });
};


////Map Here
$scope.lat=37.779041320202126;
$scope.lon=-122.41946411132812;
$scope.map = { center: { latitude: 37.779041320202126, longitude: -122.41946411132812 }, zoom: 15};
$scope.marker = { id: 0, coords: { latitude: 37.779041320202126, longitude: -122.41946411132812 },
options: { draggable: true },
events: {
  dragend: function (marker, eventName, args) {
   $scope.lat = marker.getPosition().lat();
   $scope.lon = marker.getPosition().lng();

   $scope.marker.options = {
    draggable: true,
    labelAnchor: "100 0",
    labelClass: "marker-labels"
  };
}
}
}
////Map Here





$scope.showimageList = function(size){
  var amazon = $scope.amazon;
  var modalInstance = $modal.open({
    templateUrl: 'imagelist.html',
    controller: imagelistCTRL,
    size: size,
    resolve: {
      path: function() {
        return amazon
      }
    }

  });
}

var pathimage = "";

var pathimages = function(){
  $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
  $scope.amazonpath= path;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    CenterPage.loadimage(function(data){
      if(data.error == "NOIMAGE" ){
        $scope.imggallery=false;
        $scope.noimage = true;
      }else{
        $scope.noimage = false;
        $scope.imggallery=true;
        $scope.imagelist = data;
      }
    });
  }
  loadimages();

  $scope.path=function(path){
    var texttocut = Config.amazonlink + '/uploads/banner/';
    var newpath = path.substring(texttocut.length); 
    pathimage = newpath;
    pathimages();
    $modalInstance.dismiss('cancel');
  }

  $scope.upload = function(files) {
   $scope.upload(files);  
 };

 $scope.delete = function(id){
  var modalInstance = $modal.open({
    templateUrl: 'delete.html',
    controller: deleteCTRL,
    resolve: {
      imgid: function() {
        return id
      }
    }

  });

}

var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
     url: Config.ApiURL+"/centernewsimage/delete/"+ imgid,
     method: "get",
     headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}


$scope.upload = function (files) 
{
  var filename
  var filecount = 0;
  if (files && files.length) 
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++) 
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;
        var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                      var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                      promises = Upload.upload({

                       url:Config.amazonlink, 
                       method: 'POST',
                       transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                key: 'uploads/banner/' + renamedFile, 
                                AWSAccessKeyId: Config.AWSAccessKeyId,
                                acl: 'private', 
                                policy: Config.policy, 
                                signature: Config.signature, 
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
                              },
                              file: file
                            })
                      promises.then(function(data){

                        filecount = filecount + 1;
                        filename = data.config.file.name;
                        var fileout = {
                          'imgfilename' : renamedFile,
                          'userid' : userid
                        };


  ////UPLOAD FACTORY
  CenterPage.saveimage(fileout, function(data){
    if(data[0].success){
     loadimages();
     if(filecount == files.length)
     {
      $scope.imageloader=false;
      $scope.imagecontent=true;
    }
  }else{
   $scope.imageloader=false;
   $scope.imagecontent=true;
 }
});



});
                    }



                  }
                }
              };



              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');

              };
            };
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////






$scope.save = function(center){
  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };
  $scope.isSaving = true;
  CenterPage.savecenter(center, function(data){
    if(data[0].success){
     $scope.isSaving = false;
     $scope.alerts.push({type: 'success', msg: 'Center successfully saved!'});
   }else{
    $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
  }
});
}



jQuery(function($){
 $("#phone").mask("(999) 999-9999");
});





});'use strict';

/* Controllers */

