'use strict';
app.controller('Updatecenter', function($scope, $state, $stateParams ,$q, $http, Config, $modal, UpdateCenter, CenterPage){
	 $scope.onpagetitle = function convertToSlug(Text){
   CenterPage.convertslug(Text, function(data){
     $scope.center.slugs = data;
   });
 }
	$scope.centerid=$stateParams.centerid;

	CenterPage.statelist( function(data){
		$scope.statelist = data;
	});

	$scope.selectstate =function(state){

		CenterPage.citylist(state, function(data){
			$scope.citylist = data;
		});
	};

	UpdateCenter.getdata($stateParams.centerid, function(data){

		$scope.center=data.data;
		$scope.amazonpath=data.data.banner;
    $scope.defaultmanager = data.data.manager;
    $scope.centerManager = data.users;


		CenterPage.citylist(data.state, function(data){
			$scope.citylist = data.state;
		});
	});


$scope.showimageList = function(size,path){
  var amazon = $scope.amazon;
  var modalInstance = $modal.open({
    templateUrl: 'imagelist.html',
    controller: imagelistCTRL,
    size: size,
    resolve: {
      path: function() {
        return amazon
      }
    }

  });
}

var pathimage = "";

var pathimages = function(){
  $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
  $scope.amazonpath= path;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    CenterPage.loadimage(function(data){
      if(data.error == "NOIMAGE" ){
              $scope.imggallery=false;
              $scope.noimage = true;
            }else{
              $scope.noimage = false;
              $scope.imggallery=true;
              $scope.imagelist = data;
            }
    });
  }
  loadimages();

  $scope.path=function(path){
    var texttocut = Config.amazonlink + '/uploads/banner/';
    var newpath = path.substring(texttocut.length); 
    pathimage = newpath;
    pathimages();
    $modalInstance.dismiss('cancel');
  }

  $scope.upload = function(files) {
   $scope.upload(files);  
 };


 $scope.upload = function (files) 
 {
  var filename
  var filecount = 0;
  if (files && files.length) 
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++) 
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;
        var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
        var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

        promises = Upload.upload({

         url:Config.amazonlink, 
         method: 'POST',
         transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                key: 'uploads/banner/' + renamedFile, 
                                AWSAccessKeyId: Config.AWSAccessKeyId,
                                acl: 'private', 
                                policy: Config.policy, 
                                signature: Config.signature, 
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
                              },
                              file: file
                            })
        promises.then(function(data){

          filecount = filecount + 1;
          filename = data.config.file.name;
          var fileout = {
            'imgfilename' : renamedFile
          };


  ////UPLOAD FACTORY
  CenterPage.saveimage(fileout, function(data){
    if(data[0].success){
     loadimages();
     if(filecount == files.length)
     {
      $scope.imageloader=false;
      $scope.imagecontent=true;
    }
  }else{
   $scope.imageloader=false;
   $scope.imagecontent=true;
 }
});



});
      }



    }
  }
};



$scope.cancel = function() {
  $modalInstance.dismiss('cancel');

};
};

$scope.save = function(center){
  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  UpdateCenter.savecenter(center, function(data){
    if(data[0].success){
     $scope.alerts.push({type: 'success', msg: 'Center successfully saved!'});

   }else{
    $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
  }
});
}


  jQuery(function($){
   $("#phone").mask("(999) 999-9999");
  });






//CENTER SCHEDULE GOES HERE












});



