'use strict';
app.controller('Managecenter', function($scope, $state ,$q, $http, Config, $modal, ManageCenter){

	$scope.alerts = [];

	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	$scope.currentstatusshow = '';


	var paginate = function (off, keyword) {
		ManageCenter.listcenter(num,off, keyword, function(data){
			$scope.data = data;

			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}

	paginate(off, keyword);

	$scope.search = function (keyword) {
		 var off = 1;
    	 paginate(off, keyword);
	}

	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
    	paginate(off, searchito);
	}

	$scope.setPage = function (pageNo) {
		var searchito = $scope.searchtext;
    	paginate(pageNo, searchito);
	};

	$scope.changestatus = function(id,status){
		$http({
				url:  Config.ApiURL+"/center/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				loadpage();
			})
	};

	$scope.resetsearch = function(){
          $scope.searchtext = undefined;
          paginate(off, keyword);
  	}


	var loadpage = function(){

		ManageCenter.listcenter(num,$scope.bigCurrentPage, $scope.searchtext, function(data){
			$scope.data = data;
			$scope.maxSize = 10;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});

	}


	$scope.deletepage = function(pageid) {
		var modalInstance = $modal.open({
			templateUrl: 'CenterDelete.html',
			controller: pageDeleteCTRL,
			resolve: {
				id: function() {
					return pageid
				}
			}
		});
	}


	var successloadalert = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({ type: 'success', msg: 'Center successfully Deleted!' });
	}
	var errorloadalert = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({ type: 'danger', msg: 'Something went wrong Center not Deleted!' });
	}



	var pageDeleteCTRL = function($scope, $modalInstance, id) {
		$scope.ok = function() {
			ManageCenter.deletecenter(id, function(data){
				if(data[0].success){
					loadpage();
					$modalInstance.close();
					successloadalert();
				}else{
					loadpage();
					$modalInstance.close();
					errorloadalert();
				}
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


	};



	$scope.editpage = function(id) {
		$scope.newsid
		var modalInstance = $modal.open({
			templateUrl: 'CenterEdit.html',
			controller: pageEditCTRL,
			resolve: {
				id: function() {
					return id
				}
			}
		});
	}
	var pageEditCTRL = function($scope, $modalInstance, id, $state) {
	
		$scope.ok = function() {
			$state.go('editCenter', {centerid: id });
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}


});'use strict';

/* Controllers */

