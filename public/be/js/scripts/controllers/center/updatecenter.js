'use strict';
app.controller('Updatecenter', function($scope, $state, $stateParams ,$q, $http, Config, $modal, UpdateCenter, CenterPage, Sched, Centermap){

  $scope._showForm = true;
  $scope.cntrMap =false;

  $scope.hideForm = function(){
   $scope._showForm = false;
   $scope.cntrMap =false;
 }
 $scope.showForm = function(){
   $scope._showForm = true;
 }
 $scope.ShowMap = function(){
  $scope.cntrMap =true;
  $scope._showForm = false;
}

$scope.onpagetitle = function convertToSlug(Text){
 CenterPage.convertslug(Text, function(data){
   $scope.center.slugs = data;
 });
}
$scope.centerid=$stateParams.centerid;

CenterPage.statelist( function(data){
  $scope.statelist = data;
});

$scope.selectstate =function(state){

  CenterPage.citylist(state, function(data){
   $scope.citylist = data;
 });
};

UpdateCenter.getdata($stateParams.centerid, function(data){
  $scope.center=data.data;
  $scope.amazonpath=data.data.banner;
  $scope.defaultmanager = data.data.manager;
  $scope.centerManager = data.users;


  CenterPage.citylist(data.state, function(data){
   $scope.citylist = data.state;
 });
});




$scope.showimageList = function(size,path){
  var amazon = $scope.amazon;
  var modalInstance = $modal.open({
    templateUrl: 'imagelist.html',
    controller: imagelistCTRL,
    size: size,
    resolve: {
      path: function() {
        return amazon
      }
    }

  });
}

var pathimage = "";

var pathimages = function(){
  $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
  $scope.amazonpath= path;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    CenterPage.loadimage(function(data){
      if(data.error == "NOIMAGE" ){
        $scope.imggallery=false;
        $scope.noimage = true;
      }else{
        $scope.noimage = false;
        $scope.imggallery=true;
        $scope.imagelist = data;
      }
    });
  }


  $scope.path=function(path){
    var texttocut = Config.amazonlink + '/uploads/banner/';
    var newpath = path.substring(texttocut.length); 
    pathimage = newpath;
    pathimages();
    $modalInstance.dismiss('cancel');
  }

  $scope.upload = function(files) {
   $scope.upload(files);  
 };


 $scope.upload = function (files) 
 {
  var filename
  var filecount = 0;
  if (files && files.length) 
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++) 
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;
        var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                      var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                      promises = Upload.upload({

                       url:Config.amazonlink, 
                       method: 'POST',
                       transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                key: 'uploads/banner/' + renamedFile, 
                                AWSAccessKeyId: Config.AWSAccessKeyId,
                                acl: 'private', 
                                policy: Config.policy, 
                                signature: Config.signature, 
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
                              },
                              file: file
                            })
                      promises.then(function(data){

                        filecount = filecount + 1;
                        filename = data.config.file.name;
                        var fileout = {
                          'imgfilename' : renamedFile
                        };


  ////UPLOAD FACTORY
  CenterPage.saveimage(fileout, function(data){
    if(data[0].success){
     loadimages();
     if(filecount == files.length)
     {
      $scope.imageloader=false;
      $scope.imagecontent=true;
    }
  }else{
   $scope.imageloader=false;
   $scope.imagecontent=true;
 }
});



});
                    }



                  }
                }
              };



              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');

              };
            };

            $scope.save = function(center){
              $scope.alerts = [];
              $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
              };

              UpdateCenter.savecenter(center, function(data){

                if(data[0].success){
                 $scope.alerts.push({type: 'success', msg: 'Center successfully saved!'});

               }else{
                $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
              }
            });
            }


            jQuery(function($){
             $("#phone").mask("(999) 999-9999");
           });





//@========================
//CENTER SCHEDULE GOES HERE
//@========================
//@========================




var loadscheds =function(){
  Sched.getsched($scope.centerid, function(data){

      // console.log(data);
      $scope.sched = data;
    });
}
loadscheds();

$scope.schedmodal = function(day, centerid, userid) {


  var modalInstance = $modal.open({
    templateUrl: 'schedule.html',
    controller: scheduleCTRL,
    resolve: {
      day: function() {
        return day
      },
      centerid: function() {
        return $scope.centerid
      },
      userid: function(){
        return userid
      }
    }
  });
}  
var scheduleCTRL = function($scope, $modalInstance, day, centerid, userid,  $http, $stateParams ) {
 
  $scope.userid = userid;
  $scope.centerid= centerid;
  $scope.day =day;
  $scope.save =function(sched){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.isSaving = true;
    Sched.savesched(sched, function(data){

      console.log(data);

      if(data[0].success){
       loadscheds();
       $scope.isSaving = false;
       $scope.alerts.push({type: 'success', msg: 'New Class Schedule on '+ sched.day +' successfully Created!'});
     }else{
      $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
    }
  });
    $modalInstance.dismiss('cancel');
  }  
  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };
}; 

  ////////////////////////Update
  $scope.update = function(schedid ,day) {
    var modalInstance = $modal.open({
      templateUrl: 'scheduleedit.html',
      controller: updatescheduleCTRL,
      resolve: {
        schedid: function() {
          return schedid
        },
        day: function() {
          return day
        },
      }
    });
  }  

  var updatescheduleCTRL = function($scope, $modalInstance, schedid, day, $http, $stateParams ) {
   $scope.day =day;
   $scope.schedid = schedid;
   Sched.getschededit(schedid, function(data){
    $scope.data = data;
    
  });


   $scope.updatesched =function(sched){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.isSaving = true;
    Sched.updatesched(sched, function(data){

      console.log(data);

      if(data[0].success){
        loadscheds();
      }
    });
    $modalInstance.dismiss('cancel');

    loadscheds();
  }  

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  };
}; 

  ////////////////////////DELETE
  $scope.delete = function(schedid) {

    var modalInstance = $modal.open({
      templateUrl: 'delete.html',
      controller: DeleteCTRL,
      resolve: {
        schedid: function() {
          return schedid
        }
      }
    });
  } 
  var DeleteCTRL = function($scope, $modalInstance,  $http, $stateParams, schedid ) {

    $scope.message ="Are You Sure do You Want to delete this schedule?";
    $scope.ok = function(){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      Sched.delete(schedid,userid, function(data){
        if(data[0].success){
         loadscheds();
         $scope.isSaving = false;
         $scope.alerts.push({type: 'success', msg: 'New Class Schedule Has been Delete'});
         $modalInstance.dismiss('cancel');
       }else{
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      }
    });
    };

    $scope.cancel = function(){
      $modalInstance.dismiss('cancel');
    };
  }; 




//@========================
//CENTER MAP LOCATION GOES HERE
//@========================
//@========================


var coordinates = function(){
  Centermap.getmap($scope.centerid , function(data){

    if(data.lat==null && data.lon==null){
        ///////////////////////////////
        $scope.map = { center: { latitude: 38, longitude: -102 }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: 38, longitude: -102 },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           var lat = marker.getPosition().lat();
           var lon = marker.getPosition().lng();
           Centermap.savemap(lat, lon, $scope.centerid , function(data){
           });
           $scope.marker.options = {
            draggable: true,
            labelAnchor: "100 0",
            labelClass: "marker-labels"
          };
        }
      }
    }
         ///////////////////////////////

       }else{
        ///////////////////////////////
        $scope.map = { center: { latitude: data.lat, longitude: data.lon }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: data.lat, longitude: data.lon },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           var lat = marker.getPosition().lat();
           var lon = marker.getPosition().lng();
               // $scope.lats = lat;
               // $scope.lons = lon;


               Centermap.savemap(lat, lon, $scope.centerid , function(data){

                console.log(data);

              });


               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
              };
            }
          }
        }
      }
    }); 
}
coordinates();

});



