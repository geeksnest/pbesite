'use strict';

/* Controllers */

angular.module('app.controllers', ['pascalprecht.translate', 'ngCookies'])

  .controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window',
    function(              $scope,   $translate,   $localStorage,   $window ) {
      // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');


      // config
      $scope.app = {
        name: 'B & B',
        version: '1.0.0',
        // for chart colors
        color: {
          primary: '#7266ba',
          info:    '#23b7e5',
          success: '#27c24c',
          warning: '#fad733',
          danger:  '#f05050',
          light:   '#e8eff0',
          dark:    '#3a3f51',
          black:   '#1c2b36'
        },
        settings: {
          themeID: 1,
          navbarHeaderColor: 'bg-white',
          navbarCollapseColor: 'bg-white-only',
          asideColor: 'bg-black',
          headerFixed: true,
          asideFixed: false,
          asideFolded: false
        }
      }

      // save settings to local storage
      if ( angular.isDefined($localStorage.settings) ) {
        $scope.app.settings = $localStorage.settings;
      } else {
        $localStorage.settings = $scope.app.settings;
      }
      $scope.$watch('app.settings', function(){ $localStorage.settings = $scope.app.settings; }, true);

      // angular translate
      $scope.lang = { isopen: false };
      $scope.langs = {en:'English', de_DE:'German', it_IT:'Italian'};
      $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
      $scope.setLang = function(langKey, $event) {
        // set the current lang
        $scope.selectLang = $scope.langs[langKey];
        // You can change the language during runtime
        $translate.use(langKey);
        $scope.lang.isopen = !$scope.lang.isopen;
      };

      function isSmartDevice( $window )
      {
          // Adapted from http://www.detectmobilebrowsers.com
          var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
          // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
          return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
      }

  }])

  .controller('TypeaheadDemoCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.selected = undefined;
    $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
    // Any function returning a promise object can be used to load values asynchronously
    $scope.getLocation = function(val) {
      return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
        params: {
          address: val,
          sensor: false
        }
      }).then(function(res){
        var addresses = [];
        angular.forEach(res.data.results, function(item){
          addresses.push(item.formatted_address);
        });
        return addresses;
      });
    };
  }])
  ;
  app.controller('NewmessageCtrl',function($scope, Config,$cookies, $cookieStore,$http,$modal,$interval) {
    
    $scope.alerts = [];
    var count =0;
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.hideme = true;
    $scope.showmore = false;

    

    var loadlist = function(){
      $http({
        url:  Config.ApiURL+"/dashboard/notification/list/"+ userid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.notiflist = data.data;
        if(data.countnew == 0){
          $scope.count = 0;
          $scope.hideme = false;
          $scope.showmore = false;
        }
        else if(data.countnew <= 50){
          $scope.count = data.countnew;
          $scope.hideme = true;
        }else{
          $scope.count = 50;
          $scope.hideme = true;
        }
        if(data.countnew > 10){
          $scope.showmore = true;
        }
        if(count!=data.countnew)
          {var audio = new Audio('/notifi.mp3');
        audio.play();
        count=data.countnew;
        $('#notif').addClass('animated1 shake infinite');
       }
      })
    }
    $scope.removeclass =function(){
      $('#notif').removeClass('animated1 shake infinite');
    }
    var loadlist1 = function(){
      $http({
        url:  Config.ApiURL+"/dashboard/notification/list/"+ userid,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.notiflist = data.data;
        count = data.countnew;
        if(data.countnew == 0){
          $scope.count = 0;
          $scope.hideme = false;
          $scope.showmore = false;
        }
        else if(data.countnew <= 50){
          $scope.count = data.countnew;
          $scope.hideme = true;
        }else{
          $scope.count = 50;
          $scope.hideme = true;
        }
        if(data.countnew > 10){
          $scope.showmore = true;
        }
        $('#notif').addClass('animated1 shake infinite');
      })
    }
    loadlist1();

    $interval(function(){
      loadlist();
    },10000);

    var limitStep = 5;
    $scope.limit = limitStep;
    $scope.incrementLimit = function() {
     $scope.limit += limitStep;
    };
    $scope.decrementLimit = function() {
     $scope.limit -= limitStep;
    };





    var messsagesent = function(){
            $scope.alerts.push({ type: 'success', msg: 'MESSAGE SENT!' });
    }

    ///MODAL REVIEW REQUEST

        $scope.reviewmodal = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'requestReview.html',
                controller: reviewrequestCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewrequestCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/request/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/request/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.email = data.email;
            $scope.reply.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })

        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/request/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                messsagesent();
                $modalInstance.dismiss('cancel');
                loadlist();
            }).error(function (data, status, headers, config) {

            });


        };

    };



    ///////////////////////

    ///MODAL appointment

        $scope.reviewmodal1 = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'appointmentReview.html',
                controller: reviewappointmentCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewappointmentCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/appointment/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/appointment/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.timesched = data.timesched;
            $scope.datesched = data.datesched;
            $scope.reply.email = data.email;
            $scope.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })
        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/appointment/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                loadlist();
                messsagesent();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
            }); 

        };

    };
});