'use strict';

/* Controllers */

app.controller('Createpage', function($scope, $state ,$q, $http, Config, $modal, Addpage,Fileupload, $anchorScroll){



  
  $scope.stat="true";
  $scope.onpagetitle = function convertToSlug(Text)
  {
    if(Text=="" || Text==null){
      $scope.page.slugs = " ";
    }else{
     Addpage.convertslug(Text, function(data){
       $scope.page.slugs = data;
     });
   }

 }
    // SELECT MENU OPTION
    $scope.radio = function(text)
    {
      if(text == 1)
      {
        $scope.sidebar = true;
        $scope.bghead1="panel panel-default";
        $scope.bghead="panel-heading font-bold";
        $scope.parentsub = false;
      }
      else
      {
       $scope.sidebar = true;
       $scope.bghead1="panel b-a";
       $scope.bghead="panel-heading no-border bg-primary";

     }
   }
    //LIST MAIN MENU
    Addpage.parentmenu(function(data){
      $scope.data = data;
    });
    //END LIST MAIN MENU
    $scope.showsub= function(id){
     if($scope.page.menutyped=="childsub"){
      Addpage.parentsub(id, function(data){
        $scope.listsub = data;
      });  
      $scope.parentsub = true;
    }else{
     $scope.parentsub = false;
   }
 }
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.showimageList = function(size,path){
  var amazon = $scope.amazon;
  var modalInstance = $modal.open({
    templateUrl: 'imagelist.html',
    controller: imagelistCTRL,
    size: size,
    resolve: {
      path: function() {
        return amazon
      }
    }

  });
}

var pathimage = "";

var pathimages = function(){
  $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
  $scope.amazonpath= path;
  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    Addpage.loadimage(function(data){
      if(data.error == "NOIMAGE" ){
        $scope.imggallery=false;
        $scope.noimage = true;
      }else{
        $scope.noimage = false;
        $scope.imggallery=true;
        $scope.imagelist = data;
      }
    });
  }
  loadimages();

  $scope.path=function(path){
   var texttocut = Config.amazonlink + '/uploads/banner/';
   var newpath = path.substring(texttocut.length); 
   pathimage = newpath;
   pathimages();
   $modalInstance.dismiss('cancel');
  }



  $scope.upload = function(files) {
   $scope.upload(files);  
 };

 $scope.delete = function(id){
  var modalInstance = $modal.open({
    templateUrl: 'delete.html',
    controller: deleteCTRL,
    resolve: {
      imgid: function() {
        return id
      }
    }

  });

}

var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
     url: Config.ApiURL+"/bannerimage/delete/"+ imgid,
     method: "get",
     headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}


$scope.upload = function (files) 
{
  var filename
  var filecount = 0;
  if (files && files.length) 
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++) 
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;

        var fileExtension = '.' + file.name.split('.').pop();

                // rename the file with a sufficiently random value and add the file extension back
                var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
                
                promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                  key: 'uploads/banner/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                              })
promises.then(function(data){

  filecount = filecount + 1;
  filename = data.config.file.name;
  var fileout = {
    'imgfilename' : renamedFile
  };
  $http({
    url: Config.ApiURL + "/upload/banner",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(fileout)
  }).success(function (data, status, headers, config) {
    loadimages();
    if(filecount == files.length)
    {
      $scope.imageloader=false;
      $scope.imagecontent=true;
    }

  }).error(function (data, status, headers, config) {
    $scope.imageloader=false;
    $scope.imagecontent=true;
  });

});
}



}
}
};





$scope.cancel = function() {
  $modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////

$scope.savePage = function(page){
  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };
  Addpage.savepage(page, function(data){
    if(data[0].success){
     $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
     $scope.sidebar = false;
     $scope.parentsub = false;
      $scope.formpage.$setPristine(true);
      $scope.page ="";
   }else{
    $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
  }
});
}

})




