
// Flot Chart controller

// app.controller('UserloginCtrl', ['$scope', '$http', function($scope, $http, User,Config) {
	
// }]);

app.controller('UserStudentListCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config) {
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/userstudent/list/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
        paginate(off, searchito);
    };
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
          $scope.searchtext = undefined;
          paginate(off, keyword);
  }


	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				loadpage();
			})
	};

	var loadpage = function(){
        $http({
			url: Config.ApiURL+"/userstudent/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;
		});
    }



	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);

