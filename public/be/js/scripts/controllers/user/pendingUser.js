

app.controller('PendingListCtrl',  function ($http, $scope, $stateParams, $modal,$sce,Config, PbeUser, $templateCache, $window){
	$scope.clk = function(){
		$window.location.reload();
	}
	var _listUser = function(type, _role){
		$scope.data = {};
		var num = 10;
		var off = 1;
		var keyword = null;
		var paginate = function (off, keyword) {
			PbeUser.list(type, num, off, keyword, _role, function(_rdata){
				$scope.pendinguser = _rdata.data.length;
				$scope.data = _rdata;
				$scope.maxSize = 5;
				$scope.bigTotalItems = _rdata.total_items;
				$scope.bigCurrentPage = _rdata.index;
			});
		}
		$scope.search = function (keyword) {
			var off = 1;
			paginate(off, keyword);
		}
		$scope.numpages = function (off, keyword) {
			var searchito = $scope.searchtext;
			paginate(off, searchito);
		}
		$scope.setPage = function (off) {
			var searchito = $scope.searchtext;
			paginate(off, searchito);
		};
		paginate(off, keyword);
	}

	$scope._listall = function(){
		_listUser('listall','');
		//DELETE USER
		$scope.alldelete = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: alldeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var alldeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('listall','');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				_listUser('listall','');
			})
		};
		
	}

	$scope._pending = function(){
		_listUser('pending','');
		//DELETE USER
		$scope.deletepending = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: tacherdeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var tacherdeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('pending','');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				_listUser('userteacher','teacher');
			})
		};
	}

	$scope._addedusers = function(){
		_listUser('addedusers','');
		//DELETE USER
		$scope.deletepending = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: tacherdeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var tacherdeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('addedusers','');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				_listUser('userteacher','teacher');
			})
		};
	}


	$scope._teacher = function(){
		_listUser('userteacher','teacher');
		$scope.reset = function(){
			_listUser('userteacher','teacher');
		}
		//DELETE USER
		$scope.tacherdelete = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: tacherdeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var tacherdeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('userteacher','teacher');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {

				console.log(data);
				_listUser('userteacher','teacher');
			})
		};
	}

	$scope._mentor = function(){
		$scope.reset = function(){
			_listUser('usermentor','mentor');
		}
		_listUser('usermentor','mentor');

		//DELETE USER
		$scope.mentordelete = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: mentordeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var mentordeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('usermentor','mentor');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}

		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				_listUser('usermentor','mentor');
			})
		};
		
	}

	$scope._leader = function(){
		$scope.reset = function(){
			_listUser('usermentor','leader');
		}
		_listUser('usermentor','leader');
		//DELETE USER
		$scope.leaderdelete = function(user){
			var modalInstance = $modal.open({
				templateUrl: 'userDelete.html',
				controller: leaderdeletedltCTRL,
				resolve: {dltuser: function () {return user;}}
			});
		};
		$scope.alerts = [];
		$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
		var alertme = function(){$scope.alerts.splice(0, 1);$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});}
		var leaderdeletedltCTRL = function ($scope, $modalInstance, dltuser) {
			$scope.dltuser = dltuser;
			$scope.ok = function (dltuser) {
				$http({
					url:  Config.ApiURL+"/user/delete/"+dltuser,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$modalInstance.dismiss('cancel');
					alertme();
					_listUser('usermentor','leader');
				})
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		//DELETE USER
		$scope.changestatus = function(id,status){
			$http({
				url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				_listUser('usermentor','leader');
			})
		};
	}

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

	$scope.review = function(_gdata){
		var modalInstance = $modal.open({
			templateUrl: 'review.html',
			controller: reviewCtrl,
			resolve: {
				_ddata: function () {
					return _gdata;
				}
			}
		});
	}
	var reviewCtrl = function($scope, $modalInstance, $state, _ddata){
		$scope.user =_ddata;
		$scope.confirm = function(_gdata){
			$http({
				url: Config.ApiURL +"/confrim/user",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(_gdata)
			}).success(function (data, status, headers, config) {
				_listUser('pending','');
				$modalInstance.dismiss('cancel');
				console.log(data);
			})
		}
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
});



app.controller('UserListAllCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/listall/list/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/listall/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}
	
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);


app.controller('UserListTeacherCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/userteacher/list/" + num + '/' + off + '/' + keyword +'/teacher',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/userteacher/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext +'/teacher',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);

app.controller('UserListMentorCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/usermentor/list/" + num + '/' + off + '/' + keyword +'/mentor',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/usermentor/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext +'/mentor',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);


app.controller('UserListLeaderrCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/usermentor/list/" + num + '/' + off + '/' + keyword +'/leader',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/usermentor/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext +'/leader',
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);