'use strict';

/* Controllers */
app.controller('BlogCtrl', function($scope, $state ,$q, $http,$stateParams, Config,Upload, $modal,$sce, NewsCenter, BlogFeatured, $filter){

   ////LIST CATEGORY
   var _categ = function(){
       BlogFeatured.loadcategory(function(data){
        $scope.category = data;
    });
   }
   _categ();
    ////LIST TAGS
    // BlogFeatured.loadtags(function(data){

    //     var newlist = [];
    //     $scope.select2Options = {
    //         'multiple': true,
    //         'simple_tags': true,
    //         'tags': newlist  
    //     };
    //     $scope.tag = newlist;
    // });


    var loadtags = function()
    {
        console.log('tags display');
         BlogFeatured.loadtags(function(data){

            console.log(data);
            var newlist = [];
            data.map(function(val){
                newlist.push(val.tags);
            });
            $scope.select2Options = {
                'multiple': true,
                'simple_tags': true,
                'tags': newlist  // Can be empty list.
            };
            $scope.tag = newlist;
            console.log($scope.tag, "HERE =============")
        })
    }
    loadtags();




    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.videocontent=true;
    $scope.contentvideo='';
    $scope.thumbnails = "image";    
    $scope.news = {
      title: "",
      category: '',
      tag: []
  };


  $scope.editslug = false;
  $scope.editedslug = false;
  var slugstorage = "";

  var orinews = angular.copy($scope.news);


  $scope.onnewstitle = function convertToSlug(Text)
  {
    if(Text != null  && $scope.editedslug == false && $scope.editslug == false)
    {
        var text1 = Text.replace(/[^\w ]+/g,'');
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
}

$scope.onslugs = function(Text){
    if(Text != null)
    {
        $scope.news.slugs = Text.replace(/\s+/g, '-').toLowerCase();
    }
}

$scope.editnewsslug = function(){
    $scope.editslug = true;
    slugstorage = $scope.news.slugs;
}

$scope.cancelnewsslug = function(title) {
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.news.slugs = slugstorage;
    }else {
        $scope.news.slugs = '';
        $scope.editedslug = false;
    }
    $scope.editslug = false;
}

$scope.setslug = function(slug){
    $scope.editedslug = true;
    $scope.editslug = false;
    if(slug != null)
    {
        $scope.news.slugs = slug.replace(/\s+/g, '-').toLowerCase();

    }
}

$scope.clearslug = function(title){
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.news.slugs = title.replace(/\s+/g, '-').toLowerCase();
    }else {
        $scope.news.slugs = '';
        $scope.editedslug = false;
    }
}

$scope.preview = function(news){
    $scope.author.map(function(val){
        if(news.author == val.authorid){
           news['authorname'] = val.name;
           news['authorimage'] = val.image;
           news['authorabout'] = val.about;
       }
   });
    var catlist = [];
    $scope.category.map(function(val){
        news.category.map(function(val2){
            if(val['categoryid'] == val2){
                catlist.push(val['categoryname']);
            }
        });
    });

    news['categoryname'] = catlist.join();
    news['tagname'] = news['tag'].join();
    var returnYoutubeThumb = function(item){
        var x= '';
        var thumb = {};
        if(item){
            var newdata = item;
            var x;
            x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
            thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
            thumb['yid'] = x[1];
            return x[1];
        }else{
            return x;
        }
    }
    if(news['featuredthumbtype'] == 'video'){
        news['featuredthumb'] = returnYoutubeThumb(news['featuredthumb']);
    }
    // console.log(news);
    news.date = $filter('date')(news.date,'yyyy-MM-dd');
    window.open(Config.BaseURL + "/blog/preview?" + $.param(news));

    console.log(news);
}

var loadauthor = function(){
        // Createnews.loadauthor(function(data){
        // $scope.author = data;
        // });
NewsCenter.listauthor(function(data){
  // console.log(data);
  $scope.author = data;
});
}

loadauthor();







$scope.saveNews = function(news,status){
    $scope.news.status = status;
    news.date = $filter('date')(news.date,'yyyy-MM-dd');
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.isSaving = true;
    $scope.vidpath='';

    BlogFeatured.saveblog(news,function(_rdata){
        console.log(_rdata);
        if(_rdata.error){
          $scope.alerts =[{type: 'danger', msg: 'Something Went Wrong PLease Check your Fields.'}];
          $scope.isSaving = false;

      }else{
        $scope.isSaving = false;
        $scope.alerts =[{type: 'success', msg: 'Blog successfully saved!'}];
        $scope.news = angular.copy(orinews);
        $scope.formnews.$setPristine();
        $scope.news.tag = [];
        $scope.news.date = moment().format('YYYY-MM-DD');

    }

});
}


$scope.alertss = [];

$scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
};


$scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
};

$scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
};

    // $scope.user.date = moment().format('YYYY-MM-DD');

    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            size:'lg',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'blogimages';
                $scope.videoenabled=true;
                $scope.mediaGallery = 'all';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                // console.log($scope.s3link);

                $scope.set = function(id){
                    // console.log(id);
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        $scope.copy();
                    }
                }

                var returnYoutubeThumb = function(item){
                    var x= '';
                    var thumb = {};
                    if(item){
                        var newdata = item;
                        var x;
                        x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                        thumb['yid'] = x[1];
                        return thumb;
                    }else{
                        return x;
                    }
                }

                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/blogimages/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimage = function() {
                  BlogFeatured.loadimage(function(_rdata){

                    $scope.imagelength = _rdata.length;
                    $scope.imagelist   = _rdata
                });
              }
              loadimage();

              var loadvideo = function(){
                  BlogFeatured.loadvideo( function(data){
                    console.log(data);
                    // console.log(data);
                    for (var x in data){
                        var newd = returnYoutubeThumb(data[x].embed);
                        data[x].videourl = newd.url;
                        data[x].youtubeid = newd.yid;
                    }
                        // console.log(data);

                        $scope.videolist = data;
                        $scope.videoslength = data.length;
                    });
              }
              loadvideo();

              $scope.$watch('files', function () {
                $scope.upload($scope.files);

            });

              $scope.upload = function (files)
              {

                var filename
                var filecount = 0;
                if (files && files.length)
                {
                    $scope.imageloader=true;
                    $scope.imagecontent=false;

                    for (var i = 0; i < files.length; i++)
                    {
                        var file = files[i];

                        if (file.size >= 2000000)
                        {
                            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                            filecount = filecount + 1;

                            if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }


                        }
                        else

                        {

                            var promises;

                            promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/blogimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){
    filecount = filecount + 1;
    filename = data.config.file.name;
    var fileout = {
        'filename' : filename
    };
    BlogFeatured.saveimage(fileout, function(data, rdata){
     loadimage();

     if(filecount == files.length)
     {
        $scope.imageloader=false;
        $scope.imagecontent=true;
    }
});





});
}

}
}
};

$scope.savevid = function(newsvid) {
    BlogFeatured.savevideo({video:newsvid}, function(_rdata){
        loadvideo();
        $("#embed").val(" ");
    });
                        // if(newsvid){
                        //     var x = newsvid.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        //     // console.log(x);
                        //     if(x==null){
                        //         $scope.invalidvideo = true;
                        //         $scope.video='';
                        //     }else{
                        //         var newslink = { 'newsvid': newsvid }
                        //         Createnews.saveVid(newslink,function(data){
                        //             loadvideo();
                        //             $scope.video='';
                        //             $scope.invalidvideo = false;
                        //             // console.log(data);
                        //         });
                        //     }
                        // }
                    };



                    $scope.deletevideo = function (videoid, $event)
                    {
                    // var datavideo = {
                    //     'videoid' : videoid
                    // };
                    // Createnews.deleteVideo(datavideo, function(data){
                    //     loadvideo();
                    // });
BlogFeatured.deletevideo(videoid, function(_rdata){
  loadvideo();
})
$event.stopPropagation();
}

$scope.deletenewsimg = function (dataimg, $event)
{
                    // var fileout = {
                    //     'filename' : dataimg
                    // };
                    // Createnews.deleteImage(fileout, function(data) {
                    //     loadimages();

                    // });
BlogFeatured.deletebanner(dataimg, function(_rdata){
  loadimage();
});
$event.stopPropagation();
}

$scope.copy = function() {
                    // console.log($scope.contentvideo);
                    var text = '';
                    if($scope.contentvideo.filename){
                        text = Config.amazonlink + '/uploads/blogimages/' + $scope.contentvideo.filename;
                    }else if($scope.contentvideo.videoid){
                        text = $scope.contentvideo.video;
                    }
                    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                    if(pp != "" && pp !== null) {
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.ok = function(category) {
                    $modalInstance.close($scope.contentvideo);
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }


            },
            resolve: {
                type: function(){
                    return type;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
                // console.log("============================");
                // console.log(res);
                if(res.filename){
                    $scope.news.imagethumb = res.filename;
                    $scope.vidpath = '';
                    $scope.news.featuredthumbtype='banner';
                    $scope.news.featuredthumb = res.filename;
                }else if(res.vidid){
                    $scope.news.imagethumb = '';
                    $scope.news.featuredthumbtype='video';
                    $scope.vidpath = $sce.trustAsHtml(res.embed);
                    // console.log($scope.vidpath);
                    $scope.news.featuredthumb = res.embed;
                }
            });

    }


    /* Add Tags*/
    $scope.addtags = function() {
        var modalInstance = $modal.open({
            templateUrl: 'tagsAdd.html',
            controller: function($scope, $modalInstance) {
                var tagsslugs = '';
            // console.log('addtags');

            $scope.ontagstitle = function convertToSlug(Text)
            {
                if(Text == null)
                {
                    $scope.alerts =[{type: 'danger', msg: 'Tag Title is required.'}];
                }
                else
                {
                    var text1 = Text.replace(/[^\w ]+/g,'');
                    tagsslugs = angular.lowercase(text1.replace(/ +/g,'-'));
                }
            }

            $scope.ok = function(tags) {
                Tags.addtags(tags, function(data){
                    if(data.hasOwnProperty('error')){
                        if(data.error.hasOwnProperty('existTags')){
                            $scope.alerts =[{type: 'danger', msg: 'Tag Title already exists.'}];
                        }
                    }else {
                        loadtags();
                        // console.log(data);
                        $modalInstance.close();
                    }
                });
                tags['slugs'] = tagsslugs;
            }
            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            }

        },
        resolve: {
        }
    });
}

$scope.addcategory = function() {
    var modalInstance = $modal.open({
        templateUrl: 'categoryAdd.html',
        controller: function($scope, $modalInstance) {
           $scope.bt1=true;
           $scope.bt2=false;
           var categoryslugs = '';
           $scope.oncategorytitle = function convertToSlug(Text)
           {
            if(Text != null){
                var text1 = Text.replace(/[^\w ]+/g,'');
                var quotes = angular.lowercase(text1.replace(/'+/g,'‘'));
                categoryslugs = angular.lowercase(quotes.replace(/ +/g,'-'));

                $http({
                    url: Config.ApiURL + "/validate/blogcateg/"+Text,
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data, status, headers, config) {
                        console.log(data);
                        if(data==1){
                            $scope.notification = true;
                            $scope.bt1=false;
                            $scope.bt2=true;
                        }else{
                            $scope.notification = false;
                            $scope.bt1=true;
                            $scope.bt2=false;
                        }
                    });
                }

            }

            $scope.ok = function(category) {
               _categ();
               category['slugs'] = categoryslugs;
               $http({
                url: Config.ApiURL + "/blog/savecategory",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(category)
            }).success(function(data, status, headers, config) {
                _categ();
                $modalInstance.close();
                $scope.success = true;
            }).error(function(data, status, headers, config) {
                $scope.status = status;
            });
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    },
    resolve: {

    }
});
}




});

