'use strict';
app.controller('LeaderCtrl', function($scope, $state ,$q, $http, Config, $modal,$sce,Leaders){

   $scope.oneAtATime = true;
    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };

  $scope._embed = function(_gdata){
    $scope.rdata =  $sce.trustAsHtml(_gdata.replace('width="420"', ""));
  }

  $scope.save = function(_gdata){
    Leaders.savevideo(_gdata,function(_rdata){
       loadvideo();
      console.log(_rdata);
    });
  }

  var loadvideo = function(){
      Leaders.loadvideo( function(_rdata){
        console.log(_rdata);
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        $scope.videolist   = _rdata
      });
  }
  loadvideo();

  $scope.delete = function(id){
    var modalInstance = $modal.open({templateUrl: 'delete.html',controller: deletevidCTRL,
      resolve: {vidid: function(){return id }}
    });
  }
  var deletevidCTRL = function($scope, $modalInstance, vidid) {
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Video?";
      $scope.ok = function() {
        Leaders.deletevideo(vidid, function(_rdata){
          console.log(_rdata);
          loadvideo();
          $modalInstance.close();
        })
      };
      $scope.cancel = function(){$modalInstance.close()};
  }

});

