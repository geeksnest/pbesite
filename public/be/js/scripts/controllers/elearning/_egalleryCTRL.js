'use strict';
app.controller('EgalleryCTRL', function($scope, $state ,$q, $http, Config, $modal,$sce, Egallery, Upload){

   $scope.amazonpath= Config.amazonlink;

    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimage = function(){
      Egallery.loadimage(function(_rdata){
        console.log(_rdata);
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        $scope.imagelist   = _rdata
      });
    }
    loadimage();

    $scope.path=function(path){
      pathimage1 =path.trim();
      featimages();
      $modalInstance.dismiss('cancel');
    }
    $scope.upload = function(files) {
      $scope.upload(files);  
    };
    $scope.upload = function (files) {
      $scope.alertss = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      var filename;
      var filecount = 0;
      var _contloader = function(_loader, _content){
        $scope.imageloader = _loader;
        $scope.imagecontent = _content;
      }

      if (files && files.length) {
        _contloader(true, false);

        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          if (file.size >= 2000000){
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            filecount = filecount + 1;
            if(filecount == files.length){
              _contloader(false, true);
            }
          }
          else
          {
            var promises;
            var fileExtension = '.' + file.name.split('.').pop();
            var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

            promises = Upload.upload({
              url:Config.amazonlink, 
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/banner/' + renamedFile, 
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', 
                policy: Config.policy, 
                signature: Config.signature, 
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
              },
              file: file
            })
              promises.then(function(data){
              filecount = filecount + 1;
              filename = data.config.file.name;
              var _rdata = {'filename' : renamedFile };


              var modalInstance = $modal.open({
                templateUrl: 'egallerysave.html',
                controller: saveimgCTRL,
                resolve:{
                  imgid: function(){
                    return renamedFile
                  }
                }
              });


             //  Egallery.saveimage(_rdata, function(data, rdata){
             //    console.log(data);
             //    loadimage();
             //    if(data[0].success){
             //      if(filecount == files.length){
             //        _contloader(false, true);
             //      }
             //    }else{
             //     _contloader(false, true);
             //   }
             // });
            });

            var saveimgCTRL = function($scope, $modalInstance, imgid, Config) {
              $scope.imgfilename =imgid;
              $scope.amazonpath= Config.amazonlink;
              $scope.imgSave = function(_gdata){
                Egallery.saveimage(_gdata, function(data, rdata){
                        loadimage();
                        if(data[0].success){
                          if(filecount == files.length){
                            _contloader(false, true);
                            $modalInstance.dismiss('cancel');
                          }

                        }else{
                          _contloader(false, true);
                          $modalInstance.dismiss('cancel');
                       }
                });
               
              }

              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            }  




          }
        }
      }
    };


    $scope.edit = function(id, img, title, description ){
       var modalInstance = $modal.open({
                templateUrl: 'egalleryupdate.html',
                controller: saveimgCTRL,
                resolve:{
                  _data: function(){
                    return {id:id,img:img,imgtitle:title,imgdesc:description}
                  }
                }
              });
    }
    var saveimgCTRL = function($scope, $modalInstance, _data,Config) {
      console.log(_data);

      $scope.imgfilename =_data.img;
      $scope.amazonpath= Config.amazonlink;


      $scope.img = _data;

      $scope.imgSave = function(_gdata){

        console.log(_gdata);
        Egallery.updateimage(_gdata, function(data, rdata){
                loadimage();
                if(data[0].success){
                    if(filecount == files.length){
                        _contloader(false, true);
                        $modalInstance.dismiss('cancel');
                    }

                }else{
                      _contloader(false, true);
                      $modalInstance.dismiss('cancel');
                }
        });
       
      }

      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }







    $scope.delete = function(id){
      var modalInstance = $modal.open({
        templateUrl: 'delete.html',controller: deleteCTRL,
        resolve: {imgid: function() {return id}}
      });
    }
    var deleteCTRL = function($scope, $modalInstance, imgid) {
      $scope.alerts = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Photo?";
      $scope.ok = function() {
        Egallery.deletebanner(imgid, function(_rdata){
          loadimage();
          $modalInstance.close();
        });
      };
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }
    /////UPLOAD CODE START HERE
    $scope.cancel = function() {https://powerbrain.s3.amazonaws.com/uploads/banner/hrnzf22zkt91446519670600.jpg
      $modalInstance.dismiss('cancel');
    };

});

