'use strict';
app.controller('ETrainingCTRL', function($scope, $state ,$q, $http,$stateParams, Config, $modal,$sce, Training, Tmedia){

$scope.theme =[
{name:"Red", color:"#ee674e"},
{name:"Green", color:"#87c260"},
{name:"Gold", color:"#f8a71a"},
{name:"Blue", color:"#13a1cc"},
{name:"Dark Blue", color:"#28bab6"},
];
console.log($scope.theme);
  $scope.idno = $stateParams.idno;
  console.log($scope.idno);

  Training.edittraining($scope.idno, function(_rdata){
   $scope.traininglist = _rdata;
   $scope.featpath = _rdata[0].tlogo;
   console.log(_rdata);
});




  $scope.featuredbanner = function(size){
    console.log(size);
    var featbanner = $scope.amazon;
    var modalInstance = $modal.open({templateUrl: 'imagelist.html',controller: featuredlistCTRL,size: size,
      resolve: {path: function() { return featbanner}}
    });
  }

  var pathimage1 = " ";
  var featimages = function(){
    $scope.featpath=pathimage1;
  }
  var featuredlistCTRL = function( $modalInstance,$scope, Upload , Config, path, BlogFeatured){
    $scope.amazonpath= Config.amazonlink;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimage = function() {
      BlogFeatured.loadimage(function(_rdata){
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        $scope.imagelist   = _rdata
      });
    }
    loadimage();

    $scope.path=function(path){
      pathimage1 =path.trim();
      featimages();
      $modalInstance.dismiss('cancel');
    }
    $scope.upload = function(files) {
      $scope.upload(files);  
    };
    $scope.upload = function (files) {
      $scope.alertss = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      var filename;
      var filecount = 0;
      var _contloader = function(_loader, _content){
        $scope.imageloader = _loader;
        $scope.imagecontent = _content;
      }

      if (files && files.length) {
        _contloader(true, false);

        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          if (file.size >= 2000000){
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            filecount = filecount + 1;
            if(filecount == files.length){
              _contloader(false, true);
            }
          }
          else
          {
            var promises;
            var fileExtension = '.' + file.name.split('.').pop();
            var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

            promises = Upload.upload({
              url:Config.amazonlink, 
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/banner/' + renamedFile, 
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', 
                policy: Config.policy, 
                signature: Config.signature, 
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
              },
              file: file
            })


              promises.then(function(data){
              filecount = filecount + 1;
              filename = data.config.file.name;
              var _rdata = {'filename' : renamedFile };
              BlogFeatured.saveimage(_rdata, function(data, rdata){
                console.log(data);
                loadimage();
                if(data[0].success){
                  if(filecount == files.length){
                    _contloader(false, true);
                  }
                }else{
                 _contloader(false, true);
               }
             });
            });
          }
        }
      }
    };

    $scope.delete = function(id){
      var modalInstance = $modal.open({
        templateUrl: 'delete.html',controller: deleteCTRL,
        resolve: {imgid: function() {return id}}
      });
    }
    var deleteCTRL = function($scope, $modalInstance, imgid) {
      $scope.alerts = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Photo?";
      $scope.ok = function() {
        BlogFeatured.deletebanner(imgid, function(_rdata){
          loadimage();
          $modalInstance.close();
        });
      };
      $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
      };
    }
    /////UPLOAD CODE START HERE
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }



  $scope.submitData = function(_gdata){
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    console.log(_gdata);
    Training.updatetraining(_gdata, function(_rdata){

      if(_rdata[0].error){
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Successfully Updated'});
   

      }
    });
  }

});

