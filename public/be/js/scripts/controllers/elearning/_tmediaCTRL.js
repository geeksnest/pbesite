'use strict';
app.controller('TmediaCtrl', function($scope, $state ,$q,$stateParams, $http, Config, $modal,$sce, Tmedia, Training){
 $scope.idno = $stateParams.idno;

 Training.loadtraining(function(_rdata){
  $scope.traininglist = _rdata;
});

 var _listmedia = function(){
  Tmedia.loadtmedia($stateParams.idno, function(_rdata){
    if(!_rdata.error){
      $scope.rmedia = _rdata 
    }else{
      $scope.rmedia = [] ;
    }
    
  });
}
_listmedia();

$scope.addmedia = function(size){ var modalInstance = $modal.open({templateUrl: 'addmedia.html',controller: mediaCTRL,size: size});}
var mediaCTRL = function( $modalInstance, $scope, $stateParams, Upload, Config){
  $scope.idno = $stateParams.idno;
  $scope.cl = $stateParams.hideopt;

  if($scope.idno == 'intro'){
    $scope.classfor = false;
    $("#selectfor").required = false;
  }else if($scope.idno == 'tr'){
    $scope.classfor = false;
    $("#selectfor").required = false;
  }
  else if($scope.idno == 'leaders'){
    $scope.classfor = false;
    $("#selectfor").required = false;
  }
  else if($scope.idno == 'mentors'){
    $scope.classfor = false;
    $("#selectfor").required = false;
  }
  else if($scope.cl == 'cl'){
   $scope.classfor = false;
   $("#selectfor").required = false;
 }else{
   $scope.classfor = true;
   $("#selectfor").attr('required', true);
 } 



  $scope.editslug = false;
  $scope.editedslug = false;
  var slugstorage = "";

  $scope.ontitle = function convertToSlug(Text)
  {
    console.log(Text);
    if(Text != null  && $scope.editedslug == false && $scope.editslug == false)
    {
        var text1 = Text.replace(/[^\w\s]/gi, '');
        $scope.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
}

$scope.onslugs = function(Text){
    if(Text != null)
    {
        var text1 = Text.replace(/[^\w\s]/gi, '');
        $scope.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
}

$scope.editnewsslug = function(){
    $scope.editslug = true;
    slugstorage = $scope.slugs;
}

$scope.cancelnewsslug = function(title) {
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.slugs = slugstorage;
    }else {
        $scope.slugs = '';
        $scope.editedslug = false;
    }
    $scope.editslug = false;
}

$scope.setslug = function(slug){
    $scope.editedslug = true;
    $scope.editslug = false;
    if(slug != null)
    {
        $scope.slugs = slug.replace(/\w\s+/g, '-').toLowerCase();

    }
}

$scope.clearslug = function(title){
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.slugs = title.replace(/\w\s+/g, '-').toLowerCase();
    }else {
        $scope.slugs = '';
        $scope.editedslug = false;
    }
}







 $scope.imageloader=false;
 $scope.imagecontent=true;
 $scope.noimage = false;
 $scope.imggallery = false;
 $scope.Updata = true;
 $scope._embed = function(_gdata){$scope.preview =  $sce.trustAsHtml(_gdata);}
 var _pdf =[];
 var _loadarray = function(){
  $scope.pdf = _pdf;
    //console.log($scope.pdf);
  } 


  $scope.upload = function (files) {
    $scope.alertss = [];
    $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
    var filename;
    var filecount = 0;
    var _contloader = function(_loader, _content){
      $scope.imageloader = _loader;
      $scope.imagecontent = _content;
    }
    if (files && files.length) {
      _contloader(true, false);
      $scope.Updata = false;
      for (var i = 0; i < files.length; i++) {
        var file = files[i];

        if (file.size >= 20000000){
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;
          if(filecount == files.length){
            _contloader(false, true);
            $scope.Updata = true;
          }
        }
        else
        {
          var promises;
          var filename = file.name;

          promises = Upload.upload({
            url:Config.amazonlink, 
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/banner/' + filename, 
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private', 
              policy: Config.policy, 
              signature: Config.signature, 
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
            },
            file: file
          })
          promises.then(function(data){
            filecount = filecount + 1;
            $scope.Updata = true;
            _pdf.push({name:filename});
            _loadarray();
            _contloader(false, true);

          });
        }
      }
    }
  };

  $scope._remove = function(index){
    console.log(index);
    _pdf.splice(index, 1);
    _loadarray();
    c
  }

  $scope.submitData = function(_gdata){

    console.log(_gdata);
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    Tmedia.savetmedia(_gdata, function(_rdata){
      var rdata =  _rdata.result ;
      if(!rdata[0].error){
        _listmedia();
        angular.forEach(_pdf, function(value, key) {
          Tmedia.savetpdf({parentid:_rdata.idno, filename:value.name},function(_data){
            console.log(_data);
          });
        }); 
        $scope.alerts.push({type: 'success', msg: 'Successfully Save'});
        $modalInstance.dismiss('cancel');
      }else{
       $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
     }

   });
  }
  $scope.cancel = function() {$modalInstance.dismiss('cancel');};
}



$scope.edit = function(idno, size){var modalInstance = $modal.open({templateUrl: 'editmedia.html',controller: editmedia,size: size,resolve: {idno: function() {return idno}}});}
var editmedia = function( $modalInstance, $scope, $stateParams, Upload, Config, idno){

 $scope.idno = $stateParams.idno;
 $scope.cl = $stateParams.hideopt;

 if($scope.idno == 'intro'  ){
  $scope.classfor = false;
  $("#selectfor").required = false;
}else if($scope.idno == 'tr'){
  $scope.classfor = false;
  $("#selectfor").required = false;
}
else if($scope.idno == 'leaders'){
  $scope.classfor = false;
  $("#selectfor").required = false;
}
else if($scope.idno == 'mentors'){
  $scope.classfor = false;
  $("#selectfor").required = false;
}
else if($scope.cl == 'cl'){
 $scope.classfor = false;
 $("#selectfor").required = false;
}else{
 $scope.classfor = true;
 $("#selectfor").attr('required', true);
} 



$scope.editslug = false;
  $scope.editedslug = false;
  var slugstorage = "";

  $scope.ontitle = function convertToSlug(Text)
  {
    console.log(Text);
    if(Text != null  && $scope.editedslug == false && $scope.editslug == false)
    {
        var text1 = Text.replace(/[^\w\s]/gi, '');
        $scope.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
}

$scope.onslugs = function(Text){
    if(Text != null)
    {
        var text1 = Text.replace(/[^\w\s]/gi, '');
        $scope.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
}

$scope.editnewsslug = function(){
    $scope.editslug = true;
    slugstorage = $scope.slugs;
}

$scope.cancelnewsslug = function(title) {
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.slugs = slugstorage;
    }else {
        $scope.slugs = '';
        $scope.editedslug = false;
    }
    $scope.editslug = false;
}

$scope.setslug = function(slug){
    $scope.editedslug = true;
    $scope.editslug = false;
    if(slug != null)
    {
        $scope.slugs = slug.replace(/\w\s+/g, '-').toLowerCase();

    }
}

$scope.clearslug = function(title){
    if(title != null)
    {
        $scope.editedslug = false;
        $scope.slugs = title.replace(/\w\s+/g, '-').toLowerCase();
    }else {
        $scope.slugs = '';
        $scope.editedslug = false;
    }
}







var _editmedia = function(){
  Tmedia.edittmedia(idno, function(_rdata){
    console.log(_rdata);
    $scope.slugs = _rdata.tmedia[0].slugs
    $scope.emedia = _rdata.tmedia 
    var _embed= _rdata.tmedia[0].embed;
    $scope.preview =  $sce.trustAsHtml(_embed);
    $scope.dbpdf = _rdata.pdf;
    _listmedia();
  });
}
_editmedia();

$scope.idno = $stateParams.idno;
_listmedia();


$scope.imageloader=false;
$scope.imagecontent=true;
$scope.noimage = false;
$scope.imggallery = false;
$scope.Updata = true;
$scope._embed = function(_gdata){ $scope.preview =  $sce.trustAsHtml(_gdata);}
var _pdf =[];
var _loadarray = function(){
  $scope.pdf = _pdf;
    //console.log($scope.pdf);
  } 


  $scope.upload = function (files) {
    $scope.alertss = [];
    $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
    var filename;
    var filecount = 0;
    var _contloader = function(_loader, _content){
      $scope.imageloader = _loader;
      $scope.imagecontent = _content;
    }

    if (files && files.length) {
      _contloader(true, false);
      $scope.Updata = false;
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (file.size >= 20000000){
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;
          if(filecount == files.length){
            _contloader(false, true);
            $scope.Updata = true;
          }
        }else{
          var promises;
          var filename = file.name;
          promises = Upload.upload({
            url:Config.amazonlink, 
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/banner/' + filename, 
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private', 
              policy: Config.policy, 
              signature: Config.signature, 
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
            },
            file: file
          })
          promises.then(function(data){
            filecount = filecount + 1;
            $scope.Updata = true;
            //_pdf.push({name:filename});
            _loadarray();
            _contloader(false, true);

            Tmedia.savetpdf({parentid:idno, filename:filename},function(_data){
              console.log(_data);
              _editmedia();
            });
          });
        }
      }
    }
  };


  $scope._delete= function(_gata){
    var r = confirm("Are You Sure Do You Want To Delete This File?");
    if (r == true) {
      Tmedia.deletetpdf(_gata, function(_rdata){
       console.log(_rdata);
       _listmedia();
       _editmedia();
     });
    }
  }

  $scope._remove = function(index){
    console.log(index);
    _pdf.splice(index, 1);
    _loadarray();
    c
  }

  $scope.update = function(_gdata){
    console.log(_gdata);
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.alerts.push({type: 'success', msg: 'Successfully Save'});
    Tmedia.updatemedia(_gdata, function(_rdata){
      var rdata =  _rdata.result ;
      if(!rdata[0].error){
        console.log("Continue");
        $scope.alerts.push({type: 'success', msg: 'Successfully Updated'});
        $modalInstance.dismiss('cancel');
        _listmedia();
      }else{
       $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
     }

   });

    _listmedia();
  }
  $scope.cancel = function() {$modalInstance.dismiss('cancel');_listmedia();};

}

$scope.delete = function(id){
  var modalInstance = $modal.open({
    templateUrl: 'delete.html',controller: deleteCTRL,
    resolve: {id: function() {return id}}
  });
}
var deleteCTRL = function($scope, $modalInstance, id) {
  $scope.alerts = [];
  $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
    Tmedia.deletetmedia(id, function(_rdata){
      _listmedia();
      $modalInstance.close();
    });
  };
  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
}

//FEATURED VIDEO START HERE

var _getfeatured = function(){
  Tmedia.loadfeatured($stateParams.idno, function(_rdata){

    if(!_rdata.error){
      console.log(_rdata);
      $scope.utube =$sce.trustAsHtml(_rdata[0].embed);
    }
  });
}
_getfeatured();

var embed = "";
$scope.msg = [];
$scope.video = function(){
  _modals(true, false)
}
$scope.slctfeatured = function(size){
  var modalInstance = $modal.open({templateUrl: 'featvideo.html', controller:videolistCTRL, size: size,
      //resolve: {path: function() {return featbanner}}
    });
}
var videolistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams,  BlogFeatured){

  var _closevideo = function(){
    $modalInstance.dismiss('cancel');
  }

  var loadvideo = function() {
    BlogFeatured.loadvideo( function(_rdata){
      $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
      $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
      $scope.videolist   = _rdata
    });
  }
  loadvideo();

  $scope.path=function(path){
    Tmedia.featvideo({idno:$stateParams.idno,embed:path},function(_rdata){
      _getfeatured();
      console.log(_rdata);
      $modalInstance.close()
    });
      //console.log({idno:$stateParams.idno,embed:path});
      // embed = path;
      // featvideo();
      // _closevideo();
    }

    $scope.savethis = function(embed){
      BlogFeatured.savevideo({video:embed.video}, function(_rdata){
        loadvideo();
        $("#embed").val(" ");
      });
    }

    $scope.delete = function(id){
      var modalInstance = $modal.open({templateUrl: 'delete.html',controller: deletevidCTRL,
        resolve: {vidid: function(){return id }}
      });
    }
    var deletevidCTRL = function($scope, $modalInstance, vidid) {
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Video?";
      $scope.ok = function() {
        BlogFeatured.deletevideo(vidid, function(_rdata){
          loadvideo();
          $modalInstance.close();
        })
      };
      $scope.cancel = function(){$modalInstance.close()};
    }
    $scope.cancel = function(){_closevideo()};
  }
  var featvideo = function(){
    console.log("Featured");
    $scope.utube = embed; $scope.watchvideo =$sce.trustAsHtml(embed);
  }




});

