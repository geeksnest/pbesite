'use strict';
app.controller('SchoolCtrl', function($scope, $state ,$q, $http, Config, $sce, $modal,schoolF){
  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
};
$scope.editcategoryshow = false;
$scope.data = {};
var num = 10;
var off = 1;
var keyword = null;
var paginate = function(off, keyword) {
    $http({
        url: Config.ApiURL + "/school/list/" + num + '/' + off + '/' + keyword,
        method: "GET",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }).success(function(data, status, headers, config) {
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    }).error(function(data, status, headers, config) {
        $scope.status = status;
    });
}
paginate(off, keyword);

$scope.search = function (searchkeyword) {
    var off = 0;
    paginate(off, searchkeyword);
}

$scope.numpages = function (off, keyword) {
    paginate(off, $scope.searchtext);
}

$scope.setPage = function (pageNo) {
    paginate(pageNo, $scope.searchtext);
    off = pageNo;
};
$scope.resetsearch = function(){
    var keyword = undefined;
    $scope.searchtext = undefined;
    paginate(1, $scope.searchtext);
}

$scope.addtags = function() {
    var modalInstance = $modal.open({
        templateUrl: 'tagsAdd.html',
        controller: addtagsCTRL,
        resolve: {

        }
    });
}

var addtagsCTRL = function($scope, $modalInstance) {
   $scope.bt1=true;
   $scope.bt2=false;
   var tagslugs = '';
   $scope.ontags = function convertToSlug(Text)
   {
    if(Text != null)
    {
     var text1 = Text.replace(/[^\w ]+/g,'');
     tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
     $http({
        url: Config.ApiURL + "/validate/school/"+Text,
        method: "GET",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            if(data==1){
                $scope.notification = true;
                $scope.bt1=false;
                $scope.bt2=true;
            }else{
                $scope.notification = false;
                $scope.bt1=true;
                $scope.bt2=false;
            }
        });
    }

}

$scope.ok = function(tags) {

    tags['slugs'] = tagslugs;
    $http({
        url: Config.ApiURL + "/school/save",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(tags)
    }).success(function(data, status, headers, config) {
        paginate(off, keyword);
        $modalInstance.close();
        $scope.success = true;
    }).error(function(data, status, headers, config) {
        $scope.status = status;
    });
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
}
var loadalert = function(){
 $scope.alerts.splice(0, 1);
 $scope.alerts.push({ type: 'success', msg: 'Tag successfully Deleted!' });
}

var deletetagsInstanceCTRL = function($scope, $modalInstance, id, $state) {

    $scope.ok = function() {
        var tags = {
            'tags': id
        };
        $http({
            url: Config.ApiURL + "/school/delete/" + id,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(tags)
        }).success(function(data, status, headers, config) {
            paginate(off, keyword);
            $modalInstance.close();
            loadalert();
        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    }

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }
}
$scope.tagsDelete = function(id) {
    var modalInstance = $modal.open({
        templateUrl: 'tagsDelete.html',
        controller: deletetagsInstanceCTRL,
        resolve: {
            id: function() {
                return id
            }
        }
    });
}
$scope.alerts = [];
$scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
};

$scope.validate = function(){
}


$scope.updatetags = function(data,memid) {
    schoolF.update(data,memid , function(_rdata){
        console.log(_rdata);

        var _errorFlag = function(){
             setTimeout(function () 
            {
               $scope.$apply(function()
               {
                  $scope.showflagerror = false;
                  $scope.showflag = false;
             });
           }, 3500);
        }

        if(_rdata.error){
            $scope.showflagerror= memid;
            _errorFlag();
           
        }else{
            $scope.showflag= memid;
            _errorFlag();
            
        }
    });
}
});




