'use strict';
app.controller('NewsCateg', function($scope, $state ,$q, $http, Config, $sce, $modal,CategTags){
	$scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.editcategoryshow = false;
    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var paginate = function(off, keyword) {
        $http({
            url: Config.ApiURL + "/blog/managecategory/" + num + '/' + off + '/' + keyword,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).success(function(data, status, headers, config) {
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    }
    paginate(off, keyword);
    $scope.search = function (searchkeyword) {
        var off = 1;
        paginate(off, searchkeyword);
    }
    
    $scope.numpages = function (off, keyword) {
        paginate(off, $scope.searchtext);
    }

    $scope.setPage = function (pageNo) {
        paginate(pageNo, $scope.searchtext);
        off = pageNo;
    };

    $scope.resetsearch = function(){
      var keyword = undefined;
      $scope.searchtext = undefined;    
      paginate(1, $scope.searchtext);
  }
  $scope.addcategory = function() {
    var modalInstance = $modal.open({
        templateUrl: 'categoryAdd.html',
        controller: addcategoryCTRL,
    });
}
var addcategoryCTRL = function($scope, $modalInstance) {
 $scope.bt1=true;
 $scope.bt2=false;
 var categoryslugs = '';
 var quotes = '';
 $scope.oncategorytitle = function convertToSlug(Text)
 {
    if(Text != null){
        var text1 = Text.replace(/[^\w ]+/g,'');
        quotes = angular.lowercase(text1.replace(/'+/g,'‘'));
        categoryslugs = angular.lowercase(quotes.replace(/ +/g,'-'));
        $http({
            url: Config.ApiURL + "/blogvalidate/categ/"+Text,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                if(data==1){
                    $scope.notification = true;
                    $scope.bt1=false;
                    $scope.bt2=true;
                }else{
                    $scope.notification = false;
                    $scope.bt1=true;
                    $scope.bt2=false;
                }
            });
        }

    }

    $scope.ok = function(category) {
        category['slugs'] = categoryslugs;
        $http({
            url: Config.ApiURL + "/blog/savecategory",
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(category)
        }).success(function(data, status, headers, config) {
            paginate(off, keyword);
            $modalInstance.close();
            $scope.success = true;
        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    }
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }
}
var loadalert = function(){
   $scope.alerts.splice(0, 1);
   $scope.alerts.push({ type: 'success', msg: 'Category successfully Deleted!' });
}
var deletecategoryInstanceCTRL = function($scope, $modalInstance, id, $state) {
    $scope.ok = function() {
        var news = {'news': id};
        $http({
            url: Config.ApiURL + "/blog/categorydelete/" + id,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(news)
        }).success(function(data, status, headers, config) {
            paginate(off, keyword);
            $modalInstance.close();
            loadalert();
        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    }

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }
}
$scope.categoryDelete = function(id) {
    var modalInstance = $modal.open({
        templateUrl: 'categoryDelete.html',
        controller: deletecategoryInstanceCTRL,
        resolve: {
            id: function() {
                return id
            }
        }
    });
}
$scope.alerts = [];
$scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
};
$scope.updatecategory = function(data,memid) {

    console.log(memid);
    $http({
        url: Config.ApiURL + "/blog/updatecategorynames",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({id:memid, name:data.replace(/\\/g,'') })
    }).success(function(data, status, headers, config) {
        console.log(data);

        $scope.alerts.push({ type: 'success', msg: 'Category successfully updated!' });
        if(data.error){
            $scope.showflagerror= memid;
            setTimeout(function(){
             $scope.$apply(function()
             {
               $scope.showflagerror = false;
           });
         }, 3500);
        }else{
            paginate(off, keyword);
            $scope.showflag= memid;
            setTimeout(function(){
               $scope.$apply(function(){
                 $scope.showflag = false;
             });
           }, 3500);
        }

    }).error(function(data, status, headers, config) {


    });

}
});




