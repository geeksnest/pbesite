'use strict';

/* Controllers */

app.controller('BlogCtrl', function($scope, $state ,$q, $http,$stateParams, Config, $modal,$sce, NewsCenter, BlogFeatured){
  $scope.ontitle = function convertToSlug(Text){
    if(Text != null){
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
  }
  var _list_author = function(){
    NewsCenter.listauthor(function(data){$scope.authorlist = data;});
  }
  _list_author();
  var _modals = function(_video, _banner){
    $scope.showvideo = _video; 
    $scope.showbanner = _banner;
  }
  _modals(false, false);
  var embed = "";
  $scope.msg = [];
  $scope.video = function(){
    _modals(true, false)
  }
  $scope.featuredvideo = function(size,path){var featbanner = $scope.amazon;var modalInstance = $modal.open({templateUrl: 'featvideo.html', controller:videolistCTRL, size: size,resolve: {path: function() {return featbanner}}});}
  var videolistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, BlogFeatured){
    var _closevideo = function(){
      $modalInstance.dismiss('cancel');
    }
    var loadvideo = function() {
      BlogFeatured.loadvideo( function(_rdata){
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        $scope.videolist   = _rdata
      });
    }
    loadvideo();
    $scope.path=function(path){
      embed = path;
      featvideo();
      _closevideo();
    }

    $scope.savethis = function(embed){
      BlogFeatured.savevideo({video:embed.video}, function(_rdata){
        loadvideo();
        $("#embed").val(" ");
      });
    }

    $scope.delete = function(id){
      var modalInstance = $modal.open({templateUrl: 'delete.html',controller: deletevidCTRL,
        resolve: {vidid: function(){return id }}
      });
    }
    var deletevidCTRL = function($scope, $modalInstance, vidid) {
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Video?";
      $scope.ok = function() {
        BlogFeatured.deletevideo(vidid, function(_rdata){
          loadvideo();
          $modalInstance.close();
        })
      };
      $scope.cancel = function(){$modalInstance.close()};
    }
    $scope.cancel = function(){_closevideo()};
  }
  var featvideo = function(){
    $scope.utube = embed; $scope.watchvideo =$sce.trustAsHtml(embed.replace('width="560"', ""));
  }
  $scope.banner = function(){ 
    _modals(false, true)
  }

  $scope.featuredbanner = function(size,path){
    var featbanner = $scope.amazon;
    var modalInstance = $modal.open({templateUrl: 'imagelist.html',controller: featuredlistCTRL,size: size,
      resolve: {path: function() { return featbanner}}
    });
  }

  var pathimage1 = " ";
  var featimages = function(){
    $scope.featpath=pathimage1;
    console.log($scope.featpath);
  }
  var featuredlistCTRL = function( $modalInstance,$scope, Upload , Config, path, BlogFeatured){
    $scope.amazonpath= Config.amazonlink;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimage = function() {
      BlogFeatured.loadimage(function(_rdata){
        console.log(_rdata);
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        $scope.imagelist   = _rdata
      });
    }
    loadimage();

    $scope.path=function(path){
      pathimage1 =path.trim();
      featimages();
      $modalInstance.dismiss('cancel');
    }
    $scope.upload = function(files) {
      $scope.upload(files);  
    };
    $scope.upload = function (files) {
      $scope.alertss = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      var filename;
      var filecount = 0;
      var _contloader = function(_loader, _content){
        $scope.imageloader = _loader;
        $scope.imagecontent = _content;
      }

      if (files && files.length) {
        _contloader(true, false);

        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          if (file.size >= 2000000){
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            filecount = filecount + 1;
            if(filecount == files.length){
              _contloader(false, true);
            }
          }
          else
          {
            var promises;
            var fileExtension = '.' + file.name.split('.').pop();
            var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

            promises = Upload.upload({
              url:Config.amazonlink, 
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'media/pdf/' + renamedFile, 
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', 
                policy: Config.policy, 
                signature: Config.signature, 
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
              },
              file: file
            })


            promises.then(function(data){
              filecount = filecount + 1;
              filename = data.config.file.name;
              var _rdata = {'filename' : renamedFile };
              BlogFeatured.saveimage(_rdata, function(data, rdata){
                console.log(data);
                loadimage();
                if(data[0].success){
                  if(filecount == files.length){
                    _contloader(false, true);
                  }
                }else{
                 _contloader(false, true);
               }
             });
            });
          }
        }
      }
    };

    $scope.delete = function(id){
      var modalInstance = $modal.open({
        templateUrl: 'delete.html',controller: deleteCTRL,
        resolve: {imgid: function() {return id}}
      });
    }
    var deleteCTRL = function($scope, $modalInstance, imgid) {
      $scope.alerts = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Photo?";
      $scope.ok = function() {
        BlogFeatured.deletebanner(imgid, function(_rdata){
          loadimage();
          $modalInstance.close();
        });
      };
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }
    /////UPLOAD CODE START HERE
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var _loadblog = function(){
    BlogFeatured.loadblog(function(_rdata){
      $scope.blog = _rdata;
    });
  }
  _loadblog();

  $scope._delete= function(_gata){
    var r = confirm("Are You Sure Do You Want To Delete This blog?");
    if (r == true) {
        BlogFeatured.deletetblog(_gata, function(_rdata){
          console.log(_rdata);
          //_listmedia();
          _loadblog();
      });
    }
  }


  $scope.save = function(_gdata){
    // $scope.formpage.$setPristine(true);
    // $scope.news ="";
   
   BlogFeatured.saveblog(_gdata, function(_rdata){
    if(!_rdata.error){
      $scope.alerts.splice(0, 1);
      $scope.alerts.push({ type: 'success', msg: 'Blog successfully Save!' });


       $scope.mp = function() {
        $scope.formblog.$pristine=true;
        $scope.formblog.$dirty=false;
        angular.forEach($scope.formblog,function(input){
          if(typeof(input.$dirty) !== "undefined"){
            input.$dirty=false;
            input.$pristine=true;
          }
        });
      }
      _loadblog();
  
    }else{
      $scope.alerts.push({ type: 'danger', msg: 'Blog Didnt save samothing went Worng' });
    }

    _loadblog();
  });
}

});


///CALENDAR CONTROLLER
app.controller('CalendarCTRL', function($scope){
  //DATE
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };
    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };
    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

  });

