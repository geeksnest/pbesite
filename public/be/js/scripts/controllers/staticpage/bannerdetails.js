'use strict';

  /*
 * 
 *▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
 * █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
 * █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀  
 *
 * Readble as possible Rock in Roll to the World
 * hang on and play the code 
 */

 app.controller('DetailsCtrl', function($scope, $state ,$q, $http, Config, $modal, dataFactory){
  $scope.webview = true;
  $scope.mobileview = false;
  // Load Data
  var loadData = function(){
    dataFactory.getData({
      apiUrl:"/pagestatic/page/banner" //Your API Url
    },   
    function(_rdata){
      // Beyond here Do Something about the result
      var dlist = [];
      angular.forEach(_rdata, function(value, key) {
        angular.forEach(value.body, function(objdata, title){
         var loc = objdata.position;
         dlist.push({
          id          :value.id,// Primary Key 
          page        :title, // Page Title
          title       :objdata.title, // Information Title
          desc        :objdata.desc,  // Information Description
          banner      :objdata.banner, // Banner Image
          locpost     :objdata.position, // Information Position (Left, Center or Right)
          titlecolor  :objdata.titleStyle.color,  // Information Title Text Color
          titlealign  :objdata.titleStyle.textalign, // Information Title Position (Left, Center or Right)
          desccolor   :objdata.descStyle.color,  // Descritpion Title Text Color
          descalign   :objdata.descStyle.textalign, // Information Descritpion Text Alignment  (Left, Center , Right  or Justify)
          bgcolor     :objdata.bgcolor, // Information Container Background Color
          opacity     :objdata.opacity // Information Container Background Opacity color(RGBA)
        });
       });
      });
      $scope._dLsit = dlist;
      console.log(dlist);
    });
  }
  loadData();

  // Change Banner Info upon click text postion
  $scope.position = function(index, position, _dLsit){
      _dLsit[index].locpost = position; // Replace Banner Info  Position (Left , Canter or Right)
  }
  // Adjust background Opacity
  $scope.opacity = function(index, value, bg,  _dLsit){
     var bg = bg.toString();
     var splitStr = bg.split(",");
     _dLsit[index].bgcolor = splitStr[0]+","+splitStr[1]+","+splitStr[2]+","+ (value==10 ? "1" : "."+value)+")";
  }

  // Save Data
  $scope.save = function(index, dlist, _gdata){
    dataFactory.saveData({
      apiUrl:"/pagestatic/bannerSave"
    },{
      id:dlist[index].id,
      body:'{"'+dlist[index].page+'":{"title":"'+_gdata.title+'","titleStyle":{"color":"'+_gdata.titleStyle.color+'","textalign":"'+_gdata.titleStyle.textalign+'"},"desc":"'+_gdata.desc+'","descStyle":{"color":"'+_gdata.descStyle.color+'","textalign":"'+_gdata.descStyle.textalign+'"},"banner":"'+dlist[index].banner+'","position":"'+_gdata.position+'","bgcolor":"'+_gdata.bg+'","opacity":"'+_gdata.opcity+'"}}'
    },
    function(_rdata){
      // Beyond here Do Something about the return data
      if(_rdata.success){
        var modalInstance = $modal.open({
          templateUrl: 'message.html',
          controller: function($scope, $modalInstance, $state){
            $scope.msg = "Banner Information has Been Updated";
            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            }
          },
        });
      }else{
        console.log("Something Went Wrong");
      }
    });
  }


  /*
  ****
  *MEDIA GALLERY
  ****
  */

  $scope.mediaGallery = function(index, amazon, dlist){
    var modalInstance = $modal.open({
      size: 'lg',
      templateUrl: 'mediaList.html',
      controller:function($modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams){
        $scope.amazonpath   = amazon;
        //IMAGE LOADER
        var _imgLoader =function(imageloader, imagecontent){
          $scope.imageloader  = imageloader;
          $scope.imagecontent = imagecontent;
        }
        _imgLoader(false, true);

        //IMAGE VIEW
        var _image =function(noimage, imggallery){
          $scope.noimage      = noimage;
          $scope.imggallery   = imggallery;
        }
        _image(false, false);

        //LIST LOAD IMAGE
        var loadimages =function(){
          dataFactory.getData({
            apiUrl:"/pagestatic/listimages" //Your API Url
          },   
          function(_rdata){
            // Beyond here Do Something about the result
            if(_rdata.error == "NOIMAGE" ){
              _image(true, false);
            }else{
              _image(false, true);
              $scope.imagelist = _rdata;
            }
          });
        }
        loadimages();

        //DELETE IMAGE
        $scope.delete = function(imgid){
          var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller:function($scope, $modalInstance){
              $scope.message="Are you sure do you want to delete this Photo?";
              $scope.ok = function(){
                dataFactory.getData({
                  apiUrl:"/pagestatic/delete/"+ imgid //Your API Url
                },   
                function(_rdata){
                  // Beyond here Do Something about the result
                  var modalInstance = $modal.open({
                    templateUrl: 'message.html',
                    controller: function($scope, $modalInstance, $state){
                      $scope.msg = "Image Successfully Deleted";
                      $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                      }
                    },
                  });
                  loadimages();
                  $modalInstance.close();
                });
              };
            }
          });
        }

        // Apply new selected banner
        $scope.path=function(path){
          dlist[index].banner = path;
          $scope._dLsit = dlist;
          $modalInstance.dismiss('cancel');
        }
        
        // Upload new Image
        $scope.uploadimge = function (files){

          dataFactory.imageUpload({
            dataFile:files, //Image or File to be upload
            apiUrl:"/pagestatic/saveimage", //Your API Url
            amazonS3:"uploads/bannerimage/" //Amazon S3 File path
          },
          function(loader){
            _imgLoader(loader.imageloader, loader.imagecontent) // Loader
          },
          function(rdata){
            // Beyond here Do Something about the result
            loadimages();
          });
        }

        $scope.cancel =function() {
          $modalInstance.dismiss('cancel');
        };
      }
    });
  }
});



