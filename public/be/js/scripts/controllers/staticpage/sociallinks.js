'use strict';
 /*
 * Kyben Bogie \m/(-_^)\m/ oohhaa
 *
 * Readble as possible Rock in Roll to the World
 */

 app.controller('socialLinksCtrl', function($scope, $state ,$q, $http, Config, $modal, dataFactory){

  var loadData = function(){
    dataFactory.getData({
      apiUrl:"/pagestatic/page/social"
    },   
    function(_rdata){
      $scope.datalist = _rdata[0];
      $scope._dLsit = _rdata[0].body["_socialLinks"];
    });
  }
  loadData();

  $scope.updatealbumname = function(index, url, data , dlist){
    data[index].url =url;
    dataFactory.saveData({
      apiUrl:"/pagestatic/bannerSave"
    },{
      id:dlist.id,
      body:'{"_socialLinks":'+JSON.stringify(data)+'}'
    },
    function(_rdata){
      if(_rdata.success){
        var modalInstance =$modal.open({
          templateUrl: 'message.html',
          controller: function($scope, $modalInstance, $state){
            $scope.msg = "Successfully Updated";
            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            }
          },
        });
      }else{
        console.log("Something Went Wrong");
      }
    });
  }
});




