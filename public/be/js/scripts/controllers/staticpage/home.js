'use strict';

 /*
 * Kyben Bogie \m/(-_^)\m/ oohhaa
 *
 * Readble as possible Rock in Roll to the World
 */

 app.controller('homeCtrl', function($scope, $state ,$q, $http, Config, $modal, dataFactory){

  var loadData = function(){
    dataFactory.getData({
      apiUrl:"/pagestatic/page/home"
    },   
    function(_rdata){
      var pbtc = [];
      var pbi = [];
      var fb = [];
      angular.forEach(_rdata, function(value, key) {
        angular.forEach(value.body, function(objdata, title){
          if(title=="Power Brain Training Center"){
            pbtc.push({id:value.id, mtitle:title, title:objdata.title, desc:objdata.desc,banner:objdata.banner});
          }else if(title=="PB Information"){
            pbi.push({id:value.id, mtitle:title, title:objdata.title, desc:objdata.desc,banner:objdata.banner});
          }else if(title=="5 Benefits"){
            fb.push({id:value.id, mtitle:title, title:objdata.title, desc:objdata.desc,banner:objdata.banner});
          }
       });
      });
      $scope._pbtc = pbtc;
      $scope._pbi = pbi;
      $scope._fb  = fb;
    });
  }
  loadData();

  $scope.bannerInfo = function(index, dlist){
    var modalInstance = $modal.open({
      templateUrl:'bannerinfo.html',
      controller:function($scope, $modalInstance, $state){
        $scope.btitle = dlist[index].title;
        $scope.bdesc  = dlist[index].desc;
        $scope.ok = function(data){
          dlist[index].title = data.btitle;
          dlist[index].desc  = data.bdesc;
          $scope._org_dLsit = dlist;
          $scope._dLsit = dlist;
          $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        }
      }
    });
  }

  $scope.save = function(index, dlist){
    dataFactory.saveData({
      apiUrl:"/pagestatic/bannerSave"
    },{
      id:dlist[index].id,
      body:'{"'+dlist[index].mtitle+'":{"title":"'+dlist[index].title+'","desc":"'+dlist[index].desc+'","banner":"'+dlist[index].banner+'"}}'
    },
    function(_rdata){
      if(_rdata.success){
        var modalInstance = $modal.open({
          templateUrl: 'message.html',
          controller: function($scope, $modalInstance, $state){
            $scope.msg = "Successfully Updated";
            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            }
          },
        });
      }else{
        console.log("Something Went Wrong");
      }
    });
  }


  /*
  ****
  *MEDIA GALLERY
  ****
  */

  $scope.mediaGallery = function(index, amazon, dlist){
    var modalInstance = $modal.open({
      size: 'lg',
      templateUrl: 'mediaList.html',
      controller:function($modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams){
        $scope.amazonpath   = amazon;
        
        //IMAGE LOADER
        var _imgLoader =function(imageloader, imagecontent){
          $scope.imageloader  = imageloader;
          $scope.imagecontent = imagecontent;
        }
        _imgLoader(false, true);

        //IMAGE VIEW
        var _image =function(noimage, imggallery){
          $scope.noimage      = noimage;
          $scope.imggallery   = imggallery;
        }
        _image(false, false);

        //LIST LOAD IMAGE
        var loadimages =function(){
          dataFactory.getData({
            apiUrl:"/pagestatic/listimg"
          },   
          function(_rdata){
            if(_rdata.error == "NOIMAGE" ){
              _image(true, false);
            }else{
              _image(false, true);
              $scope.imagelist = _rdata;
            }
          });
        }
        loadimages();
        //DELETE IMAGE

        $scope.delete = function(imgid){
          var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller:function($scope, $modalInstance){
              $scope.message="Are you sure do you want to delete this Photo?";
              $scope.ok = function(){
                dataFactory.getData({
                  apiUrl:"/pagestatic/dltimg/"+ imgid
                },   
                function(_rdata){
                  var modalInstance = $modal.open({
                    templateUrl: 'message.html',
                    controller: function($scope, $modalInstance, $state){
                      $scope.msg = "Image Successfully Deleted";
                      $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                      }
                    },
                  });
                  loadimages();
                  $modalInstance.close();
                });
              };
            }
          });
        }

        //SELECT IMAGE
        $scope.path=function(path){
          // pathimage = path;
          // pathimages();
          dlist[index].banner = path;
          $scope._dLsit = dlist;
           $modalInstance.dismiss('cancel');
        }

        $scope.upload = function(files){
          $scope.uploads(files);  
        };
        $scope.uploads = function (files){
          var filename;
          var filecount = 0;
          if(files && files.length){
            _imgLoader(true, false);
            for(var i=0;i < files.length;i++){
              var file = files[i];
              if(file.size >= 2000000){
                $scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                filecount = filecount + 1;
                if(filecount == files.length){
                  $scope.imageloader=false;
                  $scope.imagecontent=true;
                }
              }else{
                var promises;
                 promises = Upload.upload({
                    url: Config.amazonlink, //S3 upload url including bucket name
                    method: 'POST',
                    transformRequest: function (data, headersGetter) {
                      var headers = headersGetter();
                      delete headers['Authorization'];
                      return data;
                    },
                    fields : {
                      key: 'uploads/bannerimage/' + file.name, // the key to store the file on S3, could be file name or customized
                      AWSAccessKeyId: Config.AWSAccessKeyId,
                      acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                      policy: Config.policy, // base64-encoded json policy (see article below)
                      signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                    },
                    file: file
                 })

                promises.then(function(data){
                  console.log(data);
                  filecount = filecount + 1;

                  dataFactory.saveData({
                    apiUrl:"/pagestatic/saveimg"
                  },{
                    'imgfilename' : data.config.file.name
                  },
                  function(_rdata){
                    if(filecount == files.length){
                      $scope.imageloader=false;
                      $scope.imagecontent=true;
                    }
                    loadimages();
                  });
                });
              }
            }
          }
        }
        $scope.cancel =function() {
          $modalInstance.dismiss('cancel');
        };
      }
    });
  }
  
});




