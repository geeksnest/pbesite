'use strict';

 /*
 * Kyben Bogie \m/(-_^)\m/ oohhaa
 *
 * Readble as possible Rock in Roll to the World
 */

 app.controller('ReqinfoCtrl', function($scope, $state ,$q, $http, Config, $modal, dataFactory){

    // Load Data
    var loadData = function(){
      dataFactory.getData({
      apiUrl:"/pagestatic/page/reqinfo" //Your API Url
    },   
    function(_rdata){
      console.log(_rdata);
      // Beyond here Do Something about the result
      $scope.body = _rdata[0].body;
      $scope.did = _rdata[0].id;
      $scope.img = _rdata[0].img;
    })
    }
    loadData();

    $scope.imageselected = false;
    // $scope.loadingg = false;
    $scope.imgAlerts = [];
    $scope.closeAlert = function(index) {
      $scope.imgAlerts.splice(index, 1);
    };

    $scope.imageselected = false;

    $scope.prepare = function(file){
      if(file && file.length){
        if(file[0].size > 2000000){
          $scope.imageselected = false;
          console.log('File is too big!');
          $scope.imgAlerts = [{type: 'danger', msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      }else{
        console.log("below maximum");
        $scope.projImg = file;
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }




  $scope.save = function(_gdata){

     var imgUpload = function( files){
      dataFactory.imageUpload({
      dataFile:files, //Image or File to be upload
      apiUrl:null, //Your API Url (default value "null" if you wont save the image on your api)
      amazonS3:"uploads/bannerimage/" //Amazon S3 File path
      },
      function(loader){
        //Loader HERE true or false return value
      },
      function(rdata){
        saveData(rdata.config.file.name);
      });
    }

    var saveData = function(img){
      dataFactory.saveData({
        apiUrl:"/pagestatic/otherSave"
      },{
        id:_gdata.id,
        body:_gdata.body,
        img:img 
      },
      function(_rdata){
        if(_rdata.success){
          var modalInstance = $modal.open({
            templateUrl: 'message.html',
            controller: function($scope, $modalInstance, $state){
              $scope.msg = "Schedule an Introductory Session Info successfully Updated Updated";
              $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
              }
            },
          });
        }else{
          console.log("Something Went Wrong");
        }
      });
    } 

    var img = (_gdata.files == undefined ? saveData(_gdata.img)  : imgUpload(_gdata.files));

   
  }


});



