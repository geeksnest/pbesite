
// Flot Chart controller

// app.controller('UserloginCtrl', ['$scope', '$http', function($scope, $http, User,Config) {
	
// }]);
app.controller('AddUserCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config','PbeUser', '$location', '$anchorScroll', function ($http, $scope, $stateParams, $modal,$sce,Config,PbeUser, $location, $anchorScroll){

	// Default
	$scope.notReq= true;
	$scope.req= false;
	$scope._req = function(_gdata){
		if(_gdata){
			$scope.notReq= false;
			$scope.req= true;
		}else{
			$scope.notReq= true;
			$scope.req= false;
		}
	}


	//VALIDATE USERNAME
	$scope.stat="1";
	$scope.chkusername =function(username){
		PbeUser.checkusername(username, function(data){
			$scope.usrname= data.exists;
			if(data.exists == true){
				$scope.form.$invalid = true;
			}
		});
	};
	//VALIDATE EMAIL
	$scope.chkemail =function(email){
		PbeUser.checkemail(email, function(data){
			$scope.usremail= data.exists;
			if(data.exists == true){
				$scope.form.$invalid = true;
			}
		});

	};
	//VALIDATE PASSWORD IF MATCH
	$scope.confirmpass =function(confirmpass, password){
		PbeUser.password(confirmpass, password, function(data){
			$scope.pwdconfirm = data;
			if(data == true){
				$scope.form.$invalid = true;
			}
		});
	};

	$scope.showimageList = function(size,path){
		var amazon = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'imagelist.html',
			controller: imagelistCTRL,
			size: size,
			resolve: {
				path: function() {
					return amazon
				}
			}

		});
	}
	var pathimage = "";
	var pathimages = function(){
		$scope.amazonpath=pathimage;
	}

	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
		$scope.amazonpath= path;
		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.noimage = false;
		$scope.imggallery = false;

		var loadimages = function() {
			PbeUser.loadimage(function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.imagelist = data;
				}
			});
		}
		loadimages();

		$scope.path=function(path){
			var texttocut = Config.amazonlink + '/uploads/banner/';
			var newpath = path.substring(texttocut.length); 
			pathimage = newpath;
			pathimages();
			$modalInstance.dismiss('cancel');
		}



		

		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deleteCTRL,
				resolve: {
					imgid: function() {
						return id
					}
				}

			});

		}

		var deleteCTRL = function($scope, $modalInstance, imgid) {
			$scope.alerts = [];
			$scope.closeAlert = function (index) {
				$scope.alerts.splice(index, 1);
			};

			$scope.message="Are you sure do you want to delete this Photo?";
			$scope.ok = function() {
				$http({
					url: Config.ApiURL+"/bannerimage/delete/"+ imgid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
					loadimages();
					$modalInstance.close();
				}).error(function(data, status, headers, config) {
					loadimages();
					$modalInstance.close();
					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
				});
			};

			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}

		$scope.upload = function(files) {
			$scope.upload(files);  
		};

		$scope.upload = function (files) 
		{
			var filename
			var filecount = 0;
			if (files && files.length) {
				$scope.imageloader=true;
				$scope.imagecontent=false;

				for (var i = 0; i < files.length; i++){
					var file = files[i];

					if (file.size >= 2000000){
						$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
						filecount = filecount + 1;

						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}
					}
					else{
						var promises;

						var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                      var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                      promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/banner/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

	filecount = filecount + 1;
	filename = data.config.file.name;
	var fileout = {
		'imgfilename' : renamedFile
	};
	$http({
		url: Config.ApiURL + "/upload/banner",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(fileout)
	}).success(function (data, status, headers, config) {
		loadimages();
		if(filecount == files.length)
		{
			$scope.imageloader=false;
			$scope.imagecontent=true;
		}

	}).error(function (data, status, headers, config) {
		$scope.imageloader=false;
		$scope.imagecontent=true;
	});

});
}
}
}
};
$scope.cancel = function() {
	$modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
	//DATE
	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
    	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
    	$scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
    };

    $scope.dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1,
    	class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    //END OF DATE
	//SAVE DATA

	$scope.submitData = function(user){

		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		PbeUser.cmssaveuser(user, function(data){
			console.log(data);

			if(data[0].success){
				$scope.user = "";
				$scope.amazonpath = "";
				$scope.form.$setPristine(true);
				$scope.alerts.splice(0, 1);
				$scope.alerts.push({type: 'success', msg: 'New User has been successfuly created'});
			}else{
				$scope.alerts.splice(0, 1);
				$scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
			}
		});
};
}]);


app.controller('UserListCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){

	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/user/list/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING

	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}


	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/user/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}



	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('cmsupdateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);


app.controller('UserListAdminCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/user/listadmin/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING
	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/user/listadmin/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}



	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){

		console.log("Fuck");

		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('cmsupdateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

}]);


app.controller('UserListManagerCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config',    function ($http, $scope, $stateParams, $modal,$sce,Config){
	//LIST ALL USERS
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL+"/user/listmanager/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (keyword) {
		var off = 1;
		paginate(off, keyword);
	}
	$scope.numpages = function (off, keyword) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	}
	$scope.setPage = function (off) {
		var searchito = $scope.searchtext;
		paginate(off, searchito);
	};
	paginate(off, keyword);
	//END USER LISTING
	$scope.resetsearch = function(){
		$scope.searchtext = undefined;
		paginate(off, keyword);
	}
	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};

	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/user/listmanager/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:  Config.ApiURL+"/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				$modalInstance.dismiss('cancel');
				alertme();
				loadpage();
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){

		console.log("Fuck");

		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('cmsupdateuser', {userid:update});
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
}]);



app.controller('ProfileUpdateCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config', 'PbeUser',   function ($http, $scope, $stateParams, $modal,$sce,Config, PbeUser) {
	$http({
		url: Config.ApiURL+"/user/info/"+userid,
		method: "GET",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function (data, status, headers, config) {
		$scope.user = data;
		$scope.amazonpath= data.banner;
		$scope.defaultuser = data.username;
		$scope.defaultemail = data.email;
	});

	//VALIDATE username
	$scope.chkusername =function(username){
		if($scope.defaultuser != username){
			PbeUser.checkusername(username, function(data){
				$scope.usrname= data.exists;
				if(data.exists == true){
					$scope.myForm.$invalid = true;
				}
			});
		}else{
			$scope.usrname= false;
		}
	};
	//VALIDATE EMAIL
	$scope.chkemail =function(email){
		if($scope.defaultemail != email){
			PbeUser.checkemail(email, function(data){
				$scope.usremail= data.exists;
				if(data.exists == true){
					$scope.myForm.$invalid = true;
				}
			});
		}
		else{
			$scope.usremail= false;
		}
	};
	$scope.showimageList = function(size,path){
		var amazon = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'imagelist.html',
			controller: imagelistCTRL,
			size: size,
			resolve: {
				path: function() {
					return amazon
				}
			}

		});
	}
	var pathimage = "";
	var pathimages = function(){
		$scope.amazonpath=pathimage;
	}
	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
		$scope.amazonpath= path;
		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.noimage = false;
		$scope.imggallery = false;

		var loadimages = function() {
			PbeUser.loadimage(function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.imagelist = data;
				}
			});
		}
		loadimages();

		$scope.path=function(path){
			var texttocut = Config.amazonlink + '/uploads/banner/';
			var newpath = path.substring(texttocut.length); 
			pathimage = newpath;
			pathimages();
			$modalInstance.dismiss('cancel');
		}



		$scope.upload = function(files) {
			$scope.upload(files);  
		};

		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deleteCTRL,
				resolve: {
					imgid: function() {
						return id
					}
				}

			});
		}
		var deleteCTRL = function($scope, $modalInstance, imgid) {

			$scope.alerts = [];

			$scope.closeAlert = function (index) {
				$scope.alerts.splice(index, 1);
			};

			$scope.message="Are you sure do you want to delete this Photo?";
			$scope.ok = function() {
				$http({
					url: Config.ApiURL+"/bannerimage/delete/"+ imgid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
					loadimages();
					$modalInstance.close();
				}).error(function(data, status, headers, config) {
					loadimages();
					$modalInstance.close();
					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
				});
			};

			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}

		$scope.upload = function (files) {
			var filename
			var filecount = 0;
			if (files && files.length) 
			{
				$scope.imageloader=true;
				$scope.imagecontent=false;

				for (var i = 0; i < files.length; i++) 
				{
					var file = files[i];

					if (file.size >= 2000000)
					{
						$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
						filecount = filecount + 1;

						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}
					}
					else
					{
						var promises;

						var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                      var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                      promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/banner/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

	filecount = filecount + 1;
	filename = data.config.file.name;
	var fileout = {
		'imgfilename' : renamedFile
	};
	$http({
		url: Config.ApiURL + "/upload/banner",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(fileout)
	}).success(function (data, status, headers, config) {
		loadimages();
		if(filecount == files.length)
		{
			$scope.imageloader=false;
			$scope.imagecontent=true;
		}

	}).error(function (data, status, headers, config) {
		$scope.imageloader=false;
		$scope.imagecontent=true;
	});

});
}



}
}
};

$scope.cancel = function() {
	$modalInstance.dismiss('cancel');
};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.updateData = function(user){
	$scope.alerts = [];
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
	$http({
		url:Config.ApiURL+"/user/update",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(user)
	}).success(function (data, status, headers, config) {

		if(data.success == "TOOSHORT")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Password is too Short'});
		}
		if(data.success == "OLDPASSWORDNOTMATCH")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Old Password not Match'});
		}
		if(data.success == "CONFIRMPASSWORDNOTMATCH")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Confirm Password not Match'});
		}
		if(data.success == "UPDATED")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'success', msg: 'User has been successfuly updated'});

		}
	})
};


	//DATE
	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
    	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
    	$scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
    };

    $scope.dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1,
    	class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}]);



app.controller('UserUpdateCtrl', ['$http', '$scope', '$stateParams', '$modal','$sce','Config', 'PbeUser',   function ($http, $scope, $stateParams, $modal,$sce,Config, PbeUser) {
	$http({
		url: Config.ApiURL+"/user/info/"+$stateParams.userid,
		method: "GET",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function (data, status, headers, config) {
		$scope.user = data;
		$scope.defaultuser = data.username;
		$scope.defaultemail = data.email;
		$scope.amazonpath= data.banner;
	});

	//VALIDATE username
	$scope.chkusername =function(username){
		if($scope.defaultuser != username){
			PbeUser.checkusername(username, function(data){
				$scope.usrname= data.exists;
				if(data.exists == true){
					$scope.myForm.$invalid = true;
				}
			});
		}
		else{
			$scope.usrname= false;
		}
	};
	//VALIDATE EMAIL
	$scope.chkemail =function(email){
		if($scope.defaultemail != email){
			PbeUser.checkemail(email, function(data){
				$scope.usremail= data.exists;
				if(data.exists == true){
					$scope.myForm.$invalid = true;
				}
			});
		}
		else{
			$scope.usremail= false;
		}

	};


	$scope.showimageList = function(size,path){
		var amazon = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'imagelist.html',
			controller: imagelistCTRL,
			size: size,
			resolve: {
				path: function() {
					return amazon
				}
			}

		});
	}
	var pathimage = "";
	var pathimages = function(){
		$scope.amazonpath=pathimage;
	}

	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
		$scope.amazonpath= path;
		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.noimage = false;
		$scope.imggallery = false;

		var loadimages = function() {
			PbeUser.loadimage(function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.imagelist = data;
				}
			});
		}
		loadimages();

		$scope.path=function(path){
			var texttocut = Config.amazonlink + '/uploads/banner/';
			var newpath = path.substring(texttocut.length); 
			pathimage = newpath;
			pathimages();
			$modalInstance.dismiss('cancel');
		}



		$scope.upload = function(files) {
			$scope.upload(files);  
		};

		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deleteCTRL,
				resolve: {
					imgid: function() {
						return id
					}
				}

			});

		}

		var deleteCTRL = function($scope, $modalInstance, imgid) {

			$scope.alerts = [];

			$scope.closeAlert = function (index) {
				$scope.alerts.splice(index, 1);
			};

			$scope.message="Are you sure do you want to delete this Photo?";
			$scope.ok = function() {
				$http({
					url: Config.ApiURL+"/bannerimage/delete/"+ imgid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
					loadimages();
					$modalInstance.close();
				}).error(function(data, status, headers, config) {
					loadimages();
					$modalInstance.close();
					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
				});
			};

			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}


		$scope.upload = function (files) 
		{
			var filename
			var filecount = 0;
			if (files && files.length) 
			{
				$scope.imageloader=true;
				$scope.imagecontent=false;

				for (var i = 0; i < files.length; i++) 
				{
					var file = files[i];

					if (file.size >= 2000000)
					{
						$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
						filecount = filecount + 1;

						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}
					}
					else
					{
						var promises;

						var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                      var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                      promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/banner/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

	filecount = filecount + 1;
	filename = data.config.file.name;
	var fileout = {
		'imgfilename' : renamedFile
	};
	$http({
		url: Config.ApiURL + "/upload/banner",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(fileout)
	}).success(function (data, status, headers, config) {
		loadimages();
		if(filecount == files.length)
		{
			$scope.imageloader=false;
			$scope.imagecontent=true;
		}

	}).error(function (data, status, headers, config) {
		$scope.imageloader=false;
		$scope.imagecontent=true;
	});

});
}



}
}
};





$scope.cancel = function() {
	$modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.updateData = function(user){
	$scope.alerts = [];
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
	$http({
		url:Config.ApiURL+"/user/update",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(user)
	}).success(function (data, status, headers, config) {

		if(data.success == "TOOSHORT")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Password is too Short'});
		}
		if(data.success == "OLDPASSWORDNOTMATCH")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Old Password not Match'});
		}
		if(data.success == "CONFIRMPASSWORDNOTMATCH")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'danger', msg: 'Confirm Password not Match'});
		}
		if(data.success == "UPDATED")
		{
			$scope.alerts.splice(0, 1);
			$scope.alerts.push({type: 'success', msg: 'User has been successfuly updated'});

		}

	})

};


	//DATE
	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
    	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
    	$scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
    };

    $scope.dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1,
    	class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}]);




