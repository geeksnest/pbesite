'use strict';
app.controller('Editnews', function($scope, $state ,$q,$stateParams,  $http, Config,$sce, $modal,NewsCenter, Upload){
	
	$scope.stat = "true";
	$scope.bt1=true;
	$scope.bt2=false;
	$scope.onpagetitle = function convertToSlug(Text){
		if(Text=="" || Text==null){
			$scope.news.slugs = " ";
		}else if($scope.defaulttitle == Text){
			$scope.notification = false;
			$scope.bt1=true;
			$scope.bt2=false;
		}
		else{
			NewsCenter.convertslug(Text, function(data){
				$scope.news.slugs = data;
			},function(datavalidate){

				if(datavalidate==1){
					$scope.notification = true;
					$scope.bt1=false;
					$scope.bt2=true;
				}else{
					$scope.notification = false;
					$scope.bt1=true;
					$scope.bt2=false;
				}
			}
			);
		}

	}
	////LIST CATEGORY
	NewsCenter.loadcategory(function(data){$scope.category = data;});
 	////LIST TAGS
 	NewsCenter.loadtags(function(data){$scope.tag = data;});
 	NewsCenter.listauthor(function(data){
 		$scope.authorlist = data;
 		NewsCenter.listcenter(function(data){
 			$scope.centerlist = data;
 			$scope.newsid= $stateParams.newsid;
 			NewsCenter.editnews($stateParams.newsid, function(data){

 				$scope.news = data.data;
 				$scope.defaulttitle = data.data.title;
 				$scope.amazonpath=data.data.banner;
 				$scope.featpath = data.data.feat;

 				centeridz= data.data.centerid;


 				if(data.data.featuredoption=="video"){
 					$scope.showvideo = true;
 					$scope.showbanner = false;
 					$scope.datafeat = data.data.feat;
 					$scope.watchvideo =$sce.trustAsHtml(data.data.feat.replace('width="420"', ""));
 					

 				}else if(data.data.featuredoption=="banner"){
 					$scope.showvideo = false;
 					$scope.showbanner = true;
 					$scope.featpath = data.data.feat;
 				}

 			});
 		});
 	});
	//////Featured Banner
	$scope.showvideo = false;
	$scope.showbanner = false;
	$scope.video = function(){
		$scope.showvideo = true;
		$scope.showbanner = false;
	}
	$scope.banner = function(){
		$scope.showvideo = false;
		$scope.showbanner = true;
	}
	$scope.paste=function(embed){
		$scope.datafeat = embed;
		$scope.watchvideo =$sce.trustAsHtml(embed.replace('width="420"', ""));
	}




	$scope.featuredbanner = function(size){
		var featbanner = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'imagelist.html',
			controller: featuredlistCTRL,
			size: size,
			resolve: {
				path: function() {
					return featbanner
				}
			}
		});
	}

	var pathimage = "";
	var featimages = function(){
		$scope.featpath=pathimage;

	}
	var featuredlistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
		$scope.amazonpath= path;
		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.noimage = false;
		$scope.imggallery = false;

		var loadimages = function() {
			NewsCenter.loadimage(function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.imagelist = data;
				}
			});
		}
		loadimages();

		$scope.path=function(path){
			var texttocut = Config.amazonlink + '/uploads/banner/';
            var newpath = path.substring(texttocut.length); 
            pathimage = newpath;
            featimages();
            $modalInstance.dismiss('cancel');
		}


		$scope.upload = function(files) {
			$scope.upload(files);  
		};
		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deleteCTRL,
				resolve: {
					imgid: function() {
						return id
					}
				}

			});
		}

	}
	/////End Featured Banner


	////FEATURED Video
	$scope.slctcenterid = function(cntrid){
		$scope.msg = [];
		$scope.closemsg = function (index) {
			$scope.msg.splice(index, 1);
		};
		if(centeridz=="" || centeridz == null){
			$scope.msg.push({ type: 'success', msg: 'Select Option Above (Video / Picture Banner)' });
		}
		centeridz = cntrid;
	}

	$scope.featuredvideo = function(size,path){
		var featbanner = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'featvideo.html',
			controller: videolistCTRL,
			size: size,
			resolve: {
				path: function() {
					return featbanner
				}
			}
		});
	}
	var embed = "";
	var featvideo = function(){
		$scope.datafeat = embed;
		$scope.watchvideo =$sce.trustAsHtml(embed.replace('width="420"', ""));
	}
	var videolistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, NewsCenter){

		
		$scope.centerids = centeridz;
		var loadvideo = function() {
			NewsCenter.loadvideo(centeridz, function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.videolist = data;
				}
			});
		}
		loadvideo();
		$scope.path=function(path){
			embed = path;
			featvideo();
			$modalInstance.dismiss('cancel');
		}

		$scope.savethis = function(embed){
			$http({
				url: Config.ApiURL + "/save/embed",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(embed)
			}).success(function (data, status, headers, config) {
				loadvideo();
			});
		}

		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deletevidCTRL,
				resolve: {
					vidid: function() {
						return id
					}
				}

			});
		}
		var deletevidCTRL = function($scope, $modalInstance, vidid) {
			$scope.alerts = [];
			$scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
			$scope.message="Are you sure do you want to delete this Video?";
			$scope.ok = function() {
				$http({
					url: Config.ApiURL+"/video/delete/"+ vidid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
					loadvideo();
					$modalInstance.close();
				}).error(function(data, status, headers, config) {
					loadvideo();
					$modalInstance.close();
					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
				});
			};
			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}



		/////Close Modal
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};

	}
	////// END Video





	$scope.showimageList = function(size,path){
		var amazon = $scope.amazon;
		var modalInstance = $modal.open({
			templateUrl: 'imagelist.html',
			controller: imagelistCTRL,
			size: size,
			resolve: {
				path: function() {
					return amazon
				}
			}

		});
	}

	var pathimage = "";

	var pathimages = function(){
		$scope.amazonpath=pathimage;
	}

	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
		$scope.amazonpath= path;
		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.noimage = false;
		$scope.imggallery = false;
		var loadimages = function() {
			NewsCenter.loadimage(function(data){
				if(data.error == "NOIMAGE" ){
					$scope.imggallery=false;
					$scope.noimage = true;
				}else{
					$scope.noimage = false;
					$scope.imggallery=true;
					$scope.imagelist = data;
				}
			});
		}
		loadimages();

		$scope.path=function(path){
			var texttocut = Config.amazonlink + '/uploads/banner/';
            var newpath = path.substring(texttocut.length); 
            pathimage = newpath;
            pathimages();
            $modalInstance.dismiss('cancel');
		}

		$scope.upload = function(files) {
			$scope.upload(files);  
		};

		$scope.delete = function(id){
			var modalInstance = $modal.open({
				templateUrl: 'delete.html',
				controller: deleteCTRL,
				resolve: {
					imgid: function() {
						return id
					}
				}

			});
		}

		var deleteCTRL = function($scope, $modalInstance, imgid) {

			$scope.alerts = [];

			$scope.closeAlert = function (index) {
				$scope.alerts.splice(index, 1);
			};

			$scope.message="Are you sure do you want to delete this Photo?";
			$scope.ok = function() {
				$http({
					url: Config.ApiURL+"/centernewsimage/delete/"+ imgid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {

					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
					loadimages();
					$modalInstance.close();
				}).error(function(data, status, headers, config) {
					loadimages();
					$modalInstance.close();
					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
				});
			};

			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}


		$scope.upload = function (files) {

			var filename
			var filecount = 0;
			if (files && files.length) 
			{
				$scope.imageloader=true;
				$scope.imagecontent=false;

				for (var i = 0; i < files.length; i++) 
				{
					var file = files[i];

					if (file.size >= 2000000)
					{
						$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
						filecount = filecount + 1;

						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}
					}
					else
					{
						var promises;

						var fileExtension = '.' + file.name.split('.').pop();

                  		// rename the file with a sufficiently random value and add the file extension back
                  		var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

                  		promises = Upload.upload({

                  			url:Config.amazonlink, 
                  			method: 'POST',
                  			transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                            	key: 'uploads/banner/' + renamedFile, 
                            	AWSAccessKeyId: Config.AWSAccessKeyId,
                            	acl: 'private', 
                            	policy: Config.policy, 
                            	signature: Config.signature, 
                            	"Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
                            },
                            file: file
                        })
                  		promises.then(function(data){
                  			filecount = filecount + 1;
                  			filename = data.config.file.name;
                  			var fileout = {
                  				'imgfilename' : renamedFile
                  			};

                  			NewsCenter.saveimage(fileout, function(data){
                  				if(data[0].success){
                  					loadimages();
                  					if(filecount == files.length)
                  					{
                  						$scope.imageloader=false;
                  						$scope.imagecontent=true;
                  					}
                  				}else{
                  					$scope.imageloader=false;
                  					$scope.imagecontent=true;
                  				}
                  			});



                  		});
                  	}



                  }
              }
          };

          $scope.cancel = function() {
          	$modalInstance.dismiss('cancel');
          };

      };
      
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////

$scope.save = function(news){
	$scope.alerts = [];
	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.isSaving = true;
	NewsCenter.updatenews(news, function(data){

		if(data[0].success){
			$scope.isSaving = false;
			$scope.alerts.push({type: 'success', msg: 'Center News successfully Updated!'});
		}else{
			$scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
		}
	});
}

//DATE
$scope.today = function() {
	$scope.dt = new Date();
};
$scope.today();
$scope.clear = function () {
	$scope.dt = null;
};
    // Disable weekend selection
    $scope.disabled = function(date, mode) {
    	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
    	$scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
    };

    $scope.dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1,
    	class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];


});



