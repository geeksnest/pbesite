'use strict';
app.controller('Managenews', function($scope, $state ,$q, $http, Config, $modal,NewsCenter, Upload){
	$scope.alerts = [];
	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = undefined;

	$scope.search = function (keyword) {
		NewsCenter.listnews('10',off, keyword, centerid, function(data){
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});

	}

	$scope.numpages = function (off, keyword) {
			NewsCenter.listnews('10', off, $scope.searchtext, centerid, function(data){
			$scope.data = data;

			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}

	$scope.setPage = function (pageNo) {
		NewsCenter.listnews(num,pageNo, $scope.searchtext, centerid, function(data){
			$scope.data = data;

			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	};
	var loadnews = function(){
		NewsCenter.listnews(num,off, keyword, centerid, function(data){

			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});

	}
	loadnews();
	 $scope.resetsearch = function(){
          $scope.searchtext = undefined;
          loadnews();
  }


	var successloadalert = function(){
		
			$scope.alerts.splice(0, 1);
		$scope.alerts.push({ type: 'success', msg: "News has been successfully deleted" });
	}
	var errorloadalert = function(){
		$scope.alerts.push({ type: 'danger', msg: "Something is Wrong...News Not Deleted" });
	}
	$scope.deletepage = function(newsid) {
		var modalInstance = $modal.open({
			templateUrl: 'delete.html',
			controller: pageDeleteCTRL,
			resolve: {
				id: function() {
					return newsid
				}
			}
		});
	}

	var pageDeleteCTRL = function($scope, $modalInstance, id) {
		$scope.message = "Are You sure you want to delete this News";
		$scope.ok = function() {
			NewsCenter.deletenews(id, function(data){
				if(data[0].success){
					loadnews();
					$modalInstance.close();
					successloadalert();
				}else{
					loadnews();
					$modalInstance.close();
					errorloadalert();
					
				}
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};


	};



	$scope.editpage = function(id) {
	
		var modalInstance = $modal.open({
			templateUrl: 'edit.html',
			controller: pageEditCTRL,
			resolve: {
				id: function() {
					return id
				}
			}
		});
	}
	var pageEditCTRL = function($scope, $modalInstance, id, $state) {
		$scope.message = "Are You sure you want to Edit this News";

		$scope.ok = function() {
			$state.go('editnews', {newsid: id });
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}


	/* Controllers */
	$scope.changestatus = function(id,status){
		$http({
			url:  Config.ApiURL+"/centernews/changestatus/"+ id + '/' + status,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			console.log(data);
			loadpage();
		})
	};

	var loadpage = function(){
		$http({
			url: Config.ApiURL+"/centers/news/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext +'/'+ centerid,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
		});
	}


});'use strict';

/* Controllers */

