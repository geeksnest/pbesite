'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Auditlogs', function($scope, $state, Upload ,$q, $http, Config, $stateParams ,$modal){

    //LIST ALL testimonials
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = undefined;
  var paginate = function (off, keyword) {
    $http({
      url: Config.ApiURL+"/auditlogs/list/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
    });
  }
  $scope.search = function (keyword) {
    var off = 1;
    paginate(off, keyword);
  }
  $scope.numpages = function (off, keyword) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);

  }
  $scope.setPage = function (off) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);
  };
  paginate(off, keyword);
  //END USER LISTING

  $scope.resetsearch = function(){
          $scope.searchtext = undefined;
          paginate(off, keyword);
  }

  $scope.deletealllogs = function(){
   var modalInstance = $modal.open({
    templateUrl: 'delete.html',
    controller: deleteCTRL,
  });

 }
 var successloadalert = function(){ $scope.alerts.splice(0, 1); $scope.alerts.push({ type: 'success', msg: 'All Logs successfully Deleted!' });}
 var errorloadalert = function(){$scope.alerts.push({ type: 'danger', msg: 'Something went wrong Image not Deleted!' });}

 
 var deleteCTRL = function($scope, $modalInstance) {
  $scope.message="Are you sure do you want to delete all logs?";
  $scope.ok = function() {
   $http({
      url: Config.ApiURL+"/auditlogs/delete",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function(data, status, headers, config) {
    $modalInstance.close();
    successloadalert();
    paginate(off, keyword);
  }).error(function(data, status, headers, config) {
    $modalInstance.close();
    errorloadalert();
  });


};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}





});