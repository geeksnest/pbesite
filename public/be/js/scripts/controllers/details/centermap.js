'use strict';
app.controller('Centermap', function($scope, $state ,$q, $http, Config, $modal, Centermap, $log, $timeout){

  var coordinates = function(){
    Centermap.getmap(centerid , function(data){
      if(data.lat==null && data.lon==null){

        ///////////////////////////////
        $scope.map = { center: { latitude: 38, longitude: -102 }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: 38, longitude: -102 },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           var lat = marker.getPosition().lat();
           var lon = marker.getPosition().lng();
               Centermap.savemap(lat, lon, centerid , function(data){
              });
               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
              };
            }
          }
        }
         ///////////////////////////////

      }else{
        ///////////////////////////////
        $scope.map = { center: { latitude: data.lat, longitude: data.lon }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: data.lat, longitude: data.lon },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           var lat = marker.getPosition().lat();
           var lon = marker.getPosition().lng();
               // $scope.lats = lat;
               // $scope.lons = lon;

               // console.log(lat+'/'+lon);

               Centermap.savemap(lat, lon, centerid , function(data){
              });


               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
              };
            }
          }
        }
}
}); 
}
coordinates();







});






