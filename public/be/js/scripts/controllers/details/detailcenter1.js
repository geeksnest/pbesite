'use strict';
app.controller('Createdetail', function($scope, $state ,$q, $http, Config, $modal, Centerdetails){

 $scope.mytime = new Date();
 $scope.hstep = 1;
 $scope.mstep = 15;
 $scope.hstep1 = 1;
 $scope.mstep1 = 15;
 $scope.options = {
  hstep: [1, 2, 3],
  mstep: [1, 5, 10, 15, 25, 30],
  hstep1: [1, 2, 3],
  mstep1: [1, 5, 10, 15, 25, 30]
};

$scope.ismeridian = true;
$scope.ismeridian2 = true;
$scope.toggleMode = function() {
  $scope.ismeridian = ! $scope.ismeridian;
  $scope.ismeridian2 = ! $scope.ismeridian2;
};

$scope.update = function() {
  var d = new Date();
  d.setHours( 14 );
  d.setMinutes( 0 );
  $scope.mytime = d;
};
$scope.changed = function () {
  
};



$scope.addEventModal = function(size, centerid,newsid){

  var modalInstance = $modal.open({
    templateUrl: 'addevent.html',
    controller: addeventCTRL,
    size: size,
    resolve: {
      centerid: function() {
        return centerid
      },
      newsid: function() {
        return newsid
      },
    }
  });
}

var addeventCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams , centerid,newsid){

  $scope.centerid = centerid;
    // Insert Data
    $scope.isSaving = false;
    $scope.SaveActivity = function(act){
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };
        if (act.dto == undefined) {act.dto = act.dfrom;}
        if (act.mytime == undefined) {act.mytime = new Date();}
        $scope.isSaving = true;
        $http({
          url: Config.ApiURL + "/center/event",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(act)
        }).success(function (data, status, headers, config) {

          if(data.hasOwnProperty('ErrDate')){
            $scope.isSaving = false;
            $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
            $scope.formcalen.$setPristine(false);
          }else{
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'New Activity Added!'});
            $scope.reset();
            $scope.formcalen.$setPristine(true);
          }
        }).error(function (data, status, headers, config, days_between) {
          $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
        });
      }

      $scope.reset = function () {
        $scope.act = {};
        $scope.formcalen.$setPristine(true);        
      };

    // Upload Picture
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');

    };
  };
//   $scope.editEventModal = function(size, centerid,newsid){

//   var modalInstance = $modal.open({
//     templateUrl: 'editevent.html',
//     controller: editeventCTRL,
//     size: size,
//     resolve: {
//       centerid: function() {
//         return centerid
//       },
//       newsid: function() {
//         return newsid
//       },
//     }
//   });
// }
//   var editeventCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams , centerid,newsid){
//     $http({
//       url:Config.ApiURL  + "/calendar/editactivity/" + newsid ,
//       method: "GET",
//       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//     }).success(function (data, status, headers, config) {
//       $scope.title= data.title;
//       $scope.act = data;
//     }).error(function (data, status, headers, config) {
//       $scope.status = status;
//     });

//   };
});

app.controller('viewcalendarCtrl', function($scope, $http, $parse, $modal, $state,Config, $compile,uiCalendarConfig){
 

    // list View    
    var loadCalendar = function(){
        $http({
            url: Config.ApiURL +"/calendar/listview", 
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            $scope.dataCalendar = data;
        }).error(function (data) {
            $scope.status = status;
        });
    }
    loadCalendar();



     var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };
    /* event source that contains custom events on the scope */
    $scope.events = [
      {title: 'All Day Event',start: new Date(y, m, 1)},
      {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
      {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
      {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        $scope.alertMessage = (date.title + ' was clicked ');
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
    /* add custom event*/
    $scope.addEvent = function() {
      $scope.events.push({
        title: 'Open Sesame',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        className: ['openSesame']
      });
    };
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.changeView = function(view,calendar) {
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };
     /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) { 
        element.attr({'tooltip': event.title,
                     'tooltip-append-to-body': true});
        $compile(element)($scope);
    };
    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender
      }
    };

    $scope.changeLang = function() {
      if($scope.changeTo === 'Hungarian'){
        $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
        $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
        $scope.changeTo= 'English';
      } else {
        $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        $scope.changeTo = 'Hungarian';
      }
    };
    /* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
      

  

});









app.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };
  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
}]);







app.controller('TimepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.mytime = new Date();
  $scope.hstep = 1;
  $scope.mstep = 15;
  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };
  $scope.ismeridian = true;
  $scope.toggleMode = function () {
    $scope.ismeridian = !$scope.ismeridian;
  };
  $scope.update = function () {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };
  $scope.changed = function () {
  };
  $scope.clear = function () {
    $scope.mytime = null;
  };
}]);


/* Controllers */

