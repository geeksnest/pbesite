'use strict';
app.controller('CalendarCtrl', function($scope, $state ,$q, $http, Config, $modal, Centerdetails, moment , $log, $timeout, uiCalendarConfig){

  if(centeridz!=undefined){
    console.log("login as Center manager");
    $scope.adminls = false;
    $scope.csls = true;

    $scope.idcenter = centeridz;
  }else{
    $scope.adminls = true;
    $scope.csls = false;
  }

// =========================================================================================
//# Time Select
// =========================================================================================
$scope.mytime = new Date();
$scope.hstep = 1;
$scope.mstep = 15;
$scope.hstep1 = 1;
$scope.mstep1 = 15;
$scope.options = {
  hstep: [1, 2, 3],
  mstep: [1, 5, 10, 15, 25, 30],
  hstep1: [1, 2, 3],
  mstep1: [1, 5, 10, 15, 25, 30]
};

$scope.ismeridian = true;
$scope.ismeridian2 = true;
$scope.toggleMode = function() {
  $scope.ismeridian = ! $scope.ismeridian;
  $scope.ismeridian2 = ! $scope.ismeridian2;
};

$scope.update = function() {
  var d = new Date();
  d.setHours( 14 );
  d.setMinutes( 0 );
  $scope.mytime = d;
};
$scope.changed = function () {

};

// =========================================================================================
//# ADD EVENT
// =========================================================================================




var loadCalendar = function(){
  $http({
    url: Config.ApiURL +"/calendar/listview", 
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {

    $scope.dataCalendar = data;
  }).error(function (data) {
    $scope.status = status;
  });
}
loadCalendar();

$scope.deleteactivity = function(id){
 var modalInstance = $modal.open({
  templateUrl: 'delete.html',
  controller: deleteCTRL,
  resolve: {
    eventid: function() {
      return id
    }
  }

});

}
var deleteCTRL = function($scope, $modalInstance, eventid) {
  $scope.message="Are you sure do you want to delete this Event?";
  $scope.ok = function() {
   $http({
     url: Config.ApiURL+"/activity/delete/"+ eventid,
     method: "get",
     headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    loadCalendar();
    $scope.events = [];
    uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
    $modalInstance.close();
    $scope.alerts.push({type: 'success', msg: 'Activity Deleted!'});
  }).error(function(data, status, headers, config) {
    $modalInstance.close();
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}


$scope.editEventModal = function(size, centerid, eventid){

  var modalInstance = $modal.open({
    templateUrl: 'editevent.html',
    controller: editeventCTRL,
    size: size,
    resolve: {
      centerid: function() {
        return centerid
      },
      eventid: function() {
        return eventid
      },
    }
  });
}
var editeventCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams , centerid,eventid){
  $http({
    url:Config.ApiURL  + "/calendar/editactivity/" + eventid ,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {

    console.log(data);


    $scope.data = data;
    $scope.mapalert = [];

    $scope.closeAlert = function (index) {
      $scope.mapalert.splice(index, 1);
    };
    if(data.centerid=="global"){
      $scope.formwidth = "col-sm-6";
      $scope.lat = data.lat;
      $scope.lon = data.lon;
      $scope.map= true;
        //CENTERMAP////////////////////////////////////////////////
        $scope.map = { center: { latitude: data.lat, longitude: data.lon }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: data.lat, longitude: data.lon },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           $scope.lat = marker.getPosition().lat();
           $scope.lon = marker.getPosition().lng();
           $http({
            url: Config.ApiURL + "/update/eventlocation/"+marker.getPosition().lat()+"/"+marker.getPosition().lng()+"/"+eventid ,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).success(function (data, status, headers, config) {
            $scope.mapalert.splice(0, 1);
            $scope.mapalert.push({type: 'success', msg: 'Event Location Updated'});
          }).error(function (data, status, headers, config) {
          });

          $scope.marker.options = {
            draggable: true,
            labelAnchor: "100 0",
            labelClass: "marker-labels"
          };
        }
      }
    }


  }else{
    $scope.formwidth = "col-sm-12";
    $scope.map= false;
  }






  $scope.mytime =data.mytime;
  $scope.hstep = 1;
  $scope.mstep = 15;
  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };
  $scope.ismeridian = false;
  $scope.toggleMode = function () {
    $scope.ismeridian = !$scope.ismeridian;
  };
  $scope.update = function () {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };
  $scope.changed = function () {
  };
  $scope.clear = function () {
    $scope.mytime = null;
  };
















}).error(function (data, status, headers, config) {
  $scope.status = status;
});




$scope.SaveUpdate = function(data){
 $scope.alerts = [];
 $scope.closeAlert = function (index) {
  $scope.alerts.splice(index, 1);
};
if (data.dfrom == undefined) {data.dfrom = new Date(data.dfrom2);}else{data.dfrom = new Date(data.dfrom);}
if (data.dto == undefined) {data.dto = new Date(data.dto2);}else{data.dto = new Date(data.dto)};
if (data.mytime == undefined) {data.mytime = new Date(data.mytime2);}else{data.mytime = data.mytime;}
$http({
  url: Config.ApiURL + "/calendar/updateactivity",
  method: "POST",
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  data: $.param(data)
}).success(function (data, status, headers, config) {
  $scope.events = [];
  loadCalendar();
  uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
  if(data == 1){
    $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
  }else{
    $scope.alerts.push({type: 'success', msg: 'Activity Updated!'});
  }
}).error(function (data, status, headers, config) {
  $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
});
}

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');

};


};

});



app.controller('viewcalendarCtrl', function($scope, $http, $parse, $modal, $state,Config, moment,$compile,uiCalendarConfig , uiGmapGoogleMapApi){

 var date = new Date();
 var d = date.getDate();
 var m = date.getMonth();
 var y = date.getFullYear();

 
 var getEventsPerMonth = function(callback){

   $http({
    url: Config.ApiURL+"/calendar/manageactivity",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {
   console.log(data);
   var events = []; 
   $scope.calendarView = 'month';
   $scope.calendarDay = new Date();
   if(data.error!='null'){
    angular.forEach(data, function(value, key) {



      var _bg ='';
      if(data[key].centerid=='global'){
       _bg ='bg-success bg';
     }else{
      switch(data[key].centername) {
        case 'Fairfax':
        _bg ='bg-primary bg';
        break;
        case 'Bayside':
        _bg ='bg-warning bg';
        break;
        case 'Mesa':
        _bg ='bg-dark bg';
        break;
        default: _bg ='bg-success bg';
      }
    }


    events.push({
      title: data[key].title,
      start:  moment(data[key].datef),
      end: moment(data[key].datet).add('days', 1),
      className: [_bg], 
      location:data[key].loc, 
      info: data[key].info,
      timestart: data[key].mytime,
      endtime: data[key].endtime,
      eventid: data[key].eventid,
      centerid: data[key].centerid
    });
  })
  }
  callback(events);
});

};


$scope.events = [];

/* alert on eventClick */
$scope.alertOnEventClick = function( event, jsEvent, view ){

  var modalInstance = $modal.open({
    templateUrl: 'editeventcal.html',
    controller: editeventCTRL,
    size: 'lg',
    resolve: {
      centerid: function() {
        return event.centerid
      },
      eventid: function() {
        return event.eventid
      },
    }
  });

};
/* alert on Drop */
$scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
 $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
};
/* alert on Resize */
$scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
 $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
};

$scope.overlay = $('.fc-overlay');
$scope.alertOnMouseOver = function( event, jsEvent, view ){
  // console.log(event);
  $scope.event = event;
  
  $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
  var wrap = $(jsEvent.target).closest('.fc-event');
  var cal = wrap.closest('.calendar');
  var left = wrap.offset().left - cal.offset().left;
  var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
  if( right > $scope.overlay.width() ) { 
    $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
  }else if ( left > $scope.overlay.width() ) {
    $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
  }else{
    $scope.overlay.find('.arrow').addClass('top');
  }
  (wrap.find('.fc-overlay').length == 0) && wrap.append( $scope.overlay );
}

/* config object */
$scope.uiConfig = {
  calendar:{
    height: 450,
    editable: true,
    header:{
      left: 'prev',
      center: 'title',
      right: 'next'
    },
    timeFormat: ' ',
    eventClick: $scope.alertOnEventClick,
    eventDrop: $scope.alertOnDrop,
    eventResize: $scope.alertOnResize,
    eventMouseover: $scope.alertOnMouseOver,
    eventStartEditable : false,
    events: function(start,end,timezone,callback){
      console.log("HITTING EVENTS!!!");
      getEventsPerMonth(function(data){
        callback(data);
      })
    },

  }
};

/* add custom event*/
$scope.addEvent = function() {
  $scope.events.push({
    title: 'New Event',
    start: new Date(y, m, d),
    className: ['b-l b-2x b-info']
  });
};

/* remove event */
$scope.remove = function(index) {
  $scope.events.splice(index,1);
};

/* Change View */
$scope.changeView = function(view,calendar) {
  uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
};
/* Change View */
$scope.renderCalender = function(calendar) {
  if(uiCalendarConfig.calendars[calendar]){
    uiCalendarConfig.calendars[calendar].fullCalendar('render');
  }
};

/* event sources array*/
$scope.eventSources = [$scope.events];










var editeventCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams , centerid,eventid, uiGmapGoogleMapApi){

  var events = {
      places_changed: function (searchBox) {
         var places = searchBox.getPlaces()
         if (places.length == 0) {
          return;
         }
         for (var i = 0, place; place = places[i]; i++) {
            $scope.lat = place.geometry.location.lat();
            $scope.lon = place.geometry.location.lng();

            $scope.map = {center: {latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() }, zoom: 10 };
            $scope.marker = {
               id: 0,
               coords: {
                  latitude: place.geometry.location.lat(),
                  longitude: place.geometry.location.lng()
               },
               options: { draggable: true },
               events: {
                  dragend: function (marker, eventName, args) {
                    console.log(marker.getPosition().lat()+'/'+marker.getPosition().lng());
                     $scope.lat = marker.getPosition().lat();
                     $scope.lon = marker.getPosition().lng();
                     var location = {
                        'centerid': $stateParams.centerid,
                        'lat': $scope.lat,
                        'lon': $scope.lon
                     }
                     $scope.marker.options = {
                        draggable: true,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                     };
                  }
               }
            }

         }
      }
   }
  $scope.searchbox = {template:'searchbox.tpl.html', events:events};



  $http({
    url:Config.ApiURL  + "/calendar/editactivity/" + eventid ,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {

    $scope.data = data;
    $scope.mapalert = [];

    $scope.closeAlert = function (index) {
      $scope.mapalert.splice(index, 1);
    };
    if(data.centerid=="global"){
      $scope.formwidth = "col-sm-6";
      $scope.lat = data.lat;
      $scope.lon = data.lon;
       $scope.searchbox = { template:'searchbox.tpl.html', events:events};
      $scope.map= true;
        //CENTERMAP////////////////////////////////////////////////
        $scope.map = { center: { latitude: data.lat, longitude: data.lon }, zoom: 8 };
        $scope.marker = { id: 0, coords: { latitude: data.lat, longitude: data.lon },
        options: { draggable: true },
        events: {
          dragend: function (marker, eventName, args) {
           $scope.lat = marker.getPosition().lat();
           $scope.lon = marker.getPosition().lng();
           $http({
            url: Config.ApiURL + "/update/eventlocation/"+marker.getPosition().lat()+"/"+marker.getPosition().lng()+"/"+eventid ,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).success(function (data, status, headers, config) {
            $scope.mapalert.splice(0, 1);
            $scope.mapalert.push({type: 'success', msg: 'Event Location Updated'});
          }).error(function (data, status, headers, config) {
          });

          $scope.marker.options = {
            draggable: true,
            labelAnchor: "100 0",
            labelClass: "marker-labels"
          };
        }
      }
    }


  }else{
    $scope.formwidth = "col-sm-12";
    $scope.map= false;
  }


  $scope.mytime =data.mytime;
  $scope.hstep = 1;
  $scope.mstep = 15;
  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };
  $scope.ismeridian = false;
  $scope.toggleMode = function () {
    $scope.ismeridian = !$scope.ismeridian;
  };
  $scope.update = function () {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };
  $scope.changed = function () {
  };
  $scope.clear = function () {
    $scope.mytime = null;
  };

}).error(function (data, status, headers, config) {
  $scope.status = status;
});




$scope.SaveUpdate = function(data){
 $scope.alerts = [];
 $scope.closeAlert = function (index) {
  $scope.alerts.splice(index, 1);
};
if (data.dfrom == undefined) {data.dfrom = new Date(data.dfrom2);}else{data.dfrom = new Date(data.dfrom);}
if (data.dto == undefined) {data.dto = new Date(data.dto2);}else{data.dto = new Date(data.dto)};
if (data.mytime == undefined) {data.mytime = new Date(data.mytime2);}else{data.mytime = data.mytime;}
$http({
  url: Config.ApiURL + "/calendar/updateactivity",
  method: "POST",
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  data: $.param(data)
}).success(function (data, status, headers, config) {
  // $scope.events = [];
  // loadCalendar();
  uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
  if(data == 1){
    $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
  }else{
    $scope.alerts.push({type: 'success', msg: 'Activity Updated!'});
  }
}).error(function (data, status, headers, config) {
  $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
});
}

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');

};


//DELETE

$scope._dlt = function(id){
 $modalInstance.dismiss('cancel');
  
 var modalInstance = $modal.open({
  templateUrl: 'delete.html',
  controller: deleteCTRL,
  resolve: {
    eventid: function() {
      return id
    }
  }

});
}

var deleteCTRL = function($scope, $modalInstance, eventid){
  $scope.message="Are you sure do you want to delete this Event?";
  $scope.ok = function(){
   $http({
     url: Config.ApiURL+"/activity/delete/"+ eventid,
     method: "get",
     headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    // loadCalendar();
    $scope.events = [];
    uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
    $modalInstance.close();
    $scope.alerts.push({type: 'success', msg: 'Activity Deleted!'});
  }).error(function(data, status, headers, config) {
    $modalInstance.close();
  });
};

$scope.cancel = function() {

  $modalInstance.dismiss('cancel');
};
}

};




});










app.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };
  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
}]);








/* Controllers */

