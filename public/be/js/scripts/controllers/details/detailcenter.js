'use strict';
app.controller('Createdetail', function($scope, $state ,$q, $http, Config, $modal, Centerdetails, moment , $log, $timeout, uiGmapGoogleMapApi, $stateParams){

  var datalat = '';
  var datalon = '';
  $scope.center= false;
  $scope.map= false;
  if(usertask=="Admin"){ $scope.option = true;
  }else{$scope.option = false;}
  $scope.centerid = centerid;
// =========================================================================================
//# Show GLOBAL MAP
// =========================================================================================

var events = {
      places_changed: function (searchBox) {
         var places = searchBox.getPlaces()
         if (places.length == 0) {
          return;
         }
         for (var i = 0, place; place = places[i]; i++) {
            $scope.lat = place.geometry.location.lat();
            $scope.lon = place.geometry.location.lng();

            $scope.map = {center: {latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() }, zoom: 10 };
            $scope.marker = {
               id: 0,
               coords: {
                  latitude: place.geometry.location.lat(),
                  longitude: place.geometry.location.lng()
               },
               options: { draggable: true },
               events: {
                  dragend: function (marker, eventName, args) {
                    console.log(marker.getPosition().lat()+'/'+marker.getPosition().lng());
                     $scope.lat = marker.getPosition().lat();
                     $scope.lon = marker.getPosition().lng();
                     var location = {
                        'centerid': $stateParams.centerid,
                        'lat': $scope.lat,
                        'lon': $scope.lon
                     }
                     $scope.marker.options = {
                        draggable: true,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                     };
                  }
               }
            }

         }
      }
   }
  $scope.searchbox = { template:'searchbox.tpl.html', events:events};

  console.log($scope.searchbox.template);



$scope.optionshow = function(option){
  if(option=="center"){
    Centerdetails.listcenters(function(data){
      $scope.centers = data
    });
    $scope.center= true;
    $scope.map= false;
    $scope.formwidth = "col-sm-12";
  }else{
    $scope.centerid = "globalid";
    $scope.center= false;
    $scope.map= true;
    $scope.formwidth = "col-sm-6";
    $scope.lat=38;
    $scope.lon=-102;
     $scope.searchbox = { template:'searchbox.tpl.html', events:events};
    $scope.map = { center: { latitude: 38, longitude: -102 }, zoom: 8 };
    $scope.marker = { id: 0, coords: { latitude: 38, longitude: -102 },
    options: { draggable: true },
    events: {
      dragend: function (marker, eventName, args) {
       $scope.lat = marker.getPosition().lat();
       $scope.lon = marker.getPosition().lng();
       $scope.marker.options = {
        draggable: true,
        labelAnchor: "100 0",
        labelClass: "marker-labels"
      };
    }
  }
}

}





}
$scope.slct = function(centerid){
  $scope.centerid = centerid
}
// =========================================================================================
//# SAVE
// =========================================================================================
$scope.SaveActivity = function(act){
  $scope.alerts = [];
  $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
  if (act.dto == undefined) {act.dto = act.dfrom;}
  if (act.mytime == undefined) {act.mytime = new Date();}
  $http({
    url: Config.ApiURL + "/center/event",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(act)
  }).success(function (data, status, headers, config) {
    if(data.hasOwnProperty('ErrDate')){
      $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
    }else{
      $scope.alerts.push({type: 'success', msg: 'New Activity Added!'});
      $scope.formevent.$setPristine(true);
      $scope.act ="";
    }
  }).error(function (data, status, headers, config, days_between) {
    $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
  });
}
});







app.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };
  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
}]);

app.controller('TimepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.mytime = new Date();
  $scope.hstep = 1;
  $scope.mstep = 15;
  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };
  $scope.ismeridian = true;
  $scope.toggleMode = function () {
    $scope.ismeridian = !$scope.ismeridian;
  };
  $scope.update = function () {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };
  $scope.changed = function () {
  };
  $scope.clear = function () {
    $scope.mytime = null;
  };
}]);
