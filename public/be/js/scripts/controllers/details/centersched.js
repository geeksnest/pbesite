'use strict';
app.controller('CenterSchedCtrl', function($scope, $state ,$q, $http, Config, $modal, Sched, $log, $timeout){


  var loadscheds =function(){
    Sched.getsched(centerid, function(data){
      $scope.sched = data;
    });
  }
  loadscheds();
  $scope.schedmodal = function(day) {
        console.log("sadasd");
    var modalInstance = $modal.open({
      templateUrl: 'schedule.html',
      controller: scheduleCTRL,
      resolve: {
        day: function() {
          return day
        }
      }
    });
  }  
  var scheduleCTRL = function($scope, $modalInstance, day,  $http, $stateParams ) {
    $scope.userid = userid;
    $scope.centerid= centerid;
    $scope.day =day;
    $scope.save =function(sched){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      $scope.isSaving = true;
      Sched.savesched(sched, function(data){
        if(data[0].success){
         loadscheds();
         $scope.isSaving = false;
         $scope.alerts.push({type: 'success', msg: 'New Class Schedule on '+ sched.day +' successfully Created!'});
       }else{
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      }
    });
      $modalInstance.dismiss('cancel');
    }  
    $scope.cancel = function(){
      $modalInstance.dismiss('cancel');
    };
  }; 

  ////////////////////////Update
  $scope.update = function(schedid ,day) {
    var modalInstance = $modal.open({
      templateUrl: 'scheduleedit.html',
      controller: updatescheduleCTRL,
      resolve: {
        schedid: function() {
          return schedid
        },
        day: function() {
          return day
        },
      }
    });
  }  

  var updatescheduleCTRL = function($scope, $modalInstance, schedid, day, $http, $stateParams ) {
     $scope.day =day;
    $scope.schedid = schedid;
    Sched.getschededit(schedid, function(data){
      $scope.data = data;
    
     });

   
    $scope.updatesched =function(sched){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      $scope.isSaving = true;

      Sched.updatesched(sched, function(data){
        if(data[0].success){
          loadscheds();
        }
      });
      $modalInstance.dismiss('cancel');

      loadscheds();
    }  

    $scope.cancel = function(){

      $modalInstance.dismiss('cancel');
    };
  }; 

  ////////////////////////DELETE
  $scope.delete = function(schedid) {

    var modalInstance = $modal.open({
      templateUrl: 'delete.html',
      controller: DeleteCTRL,
      resolve: {
        schedid: function() {
          return schedid
        }
      }
    });
  } 
  var DeleteCTRL = function($scope, $modalInstance,  $http, $stateParams, schedid ) {

    $scope.message ="Are You Sure do You Want to delete this schedule?";
    $scope.ok = function(){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      Sched.delete(schedid,userid, function(data){
        if(data[0].success){
         loadscheds();
         $scope.isSaving = false;
         $scope.alerts.push({type: 'success', msg: 'New Class Schedule Has been Delete'});
         $modalInstance.dismiss('cancel');
       }else{
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
      }
    });
    };

    $scope.cancel = function(){
      $modalInstance.dismiss('cancel');
    };
  }; 


  




});






