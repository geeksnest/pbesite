'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Manageauthor', function($scope, $state, Upload ,$q, $http, Config, $stateParams , Authormanage ,$modal){

    //LIST ALL testimonials
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = undefined;
  var paginate = function (off, keyword) {
    $http({
      url: Config.ApiURL+"/manage/listauthor/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
    });
  }
  $scope.search = function (keyword) {
    var off = 1;
    paginate(off, keyword);
  }
  $scope.numpages = function (off, keyword) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);

  }
  $scope.setPage = function (off) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);
  };
  paginate(off, keyword);
  //END USER LISTING

  $scope.resetsearch = function(){
          $scope.searchtext = undefined;
          paginate(off, keyword);
  }


   ////////////////ALERT FOR DELETE//////////////////
  $scope.alerts = [];
  $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  var alertme = function(){
    $scope.alerts.splice(0, 1); 
    $scope.alerts.push({type: 'success', msg: 'AUTHOR DELETED!'});     
  }
  var alertme2 = function(){
    $scope.alerts.splice(0, 1); 
    $scope.alerts.push({type: 'success', msg: 'AUTHOR UPDATED!'});     
  }
  ////////////////MODAL FOR DELETE//////////////////
    $scope.deletemodal = function (authorid) {
      var modalInstance = $modal.open({
      templateUrl: 'authorDelete.html',
      controller: dltCTRL,
      resolve: {
        dltauthorid: function () {
          return authorid;
        }
      }
    });
   
    }


    var dltCTRL = function ($scope, $modalInstance, dltauthorid) {
    $scope.dltauthor = dltauthorid;

    $scope.ok = function (dltauthorid) {
      Authormanage.Delauthor(dltauthorid,userid,function(data){
        var searchito = $scope.searchtext;
         paginate(off, searchito);
         alertme();
         $modalInstance.dismiss('cancel');
      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  ////////////////END MODAL FOR DELETE//////////////////
  //////////////////MODAL FOR EDIT ///////////////
  var editmodalCTRL = function($scope, $modalInstance, editauthorid, $state) {
        $scope.ok = function(){
            $scope.editauthorid = editauthorid;
            $state.go('editauthor', {editauthorid: editauthorid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
        }
  $scope.editmodal = function(editauthorid) {
        var modalInstance = $modal.open({
            templateUrl: 'authorEdit.html',
            controller: editmodalCTRL,
            resolve: {
                editauthorid: function() {
                    return editauthorid;
                }
            }
        });
    }
 ////////////////END MODAL FOR EDIT////////////////// 
});

app.controller('Editauthor', function($scope, $state, Upload ,$q, $http, Config, $stateParams , Authormanage ,$modal){
    $scope.userid = userid;
    //EDIT otor
        $http({
            url: Config.ApiURL+"/manage/authoredit/"+$stateParams.editauthorid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $scope.amazonpath = data.image;
            $scope.author = data;
          }).error(function (data, status, headers, config) {
          });
    //END OF EDIT

    // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.showimageList = function(size,path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'authorimagelist.html',
        controller: imagelistCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            }
        }

    });
}

var pathimage = "";

var pathimages = function(){
    $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;


    var loadimages = function() {
        Authormanage.loadimage(function(data){
            if(data.error == "NOIMAGE" ){
              $scope.imggallery=false;
              $scope.noimage = true;
            }else{
              $scope.noimage = false;
              $scope.imggallery=true;
              $scope.imagelist = data;
            }
        });
    }
    loadimages();

    $scope.path=function(path){
      var texttocut = Config.amazonlink + '/uploads/authorimages/';
      var newpath = path.substring(texttocut.length); 
      pathimage = newpath;
      pathimages();
      $modalInstance.dismiss('cancel');
    }

    $scope.delete = function(id){
          var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {
              imgid: function() {
                return id
              }
            }

          });

       }
 
 var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
   url: Config.ApiURL+"/authorimage/delete/"+ imgid + "/" + userid,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}




    $scope.upload = function(files) {
       $scope.upload(files);  
    };

    
   $scope.upload = function (files) 
   {
    var filename
    var filecount = 0;
    if (files && files.length) 
    {
        $scope.imageloader=true;
        $scope.imagecontent=false;

        for (var i = 0; i < files.length; i++) 
        {
            var file = files[i];

            if (file.size >= 2000000)
            {
                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                filecount = filecount + 1;
                
                if(filecount == files.length)
                {
                    $scope.imageloader=false;
                    $scope.imagecontent=true;
                }
            }
            else
            {
                var promises;

                var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
                
                promises = Upload.upload({
                    
                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/authorimages/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
                        promises.then(function(data){

                        filecount = filecount + 1;
                        filename = data.config.file.name;
                        var fileout = {
                            'imgfilename' : renamedFile,
                            'userid' : userid
                        };
                        $http({
                            url: Config.ApiURL + "/upload/authorimage",
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param(fileout)
                        }).success(function (data, status, headers, config) {
                            loadimages();
                            if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                            
                        }).error(function (data, status, headers, config) {
                            $scope.imageloader=false;
                            $scope.imagecontent=true;
                        });
                        
                    });
                    }
                    }
                    }
                    };
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');

                    };
                  };
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////

  

  


    //Update otor kasla awan

    //ALERT FOR apdate otor
  $scope.alerts = [];
  $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  var alertme = function(){
    $scope.alerts.splice(0, 1); 
    $scope.alerts.push({type: 'success', msg: 'AUTHOR SUCCESSFULLY UPDATED!'});      
  }

  //SAVE otor
  $scope.Updateauthor = function(author){
    Authormanage.updateauthor(author,function(data){
      console.log(data);
      if(data.error)
            {
              $scope.savecheck = 'Oops! Something went wrong!';
            }
            else{
              alertme();
           }

   });
  }



  //DATE
   $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];




});