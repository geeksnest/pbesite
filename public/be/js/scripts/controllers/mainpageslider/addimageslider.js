'use strict';

/* Controllers */

app.controller('Addimageslider', function($scope, $state, Upload ,$q, $http, Config, $modal ,Addimageslider ){
		

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    Addimageslider.loadimage(function(data){
       if(data.error == "NOIMAGE" ){
              $scope.imggallery=false;
              $scope.noimage = true;
            }else{
              $scope.noimage = false;
              $scope.imggallery=true;
              $scope.imagelist = data;
            }
    });
  }
  loadimages();

  $scope.delete = function(id){
   var modalInstance = $modal.open({
    templateUrl: 'delete.html',
    controller: deleteCTRL,
    resolve: {
      imgid: function() {
        return id
      }
    }

  });

 }

 
 var deleteCTRL = function($scope, $modalInstance, imgid) {
  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
   url: Config.ApiURL+"/mainpageslider/delete/"+ imgid,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
  });


};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}





$scope.upload = function(files) {
 $scope.upload(files);  
};
$scope.upload = function (files) 
{
  var filename
  var filecount = 0;
  if (files && files.length) 
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++) 
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;

        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
        var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;


        promises = Upload.upload({

         url:Config.amazonlink, 
         method: 'POST',
         transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                key: 'uploads/banner/' + renamedFile, 
                                AWSAccessKeyId: Config.AWSAccessKeyId,
                                acl: 'private', 
                                policy: Config.policy, 
                                signature: Config.signature, 
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' 
                              },
                              file: file
                            })
        promises.then(function(data){

          filecount = filecount + 1;
          filename = data.config.file.name;
          var fileout = {
            'imgfilename' : renamedFile
          };
          $http({
                url: Config.ApiURL + "/mainpage/saveslider",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout),
              }).success(function (data, status, headers, config) {
                loadimages();
                if(filecount == files.length)
                {
                  $scope.imageloader=false;
                  $scope.imagecontent=true;
                }
              })
            });
      }



    }
  }
};	


})