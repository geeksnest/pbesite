'use strict';

/* Controllers */

app.controller('Manageappointment', function($scope, $state, Upload ,$q, $http, Config, $stateParams ,Manageappointment ,$modal,$interval){

  Manageappointment.optioncenter(function(data){
        $scope.optioncenter = data;
    });
  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = undefined;
    var center = centertitle;
    $scope.searchcenter = centertitle;
  
    var loadlist  = function(){
       Manageappointment.loadlist(num,off, keyword,center, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
       });
    }
    loadlist();
    $interval(function(){
      center = $scope.searchcenter;
      keyword = $scope.searchtext;
      loadlist();
    },10000);
    //RESET
    $scope.resetsearch  = function(){
       $scope.searchtext = undefined;
       $scope.searchcenter = centertitle;
       loadlist();
    };

    $scope.search = function (keyword,center) {
        var off = 0;
        Manageappointment.loadlist('10','1', keyword, $scope.searchcenter, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });

    }
    
    $scope.numpages = function (off, keyword,center) {
        Manageappointment.loadlist('10', '1', $scope.searchtext, $scope.searchcenter, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    }

    $scope.setPage = function (pageNo) {
        Manageappointment.loadlist(num,pageNo, $scope.searchtext, $scope.searchcenter, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    };
    //END OF LIST
     $scope.deletemodal = function(id) {

        var modalInstance = $modal.open({
            templateUrl: 'appointmentDelete.html',
            controller: appointmentDeleteCTRL,
            resolve: {
                id: function() {
                    return id
                }
            }
        });
    }
    var successloadalert = function(){
        $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Appointment successfully Deleted!' });
            
    }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong Appointment not Deleted!' });
    }
    var messsagesent = function(){
        $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'MESSAGE SENT!' });
    }

    var appointmentDeleteCTRL = function($scope, $modalInstance, id) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL+"/appointment/appointmentdelete/"+ id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                loadlist();
                $modalInstance.close();
                successloadalert();
            }).error(function(data, status, headers, config) {
                loadlist();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        };



    ///MODAL appointment

        $scope.reviewmodal = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'appointmentReview.html',
                controller: reviewappointmentCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewappointmentCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/appointment/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/appointment/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.timesched = data.timesched;
            $scope.datesched = data.datesched;
            $scope.reply.email = data.email;
            $scope.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })
        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/appointment/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                loadlist();
                messsagesent();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
            }); 

        };

    };





        ////END PROPOSAL CONTROLLER



    
})