app.controller('NotifCtrl',function($scope, Config,$cookies, $cookieStore,$http,$modal,$interval) {
    
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.hideme = true;

    $scope.notiflist = {};
    var num = 10;
    var off = 1;
    var keyword = undefined;
    var loadlist = function (off, keyword) {
        $http({
          url: Config.ApiURL+"/notification/list/" + num + '/' + off + '/' + keyword + '/' + userid,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
          $scope.countnew = data.countnew;
          $scope.countreviewed = data.countreviewed;
          $scope.countreplied = data.countreplied;
          $scope.notiflist = data.data;
          $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
      }).error(function (data, status, headers, config) {
      });
    }
    $scope.search = function (keyword) {
        var off = 1;
        loadlist(off, keyword);
    }
    $scope.numpages = function (off, keyword) {
        var searchito = $scope.searchtext;
        loadlist(off, searchito);
    }
    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        loadlist(off, searchito);
    };
    loadlist(off, keyword);
      //END USER LISTING

    $scope.resetsearch = function(){
          $scope.searchtext = undefined;
          loadlist(off, keyword);
    }


    $interval(function(){
      var searchito = $scope.searchtext;
      loadlist(off, searchito);
    },10000);


    var messsagesent = function(){
            $scope.alerts.push({ type: 'success', msg: 'MESSAGE SENT!' });
    }

    ///MODAL REVIEW REQUEST

        $scope.reviewmodal = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'requestReview.html',
                controller: reviewrequestCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewrequestCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/request/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist(off, keyword);
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/request/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.email = data.email;
            $scope.reply.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })

        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/request/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                messsagesent();
                $modalInstance.dismiss('cancel');
                loadlist(off, searchito);
            }).error(function (data, status, headers, config) {

            });


        };

    };



    ///////////////////////

    ///MODAL appointment

        $scope.reviewmodal1 = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'appointmentReview.html',
                controller: reviewappointmentCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewappointmentCTRL = function($scope, $modalInstance, id, $sce) {

           $scope.process = false;
           $http({
            url: Config.ApiURL+"/appointment/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist(off, keyword);
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/appointment/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.fname = data.fname;
            $scope.lname = data.lname;
            $scope.timesched = data.timesched;
            $scope.datesched = data.datesched;
            $scope.reply.email = data.email;
            $scope.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })
        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/appointment/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                loadlist(off, keyword);
                messsagesent();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
            }); 

        };

    };

  $scope.delete = function(id){
   var modalInstance = $modal.open({
    templateUrl: 'delete.html',
    controller: deleteCTRL,
    resolve: {
      id: function() {
        return id
      }
    }

  });

 }
 var successloadalert = function(){ $scope.alerts.splice(0, 1); $scope.alerts.push({ type: 'success', msg: 'Notification successfully Deleted!' });}
 var errorloadalert = function(){$scope.alerts.push({ type: 'danger', msg: 'Something went wrong Notification not Deleted!' });}

 
 var deleteCTRL = function($scope, $modalInstance, id) {
  $scope.message="Are you sure do you want to delete this Notification?";
  $scope.ok = function() {
   $http({
   url: Config.ApiURL+"/dashboard/notification/delete/"+ id,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    loadlist(off, keyword);
    $modalInstance.close();
    successloadalert();
  }).error(function(data, status, headers, config) {
    loadlist(off, keyword);
    $modalInstance.close();
    errorloadalert();
  });


};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}



$scope.read = function(id,stat){
    $http({
        url:  Config.ApiURL+"/dashboard/notification/changestat/"+ id + "/" + stat,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        loadlist(off, keyword);
    })
}




});