'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Managetestimonials', function($scope, $state, Upload ,$q, $http, Config, $stateParams , ListTestimonials ,$modal){

    //LIST ALL testimonials
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = undefined;
  var paginate = function (off, keyword) {
    $http({
      url: Config.ApiURL+"/manage/list/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
    });
  }
  $scope.search = function (keyword) {
    var off = 1;
    paginate(off, keyword);
  }
  $scope.numpages = function (off, keyword) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);

  }
  $scope.setPage = function (off) {
    var searchito = $scope.searchtext;
    paginate(off, searchito);
  };
  paginate(off, keyword);
  //END USER LISTING

  $scope.resetsearch = function(){
          $scope.searchtext = undefined;
          paginate(off, keyword);
  }

  $scope.changestatus = function(id,status){
    $http({
        url:  Config.ApiURL+"/testimonials/changestatus/"+ id + '/' + status,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        loadpage();
      })
  };

  var loadpage = function(){
        $http({
        url: Config.ApiURL+"/manage/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      }).error(function (data, status, headers, config) {
      });
    }


   ////////////////ALERT FOR DELETE//////////////////
  $scope.alerts = [];
  $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  var alertme = function(){
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({type: 'success', msg: 'TESTIMONIAL DELETED!'});      
  }
  var alertme2 = function(){
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({type: 'success', msg: 'TESTIMONIAL UPDATED!'});      
  }
  ////////////////MODAL FOR DELETE//////////////////
    $scope.deletemodal = function (testimonialid) {
      var modalInstance = $modal.open({
      templateUrl: 'testimonialDelete.html',
      controller: dltCTRL,
      resolve: {
        dlttestimonialid: function () {
          return testimonialid;
        }
      }
    });
   
    }


    var dltCTRL = function ($scope, $modalInstance, dlttestimonialid) {
    $scope.dltuser = dlttestimonialid;

    $scope.ok = function (dlttestimonialid) {
      ListTestimonials.Deltestimonial(dlttestimonialid,function(data){
         paginate(off, keyword);
         alertme();
         $modalInstance.dismiss('cancel');
      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  ////////////////END MODAL FOR DELETE//////////////////
  //////////////////MODAL FOR EDIT ///////////////
  var editmodalCTRL = function($scope, $modalInstance, edittestimonialid, $state) {
        $scope.ok = function(){
            $scope.edittestimonialid = edittestimonialid;
            $state.go('edittestimonials', {edittestimonialid: edittestimonialid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }
  $scope.editmodal = function(edittestimonialid) {
        var modalInstance = $modal.open({
            templateUrl: 'testimonialEdit.html',
            controller: editmodalCTRL,
            resolve: {
                edittestimonialid: function() {
                    return edittestimonialid
                }
            }
        });
    }
 ////////////////END MODAL FOR EDIT////////////////// 
	//DATE
	 $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];







});

app.controller('Edittestimonials', function($scope, $state, Upload ,$q, $http, Config, $stateParams , ListTestimonials ,$modal){

    //EDIT testimonials
        $http({
            url: Config.ApiURL+"/manage/edit/"+$stateParams.edittestimonialid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $scope.testimonials = data;
             $scope.amazonpath = data.image;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
    //END OF EDIT
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.showimageList = function(size,path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'testimonialsimagelist.html',
        controller: imagelistCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            }
        }

    });
}

var pathimage = "";

var pathimages = function(){
    $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimages = function() {
        $http({
            url: Config.ApiURL + "/createtestimonials/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data.error == "NOIMAGE" ){
              $scope.imggallery=false;
              $scope.noimage = true;
            }else{
              $scope.noimage = false;
              $scope.imggallery=true;
              $scope.imagelist = data;
            }
        }).error(function(data) {
        });
    }
    loadimages();


    $scope.path=function(path){
      var texttocut = Config.amazonlink + '/uploads/testimonialimage/';
      var newpath = path.substring(texttocut.length); 
      pathimage = newpath;
      pathimages();
      $modalInstance.dismiss('cancel');
    }



    $scope.upload = function(files) {
       $scope.upload(files);  
    };

     $scope.delete = function(id){
          var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {
              imgid: function() {
                return id
              }
            }

          });

       }
 
 var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
   url: Config.ApiURL+"/testimoinialimage/delete/"+ imgid,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}

    
   $scope.upload = function (files) 
   {
    var filename
    var filecount = 0;
    if (files && files.length) 
    {
        $scope.imageloader=true;
        $scope.imagecontent=false;

        for (var i = 0; i < files.length; i++) 
        {
            var file = files[i];

            if (file.size >= 2000000)
            {
                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                filecount = filecount + 1;
                
                if(filecount == files.length)
                {
                    $scope.imageloader=false;
                    $scope.imagecontent=true;
                }
            }
            else
            {
                var promises;

                var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
                
                promises = Upload.upload({
                    
                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/testimonialimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
    promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
    var fileout = {
        'imgfilename' : renamedFile
    };
    $http({
        url: Config.ApiURL + "/createtestimonials/saveimage",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(fileout)
    }).success(function (data, status, headers, config) {
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
        
    }).error(function (data, status, headers, config) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
    });
    
});
}



}
}
};
$scope.cancel = function() {
    $modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////


    //Update Testimonial

    //ALERT FOR SAVE TESTIMONIAL
  $scope.alerts = [];
  $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  var alertme = function(){
    $scope.alerts.push({type: 'success', msg: 'TESTIMONIAL SUCCESSFULLY UPDATED!'});      
  }

  //SAVE TESTIMONIAL
  $scope.Updatetestimonials = function(testimonials){
    ListTestimonials.updatetestimonials(testimonials,function(data){
      if(data.error)
            {
              $scope.savecheck = 'Oops! Something went wrong!';
            }
            else{
              $scope.alerts.splice(0, 1);
              alertme();
                
              /*$scope.savecheck = 'TESTIMONIAL SUCCESSFULLY SAVE!';

              $("#mydiv").fadeIn();
              setTimeout(fade_out, 5000);

              function fade_out() {
              $("#mydiv").fadeOut();
              } */
           }

    });
  }



  //DATE
   $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];




});