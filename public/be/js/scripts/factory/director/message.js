app.factory('Messagedir', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		getmsg: function(centerid, callback){
			$http({
				url: Config.ApiURL + "/director/message/"+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
			
		},
		dirpage: function(msg, callback){
			$http({
				url: Config.ApiURL + "/center/director",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(msg)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		
		
		

	}
})