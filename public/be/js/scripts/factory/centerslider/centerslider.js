app.factory('Centerslider', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		
		test: function( callback){
		},
		loadimage: function(centerid, callback){
			$http({
				url: Config.ApiURL + "/centerbanner/sliderlist/"+centerid,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);

			})
		},
		saveimage: function(fileout, centerid, callback){
			var idcenter = '{"centerid":'+centerid+'}';
			$http({
				url: Config.ApiURL + "/centerpage/slider/"+fileout+'/'+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				// data: $.param(fileout, {"centerid": centerid}),
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},		
		listcenter: function(callback){
			$http({
				url: Config.ApiURL + "/list/center",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		}
		
		

	}
})