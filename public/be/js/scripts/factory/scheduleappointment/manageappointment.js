app.factory('Manageappointment', function($http, $q, Config){
  	return {
        data: {},
         loadlist: function(num, off, keyword , center, callback){
            $http({
                        url: Config.ApiURL +"/appointment/manageappointment/" + num + '/' + off + '/' + keyword + '/' + center,
                        method: "GET",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                       data = data;
                       callback(data);
                       pagetotalitem = data.total_items;
                       currentPage = data.index;
                       
                    }).error(function (data, status, headers, config) {
                       callback(data);
                    });
                },
                optioncenter: function(callback){
                  $http({
                    url: Config.ApiURL +"/option/center",
                    method: "GET",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function (data, status, headers, config) {
                    callback(data);
                  }).error(function (data, status, headers, config) {
                    callback(data);
                  });
                },
    } 
})