app.factory('Author', function($http, $q, Config){
  	return {
        data: {},
  		 saveauthor: function(author,callback)
         {
            $http({
            url: Config.ApiURL+"/createauthor/authorsave",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(author)
            }).success(function (data, status, headers, config) {
             callback(data);
            }).error(function (data, status, headers, config) {
             callback(data);
            });
         },
         loadimage: function(callback){
            $http({
                url: Config.ApiURL + "/list/authorimage",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        }
    }
   
})