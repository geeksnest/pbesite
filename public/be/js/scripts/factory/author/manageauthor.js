app.factory('Authormanage', function($http, $q, Config){
  	return {
        data: {},
        /*LOADLIST:function(callback)
         {
            $http({
            url: "http://pbeapi/manage/list",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                 callback(data);
            }).error(function (data, status, headers, config) {
       
            });
         },*/
         Delauthor:function(authorid,userid,callback)
         {
            $http({
                url: Config.ApiURL+"/manage/authordelete/"+authorid+"/"+ userid,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
            }); 
         },
         updateauthor: function(author,callback)
         {
            $http({
            url: Config.ApiURL+"/manage/authorupdate",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(author)
            }).success(function (data, status, headers, config) {
             callback(data);
            }).error(function (data, status, headers, config) {
             callback(data);
            });
         },
         loadimage: function(callback){
            $http({
                url: Config.ApiURL + "/list/authorimage",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        }
    }
   
})