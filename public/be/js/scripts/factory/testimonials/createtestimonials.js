app.factory('Testimonials', function($http, $q, Config){
  	return {
  		 savetestimonials: function(testimonials,callback){
            $http({
            url: Config.ApiURL+"/createtestimonials/testimonialsave",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(testimonials)

            }).success(function (data, status, headers, config) {
             callback(data);
            }).error(function (data, status, headers, config) {
             callback(data);
            });
         }
    }
   
})