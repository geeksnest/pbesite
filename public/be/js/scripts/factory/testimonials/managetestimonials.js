app.factory('ListTestimonials', function($http, $q, Config){
  	return {
         Deltestimonial:function(testid,callback)
         {
            $http({
                url: Config.ApiURL+"/manage/delete/"+testid,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
         },
         updatetestimonials: function(testimonials,callback)
         {
            $http({
            url: Config.ApiURL+"/manage/update",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(testimonials)

            }).success(function (data, status, headers, config) {
             callback(data);
            }).error(function (data, status, headers, config) {
             callback(data);
            });
         }
    }
   
})