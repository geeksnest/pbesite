app.factory('Centermap', function($http, $q, Config){
	return {
		test: function(callback){
			
			callback("this is a test");
		},
		savemap: function(lat, lon, centerid , callback){
			$http({
				url: Config.ApiURL + "/save/map/"+ lat+'/'+lon+'/'+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		getmap: function(centerid , callback){
			$http({
				url: Config.ApiURL +"/get/map/"+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})