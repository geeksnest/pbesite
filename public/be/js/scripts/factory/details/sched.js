app.factory('Sched', function($http, $q, Config){
	return {
		test: function(callback){
			
			callback("this is a test");
		},
		savesched: function(sched, callback){
			$http({
				url: Config.ApiURL + "/save/schedule",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(sched)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		updatesched: function(sched, callback){
			$http({
				url: Config.ApiURL + "/update/schedule",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(sched)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		getsched: function(centerid, callback){
			$http({
				url: Config.ApiURL + "/get/schedule/"+centerid,
				method: "get",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		getschededit: function(schedid, callback){
			$http({
				url: Config.ApiURL + "/get/scheduleedit/"+schedid,
				method: "get",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		delete: function(schedid, userid, callback){
			$http({
				url: Config.ApiURL + "/delete/schedule/"+schedid +"/"+ userid,
				method: "get",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},

		
	}
})