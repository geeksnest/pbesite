app.factory('Centerdetails', function($http, $q, Config){
	return {
		listcenters: function(callback){
			$http({
				url: Config.ApiURL +"/centers/eventlist",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		getmap: function(centerid , callback){
			$http({
				url: Config.ApiURL +"/get/map/"+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		}
		
	}
})