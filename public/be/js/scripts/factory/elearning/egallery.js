app.factory('Egallery', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadimage: function(callback){
			$http({
				url: Config.ApiURL+"/egallery/banner",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		loadVid: function(callback){
			$http({
				url: Config.ApiURL+"/egallery/video",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/egallery/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function (data, status, headers, config) {
				callback(data ,fileout);
			});
		},
		updateimage: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/egallery/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function (data, status, headers, config) {
				callback(data ,fileout);
			});
		},
		deletebanner: function(imgid, callback){
			$http({
				url: Config.ApiURL+"/egallery/delete/"+ imgid,
				method: "get",
				headers: {
					'Content-Type':'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data)
			}).error(function(data, status, headers, config) {
			});
		},
		savevid: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/evideo/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function (data, status, headers, config) {
				callback(data ,fileout);
			});
		},
		deletevideo: function(imgid, callback){
			$http({
				url: Config.ApiURL+"/evideo/delete/"+ imgid,
				method: "get",
				headers: {
					'Content-Type':'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data)
			}).error(function(data, status, headers, config) {
			});
		},
		updatevideo: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/evideo/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function (data, status, headers, config) {
				callback(data ,fileout);
			});
		},
	}
})