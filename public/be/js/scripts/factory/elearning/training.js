app.factory('Training', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadtraining: function(callback){
			$http({
				url: Config.ApiURL + "/training/list",
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		savetraining: function(data, callback){
		
			$http({
				url: Config.ApiURL + "/training/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		edittraining: function(idno, callback){
			$http({
				url: Config.ApiURL + "/training/edit/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		updatetraining: function(data, callback){
			$http({
				url: Config.ApiURL + "/training/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletetraining: function(vidid, callback){
			$http({
				url: Config.ApiURL+"/blogvid/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		dlttraining: function(id, callback){
			$http({
				url: Config.ApiURL+"/training/delete/"+ id,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback(data);
			});
		},
		
	}
})