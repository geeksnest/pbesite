app.factory('Mentors', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadvideo: function(callback){
			$http({
				url: Config.ApiURL + "/mentors/list/",
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		savevideo: function(embed, callback){
			$http({
				url: Config.ApiURL + "/mentors/embed",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(embed)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletevideo: function(vidid, callback){
			$http({
				url: Config.ApiURL+"/mentors/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		
	}
})