app.factory('BlogF', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		
		convertslug: function(text, callback ,validate){

			callback(angular.lowercase(text).replace(/ /g, '-'));
			$http({
				url: Config.ApiURL + "/centervalidate/title/"+text,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data, status, headers, config) {
					validate(data);
				});
				validate(text);
				
			},
			test: function( callback){
				callback("This is a Test");
				
			},
			saveimage: function(fileout, callback){
				$http({
					url: Config.ApiURL + "/centerpage/uploadbanner",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(fileout)
				}).success(function (data, status, headers, config) {
					callback(data);

				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},

			loadimage: function(callback){
				$http({
					url: Config.ApiURL + "/centerbanner/bannerlist",
					method: "GET",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function(data) {
					callback(data);

				})
			},
			savenews: function(data, callback){
				$http({
					url: Config.ApiURL + "/center/news",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(data)
				}).success(function (data, status, headers, config) {
					callback(data);
				}).error(function (data, status, headers, config) {
					callback(data);
				});
			},

			listnews: function(num, off, keyword,  callback){
				$http({
					url: Config.ApiURL +"/blog/list/" + num + '/' + off + '/' + keyword ,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					callback(data);
					pagetotalitem = data.total_items;
					currentPage = data.index;
				}).error(function (data, status, headers, config) {
					callback(data);
				});
			},
			deletenews: function(newsid, callback){
				$http({
					url: Config.ApiURL+"/blog/delete/"+ newsid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					callback(data);
				}).error(function(data, status, headers, config) {
					callback(data);
				});
			},
			editnews: function(newsid, callback){
				$http({
					url: Config.ApiURL+"/centerNews/edit/"+ newsid,
					method: "get",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
				}).success(function(data, status, headers, config) {
					callback(data);
				}).error(function(data, status, headers, config) {
					callback(data);
				});
			},
			updatenews: function(data, callback){
				
				$http({
					url: Config.ApiURL + "/news/update",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(data)
				}).success(function (data, status, headers, config) {
					callback(data);
				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},		
			listauthor: function(callback){
				$http({
					url: Config.ApiURL + "/list/author",
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data) {
					callback(data);
				})
			},		
			listcenter: function(callback){
				$http({
					url: Config.ApiURL + "/list/center",
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data) {
					callback(data);
				})
			},
			loadcategory: function(callback){
				$http({
					url: Config.ApiURL + "/news/listcategory",
					method: "GET",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function (data, status, headers, config) {
					callback(data);
				}).error(function (data, status, headers, config) {

				});
			},
			loadtags: function(callback){
				$http({
					url: Config.ApiURL + "/news/listtags",
					method: "GET",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function (data, status, headers, config) {         
					callback(data);
					
				}).error(function (data, status, headers, config) {

				});
			},
			saveembed: function(embed,centerid,callback){
				callback(embed);
				$http({
					url: Config.ApiURL + "/save/embed/"+embbed+"/"+centerid,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					callback(data);
				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},
			loadvideo: function(centerid, callback){
				$http({
					url: Config.ApiURL + "/featvid/list/"+centerid,
					method: "GET",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function(data) {
					callback(data);

				})
			},
			featimagesave: function(fileout, callback){
				$http({
					url: Config.ApiURL + "/news/featuredbanner",
					method: "POST",headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(fileout)
				}).success(function (data, status, headers, config) {
					callback(data);

				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},
			featimage: function(callback){
				$http({
					url: Config.ApiURL + "/news/featbannerlist",
					method: "GET", headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data) {
					callback(data);

				})
			},
			

		}
	})