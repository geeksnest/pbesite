app.factory('schoolF', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		convertslug: function(text, callback){
			callback(angular.lowercase(text).replace(/ /g, '-'));
			
		},
		update: function(data,memid, callback){
			$http({
				url: Config.ApiURL + "/school/update/" + data + "/"+memid,
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param(data)

			}).success(function(data, status, headers, config) {
				callback(data);
			})
		},
		
	}
})