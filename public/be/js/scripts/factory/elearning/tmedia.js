app.factory('Tmedia', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadtmedia: function(idno, callback){
			$http({
				url: Config.ApiURL + "/tmedia/list/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		edittmedia: function(idno, callback){
			$http({
				url: Config.ApiURL + "/edittmedia/list/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		savetmedia: function(data, callback){
			$http({
				url: Config.ApiURL + "/tmedia/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		savetpdf: function(data, callback){
			$http({
				url: Config.ApiURL + "/tmediapdf/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletetmedia: function(vidid, callback){
			$http({
				url: Config.ApiURL+"/tmedia/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		deletetpdf: function(idno, callback){
			$http({
				url: Config.ApiURL+"/pdf/delete/"+ idno,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		updatemedia: function(data, callback){
			$http({
				url: Config.ApiURL + "/tmedia/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		loadfeatured: function(idno, callback){
			$http({
				url: Config.ApiURL + "/efeatured/load/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		featvideo: function(data, callback){
			$http({
				url: Config.ApiURL + "/efeatured/video",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})