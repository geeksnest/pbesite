app.factory('Fileupload', function($http, $q, Config){
	return {
		upload: function(file,callback){
			var filename
			var filecount = 0;
			if (files && files.length) 
			{
				$scope.imageloader=true;
				$scope.imagecontent=false;

				for (var i = 0; i < files.length; i++) 
				{
					var file = files[i];

					if (file.size >= 2000000)
					{
						$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
						filecount = filecount + 1;

						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}
					}
					else
					{
						var promises;

						promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/banner/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

	filecount = filecount + 1;
	filename = data.config.file.name;
	var fileout = {
		'imgfilename' : filename
	};
	$http({
		url: Config.ApiURL + "/upload/banner",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: $.param(fileout)
	}).success(function (data, status, headers, config) {
		
		if(filecount == files.length)
		{
			$scope.imageloader=false;
			$scope.imagecontent=true;
		}

	}).error(function (data, status, headers, config) {
		$scope.imageloader=false;
		$scope.imagecontent=true;
	});

});
}



}
}	
}

}
})