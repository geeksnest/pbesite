app.factory('BlogFeatured', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadvideo: function(callback){
			$http({
				url: Config.ApiURL + "/blogvid/list/",
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		savevideo: function(embed, callback){
			$http({
				url: Config.ApiURL + "/blogvid/embed",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(embed)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletevideo: function(vidid, callback){
			$http({
				url: Config.ApiURL+"/blogvid/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL+"/blog/banner",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/blogbanner/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function (data, status, headers, config) {
				callback(data ,fileout);
			});
		},
		deletebanner: function(imgid, callback){
			$http({
				url: Config.ApiURL+"/blogbanner/delete/"+ imgid,
				method: "get",
				headers: {
					'Content-Type':'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data)
			}).error(function(data, status, headers, config) {
			});
		},
		saveblog: function(_gdata, callback){
			console.log(_gdata);
			$http({	
				url: Config.ApiURL + "/newblog/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(_gdata)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		loadblog: function(callback){
			$http({
				url: Config.ApiURL+"/blog/list",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		editblog: function(id,callback){
			$http({
				url: Config.ApiURL+"/editblog/list/"+id,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				console.log(data);
				callback(data);
			})
		},
		updateblog: function(_gdata, callback){
			
			$http({	
				url: Config.ApiURL + "/blog/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(_gdata)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletetblog: function(id, callback){
			$http({
				url: Config.ApiURL+"/blog/delete/"+ id,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		loadcategory: function(callback){
			$http({
				url: Config.ApiURL + "/blog/listcategory",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		loadtags: function(callback){
			$http({
				url: Config.ApiURL + "/news/listtags",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function (data, status, headers, config) {         
				callback(data);
				
			}).error(function (data, status, headers, config) {

			});
		},
	}
})