app.factory('PbeUser', function($http, $q, Config){
	return {
		test: function(callback){
			callback("test");
		},
		checkusername: function(username, callback){
			$http({
				url: Config.ApiURL+"/validate/username/"+username,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		checkEusername: function(username, callback){
			$http({
				url: Config.ApiURL+"/validate/eusername/"+username,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		checkemail: function(email, callback){
			$http({
				url:Config.ApiURL+"/validate/useremail/"+email,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},

			}).success(function (data, status, headers, config) {
				callback(data);

			});
		},
		password: function(confirmpass, password, callback){
			if(password!=confirmpass){
				callback(true);
			}else{
				callback(false);
			}
		},
		saveuser: function(user, callback){
			$http({
				url: Config.ApiURL+"/user/register",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(user)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/list/banner",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);

			})
		}




	}
})