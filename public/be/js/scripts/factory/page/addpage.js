app.factory('Addpage', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		convertslug: function(text, callback){
			callback(angular.lowercase(text).replace(/ /g, '-'));
			
		},
		savepage: function(page, callback){
			$http({
				url: Config.ApiURL + "/pages/create",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		parentmenu: function(callback){
			$http({
				url: Config.ApiURL + "/menu/main",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
				
			});
		},
		parentsub: function(id, callback){
			$http({
				url: Config.ApiURL + "/menu/sub/"+id,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/list/banner",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);

			})
		},
		displayedit: function(id, callback){
			$http({
				url: Config.ApiURL + "/page/pageedit/" + id ,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			})

		},
		updatepage: function(page, callback){
			$http({
				url: Config.ApiURL + "/pages/saveeditedpage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		}
		

	}
})