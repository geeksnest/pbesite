app.factory('Addimageslider', function($http, $q, Config){
	return {
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/list/mainpageslider",
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			})
		}
	}
})