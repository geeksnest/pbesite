app.factory('UpdateCenter', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
			callback("test");
		},
		getdata: function(id, callback){
			
			$http({
				url: Config.ApiURL +"/center/infos/"+id,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		savecenter: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/update/center",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		savemycenter: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/update/center",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		
		
		

	}
})