app.factory('ManageCenter', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
			callback("test");
		},
		listcenter: function(num, off, keyword , callback){
			$http({
				url: Config.ApiURL +"/centers/list/" + num + '/' + off + '/' + keyword,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
				pagetotalitem = data.total_items;
				currentPage = data.index;

			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletecenter: function(pageid, callback){
			$http({
				url: Config.ApiURL+"/center/delete/"+ pageid,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		
		

	}
})