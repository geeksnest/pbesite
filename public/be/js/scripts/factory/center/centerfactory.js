app.factory('CenterPage', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		convertslug: function(text, callback,validate){
			callback(angular.lowercase(angular.lowercase(text).replace(/&/g, 'and')).replace(/ /g, '-'));





				$http({
				url: Config.ApiURL + "/validate/title/"+text,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data, status, headers, config) {
					validate(data);
				});


			},
			userlist: function( callback){
				$http({
					url: Config.ApiURL + "/getuser/userlist",
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data, status, headers, config) {
					callback(data);
				});
			},
			statelist: function(callback){
				$http({
					url: Config.ApiURL + "/state/list",
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data, status, headers, config) {
					callback(data);
				});
			},
			citylist: function(state,callback){
				$http({
					url: Config.ApiURL + "/city/info/"+state,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data, status, headers, config) {
					callback(data);
				});
			},

			saveimage: function(fileout, callback){
				$http({
					url: Config.ApiURL + "/centerpage/uploadbanner",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(fileout)
				}).success(function (data, status, headers, config) {
					callback(data);

				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},
			loadimage: function(callback){
				$http({
					url: Config.ApiURL + "/centerbanner/bannerlist",
					method: "GET",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function(data) {
					callback(data);

				})
			},
			savecenter: function(fileout, callback){
				$http({
					url: Config.ApiURL + "/save/center",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(fileout)
				}).success(function (data, status, headers, config) {
					callback(data);

				}).error(function (data, status, headers, config) {
					callback(data);
				});

			},




		}
	})