'use strict';
// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'mwl.calendar',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'app.controllers',
    'ngFileUpload',
    'angularMoment',
    'uiGmapgoogle-maps',
    'ngSanitize',
    'xeditable',
    'angular.chosen',
    'ui.calendar',
    'ui.select',
    'colorpicker.module'
    ])

.run(['$rootScope', '$state', '$stateParams', function ($rootScope,   $state,   $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;        
}])

.config(
  ['$locationProvider', '$httpProvider', 'uiGmapGoogleMapApiProvider', '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' ,
  function ($locationProvider, $httpProvider, uiGmapGoogleMapApiProvider, $stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider ) {

    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });

    $httpProvider.interceptors.push(['$q', '$location',  function($q, $location, store) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                 $httpProvider.defaults.headers.common['Authorization'] = userid;
                return config;
            },
            'responseError': function(response) {
                if(response.status === 401 || response.status === 403) {
                    $location.path('/user/log');
                }
                return $q.reject(response);
            }
        };
    }]);
    

     // lazy controller, directive and service
     app.controller = $controllerProvider.register;
     app.directive  = $compileProvider.directive;
     app.factory    = $provide.factory;
     app.constant   = $provide.constant;
     app.value      = $provide.value;

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

     $urlRouterProvider   
     .otherwise('/dashboard');

     $stateProvider     

     .state('dashboard', {
        url: '/dashboard',
        templateUrl: '/pbeadmin/admin/dashboard',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/dashboard.js'
                    ]);
            }]
        }
    })
     /*USER STATE*/
     .state('userscreate', {
        url: '/userscreate',
        templateUrl: '/pbeadmin/users/create',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/users.js',
                    '/be/js/scripts/directives/validate.js',
                    // '/be/js/scripts/factory/user.js',
                    '/be/js/scripts/factory/user/user.js',

                    ]);
            }]
        }
    })
     .state('userlist', {
        url: '/userlist',
        templateUrl: '/pbeadmin/users/userlist',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/users.js',
                    '/be/js/scripts/directives/validate.js',
                    '/be/js/scripts/factory/user.js',
                    // '/be/js/scripts/controllers/user/student.js',
                    // '/be/js/scripts/controllers/user/teacher.js',
                    // '/be/js/scripts/controllers/user/mentor.js',
                    // '/be/js/scripts/controllers/user/leader.js'
                    ]);
            }]
        }
    })

      .state('cmsupdateuser', {
        url: '/cmsupdateuser/:userid',
        templateUrl: '/pbeadmin/users/cmsedituser',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/users.js',
                    '/be/js/scripts/directives/validate.js',
                    '/be/js/scripts/factory/user/user.js',
                    ]);
            }]
        }
    })

     .state('updateuser', {
        url: '/updateuser/:userid',
        templateUrl: '/pbeadmin/users/edituser',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/users.js',
                    '/be/js/scripts/directives/validate.js',
                    '/be/js/scripts/factory/user/user.js',
                    ]);
            }]
        }
    })
     .state('editprofile', {
        url: '/editprofile',
        templateUrl: '/pbeadmin/users/editprofile',
        resolve: {
            deps: ['uiLoad',
            function( uiLoad ){
                return uiLoad.load( [
                    '/be/js/scripts/controllers/users.js',
                    '/be/js/scripts/directives/validate.js',
                    '/be/js/scripts/factory/user/user.js',
                    ]);
            }]
        }
    })
     .state('logout', {
        url: '/updateuser/:userid',
        templateUrl: '/pbeadmin/admin/logout',

    })
     /*END USER STATE*/


            //rainier state


            //start createpage
            .state('createpage', {
                url: '/createpage',
                controller: 'Createpage',
                templateUrl: '/pbeadmin/pages/createpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/factory/page/addpage.js',
                            '/be/js/scripts/factory/upload/uploadimage.js',
                            ]);
                    }]
                }
                
            })
            //end createpage

            //start managepage
            .state('managepage', {
                url: '/managepage',
                controller: 'Managepage',
                templateUrl: '/pbeadmin/pages/managepage',
                
            })
            //end managepage

            //start editpage
            .state('editpage', {
                url: '/editpage/:pageid',
                controller: 'Editpage',
                templateUrl: '/pbeadmin/pages/editpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/factory/page/addpage.js',
                            ]);
                    }]
                }
                
            })
            //end editpage

          
            .state('statpage',{
                url: '/statpage',
                //controller: 'Managepage',
                templateUrl: '/pbeadmin/pages/statpage',
                
            })
            .state('pagebanner',{
                url: '/banner',
                controller: 'DetailsCtrl',
                templateUrl: '/pbeadmin/pages/banner',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/staticpage/bannerdetails.js',
                           '/be/js/scripts/factory/staticpage/statpage.js'                           
                        ]);
                    }]
                }
            })

            .state('statpage.home',{
                url: '/home',
                 controller: 'homeCtrl',
                templateUrl: '/pbeadmin/pages/home',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/staticpage/home.js',
                           '/be/js/scripts/factory/staticpage/statpage.js'                           
                        ]);
                    }]
                }
            })

            .state('statpage.sociallink',{
                url: '/siciallink',
                 controller: 'socialLinksCtrl',
                templateUrl: '/pbeadmin/pages/sociallink',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/staticpage/sociallinks.js',
                           '/be/js/scripts/factory/staticpage/statpage.js'                           
                        ]);
                    }]
                }
            })

            .state('statpage.reqinfo',{
                url: '/reqinfo',
                 controller: 'ReqinfoCtrl',
                templateUrl: '/pbeadmin/pages/reqinfo',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/staticpage/reqinfo.js',
                           '/be/js/scripts/factory/staticpage/statpage.js'                           
                        ]);
                    }]
                }
            })

            .state('statpage.schedintro',{
                url: '/schedintro',
                 controller: 'SchedintroCtrl',
                templateUrl: '/pbeadmin/pages/schedintro',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/staticpage/schedintro.js',
                           '/be/js/scripts/factory/staticpage/statpage.js'                           
                        ]);
                    }]
                }
            })
          

            //start createnews
            .state('createnews', {
                url: '/createnews',
                controller: 'Createnews',
                templateUrl: '/pbeadmin/news/createnews',
                
            })
            //end createpage

            //////////////TESTIMONIALS///////////////////

            //< CREATE TESTIMONIALS> // 
            .state('createtestimonials', {
                url: '/createtestimonials',
                controller: 'Createtestimonials',
                templateUrl: '/pbeadmin/testimonials/createtestimonial',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/testimonials/createtestimonials.js',
                            '/be/js/scripts/factory/testimonials/createtestimonials.js'
                            ]);
                    }]
                }
            })
            .state('managetestimonials', {
                url: '/managetestimonials',
                controller: 'Managetestimonials',
                templateUrl: '/pbeadmin/testimonials/managetestimonial',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/testimonials/managetestimonials.js',
                            '/be/js/scripts/factory/testimonials/managetestimonials.js'
                            ]);
                    }]
                }
            })
            .state('edittestimonials', {
                url: '/edittestimonials/:edittestimonialid',
                controller: 'Edittestimonials',
                templateUrl: '/pbeadmin/testimonials/edittestimonials',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/testimonials/managetestimonials.js',
                            '/be/js/scripts/factory/testimonials/managetestimonials.js'
                            ]);
                    }]
                }
            })

            //////////////CENTER///////////////////
            /////END TESTIMONIAL STATE////

            .state('createcenter', {
                url: '/createcenter',
                controller: 'Createcenter',
                templateUrl: '/pbeadmin/centers/createcenter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/center.js',
                            '/be/js/scripts/factory/center/centerfactory.js'
                            ]);
                    }]
                }

            })
            .state('managecenter', {
                url: '/managecenter',
                controller: 'Managecenter',
                templateUrl: '/pbeadmin/centers/managecenter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/managecenter.js',
                            '/be/js/scripts/factory/center/managecenter.js'
                            ]);
                    }]
                }

            })
            .state('editCenter', {
                url: '/editCenter/:centerid',
                controller: 'Updatecenter',
                templateUrl: '/pbeadmin/centers/updatecenter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/updatecenter.js',
                            '/be/js/scripts/factory/center/centerupdate.js',
                            '/be/js/scripts/factory/center/centerfactory.js',

                            //CLASS SCHEDULE
                            '/be/js/scripts/controllers/details/admin_centersched.js',
                            '/be/js/scripts/factory/details/sched.js',

                            //CENTER MAP
                            '/be/js/scripts/controllers/details/centermap.js',
                            '/be/js/scripts/factory/details/mapfactory.js',

                            ]);
                    }]
                }
                
            })

            .state('editCenter.centerinfo', {
                url: '/editCenterinfo',
                templateUrl: '/pbeadmin/centers/centerinfo'
            })

            .state('editCenter.sched', {
                url: '/centersched',
                templateUrl: '/pbeadmin/centers/centersched'
            })
             .state('editCenter.map', {
                url: '/centermap',
                templateUrl: '/pbeadmin/centers/centermap'
            })





            .state('managemycenter', {
                url: '/managemycenter',
                controller: 'Updatecenter',
                templateUrl: '/pbeadmin/centers/updatemycenter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/updatemycenter.js',
                            '/be/js/scripts/factory/center/centerupdate.js',
                            '/be/js/scripts/factory/center/centerfactory.js',
                            ]);
                    }]
                }
                
            })


            //MAIN PAGE SLIDER
            .state('addimageslider', {
                url: '/addimageslider',
                controller: 'Addimageslider',
                templateUrl: '/pbeadmin/mainpageslider/addimageslider',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/mainpageslider/addimageslider.js',
                            '/be/js/scripts/factory/mainpageslider/addimageslider.js',
                            ]);
                    }]
                }
            })
            //END OF MAIN PAGE SLIDER///


            //MANAGE REQUEST INFORMATION//
            .state('managerequestinfo', {
                url: '/managerequestinfo',
                controller: 'Managerequestinfo',
                templateUrl: '/pbeadmin/requestinformation/managerequestinfo',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/requestinformation/managerequestinfo.js',
                            '/be/js/scripts/factory/requestinformation/managerequestinfo.js',
                            ]);
                    }]
                }
            })
            //END MANAGE REQUEST INFORMATION//

            //MANAGE APPOINTMENT INFORMATION//
            .state('manageappointment', {
                url: '/manageappointment',
                controller: 'Manageappointment',
                templateUrl: '/pbeadmin/scheduleappointment/manageappointment',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/scheduleappointment/manageappointment.js',
                            '/be/js/scripts/factory/scheduleappointment/manageappointment.js',
                            ]);
                    }]
                }
            })
            //END MANAGE APPOINTMENT//

            .state('notifications', {
                url: '/notifications',
                controller: 'NotifCtrl',
                templateUrl: '/pbeadmin/admin/notifications',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/notifications/notif.js',
                            ]);
                    }]
                }
            })



            // Center NEws//
            .state('centernews', {
                url: '/centernews',
                controller: 'Centernews',
                templateUrl: '/pbeadmin/centernews/centernews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centernews/centernews.js',
                            '/be/js/scripts/factory/centernews/newscenter.js',
                            '/be/js/scripts/factory/centernews/_centernews.js',
                            
                            ]);
                    }]
                }
                
            })
            .state('managecenternews', {
                url: '/managecenternews',
                controller: 'Managenews',
                templateUrl: '/pbeadmin/centernews/managecenternews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centernews/managenews.js',
                            '/be/js/scripts/factory/centernews/newscenter.js',
                            ]);
                    }]
                }
                
            })
            .state('newstags', {
                url: '/newstag',
                controller: 'NewsTags',
                templateUrl: '/pbeadmin/centernews/newstags',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centernews/newstags.js',
                            '/be/js/scripts/factory/centernews/categtags.js',
                            ]);
                    }]
                }
                
            })
            .state('newscategory', {
                url: '/newscategory',
                controller: 'NewsCateg',
                templateUrl: '/pbeadmin/centernews/newscategory',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centernews/newscateg.js',
                            '/be/js/scripts/factory/centernews/categtags.js',
                            ]);
                    }]
                }
                
            })

            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'Editnews',
                templateUrl: '/pbeadmin/centernews/editnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centernews/editnews.js',
                            '/be/js/scripts/factory/centernews/newscenter.js',
                            '/be/js/scripts/factory/centernews/_centernews.js',
                            ]);
                    }]
                }
                
            })

            //Center Slider//
            .state('directormsg', {
                url: '/directormsg',
                controller: 'Directormsg',
                templateUrl: '/pbeadmin/director/directormsg',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/directormsg/director.js',
                            '/be/js/scripts/factory/director/message.js',
                            ]);
                    }]
                }
                
            })

            //Directors Message//
            .state('centerslider', {
                url: '/centerslider',
                controller: 'Manageslider',
                templateUrl: '/pbeadmin/centerslider/manageslider',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centerslider/manageslider.js',
                            '/be/js/scripts/factory/centerslider/centerslider.js',
                            ]);
                    }]
                }
                
            })


            .state('addevent', {
                url: '/addevent',
                controller: 'Createdetail',
                templateUrl: '/pbeadmin/centerdetails/addevent',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/details/detailcenter.js',
                            '/be/js/scripts/factory/details/centerdetails.js',
                            ]);
                    }]
                }
                
            })

            .state('centerevents',{
                url: '/centerevents',
                controller: 'CalendarCtrl',
                templateUrl: '/pbeadmin/centerdetails/callendar',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/details/calendar.js',
                            '/be/js/scripts/factory/details/centerdetails.js',


                            // '/vendors/fullcalendar/dist/fullcalendar.min.js',
                            // '/vendors/fullcalendar/dist/gcal.js'                          '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.0/moment.min.js',
                            '//cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.4/interact.min.js',
                            '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.0/moment.min.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendar.js',
                            '/globaljs/angular-bootstrap-calendar/src/filters/calendarTruncateEventTitle.js',
                            '/globaljs/angular-bootstrap-calendar/src/filters/calendarLimitTo.js',
                            '/globaljs/angular-bootstrap-calendar/src/filters/calendarDate.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/calendarConfig.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/calendarTitle.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/calendarHelper.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/calendarDebounce.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/moment.js',
                            '/globaljs/angular-bootstrap-calendar/src/services/interact.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCollapseFallback.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlDateModifier.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarYear.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarMonth.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarWeek.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarDay.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarSlideBox.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlCalendarHourList.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlDraggable.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlDroppable.js',
                            '/globaljs/angular-bootstrap-calendar/src/directives/mwlElementDimensions.js',
                            '/globaljs/angular-bootstrap-calendar/docs/scripts/demo.js',

                            ]);


}]
}

})

.state('centermap', {
    url: '/centermap',
    controller: 'Centermap',
    templateUrl: '/pbeadmin/centerdetails/map',
    resolve: {
        deps: ['uiLoad',
        function( uiLoad ){
            return uiLoad.load( [
                '/be/js/scripts/controllers/details/centermap.js',
                '/be/js/scripts/factory/details/mapfactory.js',

                            // '/be/js/jquery/load-google-maps.js',
                            // '/be/js/modules/ui-map.js',
                            ]);


        }]
    }

})

.state('centerschedule', {
    url: '/centerschedule',
    controller: 'CenterSchedCtrl',
    templateUrl: '/pbeadmin/centerdetails/hour',
    resolve: {
        deps: ['uiLoad',
        function( uiLoad ){
            return uiLoad.load( [
                '/be/js/scripts/controllers/details/centersched.js',
                '/be/js/scripts/factory/details/sched.js',
                ]);


        }]
    }

})


             //AUTHOR
             .state('createauthor', {
                url: '/createauthor',
                controller: 'Createauthor',
                templateUrl: '/pbeadmin/author/createauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/author/createauthor.js',
                            '/be/js/scripts/factory/author/createauthor.js'
                            ]);
                    }]
                }
            })
             .state('manageauthor', {
                url: '/manageauthor',
                controller: 'Manageauthor',
                templateUrl: '/pbeadmin/author/manageauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/author/manageauthor.js',
                            '/be/js/scripts/factory/author/manageauthor.js'
                            ]);
                    }]
                }
            })
             .state('editauthor', {
                url: '/editauthor/:editauthorid',
                controller: 'Editauthor',
                templateUrl: '/pbeadmin/author/editauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/author/manageauthor.js',
                            '/be/js/scripts/factory/author/manageauthor.js'
                            ]);
                    }]
                }
            })
            //END AUTHOR
            //SETTINGS
            .state('settings', {
                url: '/settings',
                controller: 'Settings',
                templateUrl: '/pbeadmin/settings/settings',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/settings/settings.js',
                            '/be/js/scripts/factory/settings/settings.js',
                            ]);
                    }]
                }
            })
            .state('auditlogs', {
                url: '/auditlogs',
                controller: 'Auditlogs',
                templateUrl: '/pbeadmin/auditlogs/auditlogs',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/auditlogs/auditlogs.js'
                            ]);
                    }]
                }
            })
            //////ELEARNING
           .state('elearning', {
                url: '/elearning',
                controller: 'ElearningCtrl',
                templateUrl: '/pbeadmin/elearning/page',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/main.js'
                            ]);
                    }]
                }
            })
           // .state('elearning.blog', {
            .state('blog', {
                url: '/blog',
                templateUrl: '/pbeadmin/elearning/blog',
                controller: 'BlogCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/createblog.js',
                            '/be/js/scripts/controllers/centernews/managenews.js',
                            '/be/js/scripts/factory/centernews/newscenter.js',
                            // Featured
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            ]);
                    }]
                }
            })
            .state('manageblog', {
                url: '/manageblog',
                templateUrl: '/pbeadmin/elearning/manageblog',
                controller: 'ManageBlog',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/manageblog.js',
                            '/be/js/scripts/controllers/elearning/_blog.js',
                            '/be/js/scripts/controllers/centernews/managenews.js',

                            '/be/js/scripts/factory/elearning/_manageblog.js',
                            // Featured
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            ]);
                    }]
                }
            })
            .state('blogtags', {
                url: '/blogtags',
                controller: 'NewsTags',
                templateUrl: '/pbeadmin/elearning/blogtags',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/blogtags.js',
                            '/be/js/scripts/factory/centernews/categtags.js',
                            ]);
                    }]
                }
                
            })
            .state('blogcategories', {
                url: '/blogcategories',
                controller: 'NewsCateg',
                templateUrl: '/pbeadmin/elearning/blogcategory',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/blogcateg.js',
                            '/be/js/scripts/factory/centernews/categtags.js',
                            ]);
                    }]
                }
                
            })
            .state('elearning.user', {
                url: '/euser',
                templateUrl: '/pbeadmin/elearning/user',
                controller: 'EgalleryCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_egalleryCTRL.js',
                            '/be/js/scripts/factory/elearning/egallery.js',

                            // USER LIST
                            '/be/js/scripts/controllers/user/eusers.js',
                            '/be/js/scripts/directives/validate.js',

                            '/be/js/scripts/factory/user.js',
                            
                            '/be/js/scripts/controllers/user/student.js',
                            '/be/js/scripts/controllers/user/teacher.js',
                            '/be/js/scripts/controllers/user/mentor.js',
                            '/be/js/scripts/controllers/user/leader.js',
                            '/be/js/scripts/controllers/user/pendingUser.js',
                            // Create User
                            '/be/js/scripts/factory/user/user.js',
                            '/be/js/scripts/controllers/elearning/c_school.js',
                           '/be/js/scripts/factory/elearning/f_school.js', 
                            ]);
                    }]
                }
            })

            .state('elearning.egallery', {
                url: '/egallery',
                templateUrl: '/pbeadmin/elearning/egallery',
                controller: 'EgalleryCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_egalleryCTRL.js',
                            '/be/js/scripts/factory/elearning/egallery.js',
                            ]);
                    }]
                }
            })

            .state('elearning.evideo', {
                url: '/evideo',
                templateUrl: '/pbeadmin/elearning/evideo',
                controller: 'EgalleryvideoCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_egalleryvidCTRL_bu.js',
                            '/be/js/scripts/factory/elearning/egallery.js',
                            ]);
                    }]
                }
            })

            .state('elearning.resources', {
                url: '/resources/:idno',
                templateUrl: '/pbeadmin/elearning/resources',
                controller: 'TmediaCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_tmediaCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js',  

                            '/be/js/scripts/factory/elearning/blogs/featured.js',                              
                        ]);
                    }]
                }
            })
            .state('elearning.training', {
                url: '/training/:idno',
                templateUrl: '/pbeadmin/elearning/training',
                controller: 'PagewdsubCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_trainingCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            '/be/js/scripts/factory/elearning/tmedia.js'
                            ]);
                    }]
                }
            })
             .state('elearning.edittraining', {
                url: '/edittraining/:idno',
                templateUrl: '/pbeadmin/elearning/edittraining',
                controller: 'ETrainingCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_edittrainingCTRL.js',
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js'
                            ]);
                    }]
                }
            })
            .state('elearning.tmedia', {
                url: '/tmedia/:idno/:hideopt',
                templateUrl: '/pbeadmin/elearning/_tmedia',
                controller: 'TmediaCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_tmediaCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js',                            
                        ]);
                    }]
                }
            })
            .state('elearning.intro', {
                url: '/intro/:idno',
                templateUrl: '/pbeadmin/elearning/introduction',
                controller: 'TmediaCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_tmediaCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js', 
                            '/be/js/scripts/factory/elearning/blogs/featured.js',                              
                        ]);
                    }]
                }
            })
            .state('elearning.contlinks', {
                url: '/contlinks/:idno',
                templateUrl: '/pbeadmin/elearning/contlinks',
                controller: 'PagewdsubCTRL',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_trainingCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            '/be/js/scripts/factory/elearning/tmedia.js'
                            ]);
                    }]
                }
            })
            .state('editblog', {
                url: '/editblog/:id',
                templateUrl: '/pbeadmin/elearning/blogedit',
                controller: 'EditBlogCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_blog_edit.js',
                            '/be/js/scripts/controllers/centernews/managenews.js',
                            '/be/js/scripts/factory/centernews/newscenter.js',
                            '/be/js/scripts/factory/elearning/blogs/featured.js',
                            ]);
                    }]
                }
            })
            .state('elearning.mentors', {
                url: '/mentors/:idno',
                templateUrl: '/pbeadmin/elearning/mentors',
              
                 controller: 'TmediaCtrl',
                 resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_tmediaCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js',
                            '/be/js/scripts/factory/elearning/blogs/featured.js',                            
                        ]);
                    }]
                }
            })
            .state('elearning.leaders', {
                url: '/leaders/:idno',
                templateUrl: '/pbeadmin/elearning/leaders',
          
                 controller: 'TmediaCtrl',
                 resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/elearning/_tmediaCTRL.js',
                            '/be/js/scripts/factory/elearning/training.js',
                            '/be/js/scripts/factory/elearning/tmedia.js',
                            //REMOVE LATER
                            '/be/js/scripts/factory/elearning/blogs/featured.js',                            
                        ]);
                    }]
                }
            }).state('elearning.school', {
                url: '/school',
                templateUrl: '/pbeadmin/elearning/school',
                 controller: 'SchoolCtrl',
                 resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/elearning/c_school.js',
                           '/be/js/scripts/factory/elearning/f_school.js',                
                        ]);
                    }]
                }
            })


            

        }
        ]
        )

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
});

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.resize.js',
    '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.spline.js',
    '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
    '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
    '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
    '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
    '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
    '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
    '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
    '/be/js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
    '/be/js/jquery/select2/select2-bootstrap.css',
    '/be/js/jquery/select2/select2.min.js',
    '/be/js/modules/ui-select2.js']
}
)
;