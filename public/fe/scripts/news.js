'use strict';
var app = angular.module('app',[
    'ngAnimate',
    'ngCookies',
    'ui.bootstrap',
    'mwl.calendar',
    'angularMoment'
    ])
.config(function($controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

});
app.controller('NewsCtrl', ['$scope', '$http','appConfig', function($scope , $http , appConfig) {

    
	   
   

 }]);
app.controller('viewcalendarCtrl', ['$scope', '$http','appConfig','moment', function($scope , $http , appConfig, moment) {



 //  $http({
 //    url: appConfig.ApiURL+"/calendar/manageactivity",
 //    method: "GET",
 //    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
 //  }).success(function (data, status, headers, config) {
 //   var events = [];
 //    function addDays(theDate, days) {
 //    return new Date(theDate.getTime() + days*24*60*60*1000);
 //  }
 //   angular.forEach(data, function(value, key) {
 //    $scope.events.push({
 //      title: data[key].title,
 //      type: 'success',
 //      startsAt:  data[key].datef,
 //      endsAt: addDays(new Date(data[key].datet), +1)


 //    });
 //    console.log();
 //  });
 // }).error(function (data, status, headers, config){

 // });

 $scope.calendarView = 'month';
 $scope.calendarDay = new Date();
 
 $scope.events = [
 {
  title: 'An event',
  type: 'warning',
  startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
  endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate()
}, {
  title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
  type: 'info',
  startsAt: moment().subtract(1, 'day').toDate(),
  endsAt: moment().add(5, 'days').toDate()
}, {
  title: 'This is a really long event title that occurs on every year',
  type: 'important',
  startsAt: moment().startOf('day').add(7, 'hours').toDate(),
  endsAt: moment().startOf('day').add(19, 'hours').toDate(),
  recursOn: 'year'
}
];
console.log($scope.events);

   

function showModal(action, event) {
  $modal.open({
    templateUrl: 'modalContent.html',
    controller: function($scope) {
      $scope.action = action;
      $scope.event = event;
    }
  });
}

$scope.eventClicked = function(event) {
  showModal('Clicked', event);
};

$scope.eventEdited = function(event) {
  showModal('Edited', event);
};

$scope.eventDeleted = function(event) {
  showModal('Deleted', event);
};

$scope.eventDropped = function(event) {
  showModal('Dropped', event);
};

$scope.toggle = function($event, field, event) {
  $event.preventDefault();
  $event.stopPropagation();
  event[field] = !event[field];
};



}]);
