app.factory('Egallery', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		loadimage: function(id, callback){
			$http({
				url: appConfig.ApiURL+"/egalleryfe/banner/"+id,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		loadVid: function(id, callback){
			$http({
				url: appConfig.ApiURL+"/egalleryfe/video/"+id,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		saveimage: function(fileout, callback){
			$http({
				url: appConfig.ApiURL+"/egalleryfe/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, appConfig) {
				callback(data ,fileout );
			}).error(function (data, status, headers, appConfig) {
				callback(data ,fileout);
			});
		},
		updateimage: function(fileout, callback){
			$http({
				url: appConfig.ApiURL+"/egallery/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, appConfig) {
				callback(data ,fileout );
			}).error(function (data, status, headers, appConfig) {
				callback(data ,fileout);
			});
		},
		deletebanner: function(imgid, callback){
			$http({
				url: appConfig.ApiURL+"/egallery/delete/"+ imgid,
				method: "get",
				headers: {
					'Content-Type':'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, appConfig) {
				callback(data)
			}).error(function(data, status, headers, appConfig) {
			});
		},
		savevid: function(fileout, callback){
			$http({
				url: appConfig.ApiURL+"/evideofe/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, appConfig) {
				callback(data ,fileout );
			}).error(function (data, status, headers, appConfig) {
				callback(data ,fileout);
			});
		},
		deletevideo: function(imgid, callback){
			$http({
				url: appConfig.ApiURL+"/evideo/delete/"+ imgid,
				method: "get",
				headers: {
					'Content-Type':'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, appConfig) {
				callback(data)
			}).error(function(data, status, headers, appConfig) {
			});
		},
		updatevideo: function(fileout, callback){
			$http({
				url: appConfig.ApiURL+"/evideo/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, appConfig) {
				callback(data ,fileout );
			}).error(function (data, status, headers, appConfig) {
				callback(data ,fileout);
			});
		},
	}
})