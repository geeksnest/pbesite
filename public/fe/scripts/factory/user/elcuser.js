app.factory('Euser', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		getinfo: function(userid, callback){
			$http({
				url: appConfig.ApiURL+"/user/info/"+userid,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		accverify: function(userid,code, callback){
			$http({
				url: appConfig.ApiURL +"/user/verify/" + userid + '/' + code,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data)
			})
		},
		accupdate: function(_gdata, callback){
			$http({
				url: appConfig.ApiURL +"/complete/profile",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(_gdata)
			}).success(function (data, status, headers, config) {
				callback(data);
			})
		},
		usrUpdate: function(_gdata, callback){
			$http({
				url: appConfig.ApiURL +"/usrupdate/profile",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(_gdata)
			}).success(function (data, status, headers, config) {
				callback(data);
			})
		},
		chngepass: function(_gdata, callback){
			$http({
				url: appConfig.ApiURL +"/chngepass/profile",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(_gdata)
			}).success(function (data, status, headers, config) {
				callback(data);
			})
		},
		listSchool: function(callback){
			$http({
				url: appConfig.ApiURL +"/list/school",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data)
			})
		},
		checkusername: function(username, userid,  callback){

			console.log(userid);
			$http({
				url: appConfig.ApiURL+"/validate/eusername/"+username,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		
	}
})