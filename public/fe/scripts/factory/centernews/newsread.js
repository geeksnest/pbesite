app.factory('NewsRead', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
				callback("This is a Test");
		},
		readnews: function(slugnews, callback){
			$http({
				url: appConfig.ApiURL+"/read/news/"+ slugnews,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		
		
		

	}
})