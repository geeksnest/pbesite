app.factory('NewsCenter', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		convertslug: function(text, callback){
			callback(angular.lowercase(text).replace(/ /g, '-'));
			
		},
		test: function( callback){
				callback("This is a Test");
			
		},
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/centerpage/uploadbanner",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/centerbanner/bannerlist",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);

			})
		},
		savenews: function(data, callback){

			$http({
				url: Config.ApiURL + "/center/news",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		listnews: function(num, off, keyword , centerid, callback){
			$http({
				url: Config.ApiURL +"/centers/news/" + num + '/' + off + '/' + keyword + '/' + centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
				pagetotalitem = data.total_items;
				currentPage = data.index;

			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletenews: function(newsid, callback){
			$http({
				url: Config.ApiURL+"/centerNews/delete/"+ newsid,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		editnews: function(newsid, callback){
			$http({
				url: Config.ApiURL+"/centerNews/edit/"+ newsid,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		updatenews: function(data, callback){
			
			$http({
				url: Config.ApiURL + "/news/update",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},		
		
		

	}
})