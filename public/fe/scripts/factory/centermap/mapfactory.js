app.factory('Centermap', function($http, $q, appConfig){
	return {
		test: function(callback){
			callback("this is a test");
		},
		getmap: function(centerid , callback){
			$http({
				url: appConfig.ApiURL +"/get/map/"+centerid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})