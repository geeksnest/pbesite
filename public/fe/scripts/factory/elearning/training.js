app.factory('Training', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		loadtraining: function(userid, classfor,_state, callback){
			$http({
				url: appConfig.ApiURL + "/usertraining/list/"+userid+'/'+classfor+'/'+_state,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				console.log(data);
				callback(data);
			})
		},
		savetraining: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/training/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		
		edittraining: function(idno, callback){
			$http({
				url: appConfig.ApiURL + "/training/edit/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		updatetraining: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/training/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletetraining: function(vidid, callback){
			$http({
				url: appConfig.ApiURL+"/blogvid/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},

		_savetraining: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/training/submit",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		getsent: function(usrid,classid, callback){
			$http({
				url: appConfig.ApiURL + "/training/getsent/"+usrid+'/'+classid,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		lasttraining: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/lasttraining/submit",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		lastvisit: function(usrid, callback){
			$http({
				url: appConfig.ApiURL+"/list/visit/"+usrid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},


		
	}
})