app.factory('Tmedia', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		loadtmedia: function(idno, userid, task,_state, callback){
			$http({
				url: appConfig.ApiURL + "/usrtmedia/list/"+idno+"/"+userid+"/"+task+"/"+_state,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		edittmedia: function(idno, callback){
			$http({
				url: appConfig.ApiURL + "/edittmedia/read/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		savetmedia: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/tmedia/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		savetpdf: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/tmediapdf/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		deletetmedia: function(vidid, callback){
			$http({
				url: appConfig.ApiURL+"/tmedia/delete/"+ vidid,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		deletetpdf: function(idno, callback){
			$http({
				url: appConfig.ApiURL+"/pdf/delete/"+ idno,
				method: "get",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				callback({ mtype: 'success', msg: 'Image successfully Deleted!' });
			}).error(function(data, status, headers, config) {
				callback({ mype: 'danger', msg: 'Something went wrong Image not Deleted!' });
			});
		},
		updatemedia: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/tmedia/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		loadfeatured: function(idno, callback){
			$http({
				url: appConfig.ApiURL + "/efeatured/load/"+idno,
				method: "GET", headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		featvideo: function(data, callback){
			$http({
				url: appConfig.ApiURL + "/efeatured/video",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})