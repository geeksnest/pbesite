app.factory('Blog', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		feloadblog: function(offset, page, callback){
			$http({
				url: appConfig.ApiURL+"/feblog/list/"+offset+"/"+page,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		readblog: function(id, callback){
			$http({
				url: appConfig.ApiURL+"/read/blog/"+id,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		eloadblog: function(callback){
			$http({
				url: appConfig.ApiURL+"/eblog/list",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		loadcategoryblog: function(category,offset, page, callback){
			$http({
				url: appConfig.ApiURL+"/feblog/category/"+category+'/'+offset+"/"+page,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		loadarchieveblog: function(publish,callback){
			$http({
				url: appConfig.ApiURL+"/feblog/archives/"+publish,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		tagsblog: function(tagsslugs, offset, page, callback){
			$http({
				url: appConfig.ApiURL+"/feblog/tag/"+tagsslugs+'/'+offset+"/"+page,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		authorlist: function(offset, page, authorid, callback){
			$http({
				url: appConfig.ApiURL+"/author/news/"+offset+'/'+page+"/"+authorid,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			})
		},
		
		
	}
})