app.factory('Centerinfo', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
			callback("This is a Test");
		},
		listcenter: function(num, off, callback){
			$http({
				url: appConfig.ApiURL +"/fecenters/list/" + num + '/' + off,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		optioncenter: function(callback){
			$http({
				url: appConfig.ApiURL +"/option/center",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		sendsched: function(sendsched,callback)
		{
			$http({
				url: appConfig.ApiURL+"/fe/getstarted/schedule",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(sendsched)

			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		sendrequestinfo: function(sendrequestinfo,callback){
			$http({
				url: appConfig.ApiURL+"/fe/getstarted/request",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(sendrequestinfo)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})