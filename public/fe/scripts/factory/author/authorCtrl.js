app.factory('authorFact', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
			callback("This is a Test");
		},
		aboutauthor: function(authorid, callback){
			$http({
				url: appConfig.ApiURL+"/manage/authoredit/"+ authorid,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		authornews: function(num, off, keyword ,authorid){
			$http({
				url: appConfig.ApiURL +"/author/news/" + num + '/' + off + '/' + keyword + '/' + authorid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		
	}
})