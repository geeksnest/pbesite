app.factory('Eventinfo', function($http, $q, appConfig){
	return {
		data: {},
		getpathfromlist:'',
		test: function( callback){
			callback("This is a Test");
		},
		listevent: function(num, off, keyword, callback){
			$http({
				url: appConfig.ApiURL +"/globalevent/list/" + num + '/' + off + '/' + keyword ,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})