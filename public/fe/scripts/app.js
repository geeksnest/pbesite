'use strict';
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'mwl.calendar',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'app.controllers',
    'angularMoment',
    'uiGmapgoogle-maps',
    'ngSanitize',
    'ui.calendar',
    'auth0', 
    'angular-storage', 
    'angular-jwt',
    'ngFileUpload'
    ])

.run(['$rootScope', '$state', '$stateParams','auth', function ($rootScope,   $state,   $stateParams,auth) {
     // This hooks al auth events to check everything as soon as the app starts
  auth.hookEvents();
  
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;        
}])
.config(
  ['uiGmapGoogleMapApiProvider','$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' , 'authProvider',
  function (uiGmapGoogleMapApiProvider, $stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider, authProvider ) {
     $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  

  //    authProvider.init({
  //   domain: 'YOUR_NAMESPACE',
  //   clientID: 'YOUR_CLIENT_ID'
  // });

  //   uiGmapGoogleMapApiProvider.configure({
  //       //    key: 'your api key',
  //       v: '3.17',
  //       libraries: 'weather,geometry,visualization'
  //   });

  //    app.controller = $controllerProvider.register;
  //    app.directive  = $compileProvider.directive;
  //    app.factory    = $provide.factory;
  //    app.constant   = $provide.constant;
  //    app.value      = $provide.value;

   

  //  $stateProvider
  //   // .state('success', {
  //   //     url: "/success",
  //   //     templateUrl: "/registration/success"
  //   // });
  // .state('elearning', {
  //   url: '/elearning',    
  //   templateUrl: '../elearning/class',
  
  // });

 

   

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.resize.js',
    '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.spline.js',
    '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
    '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
    '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
    '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
    '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
    '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
    '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
    '/be/js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
    '/be/js/jquery/select2/select2-bootstrap.css',
    '/be/js/jquery/select2/select2.min.js',
    '/be/js/modules/ui-select2.js']
}
)
;
