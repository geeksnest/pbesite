
app.controller('ContactCtrl',  function($scope, $http, $state,appConfig, Centerinfo){

	Centerinfo.optioncenter(function(data){
		$scope.optioncenter = data;
	});
	$scope.reset = function(){
		$scope.jb = "";
	    $scope.message.$setPristine(true);
	}

	$scope.sendrequestinfo = function(sendrequestinfo){
		Centerinfo.sendrequestinfo(sendrequestinfo,function(data){
			console.log(data);

           	$scope.jb = "";
           	$scope.message.$setPristine(true);
           	$scope.notify = true;
		});
	}

	$scope.sendsched = function(sendsched){
		Centerinfo.sendsched(sendsched,function(data){
           	$scope.jb = "";
           	$scope.message.$setPristine(true);
           	$scope.notify = true;
		});
	}
});
