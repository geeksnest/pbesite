
app.controller('viewcalendarCtrl', function($scope, $http, $parse, $modal, $state,appConfig, moment,$compile,uiCalendarConfig){

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  /* event source that pulls from google.com */
  $scope.eventSource = {
    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
          };

          /* event source that contains custom events on the scope */

          $http({
            url: appConfig.ApiURL+"/fecalendar/manageactivity/"+ centerid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {

            console.log(data);
            
            var events = [];
            $scope.calendarView = 'month';
            $scope.calendarDay = new Date();
            function addDays(theDate, days) {
              return new Date(theDate.getTime() + days*24*60*60*1000);
            }


            angular.forEach(data, function(value, key) {

             var _bg ='';
             if(data[key].centerid=='global'){
               _bg ='bg-success bg';
             }else{
              switch(data[key].centername) {
                case 'Fairfax':
                _bg ='bg-primary bg';
                break;
                case 'Bayside':
                _bg ='bg-warning bg';
                break;
                case 'Mesa':
                _bg ='bg-dark bg';
                break;
                default: _bg ='bg-success bg';
              }
            }


            $scope.events.push({
              title: data[key].title,
              start:  moment(data[key].datef),
              end: moment(data[key].datet).add('days', 1),
              className: [_bg], 
              location:data[key].loc, 
              info: data[key].info,
              timestart: data[key].mytime,
              endtime: data[key].mytimend

            });
          });

            console.log($scope.events);
          }).error(function (data, status, headers, config){
            var events = [];
          });
          $scope.events = [];

          /* alert on eventClick */
          $scope.alertOnEventClick = function( event, jsEvent, view ){
           
          };
          /* alert on Drop */
          $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
           $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
         };
         /* alert on Resize */
         $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
           $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
         };

         $scope.overlay = $('.fc-overlay');
         $scope.alertOnMouseOver = function( event, jsEvent, view ){
          $scope.event = event;
          $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
          var wrap = $(jsEvent.target).closest('.fc-event');
          var cal = wrap.closest('.calendar');
          var left = wrap.offset().left - cal.offset().left;
          var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
          if( right > $scope.overlay.width() ) { 
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
          }else if ( left > $scope.overlay.width() ) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
          }else{
            $scope.overlay.find('.arrow').addClass('top');
          }
          (wrap.find('.fc-overlay').length == 0) && wrap.append( $scope.overlay );
        }

        /* config object */
        $scope.uiConfig = {
          calendar:{
            height: "auto",
            editable: true,
            header:{
              left: 'prev',
              center: 'title',
              right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventMouseover: $scope.alertOnMouseOver,
            eventStartEditable : false,
          }
        };
        
        /* add custom event*/
        $scope.addEvent = function() {
          $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
          });
        };

        /* remove event */
        $scope.remove = function(index) {
          $scope.events.splice(index,1);
        };

        /* Change View */
        $scope.changeView = function(view,calendar) {
          uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
        };
        /* Change View */
        $scope.renderCalender = function(calendar) {
          if(uiCalendarConfig.calendars[calendar]){
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
          }
        };

        /* event sources array*/
        $scope.eventSources = [$scope.events];



      });
