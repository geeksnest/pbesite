
app.controller('viewcalendarCtrl', ['$scope','$http', '$parse', '$modal', '$state', 'appConfig','moment', function($scope, $http, $parse, $modal, $state,appConfig, moment){

  //These variables MUST be set as a minimum for the calendar to work

console.log();
  $http({
    url: appConfig.ApiURL+"/fecalendar/manageactivity/"+ centerid,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {

   var events = [];
   function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
  }
  angular.forEach(data, function(value, key) {
    $scope.events.push({
      title: data[key].title,
      type: data[key].type,
      startsAt:  data[key].datef,
      endsAt: addDays(new Date(data[key].datet), +1),
      eventid: data[key].eventid,


    });

  });
}).error(function (data, status, headers, config){

});

$scope.calendarView = 'month';
$scope.calendarDay = new Date();
//  $scope.events = [
//  {
//   title: 'An event',
//   type: 'warning',
//   startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
//   endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate()
// }, {
//   title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
//   type: 'info',
//   startsAt: moment().subtract(1, 'day').toDate(),
//   endsAt: moment().add(5, 'days').toDate()
// }, {
//   title: 'This is a really long event title that occurs on every year',
//   type: 'important',
//   startsAt: moment().startOf('day').add(7, 'hours').toDate(),
//   endsAt: moment().startOf('day').add(19, 'hours').toDate(),
//   recursOn: 'year'
// }
// ];
// console.log($scope.events);




$scope.eventClicked = function(eventid){
  var modalInstance = $modal.open({
    templateUrl: 'eventfe.html',
    controller:eventCTRL,
    resolve: {
      eventid: function() {
        return eventid
      },
    }
  });
};

var eventCTRL = function( $modalInstance,$scope, $state ,$q, $http, appConfig, $stateParams , eventid){
 $http({
  url: appConfig.ApiURL  + "/calendar/editactivity/" + eventid ,
  method: "GET",
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
}).success(function (data, status, headers, config) {
  console.log(data);
  $scope.data = data;
}).error(function (data, status, headers, config) {
  $scope.status = status;
});



$scope.cancel = function() {
  $modalInstance.dismiss('cancel');

};

};

$scope.eventEdited = function(event) {
  showModal('Edited', event);
};

$scope.eventDeleted = function(event) {
  showModal('Deleted', event);
};

$scope.eventDropped = function(event) {
  showModal('Dropped', event);
};

$scope.toggle = function($event, field, event) {
  $event.preventDefault();
  $event.stopPropagation();
  event[field] = !event[field];
};



}]);
