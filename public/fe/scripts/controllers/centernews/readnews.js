
app.controller('Readnews', function($scope , $http ,$sce, appConfig, NewsRead) {


	$scope.video = false;
	$scope.banner = false;
	NewsRead.readnews(slugnews, function(data){
	    data.data.title = data.data.title.replace(/\\/g, '');

		$scope.news = data.data;
		$scope.tags = data.tags;
		$scope.dbcategory = data.category;
		$scope.metatags = data.metatags;

		function createStringByArray(array) {
			var output = '';
			angular.forEach(array, function (object) {
				angular.forEach(object, function (value, key) {
					output += key + ',';
					output += value + ',';
				});
			});
			return output;
		}

		$scope.metakeywords = createStringByArray($scope.metatags);

		var dbdata = data.data;
		
		if(dbdata.featuredoption=="video"){
			$scope.video = true;
			$scope.banner = false;
			$scope.embed=$sce.trustAsHtml(dbdata.feat.replace('width="420"', ""));	
		}else{
			$scope.video = false;
			$scope.banner = true;
		}
	});
 });



app.controller('ListNewsbyCategory', function($scope , $http ,$sce, appConfig, NewsRead) {
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	$scope.currentstatusshow = '';
	var request = 	function(num, off, keyword ,category){
	$http({
			url: appConfig.ApiURL +"/list/categ/" + num + '/' + off + '/' + keyword + '/' + category,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {

			console.log(data);

			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;

		}).error(function (data, status, headers, config) {
			callback(data);
		});
	};

	$scope.search = function (keyword) {
		var off = 0;
		request(10,'1', keyword,category);
	}
	$scope.numpages = function (off, keyword) {
		request(10, '1', keyword,category);
	}

	$scope.setPage = function (pageNo) {
		request(num,pageNo, keyword,category);
	};
	var loadpageafterstatus = function(){
		request(num,$scope.bigCurrentPage, keyword,category);
		$scope.currentstatusshow = 0;
	}
	var loadpage = function(){
		request(num,$scope.bigCurrentPage, $scope.searchtext,category);
	}
	loadpage();
	
 });
app.controller('ListNewsbyTags', function($scope , $http ,$sce, appConfig, NewsRead) {
	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	$scope.currentstatusshow = '';
	var request = 	function(num, off, keyword ,tags){
	$http({
			url: appConfig.ApiURL +"/list/tags/" + num + '/' + off + '/' + keyword + '/' + tags,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;

		}).error(function (data, status, headers, config) {
			callback(data);
		});
	};

	$scope.search = function (keyword) {
		var off = 0;
		request(10,'1', keyword,tags);
	}
	$scope.numpages = function (off, keyword) {
		request(10, '1', keyword,tags);
	}

	$scope.setPage = function (pageNo) {
		request(num,pageNo, keyword,tags);
	};
	var loadpageafterstatus = function(){
		request(num,$scope.bigCurrentPage, keyword,tags);
		$scope.currentstatusshow = 0;
	}
	var loadpage = function(){
		request(num,$scope.bigCurrentPage, $scope.searchtext,tags);
	}
	loadpage();
	
 });
