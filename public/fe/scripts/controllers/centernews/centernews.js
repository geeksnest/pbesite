
app.controller('NewsCtrl', ['$scope', '$http','appConfig', function($scope , $http , appConfig) {
	var returnYoutubeThumb = function(item){
		var x= '';
		var thumb = {};
		if(item){
			var newdata = item;
			var x;
			x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
			thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
			thumb['yid'] = x[1];
			return thumb;
		}else{
			return x;
		}
	}

	$scope.data = {};
	var num = 10;
	var off = 1;
	var keyword = null;
	$scope.currentstatusshow = '';


	var request = 	function(num, off, keyword , centerid){
		$http({
			url: appConfig.ApiURL +"/fe/news/" + num + '/' + off + '/' + keyword + '/' + centerid,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data.data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
			angular.forEach(data.data, function(value, key) {
				if(value.featuredoption == 'video'){
					var newd = returnYoutubeThumb(value.featured);
					data.data[key].banner = newd.url;
				}else{
					data.data[key].banner = value.featured;
				}
			})
			console.log(data.data);
		}).error(function (data, status, headers, config) {
			callback(data);
		});
	};

	$scope.search = function (keyword) {
		var off = 0;
		request(10,'1', keyword, centerid, "719C4078-F7EC-4389-8A4F-294461851818");
	}

	$scope.numpages = function (off, keyword) {
		request(10, '1', keyword, centerid, "719C4078-F7EC-4389-8A4F-294461851818");
	}

	$scope.setPage = function (pageNo) {
		request(num,pageNo, keyword, centerid, "719C4078-F7EC-4389-8A4F-294461851818");
	};
	var loadpageafterstatus = function(){
		request(num,$scope.bigCurrentPage, keyword, centerid, "719C4078-F7EC-4389-8A4F-294461851818");
		$scope.currentstatusshow = 0;
	}
	var loadpage = function(){
		request(num,$scope.bigCurrentPage, $scope.searchtext, centerid, "719C4078-F7EC-4389-8A4F-294461851818");
	}

	loadpage();
}]);


app.controller('ScheduleCtrl',function($scope , $http , appConfig ,$modal,$location, $anchorScroll) {
	$scope.modal = function(size){
		var modalInstance = $modal.open({
			templateUrl: 'feschedule.html',
			controller: showCTRL,
			size: size,
			
		});
	}
	
	 $scope.mapscroll = function() {
      // set the location.hash to the id of
      // the element you wish to scroll to. 
      $location.hash('map');
      // call $anchorScroll()
      $anchorScroll();
    };
	var showCTRL = function( $modalInstance,$scope, $state,$q, $http ){

		$http({
			url: appConfig.ApiURL + "/get/schedule/"+centerid,
			method: "get",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.sched = data;

		}).error(function (data, status, headers, config) {
			callback(data);
		});



		$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};


	}


});
