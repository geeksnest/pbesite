
app.controller('AccountCtrl',  function($scope, $http,$sce, $state,$location, $window,  appConfig ,store, jwtHelper, Euser, Upload){

	var _tp 	  = jwtHelper.decodeToken(store.get('setdata')); // tokenPayload
	console.log(_tp);
	console.log(_tp);
	$scope.usrid  	= _tp.id;
	$scope.usrname 	= _tp.username 	 	!= "" ? _tp.username : "";
	$scope.fname 	= _tp.firstname  	!= "" ? _tp.firstname : "";
	$scope.lname 	= _tp.lastname 		!= "" ? _tp.lastname : "";
	var _date 		= _tp.birthday.split("-");
	$scope.byear 	= _tp.birthday		!= "" ? _date[0] : "";
	$scope.bmonth	= _tp.birthday		!= "" ? _date[1] : "";
	$scope.bday     = _tp.birthday		!= "" ? _date[2] : "";
	$scope.gender   = _tp.gender 		!= "" ? _tp.gender : "";
	console.log(_tp.school);
	$scope.school   = _tp.school 		!= "" ? _tp.school : "";
	$scope.grade    = _tp.grade 		!= "" ? _tp.grade : "";
	$scope.userprof    = _tp.profile 		!= "" ? _tp.profile : "4c79ivvlsor1448586875528.jpg";

	console.log($scope.userprof);

	Euser.listSchool(function(_rdata){
		$scope.schoolList = _rdata
	});



	$scope.imageloader=false;
	$scope.imagecontent=true;
	$scope.noimage = false;

	$scope.upload = function(files) {
		$scope.upload(files);  
	};
	$scope.upload = function (files) {
		var filename
		var filecount = 0;
		if (files && files.length){
			$scope.imageloader=true;
			$scope.imagecontent=false;
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				if (file.size >= 20000000){
					$scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
					filecount = filecount + 1;
					if(filecount == files.length){
						$scope.imageloader=false;
						$scope.imagecontent=true;
					}
				}
				else{
					var promises;
					var fileExtension = '.' + file.name.split('.').pop();
					var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

					promises = Upload.upload({
						url:appConfig.amazonlink, 
						method: 'POST',
						transformRequest: function (data, headersGetter) {
							var headers = headersGetter();
							delete headers['Authorization'];
							return data;
						},
						fields : {
							key: 'uploads/banner/' + renamedFile, 
							AWSAccessKeyId: appConfig.AWSAccessKeyId,
							acl: 'private',
							policy: appConfig.policy, 
							signature: appConfig.signature,
							"Content-Type": file.type != '' ? file.type : 'application/octet-stream'
						},
						file: file
					})
					promises.then(function(data){
						filecount = filecount + 1;
						filename = data.config.file.name;
						var fileout = {
							'imgfilename' : renamedFile
						};
						$http({
							url: appConfig.ApiURL + "/upload/banner",
							method: "POST",
							headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							data: $.param(fileout)
						}).success(function (data, status, headers, config) {
							console.log(data);
							$scope.user.profile = renamedFile;
							// console.log($scope.user.profile);
							if(filecount == files.length){
								$scope.imageloader=false;
								$scope.imagecontent=true;
							}
						}).error(function (data, status, headers, config) {
							$scope.imageloader=false;
							$scope.imagecontent=true;
						});

					});
				}
			}
		}
	};
	$scope.chkusername =function(username){


		Euser.checkusername(username, $scope.usrid, function(data){
			$scope.usrname= data.exists;
			if(data.exists == true){
				$scope.form.$invalid = true;
			}
		});
	};
	$scope.submit = function(_gdata){
		Euser.accupdate(_gdata, function(_rdata){
			// console.log(_rdata.data.token);
			if(!_rdata.error){
				store.remove('setdata');
				store.set('setdata', _rdata.data.token);
				// $window.location.href= "../../../elearning/send";
				var tp = jwtHelper.decodeToken(_rdata.data.token);
				$http({
					url: appConfig.ApiURL +"/euser/login/"+tp.id,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					console.log(data);
				})


				$http({
					url: appConfig.BaseURL +"/elearning/session/"+tp.id,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$window.location.href = '../elearning/home';
				})


			
			}else{
				console.log("Something Went Wrong");
			}
		})
	}


});
