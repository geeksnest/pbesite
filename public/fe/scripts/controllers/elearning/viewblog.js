
app.controller('BlogCtrl',  function($scope, $http,$sce, $state,appConfig,Blog, Training,Egallery,  store, jwtHelper){
	var _getstore = store.get('setdata');
	var tokenPayload = jwtHelper.decodeToken(_getstore);
	var usrid = tokenPayload.id;
	var _eloadblog = function(){
		Blog.eloadblog(function(_rdata){
			angular.forEach(_rdata, function(value, key) {
				// value.featured = $sce.trustAsHtml(value.featured);
				var _dot='';
				if(_rdata[key].description.length > 300){
					_dot = '...'
				}
				_rdata[key].title = _rdata[key].title.replace(/\\/g,'');
				_rdata[key].description = _rdata[key].description.substring(0, 300)+" "+_dot;
			});
			$scope.eblog = _rdata;
		});
	}
	_eloadblog();


	Training.lastvisit(usrid,function(_rdata){
		var _img = "";
		if(_rdata.error){
			$scope.currentProgress = false;
			$scope.currentProg = true;
			_img = '<iframe src="https://player.vimeo.com/video/141194525" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}else{
			$scope.currentProgress = true;
			_img = _rdata[0].vimeo;
			$scope.currentProg = false;
		}
		$scope.lv = false;
		if(!_rdata.error){
			$scope.pagevisit = _rdata;
			$scope.lv = true;
		}else{
			$scope.lv = false;
		}
		var returnVimeoThumb = function(item){
			var x= '';
			var thumb = {};
			if(item){
				var newdata = item;
				var x;
				x = newdata.match(/src="https:\/\/player.vimeo.com\/video\/(.*?)"+/);
				$http({
						url: 'http://vimeo.com/api/v2/video/'+ x[1] +'.json',
						method: "GET",
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					}).success(function (data, status, headers, config) {
						$scope.imgvemio = data[0].thumbnail_large;
						$scope.noProg = data[0].thumbnail_large;
						console.log($scope.noProg);
				})
			}else{
				return x;
			}
		}
		returnVimeoThumb(_img);
	});



	var loadimage = function() {
    Egallery.loadimage(function(_rdata){
        console.log(_rdata);
        $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
        $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
        console.log(_rdata);
        $scope.imagelist   = _rdata
      });
    }
    loadimage();





    
});
