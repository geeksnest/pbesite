
app.controller('VerifyCtrl',  function($scope, $http,$sce, $state,$location,$window, appConfig ,store, jwtHelper, Euser){
	var _getstore = store.get('setdata');


	$(function() {
		var progressbar = $( "#progressbar" ),
		progressLabel = $( ".progress-label" );
		progressbar.progressbar({
			value: false,
			change: function() {
				progressLabel.text( progressbar.progressbar( "value" ) + "%" );
			},
			complete: function() {
				Euser.accverify(userid, code, function(_rdata){

					console.log(_rdata);

					if(!_rdata.error){
						$scope.error = false;
						var _token = _rdata[0].token;
						store.set('setdata', _token);
						$window.location.href= "../../../elearning/account"; 
					}else{
						$scope.error = true;
					}
				});
			}
		});
		function progress() {
			var val = progressbar.progressbar( "value" ) || 0;

			progressbar.progressbar( "value", val + 2 );

			if ( val < 99 ) {
				setTimeout( progress, 80 );
			}
		}
		setTimeout( progress, 3000 );
	});


});
