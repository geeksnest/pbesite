
app.controller('trainingCtrl',  function($scope, $http, $state,appConfig,Tmedia, Training ,store, jwtHelper){
	var _idno='';
	var _getstore = store.get('setdata');
	//Decoding the token
	var tokenPayload = jwtHelper.decodeToken(_getstore);
	var usrid = tokenPayload.id;
	$scope.uid = usrid;

	Training.loadtraining(usrid, tokenPayload.task, _state, function(_rdata){
		$scope.traininglist = _rdata;
	});
});
