
app.controller('BlogarchieveCtrl',  function($scope, $http,$sce, $state,appConfig,Blog){
	// var _loadblog = function(){
	// 	Blog.editblog($stateParams.id ,function(_rdata){
	// 		$scope.gdata = _rdata;

	// 	});
	// }
	// _loadblog();
	var returnYoutubeThumb = function(item){
		var x= '';
		var thumb = {};
		if(item){
			var newdata = item;
			var x;
			x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
			thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
			thumb['yid'] = x[1];
			return thumb;
		}else{
			return x;
		}
	}

	var returnVimeoThumb = function(item){
		var x= '';
		var thumb = {};
		if(item){
			var newdata = item;
			var x;
			x = newdata.match(/src="https:\/\/player.vimeo.com\/video\/(.*?)"+/);
			thumb['url'] ="http://vimeo.com/api/v2/video/" + x[1] + ".json?callback=showThumb";  
			thumb['yid'] = x[1];
			return thumb;
		}else{
			return x;
		}
	}

		//player.vimeo.com/video/

	$scope.limit = 5;

	var _loadblog = function(){
		Blog.loadarchieveblog(publish,function(_rdata){
			angular.forEach(_rdata, function(value, key) {
					_rdata[key].title = _rdata[key].title.replace(/\\/g, '')
				if(value.featuredtype == 'video'){
					if(value.featured.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/)){
						var newd = returnYoutubeThumb(value.featured);
						_rdata[key].videourl = newd.url;
						_rdata[key].youtubeid = newd.yid;
						_rdata[key].newtype = 'youtube';
					}else{
						var newd = returnVimeoThumb(value.featured);
						$.ajax({
							type:'GET',
							url: 'http://vimeo.com/api/v2/video/' + newd.yid + '.json',
							jsonp: 'callback',
							dataType: 'jsonp',
							success: function(data){
								var id_img = ".vimeo-" + data[0].id;
								$(id_img).attr('src',data[0].thumbnail_medium);
							}
						});
						$scope.vimeoid = newd.yid;
						console.log(newd.yid);
						_rdata[key].newtype = 'vimeo';
					}
					
				}else{
					_rdata[key].newtype = 'banner';
				}
			});
			$scope.loading = false;
			if($scope.limit>=_rdata.length){
				$scope.hideloadmore = true;
			}else{
				$scope.hideloadmore = false;
			}
			console.log(_rdata);
			$scope.blog = _rdata;
		});
	}
	_loadblog();

	var limitStep = 5;

	$scope.showmoreblogs = function() {
    	$scope.loading = true;
    	_loadblog();
    	$scope.limit += limitStep;
    };

});
