
app.controller('AuthorCtrl',  function($scope , $http , appConfig,authorFact) {
	authorFact.aboutauthor(authorid, function(data){
		console.log(data);
		$scope.author =data;
	});
});
app.controller('NewsCtrl', function($scope, $http,$sce, $state,appConfig,Blog){
	var returnYoutubeThumb = function(item){
		var x= '';
		var thumb = {};
		if(item){
			var newdata = item;
			var x;
			x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
			thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
			thumb['yid'] = x[1];
			return thumb;
		}else{
			return x;
		}
	}
	var returnVimeoThumb = function(item){
		var x= '';
		var thumb = {};
		if(item){
			var newdata = item;
			var x;
			x = newdata.match(/src="https:\/\/player.vimeo.com\/video\/(.*?)"+/);
			thumb['url'] ="http://vimeo.com/api/v2/video/" + x[1] + ".json?callback=showThumb";  
			thumb['yid'] = x[1];
			return thumb;
		}else{
			return x;
		}
	}
	var offset = 0;
	var page = 10;
	var list = [];
	$scope.loading = false;
	$scope.newslist = [];
	$scope.hideloadmore = false;
	$scope.showmorelist = function(){
		console.log('showmore');
		$scope.loading = true;
		Blog.authorlist(offset, page, authorid,  function(_rdata){
			angular.forEach(_rdata, function(value, key) {
				if(value.featuredtype == 'video'){
					console.log(value.featured);
					if(value.featured.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/)){
						var newd = returnYoutubeThumb(value.featured);
						_rdata[key].videourl = newd.url;
						_rdata[key].youtubeid = newd.yid;
						_rdata[key].newtype = 'youtube';
					}else{
						var newd = returnVimeoThumb(value.featured);
						$.ajax({
							type:'GET',
							url: 'http://vimeo.com/api/v2/video/' + newd.yid + '.json',
							jsonp: 'callback',
							dataType: 'jsonp',
							success: function(data){
								var id_img = ".vimeo-" + data[0].id;
								$(id_img).attr('src',data[0].thumbnail_medium);
							}
						});
						$scope.vimeoid = newd.yid;
							//console.log(newd.yid);
							_rdata[key].newtype = 'vimeo';
						}
					}else{
						_rdata[key].newtype = 'banner';
					}
					var _dot='';
					if(_rdata[key].description.length > 260){
						_dot = '...'
					}
					_rdata[key].description = _rdata[key].description.substring(0, 260)+" "+_dot;
				});
			console.log(_rdata);
			list = list.concat(_rdata);
			offset = offset + page;
			page = page + page;
			$scope.loading = false;
			console.log(list);
			$scope.blog = list;
			if(_rdata.length < 10){
				$scope.hideloadmore = true;
			}
		});
	}



	
	
});

