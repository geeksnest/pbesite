
app.controller('LoginCtrl',  function($scope, $http, $state,$window, $timeout, appConfig, jwtHelper, store){
	$scope.error = false;
	$scope.logmein = function(_gdata){
		var username = _gdata.username;
		var password = _gdata.password;

		$http({
			url: appConfig.ApiURL +"/euser/elogin/" + username + '/' + password,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {

			if(data[0].error){
				$scope.error = true;
			}else{
				$scope.error = false;
				var _token = data[0].token;
				store.set('setdata', _token);
				var tp = jwtHelper.decodeToken(_token);
				$http({
					url: appConfig.ApiURL +"/euser/login/"+tp.id,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					console.log(data);
				})

				$http({
					url: appConfig.BaseURL +"/elearning/session/"+tp.id,
					method: "GET",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function (data, status, headers, config) {
					$window.location.href = '../elearning/home';
				})
			
			}

		})
	}
});
