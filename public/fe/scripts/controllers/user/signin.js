
app.controller('LoginCtrl',  function($scope, $http, $state,$window, $timeout, appConfig, jwtHelper, store){


	$scope.error = false;
	$scope.logmein = function(_gdata){
		var username = _gdata.username;
		var password = _gdata.password;
		$http({
				url: appConfig.ApiURL +"/user/signin/" + username + '/' + password,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				if(data[0].error){
					$scope.error = true;
				}else{
					$scope.error = false;

					var _token = data[0].token;
					//Decoding the token
					var tokenPayload = jwtHelper.decodeToken(_token);
					
					$window.location.href = '../elearning/register/'+tokenPayload.id+'/'+tokenPayload.code+'';
				}

			}).error(function (data, status, headers, config) {
				console.log(data);
			});
	}
});
