
app.controller('UserinfoCtrl',  function($scope, $http, $state,$window, appConfig, jwtHelper, store){



	var _getstore = store.get('setdata');

	if(_getstore==null){$window.location.href = '../elearning/login';}
	//Decoding the token
	var tokenPayload = jwtHelper.decodeToken(_getstore);
	//Getting the token expiration date
	var exdate = jwtHelper.getTokenExpirationDate(_getstore);
	//Checking if token is expired (true/false)
	var bool = jwtHelper.isTokenExpired(_getstore);
	//STORE GET DATA END HERE
	$scope.usrname = tokenPayload.firstname+' '+tokenPayload.lastname;

	if(tokenPayload.task=="teacher"){
		$scope._mentors = false;
		$scope._leades = false;
	}else if(tokenPayload.task=="mentor"){
		$scope._mentors = true;
		$scope._leades = false;
	}else if(tokenPayload.task=="leader"){
		$scope._mentors = false;
		$scope._leades = true;
	}
	$scope.usrid = tokenPayload.id;

	$scope.logout = function(){
		store.remove('setdata');
		var backlen = history.length;
		history.go(-backlen);

		$http({
			url: appConfig.BaseURL +"/elearning/logout",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$window.location.href = '/../elearning/login'
		})


	}
});
