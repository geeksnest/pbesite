'use strict';
app.controller('EgalleryvideoCTRL', function($scope, $state ,$q, $http, appConfig, $modal,$sce, Egallery, Upload, jwtHelper, store){
  var _getstore = store.get('setdata');

  var tokenPayload = jwtHelper.decodeToken(_getstore);




  var loadimage = function(){
    Egallery.loadVid(tokenPayload.id, function(_rdata){
      console.log(_rdata);
      $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
      $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
      $scope.videlist   = _rdata
    });
  }
  loadimage();

  var returnYoutubeThumb = function(item){
    var x= '';
    var thumb = {};
    if(item){
      var newdata = item;
      var x;
      x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
      thumb['yid'] = x[1];
      return thumb;
    }else{
      return x;
    }
  }
  var returnVimeoThumb = function(item){
    var x= '';
    var thumb = {};
    if(item){
      var newdata = item;
      var x;
      x = newdata.match(/src="https:\/\/player.vimeo.com\/video\/(.*?)"+/);
      thumb['url'] ="http://vimeo.com/api/v2/video/" + x[1] + ".json?callback=showThumb";  
      thumb['yid'] = x[1];
      return thumb;
    }else{
      return x;
    }
  }

  $scope.addVid = function(){
    // var modalInstance = $modal.open({
    //   templateUrl: 'eVidsave.html',
    //   controller: saveimgCTRL
    // });

    var modalInstance = $modal.open({
        templateUrl: 'eVidsave.html',controller: saveimgeCTRL,
        resolve: {imgid: function() {return "id"}}
      });

  }
  var saveimgeCTRL = function($scope, $modalInstance, appConfig) {
      $scope.UsErid = tokenPayload.id;
    $scope.emded = function(_gdata){
      console.log(_gdata);
      $scope.showimg =false;
      if(_gdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/)){
        var newd = returnYoutubeThumb(_gdata);
        $scope.showimg =true;
        $scope.imgfilename = newd.url;
      }else{
        var newd = returnVimeoThumb(_gdata);
        $.ajax({
          type:'GET',
          url: 'http://vimeo.com/api/v2/video/' + newd.yid + '.json',
          jsonp: 'callback',
          dataType: 'jsonp',
          success: function(data){
            $scope.showimg =true;
            $scope.imgfilename = data[0].thumbnail_large;
          }
        });
      }
    }


    $scope.imgSave = function(_gdata){

      Egallery.savevid(_gdata, function(data, rdata){
        if(data[0].success){
         loadimage();
         $modalInstance.dismiss('cancel');
       }else{
         loadimage();
         $modalInstance.dismiss('cancel');
       }
     });


    }
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }  



  $scope.edit = function(id, img, title, description, embed ){
       var modalInstance = $modal.open({
                templateUrl: 'eVidupdate.html',
                controller: saveimgCTRL,
                resolve:{
                  _data: function(){
                    return {id:id,image:img,title:title,desc:description,embed:embed}
                  }
                }
              });
    }
    var saveimgCTRL = function($scope, $modalInstance, _data,appConfig) {
      console.log(_data);
      $scope.vid = _data;
      $scope.imgSave = function(_gdata){
        console.log(_gdata);
        Egallery.updatevideo(_gdata, function(data, rdata){
                loadimage();
                if(data[0].success){
                    $modalInstance.dismiss('cancel');
                }else{
                      $modalInstance.dismiss('cancel');
                }
        });
      }
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }


  $scope.delete = function(id){
      var modalInstance = $modal.open({
        templateUrl: 'delete.html',controller: deleteCTRL,
        resolve: {imgid: function() {return id}}
      });
    }
    var deleteCTRL = function($scope, $modalInstance, imgid) {

      console.log(imgid);
      $scope.alerts = [];
      $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
      $scope.message="Are you sure do you want to delete this Video?";
      $scope.ok = function() {
        Egallery.deletevideo(imgid, function(_rdata){
          loadimage();
          $modalInstance.close();
        });
      };
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    }
});

