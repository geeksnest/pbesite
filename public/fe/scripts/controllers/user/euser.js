
app.controller('EuserinfoCtrl',  function($scope, $http, $state,$window, appConfig, jwtHelper, store, Euser, Upload){

	var _tp = jwtHelper.decodeToken(store.get('setdata'));
	$scope.usrid  = _tp.id;
	Euser.getinfo(_tp.id, function(_rdata){
		$scope.uname 	= _rdata.fname+' '+_rdata.lname;
		$scope.gender 	= _rdata.gender;
		$scope.bday 	= _rdata.bday;
		$scope.email 	= _rdata.email;
		$scope.username = _rdata.username;
		if(_prof==null || _prof== undefined ){
			$scope.user.username = _rdata.username;
			$scope.user.fname 	= _rdata.fname;
			$scope.user.lname 	= _rdata.lname;
			$scope.user.email 	= _rdata.email;
			$scope.user.gender  = _rdata.gender;
			$scope.user.task   	= _rdata.userrole;
			$scope.user.status  = _rdata.status;
			$scope.user.banner  = _rdata.banner;
			var _date 			= _rdata.bday.split("-");
			$scope.user.year 	= _date[0];
			$scope.user.month	= _date[1];
			$scope.user.day     = _date[2] ;
		}
	});

	$scope.userprof = "4c79ivvlsor1448586875528.jpg";
	
	$scope.imageloader=false;
	$scope.imagecontent=true;
	$scope.noimage = false;
	$scope.upload = function(files) {
		$scope.upload(files);  
	}
	$scope.upload = function (files) {
		var filename
		var filecount = 0;
		if (files && files.length){
			$scope.imageloader=true;
			$scope.imagecontent=false;
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				if (file.size >= 20000000){
					$scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
					filecount = filecount + 1;
					if(filecount == files.length){
						$scope.imageloader=false;
						$scope.imagecontent=true;
					}
				}
				else{
					var promises;
					var fileExtension = '.' + file.name.split('.').pop();
					var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;

					promises = Upload.upload({
						url:appConfig.amazonlink, 
						method: 'POST',
						transformRequest: function (data, headersGetter) {
							var headers = headersGetter();
							delete headers['Authorization'];
							return data;
						},
						fields : {
							key: 'uploads/banner/' + renamedFile, 
							AWSAccessKeyId: appConfig.AWSAccessKeyId,
							acl: 'private',
							policy: appConfig.policy, 
							signature: appConfig.signature,
							"Content-Type": file.type != '' ? file.type : 'application/octet-stream'
						},
						file: file
					})
					promises.then(function(data){
						filecount = filecount + 1;
						filename = data.config.file.name;
						var fileout = {
							'imgfilename' : renamedFile
						};
						
						$http({
							url: appConfig.ApiURL + "/upload/banner",
							method: "POST",
							headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							data: $.param(fileout)
						}).success(function (data, status, headers, config) {
							console.log(data);
							$scope.user.profile = appConfig.amazonlink+"/uploads/banner/"+renamedFile;
							// console.log($scope.user.profile);
							if(filecount == files.length){
								$scope.imageloader=false;
								$scope.imagecontent=true;
							}
						}).error(function (data, status, headers, config) {
							$scope.imageloader=false;
							$scope.imagecontent=true;
						});

					});
				}
			}
		}
	}

	$scope.submit = function(_gdata){
		Euser.usrUpdate(_gdata, function(_rdata){
			if(!_rdata.error){
				store.remove('setdata');
				var _token = _rdata[0].token;
				console.log(jwtHelper.decodeToken(_token));
				store.set('setdata', _token);
				$window.location.reload();
			}else{
				console.log("Something Went Wrong");
			}
		})
	}

	
	
	$scope.chngpass = function(_gdata){
		$scope.alerts = [];

		$scope.closeAlert = function (index) {
			$scope.alerts.splice(index, 1);
		};

		console.log(_gdata)
		$scope.alerts.splice(0, 1);
		if(_gdata.confirm_password != _gdata.password){
		
			$scope.alerts.push({ type: 'danger', msg: ' New Passwrod and Re-enter Password Doesn\'t Match' });
		}else{
			Euser.chngepass(_gdata , function(_rdata){
				if(!_rdata.error){
					$scope.chnge='';
					$scope.alerts.push({ type: 'success', msg: 'Password Succesfully Change' });
				}else{
					$scope.alerts.push({ type: 'danger', msg: 'Ooops Seems Like Your Old Passwrod you enter Mismatch' });
				}
			});
		}


	}
});
