
app.controller('CenterCtrl',  function($scope, $http, $state,appConfig, Centerinfo){

	$scope.data = {};
	var num = 10;
	var off = 1;
	$scope.currentstatusshow = '';
	var request = 	function(num, off ){
		Centerinfo.listcenter(num, off,function(data){
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			$scope.bigCurrentPage = data.index;
			if($scope.bigCurrentPage<11){
				$scope.hideme = false;
			};

		});
	};
	$scope.numpages = function (off) {
		request(10,off);
	}
	$scope.setPage = function (pageNo) {
		request(num,pageNo);
	};
	var loadpage = function(){
		request(num,$scope.bigCurrentPage);
	}
	loadpage();

});
