<?php echo $this->getContent(); ?>



<style type="text/css">
  .bgbanner {
    background:
    radial-gradient(black 15%, transparent 16%) 0 0,
    radial-gradient(black 15%, transparent 16%) 8px 8px,
    radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,
    radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;
    background-color:#282828;
    background-size:16px 16px;
  }
</style>

<script type="text/ng-template" id="message.html">
 "<div ng-include="'/be/tpl/message.html'"></div>"
</script>

<script type="text/ng-template" id="mediaList.html">
  <div ng-include="'/be/tpl/mediabannerList.html'"></div>
</script>

<script type="text/ng-template" id="delete.html">
  "<div ng-include="'/be/tpl/delete.html'"></div>"
</script>

<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Banner</h1>
  <a id="top"></a>
</div>
<br>
<div >
  <tabset  class="tab-container" >
    <tab heading="{[{data.title}]}" ng-repeat="data in _dLsit" >
      <div class="row">
        <div class="col-sm-9 no-border ">
          <div  class="panel panel-primary  ">
            <div class="panel-heading font-bold">
              <div class="m-b-sm pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-dark btn-sm pull-right" ng-click="webview = false; mobileview = true;"><i class="fa fa-mobile"> Mobile </i></button>
                  <button type="button" class="btn btn-dark btn-sm pull-right" ng-click="webview = true; mobileview = false;"><i class="fa fa-laptop"> Full</i></button>
                </div>           
              </div>
              Banner Preview
            </div>
            <div class="panel-body bgbanner">

              <div ng-show="webview" style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}) 0px 0px;max-width: 100%; height: 300px;background-size: cover; background-position:center center; position: relative; padding-top:50px;overflow-x: hidden;" class="file-control">
                <div class="{[{data.locpost}]}" style="background-color:{[{data.bgcolor}]};opacity:{[{"."+data.opacity}]};">
                  <h1 class="grn2" style="text-align:{[{data.titlealign}]}; color:{[{data.titlecolor}]}"><span style="border-bottom: 1px solid {[{data.titlecolor}]};">{[{data.title}]}</span></h1>
                  <p class="t1" style="text-align:{[{data.descalign}]}; color:{[{data.desccolor}]}">{[{data.desc}]}</p>
                </div>
              </div>

              <div ng-show="mobileview" style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}) 0px 0px;max-width: 480px; height: 300px;background-size: cover; background-position:center center; position: relative; padding-top:50px;overflow-x: hidden; margin:auto;" class="file-control">
                <div class="{[{data.locpost}]}" style="background-color:{[{data.bgcolor}]};">
                  <h1 class="grn2" style="text-align:{[{data.titlealign}]}; color:{[{data.titlecolor}]}"><span style="border-bottom: 1px solid {[{data.titlecolor}]};">{[{data.title}]}</span></h1>
                  <p class="t1" style="text-align:{[{data.descalign}]}; color:{[{data.desccolor}]}">{[{data.desc}]}</p>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="panel panel-primary">
            <div class="panel-heading font-bold">Banner Details</div>
            <div class="panel-body">
              <input id="amzon" type="hidden" imageonload  name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/'" ng-model="amazon">
              <label class="text-muted">Banner Image:</label>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-click="mediaGallery($index, amazon, _dLsit)"><i class="fa fa-file-image-o"></i> Media Gallery</button>
              <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}" class="img-full">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
               <div class="m-b-sm pull-right">
                <div class="btn-group">
                  <button type="button" data-ng-class="{'active' : data.titlealign == 'left'}" ng-click="data.titlealign='left'" class="btn btn-dark btn-xs  "><i class="glyphicon glyphicon-align-left"></i></button>
                  <button type="button" data-ng-class="{'active' : data.titlealign == 'center'}" ng-click="data.titlealign='center'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-center"></i></button>
                  <button type="button" data-ng-class="{'active' : data.titlealign == 'right'}" ng-click="data.titlealign='right'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-right"></i></button>
                  <button type="button" data-ng-class="{'active' : data.titlealign == 'justify'}" ng-click="data.titlealign='justify'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-justify"></i></button>
                </div>
                <input type="hidden" ng-model="data.titlecolor">   
                <input type="hidden" ng-model="data.titlealign">              
              </div>
              <label class="text-muted">Banner Title:</label>
              <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Name" ng-model="data.title" required="">
              <label class="text-muted">Banner Title Text Color:</label>
              <div class="input-group m-b ">
                <input colorpicker class="form-control" ng-model="hexPicker.titlecolor" ng-value="hexPicker.titlecolor = data.titlecolor" ng-change="data.titlecolor = hexPicker.titlecolor" type="text">
                <span class="input-group-addon" style="background:{[{data.titlecolor}]}">&nbsp;&nbsp;&nbsp;</span>
              </div>
            </div>
            <div class="form-group">
              <div class="m-b-sm pull-right">
                <div class="btn-group">
                  <button type="button" data-ng-class="{'active' : data.descalign == 'left'}" ng-click="data.descalign='left'" class="btn btn-dark btn-xs  "><i class="glyphicon glyphicon-align-left"></i></button>
                  <button type="button" data-ng-class="{'active' : data.descalign == 'center'}" ng-click="data.descalign='center'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-center"></i></button>
                  <button type="button" data-ng-class="{'active' : data.descalign == 'right'}" ng-click="data.descalign='right'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-right"></i></button>
                  <button type="button" data-ng-class="{'active' : data.descalign == 'justify'}" ng-click="data.descalign='justify'" class="btn btn-dark btn-xs "><i class="glyphicon glyphicon-align-justify"></i></button>
                </div>
                <input type="hidden" ng-model="data.desccolor">   
                <input type="hidden" ng-model="data.descalign">                 
              </div>
              <label class="text-muted">Banner Desciption:</label>
              <textarea class="form-control" rows="6" placeholder="Type your message" ng-model="data.desc"></textarea>
              <label class="text-muted">Banner Desciption Text Color:</label>
              <div class="input-group m-b ">
                <input colorpicker class="form-control" ng-model="hexPicker.desccolor" ng-value="hexPicker.desccolor = data.desccolor" ng-change="data.desccolor = hexPicker.desccolor" type="text">
                <span class="input-group-addon" style="background:{[{data.desccolor}]}">&nbsp;&nbsp;&nbsp;</span>
              </div>
            </div>
            <div class="form-group">
              <label class="text-muted">Position &nbsp;</label>
              <div class="m-b-sm">
                <div class="btn-group">
                  <button type="button" data-ng-class="{'active' : data.locpost == 'binfo-left'}" ng-click="data.locpost='binfo-left';position($index,'binfo-left',_dLsit)" class="btn btn-dark btn-sm ">Left</button>
                  <button type="button" data-ng-class="{'active' : data.locpost == 'binfo-center'}" ng-click="data.locpost='binfo-center';position($index,'binfo-center',_dLsit)" class="btn btn-dark btn-sm ">Middle</button>
                  <button type="button" data-ng-class="{'active' : data.locpost == 'binfo-right'}" ng-click="data.locpost='binfo-right';position($index,'binfo-right', _dLsit)"class="btn btn-dark btn-sm ">Right</button>
                  <input type="hidden" ng-model="data.locpost"  required="">
                </div>              
              </div>
            </div>
            <div class="form-group">
              <label class="text-muted">Banner Information Background:</label>
              <div class="input-group m-b ">
                <input colorpicker="rgba" class="form-control" ng-model="rgbaPicker.bgcolor" ng-value="rgbaPicker.bgcolor = data.bgcolor" ng-change="data.bgcolor = rgbaPicker.bgcolor" type="text">
                <span class="input-group-addon" style="background:{[{data.bgcolor}]}">&nbsp;&nbsp;&nbsp;</span>
              </div>
              <input type="hidden" ng-model="data.bgcolor">      
            </div>

            <div class="form-group">
              <label class="text-muted">Adjust Background Opacity: </label> 
              <input ng-disabled="data.bgcolor==''" id="defaultSlider" type="range" min="0" max="10" ng-model="data.opacity" ng-change="opacity($index, data.opacity,data.bgcolor, _dLsit)" /> 
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>
            <footer class="panel-footer  bg-light lter">
              <alert ng-show="$index == curTab" ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
              <button type="submit" class="btn btn-sm btn-primary pull-right" ng-click="save($index, _dLsit ,{
              title:data.title,
              titleStyle:{color:data.titlecolor,textalign:data.titlealign},
              desc:data.desc,
              descStyle:{color:data.desccolor,textalign:data.descalign},
              position:data.locpost,
              bg:data.bgcolor,
              opcity:data.opacity
            })">Save Changes</button>
            <div style="clear:both;"></div>
          </footer>
        </div>
      </div>
    </div>
  </div>
</tab>
</tabset>
</div>



