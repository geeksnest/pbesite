<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="CenterDelete.html">
  <div ng-include="'/be/tpl/CenterDelete.html'"></div>
</script>

<script type="text/ng-template" id="CenterEdit.html">
  <div ng-include="'/be/tpl/CenterEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Center List</h1>
  <a id="top"></a>
</div>
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Center List
            </div>
              <div class="panel-body">
                <div class="row wrapper">
                    <div class="col-sm-12">
                  <div class="col-sm-6">
                    <form name="rEquired">
                    <div class="input-group">
                      <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext" ng-change="search(searchtext)" required>
                      <span class="input-group-btn">
                        <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                  </div>
                      </span>
                    </div>
                    </form>
                  </div>
                </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                               
                                <th style="width:25%">Title</th>
                                <th style="width:25%">Meta Keywords</th>
                                <th style="width:25%">Center Status</th>
                                <th style="width:25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                               
                                <td>{[{ mem.title }]}</td>
                                <td><p>{[{ mem.metatitle }]}</p></td>
                                <td ng-if="mem.status == 'true'">
                                  <div>
                                  <span class="bg-success btn btn-xs" >Active
                                  </span>
                                  </div>
                                  <div>
                                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                                        <input type="checkbox" 
                                        ng-click="changestatus(mem.centerid,mem.status)" checked>
                                        <i></i>
                                  </label>
                                  </div>
                                </td>
                                <td ng-if="mem.status == 'false'">
                                  <div>
                                  <span class="bg-danger btn btn-xs" >Inactive
                                  </span>
                                  </div>
                                  <div>
                                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                                        <input type="checkbox" 
                                        ng-click="changestatus(mem.centerid,mem.status)">
                                        <i></i>
                                  </label>
                                  </div>
                                </td> 
                                <td>
                                    <a href="" ng-click="editpage(mem.centerid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="deletepage(mem.centerid)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>
  </div>
</fieldset>