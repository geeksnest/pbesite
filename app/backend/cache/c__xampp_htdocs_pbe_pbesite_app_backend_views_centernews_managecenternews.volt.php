<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="edit.html">
  <div ng-include="'/be/tpl/edit.html'"></div>
</script>

<script type="text/ng-template" id="CenterEdit.html">
  <div ng-include="'/be/tpl/CenterEdit.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Center News</h1>
  <a id="top"></a>
</div>
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News List
            </div>
            <div class="panel-body">
              <div class="row wrapper">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <form name="rEQuired">
                    <div class="input-group">
                      <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" ng-change="search(searchtext)" required>
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                      </span>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            </div>
            <div class="col-sm-12">
              <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                     
                      <th style="width:50%">News Title</th>
                      <th style="width:15%">Author</th>
                      <th style="width:20%">Date Publish</th>
                      <th style="width:25%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="mem in data.data">
                     
                      <td>{[{ mem.title }]}</td>
                      <td>{[{ mem.authorname }]}</td>
                      <td>{[{ mem.publish }]}</td>
                      
                      <td>
                        <a href="" ng-click="editpage(mem.newsid)"><span class="label bg-warning" >Edit</span></a>
                        <a href="" ng-click="deletepage(mem.newsid)"> <span class="label bg-danger">Delete</span></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="panel-body">
         <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
       </div>
     </div>
   </div>
 </fieldset>