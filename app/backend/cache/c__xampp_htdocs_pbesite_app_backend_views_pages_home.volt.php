<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="bannerinfo.html">
  <div ng-include="'/be/tpl/bannerinfo.html'"></div>
</script>
<script type="text/ng-template" id="message.html">
  <div ng-include="'/be/tpl/message.html'"></div>
</script>
<script type="text/ng-template" id="mediaList.html">
  <div ng-include="'/be/tpl/mediaList.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  "<div ng-include="'/be/tpl/delete.html'"></div>"
</script>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md" >
  <h1 class="m-n font-thin h3">Home Page</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <input id="amzon" type="hidden" imageonload  name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/'" ng-model="amazon">
  <tabset  class="tab-container" >
    <tab heading="Power Brain Training Center">
      <div class="media" ng-repeat="data in _pbtc ">
        <a class="pull-left  m-t-xs">
          <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}">
        </a>
        <div class="media-body">
          <div class="m-b-sm text-md"><h4>{[{data.title}]}</h4></div>
          <p >{[{data.desc}]}</p>
          <button id="mediaGallery" type="button" class="btn btn-info btn-xs " ng-click="mediaGallery($index, amazon, _pbtc)"><i class="icon-grid"></i> Media</button>
          <button id="bannerInfo" type="button" class="btn btn-info btn-xs "  ng-click="bannerInfo($index,_pbtc)"><i class="icon-pencil"></i> Update Info</button>
          <button id="save" type="submit" class="btn btn-xs btn-primary " ng-click="save($index, _pbtc)">Save Changes</button>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
      </div>
    </tab>
    <tab heading="PB Information">
      <div class="media" ng-repeat="data in _pbi">
       <a class="pull-left  m-t-xs">
          <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}">
        </a>
        <div class="media-body">
          <div class="m-b-sm text-md"><h4>{[{data.title}]}</h4></div>
          <p >{[{data.desc}]}</p>
          <button id="mediaGallery" type="button" class="btn btn-info btn-xs " ng-click="mediaGallery($index, amazon, _pbi)"><i class="icon-grid"></i> Media</button>
          <button id="bannerInfo" type="button" class="btn btn-info btn-xs "  ng-click="bannerInfo($index,_pbi)"><i class="icon-pencil"></i> Update Info</button>
          <button id="save" type="submit" class="btn btn-xs btn-primary " ng-click="save($index, _pbi)">Save Changes</button>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
      </div>
    </tab>
    <tab heading="5 Benefits">
      <div class="media" ng-repeat="data in _fb ">
        <a class="pull-left  m-t-xs">
          <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{data.banner}]}">
        </a>
        <div class="media-body">
          <div class="m-b-sm text-md"><h4>{[{data.title}]}</h4></div>
          <p >{[{data.desc}]}</p>
          <button id="mediaGallery" type="button" class="btn btn-info btn-xs " ng-click="mediaGallery($index, amazon, _fb)"><i class="icon-grid"></i> Media</button>
          <button id="bannerInfo" type="button" class="btn btn-info btn-xs "  ng-click="bannerInfo($index,_fb)"><i class="icon-pencil"></i> Update Info</button>
          <button id="save"type="submit" class="btn btn-xs btn-primary " ng-click="save($index, _fb)">Save Changes</button>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
      </div>
    </tab>
  </tabset>
</div>