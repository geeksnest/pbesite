<?php echo $this->getContent(); ?>
<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Add New Event</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="SaveActivity(act)" name="formevent">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Event Information
            </div>
            <div class="panel-body">
              <!-- FORM CONTENT================== -->
              <div ng-show="option"><!-- IF LOGIN AS CENTER MANAGER================== -->
                <label class="col-sm-2 control-label"><label for="title">Select Where to post Event</label> </label>
                <div class="col-sm-10">
                  <div class="input-group">
                    <label class="i-checks">
                      <input type="radio" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" name="submain" value="global" ng-model="act.adminoption" ng-click="optionshow('center')">
                      <i></i>
                      Center 
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label class="i-checks">
                      <input type="radio" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" name="submain" value="center" ng-model="act.adminoption" ng-click="optionshow('map')">
                      <i></i>
                      Global
                    </label>
                  </div>
                </div>
              </div>
              <div ng-show="center" class="line line-dashed b-b line-lg pull-in"><!--=========================================================--></div>
              <div ng-show="center">
                <label class="col-sm-2 control-label"><label for="title">Select Center</label> </label>
                <div class="col-sm-10">
                  <div ui-module="select2">
                    <select ui-select2 ng-model="act.centerid" class="form-control w-md g-pristine ng-invalid ng-invalid-required" ng-change="slct(act.centerid)">
                      <option ng-value="data.centerid" ng-repeat="data in centers">{[{data.title}]}</option>
                    </select>
                  </div>
                </div>
              </div><!-- END IF LOGIN AS CENTER MANAGER================== -->
              <div ng-show="option" class="line line-dashed b-b line-lg pull-in"><!--=========================================================--></div>

              <div class="{[{formwidth}]}"><!-- CHANGE WIDTH IF SELECTED AS GLOBAL================== -->
                <label class="col-sm-2 control-label"><label for="title">Title </label> </label>
                <div class="col-sm-10">
                  <input  type="hidden"  ng-init="act.centerid= centerid" ng-model="act.centerid" >

                  <input type="text" id="title" name="title" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.title" required="required">
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <label class="col-sm-2 control-label"><label for="title">Event Type</label> </label>
                <div class="col-sm-10">
                  <div ui-module="select2">
                    <select ui-select2 ng-model="act.type" class="form-control w-md g-pristine ng-invalid ng-invalid-required" >
                      <option value="important">Important</option>
                      <option value="warning">Warning</option>
                      <option value="info">Info</option>
                      <option value="inverse">Inverse</option>
                      <option value="success">Success</option>
                      <option value="special">Special</option>
                    </select>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <label class="col-sm-2 control-label"><label for="title">Location</label> </label>
                <div class="col-sm-10">
                  <input type="text" id="loc" name="loc" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.loc">
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>

                <label class="col-sm-2 control-label">Date of Activity</label> </label>
                <div class="col-sm-10">&nbsp;</div>

                <div class="line line-dashed b-b line-lg pull-in"></div>

                <label class="col-sm-2 control-label" style="text-align:right"><label for="title">From</label> </label>
                <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" id="dfrom" name="dfrom" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="act.dfrom" is-open="opened" min-date="minDate" max-date="'2016-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>

                <label class="col-sm-2 control-label" style="text-align:right"><label for="title">To</label> </label>
                <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" id="dto" name="dto" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="act.dto" is-open="opened" min-date="minDate" max-date="'2016-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" type="text"  disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Time of Activity</label>
                  <div class="col-lg-10">
                    <div ng-controller="TimepickerDemoCtrl" class="m-b-lg">
                      <!-- <timepicker ng-model="act.mytime" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian" ng-required="true"></timepicker> -->


                      <div class="input-group m-b" style="width:170px;">
                        <span class="input-group-btn">
                          <button class="btn btn-default" type="button">Start Time</button>
                        </span>
                        <select ng-model="act.mytime" class="form-control w-md g-pristine ng-invalid ng-invalid-required" required>
                          <option value="07:00 AM">07:00 AM</option>
                          <option value="07:30 AM">07:30 AM</option>
                          <option value="08:00 AM">08:00 AM</option>
                          <option value="08:30 AM">08:30 AM</option>
                          <option value="09:00 AM">09:00 AM</option>
                          <option value="09:30 AM">09:30 AM</option>
                          <option value="10:00 AM">10:00 AM</option>
                          <option value="10:30 AM">10:30 AM</option>
                          <option value="11:00 AM">11:00 AM</option>
                          <option value="11:30 AM">11:30 AM</option>
                          <option value="12:00 AM">12:00 AM</option>
                          <option value="12:30 AM">12:30 AM</option>
                          <option value="01:00 PM">01:00 PM</option>
                          <option value="01:30 PM">01:30 PM</option>
                          <option value="02:00 PM">02:00 PM</option>
                          <option value="02:30 PM">02:30 PM</option>
                          <option value="03:00 PM">03:00 PM</option>
                          <option value="03:30 PM">03:30 PM</option>
                          <option value="04:00 PM">04:00 PM</option>
                          <option value="04:30 PM">04:30 PM</option>
                          <option value="05:00 PM">05:00 PM</option>
                          <option value="05:30 PM">05:30 PM</option>
                          <option value="06:00 PM">06:00 PM</option>
                          <option value="06:30 PM">06:30 PM</option>
                          <option value="07:00 PM">07:00 PM</option>
                          <option value="07:30 PM">07:30 PM</option>
                          <option value="08:00 PM">08:00 PM</option>
                          <option value="08:30 PM">08:30 PM</option>
                          <option value="09:00 PM">09:00 PM</option>
                          <option value="09:30 PM">09:30 PM</option>
                          <option value="10:00 PM">10:00 PM</option>
                          <option value="10:30 PM">10:30 PM</option>
                          <option value="11:00 PM">11:00 PM</option>
                          <option value="11:30 PM">11:30 PM</option>
                          <option value="12:00 PM">12:00 PM</option>
                          <option value="12:30 AM">12:30 AM</option>
                        </select>
                      </div>
                       <div class="input-group m-b " style="width:170px;">
                        <span class="input-group-btn">
                          <button class="btn btn-default" type="button">End Time</button>
                        </span>
                        <select ng-model="act.mytimend" class="form-control w-md g-pristine ng-invalid ng-invalid-required" required>
                          <option value="07:00 AM">07:00 AM</option>
                          <option value="07:30 AM">07:30 AM</option>
                          <option value="08:00 AM">08:00 AM</option>
                          <option value="08:30 AM">08:30 AM</option>
                          <option value="09:00 AM">09:00 AM</option>
                          <option value="09:30 AM">09:30 AM</option>
                          <option value="10:00 AM">10:00 AM</option>
                          <option value="10:30 AM">10:30 AM</option>
                          <option value="11:00 AM">11:00 AM</option>
                          <option value="11:30 AM">11:30 AM</option>
                          <option value="12:00 AM">12:00 AM</option>
                          <option value="12:30 AM">12:30 AM</option>
                          <option value="01:00 PM">01:00 PM</option>
                          <option value="01:30 PM">01:30 PM</option>
                          <option value="02:00 PM">02:00 PM</option>
                          <option value="02:30 PM">02:30 PM</option>
                          <option value="03:00 PM">03:00 PM</option>
                          <option value="03:30 PM">03:30 PM</option>
                          <option value="04:00 PM">04:00 PM</option>
                          <option value="04:30 PM">04:30 PM</option>
                          <option value="05:00 PM">05:00 PM</option>
                          <option value="05:30 PM">05:30 PM</option>
                          <option value="06:00 PM">06:00 PM</option>
                          <option value="06:30 PM">06:30 PM</option>
                          <option value="07:00 PM">07:00 PM</option>
                          <option value="07:30 PM">07:30 PM</option>
                          <option value="08:00 PM">08:00 PM</option>
                          <option value="08:30 PM">08:30 PM</option>
                          <option value="09:00 PM">09:00 PM</option>
                          <option value="09:30 PM">09:30 PM</option>
                          <option value="10:00 PM">10:00 PM</option>
                          <option value="10:30 PM">10:30 PM</option>
                          <option value="11:00 PM">11:00 PM</option>
                          <option value="11:30 PM">11:30 PM</option>
                          <option value="12:00 PM">12:00 PM</option>
                          <option value="12:30 AM">12:30 AM</option>
                        </select>
                      </div>

                      
                    </div>
                  </div>
                </div>
              </div>



              <!--START MAP ////////////////////////////////////////////////////// -->




              <div ng-show="map" class="col-sm-6" style="border:1px solid #ccc;">
                Pin Location
                <input type="hidden" ng-model="act.lat" ng-value="act.lat = lat">
                <input type="hidden" ng-model="act.lon" ng-value="act.lon = lon">
                <div id="map_canvas">
                  <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
                  <ui-gmap-search-box template="searchbox.template" events="searchbox.events"></ui-gmap-search-box>
                  <ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
                </ui-gmap-marker>
              </ui-gmap-google-map>

              <div ng-cloak>
                <ul>
                  <li>coords update ctr: {[{coordsUpdates}]}</li>
                  <li>dynamic move ctr: {[{dynamicMoveCtr}]}</li>
                </ul>
              </div>
            </div>
            <style type="text/css">
              html, body, #map_canvas {
                height: 400px;
                width: 100%;
                margin: 0px;
              }
              #map_canvas {
                position: relative;
              }
              .angular-google-map-container {
                position: absolute;
                top: 0;
                bottom: 0;
                right: 0;
                left: 0;
              }
              #pac-input {
                background-color: #fff;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                margin-left: 12px;
                margin-top: :20px;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 300px;
              }
            </style>
          </div>
          <!--END MAP ////////////////////////////////////////////////////// -->
          <div class="line line-dashed b-b line-lg pull-in"></div> 

          <label class="col-sm-2 control-label"><label for="title">Short Description <font style="color:red;font-size:24px">*</font></label> </label>
          <div class="col-sm-10">
            <!-- ngIf: user.usernametaken == true -->
            <input type="text" id="shortdesc" name="shortdesc" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.shortdesc" required="required">
          </div>

          <br>
          <div class="line line-dashed b-b line-lg pull-in"></div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Full Description <font style="color:red;font-size:24px">*</font></label>
            <div class="col-sm-10">

              <textarea class="ck-editor" ng-model="act.body" required="required"></textarea>
            </div>
          </div>













          <div class="row" >
            <div class="panel-body">
              <footer class="panel-footer text-right bg-light lter">
                <a class="btn btn-default" ng-click="cancel()">Close</a>
                <button type="submit" class="btn btn-success" ng-disabled="formevent.$invalid"  scroll-to="Scrollup">Submit</button>
              </footer>
            </div>
          </div>
          <!-- FORM CONTENT================== -->

        </div>
      </div>
    </div>
  </div>

</div>
</fieldset>
</form>