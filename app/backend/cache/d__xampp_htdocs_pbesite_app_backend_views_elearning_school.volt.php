<div id="Scrollup"></div>
<script type="text/ng-template" id="tagsAdd.html">
	<div ng-include="'/be/tpl/SchoolAdd.html'"></div>
</script>
<script type="text/ng-template" id="tagsDelete.html">
	<div ng-include="'/be/tpl/tagsDelete.html'"></div>
</script>

<div >
	<tabset class="tab-container">
		<tab>
			<tab-heading><i class="fa fa-institution"></i> School </tab-heading>
			
			<fieldset ng-disabled="isSaving">
				<div class="wrapper-md">
					<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

					<div class="row">

						<div class="col-sm-12">

							<div class="panel panel-default">
								<div class="panel-heading font-bold">
									   <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="addtags()"><i class="fa fa-plus" style="width=100%;"></i>Add School</button>
									School List
								</div>
								<div class="panel-body">
									<div class="row wrapper">
										<div class="col-sm-12">
											<div class="col-sm-6">
												<form name="rEQuired">
													<div class="input-group">
														<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
														<span class="input-group-btn">
															<button class="btn btn-sm btn-default" type="submit" ng-click="search(searchtext)" ng-disabled="rEQuired.$invalid">Go!</button>
														</span>

													</div>
												</form>
											</div>
											<div class="col-sm-6">
												<div class="btn-toolbar">
													<div class="btn-group dropdown">
													</div>
													<div class="btn-group">
														<button class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="table-responsive">
										<table class="table table-striped b-t b-light">
											<thead>
												<tr>
													<th style="width:80%">School Name</th>
													<th style="width:25%">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="mem in data.data" >
													<td> <span editable-text="mem.name"  onbeforesave="updatetags($data, mem.id)" e-pattern="[a-zA-Z0-9'\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ mem.name }]}&nbsp;&nbsp;
														<a ng-show="showflag == mem.id " class="btn btn-sm btn-icon btn-success"><i class="fa fa-check"></i></a>
														<a ng-show="showflagerror == mem.id " class="bg-danger">Something went Wrong Seems like the Tag You Insert Already Exist</a>
													</span></td>
												</td>
												<td>
													<a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
													<a href="" ng-click="tagsDelete(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>


						</div>
					</div>

				</div>

				<div class="row">
					<div class="panel-body">
						<footer class="panel-footer text-center bg-light lter">
							<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
						</footer>
					</div>
				</div>

			</div>
		</fieldset>

	</tab>
</tabset>
</div>