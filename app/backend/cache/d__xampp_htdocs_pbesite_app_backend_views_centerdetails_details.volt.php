<?php echo $this->getContent(); ?>
<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="addevent.html">
  <div ng-include="'/be/tpl/addevent.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Center Details</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-6">
      <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(msg)" name="formpage">
        <fieldset ng-disabled="isSaving">
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Hours
            </div>
            <div class="panel-body">
              <input  type="hidden"  ng-init="msg.centerid='<?php echo $username['centerid']; ?>'" ng-model="msg.centerid" >
              <div class="form-group">
                <label class="col-sm-2 control-label">Mon - Fri</label>
                <div class="col-sm-4">
                  <timepicker ng-model="timefrom" ng-change="changed()" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>
                </div>
                <div class="col-sm-4">
                  <timepicker ng-model="timeto" ng-change="changed()" hour-step="hstep1" minute-step="mstep1" show-meridian="ismeridian2"></timepicker>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Saturday</label>
                <div class="col-sm-4">
                  <timepicker ng-model="timefrom" ng-change="changed()" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>
                </div>
                <div class="col-sm-4">
                  <timepicker ng-model="timeto" ng-change="changed()" hour-step="hstep1" minute-step="mstep1" show-meridian="ismeridian2"></timepicker>
                </div>
              </div>
            </div>
            <footer class="panel-footer  bg-light lter">

              <div class="pull-right">
               <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
               <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Save</button>
             </div>
             <div style="clear:both;"></div>
           </footer>
         </div>
       </fieldset>
     </form>
   </div>
   <div class="col-sm-6">
    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(msg)" name="formpage">
      <fieldset ng-disabled="isSaving">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Center Location
          </div>
          <div class="panel-body">
            Google Map
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-12">
      <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(msg)" name="formpage">
        <fieldset ng-disabled="isSaving">
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div class="panel panel-default">
           <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs " style="margin:10px 10px 0px 0px" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
            <i class="fa fa-bars"></i> List
          </button>
          <button type="button" class="btn btn-sm btn-primary btn-addon pull-right" style="margin:10px 10px 0px 0px" ng-click="addEvent('lg','<?=$username['centerid']?>')">
            <i class="fa fa-plus"></i>Add Event
          </button>

          <div class="panel-heading font-bold">
            Callendar Events
          </div>
          <div class="panel-body">
            <div class="hbox hbox-auto-xs hbox-auto-sm">










              <div ng-controller="viewcalendarCtrl">
                <div class="hbox hbox-auto-xs hbox-auto-sm">

                  <div class="col wrapper-md">

                    <div class="clearfix m-b">

                      <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-default" ng-click="today(calendar1)">today</button>
                        <div class="btn-group m-l-xs">
                          <button class="btn btn-sm btn-default" ng-click="changeView('agendaDay', calendar1)">Day</button>
                          <button class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', calendar1)">Week</button>
                          <button class="btn btn-sm btn-default" ng-click="changeView('month', calendar1)">Month</button>

                        </div>
                      </div>
                    </div>         
                    <div class="pos-rlt">

                      <!-- begin overlay data -->
                      <div class="fc-overlay">
                        <div class="panel bg-white b-a pos-rlt">
                          <span class="arrow"></span>
                          <div class="h4 font-thin m-b-sm">{[{ event.title }]}</div>
                          <div class="line b-b b-light"></div>
                          <div><i class="icon-calendar text-muted m-r-xs"></i> {[{ event.start | date:'dd.MM.yyyy' }]} </div>
                          <div class="ng-hide" ng-show='event.end'><i class="icon-clock text-muted m-r-xs"></i> {[{ event.end | date:'dd.MM.yyyy' }]}</div>
                          <div class="ng-hide" ng-show='event.location'><i class="icon-pointer text-muted m-r-xs"></i>{[{ event.location }]}</div>
                          <div class="m-t-sm">{[{ event.info }]}</div>
                          <!-- <div class="m-t-sm">data.url</div> -->
                        </div>
                      </div>
                      <!-- end overlay data -->

                      <!-- begin view calendar -->
                      <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                      <!-- end view calendar -->

                    </div>

                  </div>

                  <!-- begin list view -->
                  <div class="col w-md bg-light dk bg-auto b-l hide" id="aside">
                    <div class="wrapper">
                      <div ng-repeat="data in dataCalendar" class="bg-white-only r r-2x m-b-xs wrapper-sm b-l b-2x b-primary">     
                        <input ng-model="data.title" class="form-control m-t-n-xs no-border no-padder no-bg" disabled>
                        <a class="pull-right text-xs text-muted" ng-click="deletepage(data.actid)"><i class="fa fa-trash-o"></i></a>
                        <a class="pull-right text-xs text-muted"></a>
                        <a class="pull-right text-xs text-muted">&nbsp;</a>
                        <a class="pull-right text-xs text-muted" ng-click="updatepage(data.actid)"><i class="fa fa-edit"></i></a>
                        <div class="text-xs text-muted">{[{data.datefrom}]} - {[{data.dateto}]}</div>
                      </div>
                    </div>
                  </div>
                  <!-- end list view -->

                </div>
                <div style="clear:both"></div>
              </div>

















            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>

