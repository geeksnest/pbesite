<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>





<div class="bg-light lter b-b wrapper-md" ng-show="_showForm">
	<h1 class="m-n font-thin h3">Manage Center</h1>
	<a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(center)" name="formpage" ng-show="_showForm">
	<fieldset ng-disabled="isSaving">
		<div class="wrapper-md">
			<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
			<div class="row">
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Information
						</div>
						<div class="panel-body ">
							Center Name
							<input type="hidden" id="idcenter" ng-value="center.centerid = centerid" ng-model="center.centerid">
							<input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.title" required="required" ng-keyup="onpagetitle(center.title)">
							<br>
							<b>Page Slugs: </b>
							<input type="hidden" ng-model="center.slugs"><span ng-bind="center.slugs"></span>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-6">
								<label>Manager E-Mail</label>
								<input type="email" class="form-control" ng-model="center.email" placeholder="Enter email">
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-6">
								<label>Select Manager</label>
								<select class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="center.manager" required>
									<option ng-repeat="data in centerManager" value="{[{ data.id }]}" ng-selected="center.manager == data.id">{[{data.fname}]} {[{data.lname}]}</option>
								</select>
							</div>
							<div class="form-group col-sm-6">
								<label>Manager E-Mail</label>
								<input type="email" class="form-control" ng-model="center.email" placeholder="Enter email">
							</div>
							<div class="form-group col-sm-6">
								<label>Region</label>
								<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.region" required>
							</div>
							<div class="form-group col-sm-6">
								<label>District</label>
								<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.district" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-4">
								<label>State Abbrevations</label>
								<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.state" required>
								<!-- <div ui-module="select2">
									<select ui-select2 ng-model="center.state" class="form-control w-md g-pristine ng-invalid ng-invalid-required " ng-change="selectstate(center.state)" required>
										<option value=""></option>
										<option ng-value="data.statecode" ng-repeat="data in statelist">{[{data.state}]} </option>
									</select>
								</div> -->
							</div>
							<div class="form-group col-sm-4">
								<label>City</label>
								<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.city" required>
								<!-- <div ui-module="select2">
									<select ui-select2 ng-model="center.city" class="form-control w-md g-pristine ng-invalid ng-invalid-required" ng-change="selectcity(center.city)" required>
										<option value=""></option>
										<option ng-value="data.zip" ng-repeat="data in citylist">{[{data.city}]} </option>
									</select>
								</div> -->
							</div>
							<div class="form-group col-sm-4">
								<label>Zip Code</label>
								<input type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required" ng-model="center.zipcode" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Address
								<textarea class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.address" required>									
								</textarea>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								<label>Meta Keywords(Note: Separate Keywords with a comma)</label>
								<input type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required"  ng-model="center.metatitle" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Meta Description
								<textarea class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.desc" required>									
								</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Banner
						</div>

						<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
						<div class="panel-body">
							<div class="input-group m-b">
								<span class="input-group-btn">
									<a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
								</span>
								<input type="text" class="form-control" ng-value="center.banner = amazonpath " ng-model="center.banner" placeholder="{[{amazonpath}]}" required>

							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}">
							<div class="form-group">
								<label class="col-sm-12 control-label">MOnday to Friday Time</label>
								<div class="col-sm-12">
									From :<select ng-model="center.mffrom" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
									<option value="07:00 AM">07:00 AM</option>
									<option value="07:30 AM">07:30 AM</option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="08:30 AM">08:30 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="09:30 AM">09:30 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="10:30 AM">10:30 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="11:30 AM">11:30 AM</option>
									<option value="12:00 AM">12:00 AM</option>
									<option value="12:30 AM">12:30 AM</option>
									<option value="01:00 PM">01:00 PM</option>
									<option value="01:30 PM">01:30 PM</option>
									<option value="02:00 PM">02:00 PM</option>
									<option value="02:30 PM">02:30 PM</option>
									<option value="03:00 PM">03:00 PM</option>
									<option value="03:30 PM">03:30 PM</option>
									<option value="04:00 PM">04:00 PM</option>
									<option value="04:30 PM">04:30 PM</option>
									<option value="05:00 PM">05:00 PM</option>
									<option value="05:30 PM">05:30 PM</option>
									<option value="06:00 PM">06:00 PM</option>
									<option value="06:30 PM">06:30 PM</option>
									<option value="07:00 PM">07:00 PM</option>
									<option value="07:30 PM">07:30 PM</option>
									<option value="08:00 PM">08:00 PM</option>
									<option value="08:30 PM">08:30 PM</option>
									<option value="09:00 PM">09:00 PM</option>
									<option value="09:30 PM">09:30 PM</option>
									<option value="10:00 PM">10:00 PM</option>
									<option value="10:30 PM">10:30 PM</option>
									<option value="11:00 PM">11:00 PM</option>
									<option value="11:30 PM">11:30 PM</option>
									<option value="12:00 PM">12:00 PM</option>
									<option value="12:30 PM">12:30 PM</option>
								</select>
							</div>
							<div class="col-sm-12">
								<div class="m-b-sm">
									To :
									<select ng-model="center.mfto" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
										<option value="07:00 AM">07:00 AM</option>
										<option value="07:30 AM">07:30 AM</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="08:30 AM">08:30 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="09:30 AM">09:30 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="10:30 AM">10:30 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="11:30 AM">11:30 AM</option>
										<option value="12:00 AM">12:00 AM</option>
										<option value="12:30 AM">12:30 AM</option>
										<option value="01:00 PM">01:00 PM</option>
										<option value="01:30 PM">01:30 PM</option>
										<option value="02:00 PM">02:00 PM</option>
										<option value="02:30 PM">02:30 PM</option>
										<option value="03:00 PM">03:00 PM</option>
										<option value="03:30 PM">03:30 PM</option>
										<option value="04:00 PM">04:00 PM</option>
										<option value="04:30 PM">04:30 PM</option>
										<option value="05:00 PM">05:00 PM</option>
										<option value="05:30 PM">05:30 PM</option>
										<option value="06:00 PM">06:00 PM</option>
										<option value="06:30 PM">06:30 PM</option>
										<option value="07:00 PM">07:00 PM</option>
										<option value="07:30 PM">07:30 PM</option>
										<option value="08:00 PM">08:00 PM</option>
										<option value="08:30 PM">08:30 PM</option>
										<option value="09:00 PM">09:00 PM</option>
										<option value="09:30 PM">09:30 PM</option>
										<option value="10:00 PM">10:00 PM</option>
										<option value="10:30 PM">10:30 PM</option>
										<option value="11:00 PM">11:00 PM</option>
										<option value="11:30 PM">11:30 PM</option>
										<option value="12:00 PM">12:00 PM</option>
										<option value="12:30 PM">12:30 PM</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-12 control-label">Saturday Time</label>
							<div class="col-sm-12">
								From :
								<select ng-model="center.satfrom" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
									<option value="07:00 AM">07:00 AM</option>
									<option value="07:30 AM">07:30 AM</option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="08:30 AM">08:30 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="09:30 AM">09:30 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="10:30 AM">10:30 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="11:30 AM">11:30 AM</option>
									<option value="12:00 AM">12:00 AM</option>
									<option value="12:30 AM">12:30 AM</option>
									<option value="01:00 PM">01:00 PM</option>
									<option value="01:30 PM">01:30 PM</option>
									<option value="02:00 PM">02:00 PM</option>
									<option value="02:30 PM">02:30 PM</option>
									<option value="03:00 PM">03:00 PM</option>
									<option value="03:30 PM">03:30 PM</option>
									<option value="04:00 PM">04:00 PM</option>
									<option value="04:30 PM">04:30 PM</option>
									<option value="05:00 PM">05:00 PM</option>
									<option value="05:30 PM">05:30 PM</option>
									<option value="06:00 PM">06:00 PM</option>
									<option value="06:30 PM">06:30 PM</option>
									<option value="07:00 PM">07:00 PM</option>
									<option value="07:30 PM">07:30 PM</option>
									<option value="08:00 PM">08:00 PM</option>
									<option value="08:30 PM">08:30 PM</option>
									<option value="09:00 PM">09:00 PM</option>
									<option value="09:30 PM">09:30 PM</option>
									<option value="10:00 PM">10:00 PM</option>
									<option value="10:30 PM">10:30 PM</option>
									<option value="11:00 PM">11:00 PM</option>
									<option value="11:30 PM">11:30 PM</option>
									<option value="12:00 PM">12:00 PM</option>
									<option value="12:30 PM">12:30 PM</option>
								</select>
							</div>
							<div class="col-sm-12">
								To :
								<select ng-model="center.satto" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
									<option value="07:00 AM">07:00 AM</option>
									<option value="07:30 AM">07:30 AM</option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="08:30 AM">08:30 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="09:30 AM">09:30 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="10:30 AM">10:30 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="11:30 AM">11:30 AM</option>
									<option value="12:00 AM">12:00 AM</option>
									<option value="12:30 AM">12:30 AM</option>
									<option value="01:00 PM">01:00 PM</option>
									<option value="01:30 PM">01:30 PM</option>
									<option value="02:00 PM">02:00 PM</option>
									<option value="02:30 PM">02:30 PM</option>
									<option value="03:00 PM">03:00 PM</option>
									<option value="03:30 PM">03:30 PM</option>
									<option value="04:00 PM">04:00 PM</option>
									<option value="04:30 PM">04:30 PM</option>
									<option value="05:00 PM">05:00 PM</option>
									<option value="05:30 PM">05:30 PM</option>
									<option value="06:00 PM">06:00 PM</option>
									<option value="06:30 PM">06:30 PM</option>
									<option value="07:00 PM">07:00 PM</option>
									<option value="07:30 PM">07:30 PM</option>
									<option value="08:00 PM">08:00 PM</option>
									<option value="08:30 PM">08:30 PM</option>
									<option value="09:00 PM">09:00 PM</option>
									<option value="09:30 PM">09:30 PM</option>
									<option value="10:00 PM">10:00 PM</option>
									<option value="10:30 PM">10:30 PM</option>
									<option value="11:00 PM">11:00 PM</option>
									<option value="11:30 PM">11:30 PM</option>
									<option value="12:00 PM">12:00 PM</option>
									<option value="12:30 PM">12:30 PM</option>
								</select>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									Contact Number(ex. (999) 999-9999)<input type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required"  ng-model="center.contactnumber" required id="phone">
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="panel-body">
					<footer class="panel-footer  bg-light lter">
						<div class="pull-left">
							<div class="form-group">
								<label class="control-label col-sm-12">Status</label>
								<div class="col-sm-12">	
									<label class="checkbox-inline i-checks control-label">
										<input type="radio" name="a" ng-model="center.status" required="required" class="form-control ng-dirty ng-valid ng-valid-required" value="true"><i></i>Active
									</label>
									<label class="checkbox-inline i-checks control-label">
										<input type="radio" name="a" ng-model="center.status" required="required" class="form-control ng-dirty ng-valid ng-valid-required" value="false"><i></i>Inactive
									</label>
								</div>
							</div>  
						</div>
						<div class="pull-right">
							<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
							<button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid" scroll-to="Scrollup">Submit</button>
						</div>
						<div style="clear:both;"></div>
					</footer>
				</div>
			</div>
		</div>
	</fieldset>
</form>











