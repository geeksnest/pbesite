<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="bannerinfo.html">
  <div ng-include="'/be/tpl/bannerinfo.html'"></div>
</script>
<script type="text/ng-template" id="message.html">
  <div ng-include="'/be/tpl/message.html'"></div>
</script>
<script type="text/ng-template" id="mediaList.html">
  <div ng-include="'/be/tpl/mediaList.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md" >
  <h1 class="m-n font-thin h3">Manage Social Links</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="table-responsive">
      <input type="hidden">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:5%">Icon</th>
            <th style="width:10%">Name</th>
            <th style="width:55%">Url</th>
            <th style="width:30%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="data in _dLsit">
            <td><button class="btn btn-rounded btn-sm btn-icon btn-default"><i class="fa  {[{ data.icon }]}"></i></td>
            <td>{[{ data.social }]}</td>
            <td><span editable-text="data.url" onbeforesave="updatealbumname($index, data.url, _dLsit, datalist)" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ data.url }]}</span></td>
            <td>
              <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"> <span class="label bg-warning">Edit Url</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>


