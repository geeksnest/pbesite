<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="appointmentDelete.html">
  <div ng-include="'/be/tpl/appointmentDelete.html'"></div>
</script>
<script type="text/ng-template" id="appointmentReview.html">
  <div ng-include="'/be/tpl/appointmentReview.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Introductory Session Appointment</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md ng-scope">
	<div class="panel panel-default">
	          <div class="panel-heading font-bold">
	            Manage Appointment
	          </div>

	           <div class="panel-body">
	           		<div class="row wrapper">
		           		<div class="col-sm-12">		
		           			<div class="col-sm-6">
		           				<form name="rEquired">
		           					<div class="input-group">
		           						<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)">
		           						<?php 
		           						if($username['task'] == "CS"){ ?>
		           							<input type="hidden" ng-model="searchcenter" value="<?php echo $username['centertitle']; ?>">
		           						<?php } ?>
		           						<span class="input-group-btn">
		           							<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
		           						</span>
		           						<?php 
		           						if($username['task'] != "CS"){
		           							?>
		           							<select ng-model="searchcenter" class="form-control input-sm" ng-change="search(searchtext,searchcenter)">
		           								<option value="" style="display:none">Filter</option>
		           								<option value="undefined">ALL CENTER</option>	 	  				
		           								<option ng-repeat="data in optioncenter" value="{[{data.title}]}">{[{data.title}]}</option>
		           							</select>
		           							<?php } ?>
		           					</div>
		           				</form>
		           			</div>
		           		</div>
	           		</div>
	           		<div class="col-sm-12">
      					<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    				</div>
	           		<div class="col-sm-12">
	           		<div class="table-responsive">
	           			<table class="table table-striped b-t b-light">
				        <thead>
				          <tr>
				          	<th>Status</th>
				            <th>Name</th>
				            <th>Email</th>
				            <th>Interest Center</th>
				            <th>Date Submitted</th>
				            <th>Action</th>
				          </tr>
				        </thead>
				        <tbody>
				        	<tr ng-repeat="data in data.data">
				                  <td ng-if="data.status == 0"><span class="label bg-warning">NEW</span></td>
				                  <td ng-if="data.status == 1"><span class="label bg-primary">REVIEWED</span></td>
				                  <td ng-if="data.status == 2"><span class="label bg-success">REPLIED</span></td>
				                  <td>{[{ data.fname }]} {[{ data.lname }]}</td>
				                  <td>{[{ data.email}]}</td>
				                  <td>{[{ data.center}]}</td>				   
				                  <td>{[{ data.datesubmitted }]}
				                  <td><button type="button" class="btn m-b-xs btn-xs btn-info btn-addon" ng-click="reviewmodal(data.id)">REVIEW</button>
				                  <a class="btn m-b-xs btn-xs btn-danger btn-addon" ng-click="deletemodal(data.id)">DELETE</a></td>
				            </tr>
				        </tbody>
				        </table>
	           		</div>
	           		</div>
	           </div>
	            <footer class="panel-footer">
				       <div class="row">
				        <div class="panel-body">
				            <footer class="panel-footer text-center bg-light lter">
				              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
				            </footer>
				        </div>
				       </div>
    			</footer>
	</div>
</div>
