<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Top page Slider</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Upload Image :  
        </div>
        <div class="loader" ng-show="imageloader">
          <div class="loadercontainer">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
            Uploading your images please wait...
          </div>
        </div>
        <div ng-show="imagecontent">
          <div class="col-sml-12">
            <div class="dragdropcenter">
              <div ngf-drop ngf-select class="drop-box" 
              ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
              accept="image/*,application/pdf" ngf-change="upload($files)">Drop images here or click to upload</div>

            </div>
          </div>
          
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div style=" height:500px; overflow-y:scroll;">
            <div ng-show="noimage"><center>IMAGE GALLERY IS EMPTY</div>
            <div class="line line-dashed b-b line-lg"></div>
            <div class="col-sm-3" ng-repeat="data in imagelist" ng-show="imggallery" style="margin-bottom:15px;">

              <button class=" btn btn-rounded btn btn-icon btn-danger" ng-click="delete(data.id)" style="margin:10px 5px -50px; 5px"><i class="fa fa-trash-o"></i></button>
              <input id="pathimage" type="hidden" id="title" name="title" class="form-control" ng-init="imgamazonpath=amazonpath+'/uploads/banner/'+ data.filename" onClick="this.setSelectionRange(0, this.value.length)" ng-model="imgamazonpath">
              <div type="button" class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}');" ng-click="path(imgamazonpath)">
              </div>
            </div>
          </div>
        </div>




      </div>
    </div>
  </div>
</div>