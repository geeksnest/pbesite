<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<div >
	<tabset class="tab-container">
		<tab ng-repeat='data in traininglist'>
			<tab-heading><i class="glyphicon glyphicon-pencil"></i> Edit</tab-heading>
			<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(data)" name="form">
				<fieldset ng-disabled="isSaving">
					<div class="wrapper-md">
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<div class="col-sm-8 panel panel-default" >
							<div class="panel-heading font-bold">
								Information
							</div>
							<div class="panel-body">
							<input type="hidden" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.idno" />
								<div class="form-group">
									<label class="col-sm-2 control-label">Title</label>
									<div class="col-sm-10">
										<input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.title" required="required" pattern=".{1,50}" maxlength="50">
									</div>
								</div>
								<div class="line line-dashed b-b line-lg pull-in"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Meta Title</label>
									<div class="col-sm-10">
										<input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.metatitle" required="required" pattern=".{1,50}" maxlength="50">
									</div>
								</div>
								<div class="line line-dashed b-b line-lg pull-in"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Short Description</label>
									<div class="col-sm-10">
										<textarea id="txtarea" class="ck-editor form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.description" required></textarea>
										<div class="line line-dashed b-b line-lg"></div>
									</div>
								</div>

								
							</div>
						</div>
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-heading font-bold">
									Logo
								</div>
								<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
								<div class="panel-body">
									<div class="input-group m-b">
										<span class="input-group-btn">
											<a class="btn btn-default"  ng-click="featuredbanner('lg')">Banner</a>
										</span>
										<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-value="data.logo = featpath " ng-model="data.logo">
									</div>
								</div>
								<div  style="margin-top:5px;">
									<img style="width:100%" src="{[{data.logo}]}">
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-heading font-bold">
									Theme
								</div>
								<div class="panel-body">
									<div class="form-group">
										<select ng-model="data.theme" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" >
											<option style=" background-color: {[{clr.color}]}; color:#fff !important;" ng-repeat="clr in theme" value="{[{clr.color}]}">{[{clr.name}]}</option>
										</select>
									</div>
									<div class="form-group">
										<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
											<button disabled="disabled"  type="submit" class="btn btn-success" ng-disabled="form.$invalid || form.$pending || usrname==true || usremail==true || pwdconfirm==true" scroll-to="Scrollup">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</tab>
		</tabset>
	</div>