<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="testimonialsimagelist.html">
  <div ng-include="'/be/tpl/testimonialsimagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/javascript">  
  function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Edit Testimonials</h1>
  <a id="top"></a>
</div>
<form method="POST" name="uPdate" ng-submit="Updatetestimonials(testimonials)" class="form-validation ng-pristine ng-invalid ng-invalid-required">
<fieldset ng-disabled="isSaving">
<div class="wrapper-md ng-scope">
  <div class="col-sm-12">
                    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  </div>
  <div class="col-sm-8">
  	<div class="panel panel-default">
  	          <div class="panel-heading font-bold">
  	            Testimonials <div id="mydiv"><span class="label bg-success">{[{ savecheck }]}</span></div>
  	          </div>
  	     <div class="panel-body">
            <div class="col-sm-12">
              <div class="text-danger">
                  *All fields are required.</br>
              </div></br></br>
            </div>
  	            <div class="form-group"> 
                  <input type="hidden"  ng-model="testimonials.id">
                  <label class="col-sm-12 control-label">Title</label>
  	              <div class="col-sm-12">
  	                <input type="text" required="required" ng-model="testimonials.title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">
  	              </div>
  	            </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
  	            <div class="form-group">
  	              <label class="control-label col-sm-12">Status</label>
  	              <div class="col-sm-12">  
                   <div class="radio">
                    <label class="i-checks">
                        <input type="radio" name="a" ng-model="testimonials.status" required="required" class="form-control ng-valid ng-valid-required" value="1"><i></i>PUBLISHED
                    </label>
                    </div>
                    <label class="i-checks">
                        <input type="radio" name="a" ng-model="testimonials.status" required="required" class="form-control ng-valid ng-valid-required" value="0"><i></i>UNPUBLISHED
                    </label>
                  </div>
              	</div>
              	<div class="line line-dashed b-b line-lg pull-in"></div>

              	<div class="form-group">
              		<label class="control-label col-sm-12">Testimonial Category</label>
              			<div class="col-sm-12">
              				<div class="input-group w-md">
              					<select class="form-control m-t ng-pristine ng-invalid ng-invalid-required" ng-model="testimonials.testimonialcategory" required="">
                 					      <option value="" style="display:none;">Please choose</option>
                 					      <option value="families">For Families</option>
                 					      <option value="adults">For Adults</option>
                 					      <option value="powerbrain">PowerBrain School</option>
                 					      <option value="featured">Featured</option>
                 					</select>
                 			</div>
                 		</div>
                 	</div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>


                  <div class="form-group">
            			<label class="col-sm-12 control-label">Date published</label>
           			 <div class="col-sm-12">
                  <input type="hidden" required="required" ng-value="testimonials.hiddendate = testimonials.datepublished" ng-model="testimonials.hiddendate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">
               		<div class="input-group w-md">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="testimonials.datepublished" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
           			 </div>
          		</div>
          		<div class="line line-dashed b-b line-lg pull-in"></div>


  	            <div class="form-group">
  	              <label class="col-sm-12 control-label">Sender</label>
  	              <div class="col-sm-12">
  	                <input type="text" required="required" ng-model="testimonials.sender" class="form-control ng-invalid ng-invalid-required ng-valid-pattern ng-pristine ng-untouched">
  	              </div>
  	            </div>
  	            <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                  <label class="col-sm-12 control-label">Age</label>
                  <div class="col-sm-3">
                    <div class="input-group bootstrap-touchspin">
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input ui-jq="TouchSpin" type="text" value="" class="form-control" data-min="1" data-max="150" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" required  ng-model="testimonials.age" onkeypress='return isNumberKey(event)'>
                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                  </div>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

  	            <div class="form-group">
                    <label class="col-sm-12 control-label">Testimonial</label>
                    <div class="col-sm-12">
                    <textarea class="ck-editor" ng-model="testimonials.content" required="required" min-length="150"></textarea>
                    </div>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
  	        </div>
        </div>    
	</div> 
 <!--   ///////////////////////////////////////////////////////      -->
  <div class="col-sm-4">
  <div class="panel panel-default"> 
    <div class="panel-heading font-bold">
              Image <div id="mydiv"><span class="label bg-success">{[{ savecheck }]}</span></div>
    </div>
    <div class="panel-body">
        <div class="form-group">
                <div class="input-group m-b">
                <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
                </span>
                <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                <input type="text" class="form-control" ng-value="testimonials.image = amazonpath " ng-model="testimonials.image" placeholder="{[{amazonpath}]}" readonly required>
              </div>
                <div class="col-sm-12">
                <br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/testimonialimage/{[{amazonpath}]}" width="100%" height="100%" alt="IMAGE PREVIEW">
                </div>
        </div>
    </div>
  </div>
  </div> 
 <!--   ///////////////////////////////////////////////////////      -->
  
  <div  class="row">
    <div class="col-sm-12">
                <footer class="panel-footer text-right bg-light lter">
                    <button type="submit" class="btn btn-info" ng-disabled="uPdate.$invalid" scroll-to="Scrollup">Update Testimonial</button>
                    <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
                </footer>
    </div>
  </div>
</div>
</fieldset>
</form>
