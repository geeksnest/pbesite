<script type="text/ng-template" id="requestReview.html">
  <div ng-include="'/be/tpl/requestReview.html'"></div>
</script>
<script type="text/ng-template" id="appointmentReview.html">
  <div ng-include="'/be/tpl/appointmentReview.html'"></div>
</script>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <h1 class="m-n font-thin h3 text-black">Dashboard</h1>
          <small class="text-muted">PBE Content Management System</small>
        </div>
        <div class="col-sm-6 text-right hidden-xs">
          <div class="inline m-r text-left" ng-controller="Uniquevisitorctrl">
            <div class="m-b-xs">{[{ count }]} <span class="text-muted">Unique Visitors</span></div>
            <div ng-init="data1=[ 106,108,110,105,110,109,105,104,107,109,105,100,105,102,101,99,98 ]"
                 ui-jq="sparkline"
                 ui-options="{[{ data1 }]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}"
                 class="sparkline inline">loading...
            </div>
          </div>
          <div class="inline text-left">
            <div class="m-b-xs">$30,000 <span class="text-muted">Page Visits</span></div>
            <div ng-init="data2=[ 105,102,106,107,105,104,101,99,98,109,105,100,108,110,105,110,109 ]"
                 ui-jq="sparkline"
                 ui-options="{[{data2}]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}"
                 class="sparkline inline">loading...
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / main header -->
   
    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
      <!-- stats -->
      <div class="row">
        <div class="col-md-5">
          <div class="row row-sm text-center">
            <div class="col-xs-6">
              <div class="panel padder-v item bg-primary">
                <div class="h1 text-info font-thin h1">{[{countnews}]}</div>
                <span class="text-muted text-xs">Center news</span>
                <span class="bottom text-right">
                  <i class="glyphicon glyphicon-th-list text-muted m-r-sm"></i>
                </span>
              </div>
            </div>
            <div class="col-xs-6">
              <a href class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block">{[{ countevent }]}</span>
                <span class="text-muted text-xs">Events</span>
                <span class="bottom text-right">
                  <i class="fa icon-emoticon-smile text-muted m-r-sm"></i>
                </span>
              </a>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-warning dker item hbox no-border">
                <!-- <div class="col w-xs v-middle hidden-md">
                  <div ng-init="data1=[60,40]" ui-jq="sparkline" ui-options="{[{data1}]}, {type:'pie', height:40, sliceColors:['{[{app.color.warning}]}','#fff']}" class="sparkline inline"></div>
                </div> -->
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>{[{ countvideo }]}</span></div>
                  <span class="text-muted text-xs">Videos</span>
                <span class="bottom text-right">
                  <i class="fa  icon-social-youtube text-muted m-r-sm"></i>
                </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">Spline</div>
            <div class="panel-body">
              <div ui-jq="plot" ui-options="
              [
                { data: {[{d}]}, points: { show: true, radius: 6}, splines: { show: true, tension: 0.45, lineWidth: 5, fill: 0 } }
              ], 
              {
                colors: ['{[{app.color.info}]}'],
                series: { shadowSize: 3 },
                xaxis:{ 
                  font: { color: '#ccc' },
                  position: 'bottom',
                  ticks: [
                    [ 1, 'Jan' ], [ 2, 'Feb' ], [ 3, 'Mar' ], [ 4, 'Apr' ], [ 5, 'May' ], [ 6, 'Jun' ], [ 7, 'Jul' ], [ 8, 'Aug' ], [ 9, 'Sep' ], [ 10, 'Oct' ], [ 11, 'Nov' ], [ 12, 'Dec' ]
                  ]
                },
                yaxis:{ font: { color: '#ccc' } },
                grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
                tooltip: true,
                tooltipOpts: { content: '%x.1 is %y.4',  defaultTheme: false, shifts: { x: 0, y: 20 } }
              }
            " style="height:240px" >
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- / stats -->

      <!-- service -->
      <div class="panel hbox hbox-auto-xs no-border">
        <div class="col wrapper">
          <i class="fa fa-circle-o text-info m-r-sm pull-right"></i>
          <h4 class="font-thin m-t-none m-b-none text-primary-lt">System Performance</h4>
          <span class="m-b block text-sm text-muted">Service report of this year (updated 1 hour ago)</span>
          <div ui-jq="plot" ui-options="
            [
              { data: {[{d4}]}, lines: { show: true, lineWidth: 1, fill:true, fillColor: { colors: [{opacity: 0.2}, {opacity: 0.8}] } } }
            ], 
            {
              colors: ['{[{app.color.light}]}'],
              series: { shadowSize: 3 },
              xaxis:{ show:false },
              yaxis:{ font: { color: '#a1a7ac' } },
              grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#dce5ec' },
              tooltip: true,
              tooltipOpts: { content: '%s of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 10, y: -25 } }
            }
          " style="height:240px" >
          </div>
        </div>
        <!-- <div class="col wrapper-lg w-lg bg-light dk r-r">
          <h4 class="font-thin m-t-none m-b">Reports</h4>
          <div class="">
            <div class="text-center-folded">
              <span class="pull-right text-primary">60%</span>
              <span class="hidden-folded">Memory</span>
            </div>
            <progressbar value="60" class="progress-xs m-t-sm bg-white" animate="true" type="primary"></progressbar>
            <div class="text-center-folded">
              <span class="pull-right text-info">35%</span>
              <span class="hidden-folded">Disk</span>
            </div>
            <progressbar value="35" class="progress-xs m-t-sm bg-white" animate="true" type="info"></progressbar>
            <div class="text-center-folded">
              <span class="pull-right text-warning">25%</span>
              <span class="hidden-folded">Server Process</span>
            </div>
            <progressbar value="25" class="progress-xs m-t-sm bg-white" animate="true" type="warning"></progressbar>
          </div>
          <p class="text-muted">Dales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis</p>
        </div> -->
      </div>
      <!-- / service -->
    </div>
  </div>
  <!-- / main -->
  <!-- right col -->
  <?php   if($username['task'] != "CS"){ ?>
  <div class="col w-md bg-white-only b-l bg-auto bg-auto-right no-border-xs" ng-controller="NewmessageCtrl">
    <tabset class="nav-tabs-alt" justified="true">
      <tab>
        <tab-heading>
          <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
          <span class="badge badge-sm up bg-danger pull-right-xs" ng-show="hideme">{[{ count }]}</span>
        </tab-heading>
        <div class="wrapper-md">
          <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          </div>
          <div class="m-b-sm text-md">New Request</div>
            <div class="text-center">
             <a ui-sref="managerequestinfo" class="btn btn-sm btn-primary padder-md m-b">View all request</a>
            </div>
            <div class="streamline b-l m-b">
              <div class="sl-item b-warning b-l" ng-repeat="data in newdata">
                <div class="m-l">
                  <div class="text-muted">{[{ data.datesubmitted }]}</div>
                  <p>{[{ data.fname }]} {[{ data.lname }]} 
                  </p><button type="button" class="btn m-b-xs btn-xs btn-info btn-addon" ng-click="reviewmodal(data.id)">REVIEW</button>
                </div>
              </div>
            </div>
        </div>
      </tab>
      <tab>
        <tab-heading>
          <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
          <span class="badge badge-sm up bg-danger pull-right-xs" ng-show="hideme1">{[{ count1 }]}</span>
        </tab-heading>
        <div class="wrapper-md">
          <div class="col-sm-12">
                <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          </div>
          <div class="m-b-sm text-md">New Appointment</div>
          <div class="text-center">
            <a ui-sref="manageappointment" class="btn btn-sm btn-primary padder-md m-b">View all Apointment</a>
          </div>
          <div class="streamline b-l m-b">
              <div class="sl-item b-warning b-l" ng-repeat="data1 in newdata1">
                <div class="m-l">
                  <div class="text-muted">{[{ data1.datesubmitted }]}</div>
                  <p>{[{ data1.fname }]} {[{ data1.lname }]} 
                  </p>
                  <button type="button" class="btn m-b-xs btn-xs btn-info btn-addon" ng-click="reviewmodal1(data1.id)">REVIEW</button>
                </div>
              </div>
            </div>
        </div>
      </tab>
    </tabset>

  </div>
  <?php } ?>
  <!-- / right col -->
</div>
