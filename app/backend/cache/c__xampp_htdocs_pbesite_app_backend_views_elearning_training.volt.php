<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div >
	<tabset class="tab-container">
		<tab>
			<tab-heading><i class="glyphicon glyphicon-th-list"></i> Manage Classroom Activities Sub-Page</tab-heading>
			<div >
				<div class="panel panel-default">
					<div class="panel-heading">
						Classroom Activities Sub-Page Lists
					</div>
					<div class="panel-body b-b b-light">
						Search: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
					</div>
					<table class="table m-b-none" ui-jq="footable" data-filter="#filter" data-page-size="10">
						<thead>
							<tr>
								<th data-hide="phone,tablet">Category </th>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="data in traininglist | filter:{page:idno}">
								<td>{[{data.title}]}</td>
								<td data-value="1">
									<a href="" class="btn btn-default btn-xs" ui-sref="elearning.tmedia({idno:data.idno})"><i class="glyphicon glyphicon-search"></i>View</a>
									<a href="" class="btn btn-warning btn-xs" ui-sref="elearning.edittraining({idno:data.idno})"><i class="glyphicon glyphicon-pencil"></i>Edit</a>
									<a href="" class="btn btn-danger btn-xs" ng-click="_delete(data.idno)"><i class="glyphicon glyphicon-trash"></i>Delete </a>
								</td>
							</tr>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="10" class="text-center">
									<ul class="pagination"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</tab>
		<tab>
			<tab-heading><i class="glyphicon glyphicon-plus"></i> Add Classroom Activities Sub-Page</tab-heading>
			<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(data)" name="form">
				<fieldset ng-disabled="isSaving">
					<div class="wrapper-md">
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<input type="hidden" ng-model="data.page" ng-value="data.page = idno">
						<div class="col-sm-8 panel panel-default" >
							<div class="panel-heading font-bold">
								Information
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Title</label>
									<div class="col-sm-10">
										<input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.title" required="required" pattern=".{1,50}" maxlength="50">
									</div>
								</div>
								<div class="line line-dashed b-b line-lg pull-in"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Meta Title</label>
									<div class="col-sm-10">
										<input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.metatitle" required="required" pattern=".{1,50}" maxlength="50">
									</div>
								</div>
								<div class="line line-dashed b-b line-lg pull-in"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Short Description</label>
									<div class="col-sm-10">
										<textarea id="txtarea" class="ck-editor form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.description" required></textarea>
										<div class="line line-dashed b-b line-lg"></div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-heading font-bold">
									Logo
								</div>
								<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
								<div class="panel-body">
									<div class="input-group m-b">
										<span class="input-group-btn">
											<a class="btn btn-default"  ng-click="featuredbanner('lg')">Banner</a>
										</span>
										<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-value="data.logo = featpath " ng-model="data.logo">
									</div>
								</div>
								<div  style="margin-top:5px;">
									<img style="width:100%" src="{[{news.featbanner}]}">
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-heading font-bold">
									Theme
								</div>
								<div class="panel-body">
									<div class="form-group">
										<select ng-model="data.theme" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" >
											<option style=" background-color: {[{clr.color}]}; color:#fff !important;" ng-repeat="clr in theme" value="{[{clr.color}]}">{[{clr.name}]}</option>
										</select>
									</div>


									<div class="form-group">
										<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
										<button disabled="disabled"  type="submit" class="btn btn-success" ng-disabled="form.$invalid || form.$pending || usrname==true || usremail==true || pwdconfirm==true" scroll-to="Scrollup">Submit</button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</fieldset>
			</form>
		</tab>
	</tabset>
</div>