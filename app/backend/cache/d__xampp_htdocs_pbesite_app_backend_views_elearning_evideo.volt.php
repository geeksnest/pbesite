<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="eVidsave.html">
	<div ng-include="'/be/tpl/eVidsave.html'"></div>
</script>
<script type="text/ng-template" id="eVidupdate.html">
	<div ng-include="'/be/tpl/eVidupdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">E-Gallery Video</h1>
  <a id="top"></a>
</div>

<div class="panel-body">
	<button class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addVid()"><i class="fa fa-plus"></i>Add New Video</button>


	<alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>

	<div >
		<div style=" height:500px; overflow-y:scroll;">
			<div ng-show="noimage"><center>IMAGE GALLERY IS EMPTY</div>
			<div class="line line-dashed b-b line-lg"></div>
			<div class="col-sm-3" ng-repeat="data in videlist" style="margin-bottom:15px;" >
		
				<button class=" btn btn-rounded btn btn-icon btn-danger" ng-click="delete(data.id)" style="margin:10px 5px -50px 5px"><i class="fa fa-trash-o"></i></button>
				<button class=" btn btn-rounded btn btn-icon btn-warning" ng-click="edit(data.id, data.filename, data.title, data.description, data.embed)" style="margin:10px 5px -50px -5px"><i class="icon-pencil"></i></button>

				<input id="pathimage" type="hidden" id="title" name="title" class="form-control" ng-model="imgamazonpath"  ng-init="imgamazonpath= data.filename" onClick="this.setSelectionRange(0, this.value.length)" >
				<input type="button" class="imagegallerystyle" style="background-image: url('{[{imgamazonpath}]}');" >
			</div>
		</div>
	</div>
</div>