<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="authorDelete.html">
  <div ng-include="'/be/tpl/authorDelete.html'"></div>
</script>
<script type="text/ng-template" id="authorEdit.html">
  <div ng-include="'/be/tpl/authorEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Author</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md ng-scope">
	<div class="panel panel-default">
	          <div class="panel-heading font-bold">
	            Manage Author
	          </div>

	           <div class="panel-body">
	           		<div class="row wrapper">
	           		<div class="col-sm-12">
	           			<div class="col-sm-6">
	           				<form name="rEquired">
	           				<div class="input-group">
				              <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext" ng-change="search(searchtext)" required>
				              <span class="input-group-btn">
				              	<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
				              </span>
				            </div>
				            </form>
	           			</div>
	           		</div>
	           		</div>
	           		<div class="col-sm-12">
      					<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    				</div>
	           		<div class="col-sm-12">
	           		<div class="table-responsive">
	           			<table class="table table-striped b-t b-light">
				        <thead>
				          <tr>
				            <th>Name</th>
				            <th>Location</th>
				            <th>Occupation</th>
				            <th>Author Since</th>
				            <th>Action</th>
				          </tr>
				        </thead>
				        <tbody>
				        	<tr ng-repeat="list in data.data">
				                  <td>{[{ list.name}]}</td>
				                  <td>{[{ list.location}]}</td>
				                  <td>{[{ list.occupation}]}</td>
				                  <td>{[{ list.authorsince}]}</td>
				                  <td><button type="button" class="btn m-b-xs btn-xs btn-info btn-addon" ng-click="editmodal(list.id)">EDIT</button>
				                  <a class="btn m-b-xs btn-xs btn-danger btn-addon" ng-click="deletemodal(list.id)">DELETE</a></td>
				            </tr>
				        </tbody>
				        </table>
	           		</div>
	           		</div>



	           </div>
	           <footer class="panel-footer">
			      <div class="row">
				        <div class="panel-body">
				            <footer class="panel-footer text-center bg-light lter">
				              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
				            </footer>
				        </div>
				       </div>
    			</footer>
	</div>
</div>
