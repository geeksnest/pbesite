<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="userDelete.html">
	<div ng-include="'/be/tpl/userDelete.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
	<div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<script type="text/ng-template" id="success.html">
	<div ng-include="'/be/tpl/success.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="review.html">
	<div ng-include="'/be/tpl/review.html'"></div>
</script>
<script type="text/ng-template" id="tagsAdd.html">
	<div ng-include="'/be/tpl/SchoolAdd.html'"></div>
</script>
<script type="text/ng-template" id="tagsDelete.html">
	<div ng-include="'/be/tpl/tagsDelete.html'"></div>
</script>
<div >
	<tabset class="tab-container">
		<tab>
			<tab-heading ui-sref="elearning.user" ui-sref="elearning.user"><i class="glyphicon glyphicon-th-list"></i> User List</tab-heading>
			<div class="panel panel-default">
				<tabset  class="tab-container" >
					<tab heading="All Users" ng-controller="UserListAllCtrl" ng-click="reset()" >
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<form name="rEquired" >
							<div class="input-group">
								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
								<span class="input-group-btn">
									<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
						</form>
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Status</th>
										<th>Role</th>
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
										<td>{[{ user.fname }]} {[{ user.lname }]}</td>
										<td>{[{ user.email }]}</td>
										<td>{[{ user.username }]}</td>
										<td ng-if="user.status == 1">
											<div>
												<span class="bg-success btn btn-xs" >Active
												</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
													<i></i>
												</label>
											</div>
										</td>
										<td ng-if="user.status == 0">
											<div>
												<span class="bg-danger btn btn-xs" >Inactive</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)">
													<i></i>
												</label>
											</div>
										</td>

										<td style="text-transform: uppercase;">{[{ user.userrole }]}</td>

										<td>
											<span class="bg-warning btn btn-xs" ng-if="user.pending" >Not-Completed</span>
											<span class="bg-success btn btn-xs" ng-if="!user.pending" >Completed</span>
										</td>
		
										<td>
											<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
											<button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="panel-body">
									<footer class="panel-footer text-center bg-light lter">
										<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
									</footer>
								</div>
							</div>
						</footer>
					</tab>


					<tab heading="Teacher" ng-controller="UserListTeacherCtrl"  ng-click="reset()" >
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<form name="rEquired" >
							<div class="input-group">

								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
								<span class="input-group-btn">

									<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
						</form>
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Status</th>
										<!-- <th>Role</th> -->
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
										<td>{[{ user.fname }]} {[{ user.lname }]}</td>
										<td>{[{ user.email }]}</td>
										<td>{[{ user.username }]}</td>
										<td ng-if="user.status == 1">
											<div>
												<span class="bg-success btn btn-xs" >Active
												</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
													<i></i>
												</label>
											</div>
										</td>
										<td ng-if="user.status == 0">
											<div>
												<span class="bg-danger btn btn-xs" >Inactive</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)">
													<i></i>
												</label>
											</div>
										</td>

										<!-- <td style="text-transform: uppercase;">{[{ user.userrole }]}</td> -->

										<td>
											<span class="bg-warning btn btn-xs" ng-if="user.pending" >Not-Completed</span>
											<span class="bg-success btn btn-xs" ng-if="!user.pending" >Completed</span>
										</td>
		
										<td>
											<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
											<button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="panel-body">
									<footer class="panel-footer text-center bg-light lter">
										<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
									</footer>
								</div>
							</div>
						</footer>
					</tab>

					<tab heading="Mentor" ng-controller="UserListMentorCtrl"   ng-click="reset()"  >
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<form name="rEquired" >
							<div class="input-group">

								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
								<span class="input-group-btn">

									<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
						</form>
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Status</th>
										<!-- <th>Role</th> -->
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
										<td>{[{ user.fname }]} {[{ user.lname }]}</td>
										<td>{[{ user.email }]}</td>
										<td>{[{ user.username }]}</td>
										<td ng-if="user.status == 1">
											<div>
												<span class="bg-success btn btn-xs" >Active
												</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
													<i></i>
												</label>
											</div>
										</td>
										<td ng-if="user.status == 0">
											<div>
												<span class="bg-danger btn btn-xs" >Inactive</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)">
													<i></i>
												</label>
											</div>
										</td>

										<!-- <td style="text-transform: uppercase;">{[{ user.userrole }]}</td> -->

										<td>
											<span class="bg-warning btn btn-xs" ng-if="user.pending" >Not-Completed</span>
											<span class="bg-success btn btn-xs" ng-if="!user.pending" >Completed</span>
										</td>
		
										<td>
											<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
											<button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<footer class="panel-footer">
							<div class="row">
								<div class="panel-body">
									<footer class="panel-footer text-center bg-light lter">
										<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
									</footer>
								</div>
							</div>
						</footer>

					</tab>

					<tab heading="Leader" ng-controller="UserListLeaderrCtrl" ng-init="_leader()" ng-click="reset()"  >
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						<form name="rEquired" >
							<div class="input-group">

								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
								<span class="input-group-btn">

									<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
						</form>
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Status</th>
										<!-- <th>Role</th> -->
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
										<td>{[{ user.fname }]} {[{ user.lname }]}</td>
										<td>{[{ user.email }]}</td>
										<td>{[{ user.username }]}</td>
										<td ng-if="user.status == 1">
											<div>
												<span class="bg-success btn btn-xs" >Active
												</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
													<i></i>
												</label>
											</div>
										</td>
										<td ng-if="user.status == 0">
											<div>
												<span class="bg-danger btn btn-xs" >Inactive</span>
											</div>
											<div ng-if="!user.pending">
												<label class="i-switch i-switch-md bg-info m-t-xs m-r">
													<input type="checkbox" ng-click="changestatus(user.id,user.status)">
													<i></i>
												</label>
											</div>
										</td>

										<!-- <td style="text-transform: uppercase;">{[{ user.userrole }]}</td> -->

										<td>
											<span class="bg-warning btn btn-xs" ng-if="user.pending" >Not-Completed</span>
											<span class="bg-success btn btn-xs" ng-if="!user.pending" >Completed</span>
										</td>
		
										<td>
											<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
											<button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<footer class="panel-footer">
							<div class="row">
								<div class="panel-body">
									<footer class="panel-footer text-center bg-light lter">
										<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
									</footer>
								</div>
							</div>
						</footer>

					</tab>
				</tabset>
			</div>
		</tab>
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->	
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<tab ng-controller="PendingListCtrl" ng-init="_pending()">
			<tab-heading><i class="icon-users"></i> Pending Users({[{pendinguser}]})</tab-heading>
			<div class="panel panel-default">
				<div  id="top">
					<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
					<form name="rEquired" >
						<div class="input-group">
							<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
							<span class="input-group-btn">

								<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
							</span>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-striped b-t b-light">
							<thead>
								<tr>
									<!-- <th style="width:20px;">
										<label class="i-checks m-b-none">
											<input type="checkbox"><i></i>
										</label>
									</th> -->
									<th>Name</th>
									<th>Email</th>
									<th>Username</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'">
									<!-- <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td> -->
									<td>{[{ user.fname }]} {[{ user.lname }]}</td>
									<td>{[{ user.email }]}</td>
									<td>{[{ user.username }]}</td>
									<td ng-if="user.status == 1">
										<div>
											<span class="bg-success btn btn-xs" >Active
											</span>
										</div>
										<!-- <div >
											<label class="i-switch i-switch-md bg-info m-t-xs m-r">
												<input type="checkbox" 
												ng-click="changestatus(user.id,user.status)" checked>
												<i></i>
											</label>
										</div> -->
									</td>
									<td ng-if="user.status == 0">
										<div>
											<span class="bg-warning btn btn-xs" >Inactive
											</span>
										</div>
										<!-- <div>
											<label class="i-switch i-switch-md bg-info m-t-xs m-r">
												<input type="checkbox" 
												ng-click="changestatus(user.id,user.status)">
												<i></i>
											</label>
										</div> -->
									</td>

									<td>
										<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
										<button ng-click="deletepending(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										<button ng-click="review({userid:user.id,fname:user.fname,lname:user.lname,email:user.email,username:user.username,birthday:user.birthday,gender:user.gender,school:user.school,grade:user.grade, userrole:user.userrole, profile:user.profile})" class="btn m-b-xs btn-xs btn-primary btn-addon">Review</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<footer class="panel-footer">
						<div class="row">
							<div class="panel-body">
								<footer class="panel-footer text-center bg-light lter">
									<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
								</footer>
							</div>
						</div>
					</footer>
				</div>
			</div> 
		</tab>
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->	
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- <tab ng-controller="PendingListCtrl" ng-init="_addedusers()" ng-click="_addedusers()">
			<tab-heading><i class="icon-users"></i> Added Users ({[{pendinguser}]})</tab-heading>
			<div class="panel panel-default">
				<div  id="top">
					<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
					<form name="rEquired" >
						<div class="input-group">
							<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
							<span class="input-group-btn">

								<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
							</span>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-striped b-t b-light">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Username</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'">
									<td>{[{ user.fname }]} {[{ user.lname }]}</td>
									<td>{[{ user.email }]}</td>
									<td>{[{ user.username }]}</td>
									<td ng-if="user.status == 1">
										<div>
											<span class="bg-success btn btn-xs" >Active
											</span>
										</div>
									</td>
									<td ng-if="user.status == 0">
										<div>
											<span class="bg-warning btn btn-xs" >Inactive
											</span>
										</div>
									</td>

									<td>
										<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
										<button ng-click="deletepending(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
										<button ng-click="review({userid:user.id,fname:user.fname,lname:user.lname,email:user.email,username:user.username,birthday:user.birthday,gender:user.gender,school:user.school,grade:user.grade, userrole:user.userrole, profile:user.profile})" class="btn m-b-xs btn-xs btn-primary btn-addon">Review</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<footer class="panel-footer">
						<div class="row">
							<div class="panel-body">
								<footer class="panel-footer text-center bg-light lter">
									<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
								</footer>
							</div>
						</div>
					</footer>
				</div>
			</div> 
		</tab> -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->	
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<!-- /////// -->
		<tab>
			<tab-heading><i class="glyphicon glyphicon-plus"></i> Create User</tab-heading>
			<div class="panel panel-default">
				<div ng-controller="AddUserCtrl" id="top">
					<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user)" name="form">
						<fieldset ng-disabled="isSaving">
							<div class="wrapper-md">
								<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
								<div class="col-sm-12">
									<div class="panel panel-default">
										<div class="panel-heading font-bold">
											Account Information
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label class="col-lg-2 control-label">User Role</label>
												<div class="col-sm-10">
													<span class="badge bg-danger">Required</span>
													<div class="radio">
														<label class="i-checks">
															<input id="userrole" type="radio" name="userrole" value="teacher" ng-model="user.userrole"  ng-click="_req(false)" required="required">
															<i></i>
															<label class="text-success">Teacher/Instructor</label> ~ Access Frontend
														</label>
													</div>
													<div class="line line-dashed b-b line-lg pull-in"></div>
													<div class="radio">
														<label class="i-checks">
															<input id="userrole" type="radio" name="userrole" value="mentor" ng-model="user.userrole"  ng-click="_req(false)" required="required">
															<i></i>
															<label class="text-success">Mentor</label> ~ Access Frontend
														</label>
													</div>
													<div class="line line-dashed b-b line-lg pull-in"></div>
													<div class="radio">
														<label class="i-checks">
															<input id="userrole" type="radio" name="userrole" value="leader" ng-model="user.userrole"  ng-click="_req(false)" required="required">
															<i></i>
															<label class="text-success">Leader</label> ~ Access Frontend
														</label>
													</div>
												</div>
											</div> 
											<div class="line line-dashed b-b line-lg pull-in"></div> 
											<div class="form-group">
												<label class="col-sm-2 control-label">Username</label>
												<div class="col-sm-10">
													<span class="label bg-danger" ng-show="usrname">Username already taken. <br/></span>

													<!-- NOT REQURED -->
													<input type="text" ng-if="notReq"  id="" name="username" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" pattern=".{4,50}" maxlength="12" >
													<!-- REQURED -->
													<span class="label bg-info" ng-show="req">Username is Requred <br/></span>
													<input type="text" ng-if="req"  id="" name="username" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" pattern=".{4,50}" maxlength="12" required="required">


													<em class="text-muted">(allow 'a-zA-Z0-9', 4-12 length)</em>
												</div>
											</div>
											<div class="line line-dashed b-b line-lg pull-in"></div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Email Address </label>
												<div class="col-sm-10">
													<span class="badge bg-danger">Required</span>
													<span class="label bg-danger" ng-show="usremail">Email already taken. <br/></span>
													<input type="email" id="email" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required" pattern=".{5,200}" maxlength="200">
												</div>
											</div>
											<!-- <div class="line line-dashed b-b line-lg pull-in"></div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Password</label>
												<div class="col-sm-10">
													<input type="password" ng-if="notReq" id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.password" pattern=".{5,50}" maxlength="50" ng-change="confirmpass(user.conpass, user.password)" >
											
													<span class="label bg-info" ng-show="req">Password is Requred <br/></span>
													<input type="password" ng-if="req"  id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.password" pattern=".{5,50}" maxlength="50" ng-change="confirmpass(user.conpass, user.password)" required="required">
													<em class="text-muted">(5-50 length)</em>
												</div>
											</div>
											<div class="line line-dashed b-b line-lg pull-in"></div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Confirm Password</label>
												<div class="col-sm-10">
													<span class="label bg-danger" ng-show="pwdconfirm">Password did not match! <br/></span>
												
													<input type="password" ng-if="notReq" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass, user.password)" >
													<input type="password" ng-if="req" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass, user.password)" required="required">


													<span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span>
												</div>
											</div> -->
										</div>
									</div>
								</div>
								<div class="col-sm-8 panel panel-default" >
									<div class="panel-heading font-bold">
										User Profile
									</div>
									<div class="panel-body">

										<div class="form-group">
											<label class="col-sm-2 control-label">First Name</label>
											<div class="col-sm-10">
												<input type="text" id="fname" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" pattern=".{1,50}" maxlength="50">
											</div>
										</div>
										<div class="line line-dashed b-b line-lg pull-in"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Last Name</label>
											<div class="col-sm-10">
												<input type="text" id="lname" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname"  pattern=".{1,50}" maxlength="50">
											</div>
										</div>
										<div class="line line-dashed b-b line-lg pull-in"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Birth Date</label>
											<div class="col-sm-10">
												<div class="input-group w-md">
													<span class="input-group-btn">
														<input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
														<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
													</span>
												</div>
											</div>
										</div>
										<div class="line line-dashed b-b line-lg pull-in"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Gender</label>
											<div class="col-sm-10">
												<div class="radio">
													<label class="i-checks">
														<input id="gender" type="radio" name="gender" value="Male" ng-model="user.gender" >
														<i></i>
														Male
													</label>
												</div>
												<div class="radio">
													<label class="i-checks">
														<input id="gender" type="radio" name="gender" value="Female" ng-model="user.gender" >
														<i></i>
														Female
													</label>
												</div>
											</div>
										</div> 
										<div class="line line-dashed b-b line-lg pull-in"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">STATUS</label>
											<div class="col-sm-10">
												<div class="radio">
													<label class="i-checks">
														<input id="status" type="radio" name="status" value="1" ng-model="user.status" ng-value="user.status=stat">
														<i></i>
														Active User
													</label>
												</div>
												<div class="radio">
													<label class="i-checks">
														<input id="status" type="radio" name="status" value="0" ng-model="user.status">
														<i></i>
														Deactivate User
													</label>
												</div>
											</div> 
											<div class="line line-dashed b-b line-lg pull-in"></div>  
											<div class="form-group" >
												<div class="col-sm-4 col-sm-offset-2">
													<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
													<button id="submit" disabled="disabled"  type="submit" class="btn btn-success" ng-disabled="" scroll-to="Scrollup">Submit</button>
												</div>
												<div class="line line-dashed b-b line-lg pull-in"></div>  
												<div class="line line-dashed b-b line-lg pull-in"></div>  
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="panel panel-default">
										<div class="panel-heading font-bold">
											User Profile Picture
										</div>
										<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
										<div class="panel-body">
											<div class="input-group m-b">
												<span class="input-group-btn">
													<a class="btn btn-default"  ng-click="showprofileList('lg')">Select Image</a>
												</span>
												<!-- <input type="text" class="form-control" ng-value="user.banner = amazonpath " ng-model="user.banner" placeholder="{[{amazonpath}]}"  > -->
											
												<input id="banner" type="text" class="form-control" ng-value="user.banner = profilepath " ng-model="user.banner" placeholder="{[{profilepath}]}"  >
											</div>
										</div>
										<div class="panel-body" style="margin-top:-30px;">
											<img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{profilepath}]}">
										</div>
									</div>
								</div>
							</fieldset>
						</form>

					</div>
				</div>
			</tab>
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->	
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<!-- /////// -->
			<tab ng-controller="SchoolCtrl">
				<!-- <tab-heading><i class="fa fa-institution"></i> Schools</tab-heading> -->
				<div class="panel panel-default">
					<tabset class="tab-container"  >
						<tab>
							<tab-heading><i class="fa fa-institution"></i> School </tab-heading>

							<fieldset ng-disabled="isSaving">
								<div class="wrapper-md">
									<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
									<div class="row">
										<div class="col-sm-12">

											<div class="panel panel-default">
												<div class="panel-heading font-bold">
													<button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="addtags()"><i class="fa fa-plus" style="width=100%;"></i>Add School</button>
													School List
												</div>

												<div class="panel-body">
													<div class="row wrapper">
														<div class="col-sm-12">
															<div class="col-sm-6">
																<form name="rEQuired">
																	<div class="input-group">
																		<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
																		<span class="input-group-btn">
																			<button class="btn btn-sm btn-default" type="submit" ng-click="search(searchtext)" ng-disabled="rEQuired.$invalid">Go!</button>
																		</span>

																	</div>
																</form>
															</div>
															<div class="col-sm-6">
																<div class="btn-toolbar">
																	<div class="btn-group dropdown">
																	</div>
																	<div class="btn-group">
																		<button class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="table-responsive">
													<table class="table table-striped b-t b-light">
														<thead>
															<tr>
																<th style="width:80%">School Name</th>
																<th style="width:25%">Action</th>
															</tr>
														</thead>
														<tbody>
															<tr ng-repeat="mem in schooldata.data" >
																<td> <span editable-text="mem.name"  onbeforesave="updatetags($data, mem.id)" e-pattern="[a-zA-Z0-9'\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ mem.name }]}&nbsp;&nbsp;
																	<a ng-show="showflag == mem.id " class="btn btn-sm btn-icon btn-success"><i class="fa fa-check"></i></a>
																	<a ng-show="showflagerror == mem.id " class="bg-danger">Something went Wrong Seems like the Tag You Insert Already Exist</a>
																</span></td>
																<td>
																	<a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
																	<a href="" ng-click="tagsDelete(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
																	</a>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>

										</div>
									</div>
									<div class="row">
										<div class="panel-body">
											<footer class="panel-footer text-center bg-light lter">
												<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
											</footer>
										</div>
									</div>
								</div>
							</fieldset>
						</tab>
					</tabset>
				</div>
			</tab>



		</tabset>
	</div>