<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<script type="text/ng-template" id="authorimagelist.html">
  <div ng-include="'/be/tpl/authorimagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Add Author</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveAuthor(author)" name="formauthor">
  <div class="row">
    <div class="col-sm-8">
              <div class="panel panel-default">

                <div class="panel-heading font-bold">
                  Authors Information
                </div>
                  <div class="panel-body">
                    <label class="col-sm-2 control-label">
                      <label for="title">Name</label>
                    </label>
                    <div class="col-sm-10">
                      <input type="hidden" ng-model="author.userid" ng-value="author.userid = userid">
                      <input id="name" name="name" class="form-control ng-pristine ng-invalid ng-valid-pattern ng-untouched" ng-model="author.name" required="required" type="text">
                      
                      <div class="line line-dashed b-b line-lg"></div>
                    </div> 

                    <label class="col-sm-2 control-label">
                      <label for="title">Location</label>
                    </label>
                    <div class="col-sm-10">
                      <input id="location" name="location" class="form-control ng-pristine ng-invalid ng-valid-pattern ng-untouched" ng-model="author.location" required="required" type="text">
                      
                      <div class="line line-dashed b-b line-lg"></div>
                    </div> 

                    <label class="col-sm-2 control-label">
                      <label for="title">Occupation</label>
                    </label>
                    <div class="col-sm-10">
                      <input id="occupation" name="occupation" class="form-control ng-pristine ng-invalid ng-valid-pattern ng-untouched" ng-model="author.occupation" required="required" type="text">
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Author Since</label>
                        <div class="col-sm-10">
                          <div class="input-group w-md">
                              <span class="input-group-btn">
                                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="author.since" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                    <label class="col-sm-2 control-label">
                      <label for="title">About the Author</label>
                    </label>
                    <div class="col-sm-10">
                      <textarea class="ck-editor" ng-model="author.about" required="required"></textarea>
                    </div>
                    
                  </div>

              </div>
            </div>
            <!--   ///////////////////////////////////////////////////////      -->
  <div class="col-sm-4">
  <div class="panel panel-default"> 
    <div class="panel-heading font-bold">
              Image <div id="mydiv"><span class="label bg-success"></span></div>
    </div>
    <div class="panel-body">
        <div class="form-group">
                <div class="input-group m-b">
                <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
                </span>
                <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                <input type="text" class="form-control" ng-value="author.image = amazonpath " ng-model="author.image" placeholder="{[{amazonpath}]}" readonly required>
              </div>
                <div class="col-sm-12">
                <br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/authorimages/{[{amazonpath}]}" width="100%" height="300" alt="IMAGE PREVIEW">
                </div>
        </div>
    </div>
  </div>
  </div> 
 <!--   ///////////////////////////////////////////////////////      -->

  </div>
  <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a href="/dashboard" ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="formauthor.$invalid" scroll-to="Scrollup">Submit</button>
            </footer>
        </div>
  </div>
  </form>
</div>

  	     
