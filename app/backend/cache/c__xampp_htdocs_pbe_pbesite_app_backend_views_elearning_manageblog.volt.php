<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="categoryAdd.html">
	<div ng-include="'/be/tpl/categoryAdd1.html'"></div>
</script>
<script type="text/ng-template" id="tagsAdd.html">
	<div ng-include="'/be/tpl/tagsAdd.html'"></div>
</script>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Blog</h1>
  <a id="top"></a>
</div>
<div >
	<tabset class="tab-container">
		<tab>
			<tab-heading><i class="glyphicon glyphicon-th-list"></i> Manage Blogs</tab-heading>
			<div class="panel panel-default">
				<div class="panel-heading">
					Blog Lists
				</div>
				<div class="panel-body b-b b-light">
					Search: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
				</div>
				<div>
					<table class="table m-b-none" ui-jq="footable" data-filter="#filter" data-page-size="20">
						<thead>
							<tr>
								<th data-hide="phone,tablet">
									News Title
								</th>
								<th data-hide="phone,tablet">
									Author
								</th>
								<th data-hide="phone,tablet" >
									Date Publish
								</th>
								<th data-hide="phone">
									Status
								</th>
								<td >
								</td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="data in blog">
								<td>{[{data.title}]}</td>
								<td>{[{data.name}]}</a></td>
								<td>{[{data.publish}]}</td>
								<td data-value="1">
									<span ng-if="data.status == 1" class="label bg-success" title="Active">Active</span>
									<span ng-if="data.status == 0" class="label bg-success" title="Active">Deactivated</span>
								</td>
								<td data-value="1">
									<p>
										<a class="btn btn-sm btn-icon btn-warning" title="Edit" ui-sref="editblog({id:data.blogid})"><i class="glyphicon glyphicon-pencil"></i></a>
										<a class="btn btn-sm btn-icon btn-danger" ng-click="_delete(data.blogid)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
									</p>
								</td>
							</tr>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="10" class="text-center">
									<ul class="pagination"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</tab>
	
</tabset>
</div>