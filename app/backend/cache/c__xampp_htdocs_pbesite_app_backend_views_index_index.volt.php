<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('be/css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/app.css'); ?>
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
</head>

<body>
  <div class="container w-xxl w-auto-xs" >
  <a href class="navbar-brand block m-t">Power Brain Education</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <img src='/img/jack.png' style="width:100%;height:auto;">
    </div>
    <form name="form" class="form-validation" method="post">
      <div class="text-danger wrapper text-center" ng-show="authError">
          <?php echo $this->getContent(); ?>
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <?php echo $this->tag->textField(array('username', 'class' => 'form-control no-border', 'id' => 'inputUsername', 'placeholder' => 'Username', 'ng-model' => 'user.password', 'required' => 'required')); ?>
        </div>
        <div class="list-group-item">
          <?php echo $this->tag->passwordField(array('password', 'class' => 'form-control no-border', 'id' => 'inputPassword', 'placeholder' => 'Password', 'ng-model' => 'user.password', 'required' => 'required')); ?>
        </div>
      </div>
      <button id="login" type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a href="/pbeadmin/forgotpassword">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
</div>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('be/js/jquery/jquery.min.js'); ?>
<!-- angular -->
<?php echo $this->tag->javascriptInclude('be/js/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-bootstrap-tpls.min.js'); ?>

<!-- APP -->
 
</body>
</html>