<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">E-Learing</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm" >
  <div class="col w-md bg-light dk b-r bg-auto bg-auto-left">
  <!-- <div class="wrapper b-b bg">
      <button class="btn btn-sm btn-default pull-right visible-sm visible-xs" ui-toggle-class="show" target="#email-menu"><i class="fa fa-bars"></i></button>
      <a ui-sref="app.mail.compose" class="btn btn-sm btn-danger w-xs font-bold">Compose</a>
    </div> -->
    <div class="bg-light lter b-b wrapper-md">
    <h3 class="m-n font-thin h5">Menu</h3>
      <a id="top"></a>
    </div>
    <div class="wrapper hidden-sm hidden-xs" id="email-menu">

      <ul class="nav nav-pills nav-stacked nav-sm">

        <li ui-sref-active="active">
          <a ui-sref="elearning.user">
          <i class="glyphicon glyphicon-user icon"></i>
            <span style="margin-left:10px;">E-Learning  Community Users</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="elearning.training({idno:'training'})">
          <i class="icon-graduation"></i>
            <span style="margin-left:10px;">Classroom Activities</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="elearning.contlinks({idno:'contlinks'})">
             <i class="fa fa-chain"></i>
            <span style="margin-left:10px;">Content Links</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="elearning.egallery">
            <i class="glyphicon glyphicon-picture"></i>
            <span style="margin-left:10px;">Gallery Image</span>
          </a>
        </li>
         <li ui-sref-active="active">
          <a ui-sref="elearning.evideo">
            <i class="glyphicon glyphicon-film"></i>
            <span style="margin-left:10px;">Gallery Video</span>
          </a>
        </li>
        
        <li ui-sref-active="active">
          <a ui-sref="elearning.mentors({idno:'mentors'})">
             <i class="glyphicon glyphicon-user"></i>
            <span style="margin-left:10px;">Mentors</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="elearning.leaders({idno:'leaders'})">
              <i class="icon-users"></i>
            <span style="margin-left:10px;">Leaders</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="elearning.school">
              <i class="fa fa-institution"></i>
            <span style="margin-left:10px;">Schools</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="col">
    <div ui-view >
      
    </div>
  </div>
</div>