<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="scheduleedit.html">
	<div ng-include="'/be/tpl/scheduleedit.html'"></div>
</script>
<script type="text/ng-template" id="schedule.html">
	<div ng-include="'/be/tpl/schedule.html'"></div>
</script>

<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Center Class Schedule</h1>
	<a id="top"></a>
</div>
<div class="wrapper-md">

	<div class="panel panel-default">
		<div class="panel-heading">
			Responsive Table
		</div>
		<div class="col-md-3">
			<div class="input-group m-b">
				<input type="hidden" ng-model="userid" ng-value="userid = center.manager">
				<input type="hidden" ng-model="idcenter" ng-value="idcenter = center.centerid">
				<span class="input-group-addon"><i class="fa fa-plus"></i> Select Day</span>
				<select ng-model="selectday" class="form-control w-md g-pristine ng-invalid ng-invalid-required"  ng-change="schedmodal(selectday,idcenter,userid);selectday=''">
					<option value="monday">Monday</option>
					<option value="tuesday">Tuesday</option>
					<option value="wednesday">Wednesday</option>
					<option value="thursday">Thursday</option>
					<option value="friday">Friday</option>
					<option value="saturday">Saturday</option>
					<option value="sunday">Sunday</option>

				</select>
			</div>

		</div>
		<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
		<div class="table-responsive">
			<table class="table table-striped b-t b-light ">
				<thead class="bg-info" >
					<tr>
						<th style="text-align:left;">Monday</th>
						<th style="text-align:left;">Tuesday</th>
						<th style="text-align:left;">Wednesday</th>
						<th style="text-align:left;">Thursday</th>
						<th style="text-align:left;">Friday</th>
						<th style="text-align:left;">Saturday</th>
						<th style="text-align:left;">Sunday</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="margin-right:0px !important;">
							<!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('monday')"><i class="fa fa-plus"></i>Add Schedule</button> -->
							<ul class="list-group no-borders pull-in auto" style="padding:2px;">
								<li class="list-group-item" ng-repeat="data in sched | filter: 'monday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
									<div>
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
							<td>
								<!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('tuesday')"><i class="fa fa-plus"></i>Add Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'tuesday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>             
									</li>

								</ul>
							</td>
							<td>
								<!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('wednesday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'wednesday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
							<td>
								<!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('thursday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'thursday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
							<td>
								<!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('friday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'friday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
							<td>
								<!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('saturday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'saturday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
							<td>
								<!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('sunday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
								<ul class="list-group no-borders pull-in auto" style="padding:2px;">
									<li class="list-group-item" ng-repeat="data in sched | filter: 'sunday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
										<a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
										<a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
										<div class="clear">
											<div ng-if="data.type=='kids'">Kids Class</div>
											<div ng-if="data.type=='teen'">Teen Class</div>
											<div ng-if="data.type=='adult'">Adult Class</div>
											<small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
										</div>
									</li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<footer class="panel-footer">
				<!-- chat form -->
				<div class="pull-left inline">
					<p>
						<button class="btn btn-sm btn-icon btn-info">A</button> Adult &nbsp;&nbsp;&nbsp;
						<button class="btn btn-sm btn-icon btn-danger">K</button> Kids &nbsp;&nbsp;&nbsp;
						<button class="btn btn-sm btn-icon btn-success">T</button> Teens 
					</p>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</div>











