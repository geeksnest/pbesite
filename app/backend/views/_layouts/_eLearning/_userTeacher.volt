<form name="rEquired" >
	<div class="input-group">

		<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
		<span class="input-group-btn">

			<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
		</span>
	</div>
</form>
<div class="table-responsive">
	<table class="table table-striped b-t b-light">
		<thead>
			<tr>
				<th style="width:20px;">
					<label class="i-checks m-b-none">
						<input type="checkbox"><i></i>
					</label>
				</th>
				<th>Name</th><th>Email</th><th>Username</th><th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'">
				<td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
				<td>{[{ user.fname }]} {[{ user.lname }]}</td>
				<td>{[{ user.email }]}</td>
				<td>{[{ user.username }]}</td>
				<td ng-if="user.status == 1">
					<div>
						<span class="bg-success btn btn-xs" >Active
						</span>
					</div>
					<div >
						<label class="i-switch i-switch-md bg-info m-t-xs m-r">
							<input type="checkbox" 
							ng-click="changestatus(user.id,user.status)" checked>
							<i></i>
						</label>
					</div>
				</td>
				<td ng-if="user.status == 0">
					<div>
						<span class="bg-danger btn btn-xs" >Inactive
						</span>
					</div>
					<div>
						<label class="i-switch i-switch-md bg-info m-t-xs m-r">
							<input type="checkbox" 
							ng-click="changestatus(user.id,user.status)">
							<i></i>
						</label>
					</div>
				</td>

				<td>
					<button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>

					<button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<footer class="panel-footer">
	<div class="row">
		<div class="panel-body">
			<footer class="panel-footer text-center bg-light lter">
				<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
			</footer>
		</div>
	</div>
</footer>