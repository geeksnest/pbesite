{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="success.html">
  "<div ng-include="'/be/tpl/success.html'"></div>"
</script>
<script type="text/ng-template" id="imagelist.html">
  "<div ng-include="'/be/tpl/imagelist.html'"></div>"
</script>
<script type="text/ng-template" id="delete.html">
  "<div ng-include="'/be/tpl/delete.html'"></div>"
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>
<div ng-controller="AddUserCtrl" id="top">
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user)" name="form">
    <fieldset ng-disabled="isSaving">
      <div class="wrapper-md">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label class="col-lg-2 control-label">User Role</label>
                <div class="col-sm-10">
                  <div class="radio">
                    <label class="i-checks">
                      <input id="userrole" type="radio" name="userrole" value="Admin" ng-model="user.userrole" required="required" ng-click="_req(true)">
                      <i></i>
                      <label class="text-success">Administrator</label>  &nbsp~ Can access all the features in CMS including managing all content in the page, and creating user account, centers and other function and features in CMS.
                    </label>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div class="radio">
                    <label class="i-checks">
                      <input id="userrole"  type="radio" name="userrole" value="CS" ng-model="user.userrole" required="required" ng-click="_req(true)">
                      <i></i>
                      <label class="text-success">Center manager</label> ~ Can access on all the Center under him.
                    </label>
                  </div>
                  <!-- <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="userrole" value="teacher" ng-model="user.userrole" required="required" ng-click="_req(false)">
                      <i></i>
                      <label class="text-success">Teacher/Instructor</label> ~ Access Frontend
                    </label>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="userrole" value="mentor" ng-model="user.userrole" required="required" ng-click="_req(false)">
                      <i></i>
                      <label class="text-success">Mentor</label> ~ Access Frontend
                    </label>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="userrole" value="leader" ng-model="user.userrole" required="required" ng-click="_req(false)">
                      <i></i>
                      <label class="text-success">Leader</label> ~ Access Frontend
                    </label>
                  </div> -->
                </div>
              </div> 
              <div class="line line-dashed b-b line-lg pull-in"></div> 
              <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <span class="label bg-danger" ng-show="usrname">Username already taken. <br/></span>

                  <!-- NOT REQURED -->
                  <input id="username"  type="text" ng-if="notReq"  id="username" name="username" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" pattern=".{4,50}" maxlength="12" >
                  <!-- REQURED -->
                  <span class="label bg-info" ng-show="req">Username is Requred <br/></span>
                  <input id="username"  type="text" ng-if="req"  id="username" name="username" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" pattern=".{4,50}" maxlength="12" required="required">


                  <em class="text-muted">(allow 'a-zA-Z0-9', 4-12 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Email Address</label>
                <div class="col-sm-10">
                  <span class="label bg-danger" ng-show="usremail">Email already taken. <br/></span>
                  <input type="email" id="email" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required" pattern=".{5,200}" maxlength="200">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <!-- NOT REQURED -->
                  <input id="password" type="password" ng-if="notReq" id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.password" pattern=".{5,50}" maxlength="50" ng-change="confirmpass(user.conpass, user.password)" >
                  <!-- REQURED -->
                  <span class="label bg-info" ng-show="req">Password is Requred <br/></span>
                  <input id="password" type="password" ng-if="req"  id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.password" pattern=".{5,50}" maxlength="50" ng-change="confirmpass(user.conpass, user.password)" required="required">
                  <em class="text-muted">(5-50 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-10">
                 <span class="label bg-danger" ng-show="pwdconfirm">Password did not match! <br/></span>
                 <!-- NOT REQURED -->
                 <input type="password" ng-if="notReq" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass, user.password)" >
                 <!-- REQURED -->
                 <input type="password" ng-if="req" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass, user.password)" required="required">


                 <span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span>
               </div>
             </div>


           </div>
         </div>
       </div>
       <div class="col-sm-8 panel panel-default" >
        <div class="panel-heading font-bold">
          User Profile
        </div>
        <div class="panel-body">

          <div class="form-group">
            <label class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-10">
              <input id="fname" type="text" id="fname" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" pattern=".{1,50}" maxlength="50">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-10">
              <input id="lname" type="text" id="lname" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname"  pattern=".{1,50}" maxlength="50">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Birth Date</label>
            <div class="col-sm-10">
              <div class="input-group w-md">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Gender</label>
            <div class="col-sm-10">
              <div class="radio">
                <label class="i-checks">
                  <input id="gender" type="radio" name="gender" value="Male" ng-model="user.gender" >
                  <i></i>
                  Male
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input id="gender" type="radio" name="gender" value="Female" ng-model="user.gender" >
                  <i></i>
                  Female
                </label>
              </div>
            </div>
          </div> 
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">STATUS</label>
            <div class="col-sm-10">
              <div class="radio">
                <label class="i-checks">
                  <input id="status" type="radio" name="status" value="1" ng-model="user.status" ng-value="user.status=stat">
                  <i></i>
                  Active User
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input  id="status" type="radio" name="status" value="0" ng-model="user.status">
                  <i></i>
                  Deactivate User
                </label>
              </div>
            </div> 
            <div class="line line-dashed b-b line-lg pull-in"></div>  
            <div class="form-group">
              <div class="col-sm-4 col-sm-offset-2">
                <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
                <button  id="submit" disabled="disabled"  type="submit" class="btn btn-success" ng-disabled="form.$invalid || form.$pending || usrname==true || usremail==true || pwdconfirm==true" scroll-to="Scrollup">Submit</button>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>  
              <div class="line line-dashed b-b line-lg pull-in"></div>  
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
           User Profile Picture
         </div>
         <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
         <div class="panel-body">
           <div class="input-group m-b">
            <span class="input-group-btn">
              <a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
            </span>
            <input id="banner" type="text" class="form-control" ng-value="user.banner = amazonpath " ng-model="user.banner" placeholder="{[{amazonpath}]}"  >

          </div>
        </div>
        <div class="panel-body" style="margin-top:-30px;">
          <img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}">
        </div>
      </div>
    </div>
  </fieldset>
</form>

</div>
