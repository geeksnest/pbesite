{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="userDelete.html">
  <div ng-include="'/be/tpl/userDelete.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div> 
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1> 
  <a id="top"></a>
</div>
<tabset class="tab-container">
  <tab heading="Admin/Center Manager" ng-controller="UserListCtrl">
    <form name="rEquired" >
      <div class="input-group">
        <input id="searchtext" class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
        <span class="input-group-btn">
          <button id="refresh" type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
        </span>
      </div>
    </form>
    <div class="table-responsive">
     <table class="table table-striped b-t b-light">
      <thead>
        <tr>
          <th style="width:20px;">
            <label class="i-checks m-b-none">
              <input type="checkbox"><i></i>
            </label>
          </th>
          <th>Name</th>
          <th>Email</th>
          <th>Username</th>
          <th>Status</th>
          <th>User Role</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
          <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
          <td>{[{ user.fname }]} {[{ user.lname }]}</td>
          <td>{[{ user.email }]}</td>
          <td>{[{ user.username }]}</td>
          <td ng-if="user.status == 1">
            <div>
              <span class="bg-success btn btn-xs" >Active
              </span>
            </div>
            <div >
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input id="checkbox" type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
                <i></i>
              </label>
            </div>
          </td>
          <td ng-if="user.status == 0">
            <div>
              <span class="bg-danger btn btn-xs" >Inactive
              </span>
            </div>
            <div>
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input id="checkbox" type="checkbox" ng-click="changestatus(user.id,user.status)">
                <i></i>
              </label>
            </div>
          </td>
          <td>
            <span ng-if="user.userrole=='Admin'">Administrator</span>
            <span ng-if="user.userrole=='CS'">Center Manger</span></td>
            <td>
              <button id="update"  ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>

              <button id="delete"  ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-center bg-light lter">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
          </footer>
        </div>
      </div>
    </footer>
  </tab>


  <tab heading="Admin" ng-controller="UserListAdminCtrl">
     <form name="rEquired" >
      <div class="input-group">
        <input id="searchtext" class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
        <span class="input-group-btn">
          <button id="refresh" type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
        </span>
      </div>
    </form>
    <div class="table-responsive">
     <table class="table table-striped b-t b-light">
      <thead>
        <tr>
          <th style="width:20px;">
            <label class="i-checks m-b-none">
              <input type="checkbox"><i></i>
            </label>
          </th>
          <th>Name</th>
          <th>Email</th>
          <th>Username</th>
          <th>Status</th>
          <th>User Role</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="user in data.data " ng-if="user.username != 'superagent'" class="info">
          <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
          <td>{[{ user.fname }]} {[{ user.lname }]}</td>
          <td>{[{ user.email }]}</td>
          <td>{[{ user.username }]}</td>
          <td ng-if="user.status == 1">
            <div>
              <span class="bg-success btn btn-xs" >Active
              </span>
            </div>
            <div >
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input  id="checkbox"  type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
                <i></i>
              </label>
            </div>
          </td>
          <td ng-if="user.status == 0">
            <div>
              <span class="bg-danger btn btn-xs" >Inactive
              </span>
            </div>
            <div>
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input  id="checkbox" type="checkbox" ng-click="changestatus(user.id,user.status)">
                <i></i>
              </label>
            </div>
          </td>
          <td>
            <span ng-if="user.userrole=='Admin'">Administrator</span>
            <td>
              <button  id="update"  ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button  id="delete"  ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-center bg-light lter">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
          </footer>
        </div>
      </div>
    </footer>
  </tab>

  
  <tab heading="Center Manager" ng-controller="UserListManagerCtrl">
    <form name="rEquired" >
      <div class="input-group">
        <input id="searchtext" class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
        <span class="input-group-btn">
          <button id="refresh" type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
        </span>
      </div>
    </form>
    <div class="table-responsive">
     <table class="table table-striped b-t b-light">
      <thead>
        <tr>
          <th style="width:20px;">
            <label class="i-checks m-b-none">
              <input type="checkbox"><i></i>
            </label>
          </th>
          <th>Name</th>
          <th>Email</th>
          <th>Username</th>
          <th>Status</th>
          <th>User Role</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'" class="info">
          <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
          <td>{[{ user.fname }]} {[{ user.lname }]}</td>
          <td>{[{ user.email }]}</td>
          <td>{[{ user.username }]}</td>
          <td ng-if="user.status == 1">
            <div>
              <span class="bg-success btn btn-xs" >Active
              </span>
            </div>
            <div >
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input id="checkbox" type="checkbox" ng-click="changestatus(user.id,user.status)" checked>
                <i></i>
              </label>
            </div>
          </td>
          <td ng-if="user.status == 0">
            <div>
              <span class="bg-danger btn btn-xs" >Inactive
              </span>
            </div>
            <div>
              <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                <input id="checkbox" type="checkbox" ng-click="changestatus(user.id,user.status)">
                <i></i>
              </label>
            </div>
          </td>
          <td>
            <span ng-if="user.userrole=='CS'">Center Manger</span>
              
            </td>
             <td>
              <button  id="update"  ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button  id="delete"  ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-center bg-light lter">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
          </footer>
        </div>
      </div>
    </footer>
  </tab>

</tabset>