{{ content() }}
<script type="text/ng-template" id="addevent.html">
  <div ng-include="'/be/tpl/addevent.html'"></div>
</script>
<script type="text/ng-template" id="editevent.html">
  <div ng-include="'/be/tpl/editevent.html'"></div>
</script>
<script type="text/ng-template" id="editeventcal.html">
  <div ng-include="'/be/tpl/editeventcal.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>

<script type="text/ng-template" id="eventfe.html">
  <div ng-include="'/be/tpl/eventfe.html'"></div>
</script>



<!-- Calendar View -->
<div ng-controller="CalendarCtrl">
  <div ng-controller="viewcalendarCtrl">
    <div class="wrapper-md bg-light b-b">
      <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
        <i class="fa fa-bars"></i> List
      </button>
      <h1 class="m-n font-thin h3">Calendar of Activity </h1>
    </div>
    <div class="hbox hbox-auto-xs hbox-auto-sm">
      <div class="col wrapper-md">
        <div class="clearfix m-b">
          <a ui-sref="addevent" class="btn btn-sm btn-primary btn-addon"> <i class="fa fa-plus"></i>Add Event </a>
          <div class="pull-right">
            <button type="button" class="btn btn-sm btn-default" ng-click="today(calendar1)">today</button>
            <div class="btn-group m-l-xs">
              <button id="day" class="btn btn-sm btn-default" ng-click="changeView('agendaDay', 'calendar1')">Day</button>
              <button id="week" class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', 'calendar1')">Week</button>
              <button id="month"class="btn btn-sm btn-default" ng-click="changeView('month', 'calendar1')">Month</button>
            </div>
          </div>
        </div>




        <div class="pos-rlt">
          <div class="fc-overlay">
            <div class="panel bg-white b-a pos-rlt">
              <!-- <a class="pull-right text-xs text-muted" ng-click="deleteactivity(event.eventid)"><i class="fa fa-trash-o"></i></a> -->
              <a class="pull-right text-xs text-muted">&nbsp;</a>
              <a class="pull-right text-xs text-muted">&nbsp;</a>
              <a class="pull-right text-xs text-muted" ng-click="editEventModal('lg',event.centerid,event.eventid)"><i class="fa fa-edit"></i> Manage</a>

              <span class="arrow"></span>
              <div class="h4 font-thin m-b-sm">{[{event.title}]}</div>
              <div class="line b-b b-light"></div>
              <div><i class="icon-calendar text-muted m-r-xs"></i>Start on: {[{event.start._i | date:'longDate'}]} - {[{event.end._i | date:'longDate'}]}</div>
              <div class="ng-hide" ng-show='event.end'><i class="icon-clock text-muted m-r-xs"></i>Start at {[{ event.timestart }]} to {[{ event.endtime }]}</div>
              <i class="icon-pointer text-muted m-r-xs"></i>Location : {[{event.location}]}
              <div class="m-t-sm">{[{event.info}]}</div>
            </div>
          </div>
          <div id="calendar" class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
        </div>


      </div>
      <!-- begin list view -->
      <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
        <div class="wrapper" ng-if="adminls">
          <div ng-repeat="data in dataCalendar" class="bg-white-only r r-2x m-b-xs wrapper-sm b-l b-2x b-primary">          
            <input id="title" ng-model="data.title" class="form-control m-t-n-xs no-border no-padder no-bg">
            <a class="pull-right text-xs text-muted" ng-click="deleteactivity(data.eventid)"><i class="fa fa-trash-o"></i></a>
            <a class="pull-right text-xs text-muted">&nbsp;</a>
            <a class="pull-right text-xs text-muted">&nbsp;</a>
            <a class="pull-right text-xs text-muted" ng-click="editEventModal('lg','<?=$username['centerid']?>',data.eventid)"><i class="fa fa-edit"></i></a>
            <div class="text-xs text-muted">{[{data.datefrom | date:'longDate'}]} to {[{data.dateto | date:'longDate'}]}
              <br>Start Time : {[{ data.time | date:'shortTime'}]}</div>
            </div>
          </div>
          <div class="wrapper" ng-if="csls">
            <div ng-repeat="data in dataCalendar" class="bg-white-only r r-2x m-b-xs wrapper-sm b-l b-2x b-primary">          
              <input id="title" ng-model="data.title" class="form-control m-t-n-xs no-border no-padder no-bg">
              <div ng-if="data.centerid == idcenter">
                <a class="pull-right text-xs text-muted" ng-click="deleteactivity(data.eventid)"><i class="fa fa-trash-o"></i></a>
                <a class="pull-right text-xs text-muted">&nbsp;</a>
                <a class="pull-right text-xs text-muted">&nbsp;</a>
                <a class="pull-right text-xs text-muted" ng-click="editEventModal('lg','<?=$username['centerid']?>',data.eventid)"><i class="fa fa-edit"></i></a>
              </div>
              <div class="text-xs text-muted">{[{data.datefrom | date:'longDate'}]} to {[{data.dateto | date:'longDate'}]}
                <br>Start Time : {[{ data.time | date:'shortTime'}]}
              </div>
            </div>
          </div>
        </div>
        <!-- end list view -->

      </div>

    </div>
  </div>
