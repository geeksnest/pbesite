{{ content() }}
<script type="text/ng-template" id="addevent.html">
  <div ng-include="'/be/tpl/addevent.html'"></div>
</script>
<script type="text/ng-template" id="editevent.html">
  <div ng-include="'/be/tpl/editevent.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="eventfe.html">
  <div ng-include="'/be/tpl/eventfe.html'"></div>
</script>
<!-- Calendar View -->
<div ng-controller="CalendarCtrl">

  <div ng-controller="viewcalendarCtrl">
    <div class="wrapper-md bg-light b-b">
      <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
        <i class="fa fa-bars"></i> List
      </button>
      <h1 class="m-n font-thin h3">Calendar of Activity </h1>
    </div>

    <div class="hbox hbox-auto-xs hbox-auto-sm">

      <div class="col wrapper-md">

        <div class="clearfix m-b">
         <div class="pull-left">
         <!--  <button type="button" class="btn btn-sm btn-default" ng-click="addEventModal('lg','<?=$username['centerid']?>','newsid')">Add New Event</button> -->
          <a ui-sref="addevent" class="btn btn-default"> Add Event </a>

        </div>

        <div class="row" id="demo">
          <div class="col-md-12 ">
            <h2 class="text-center">{[{ calendarTitle }]}</h2>

            <div class="row">

              <div class="col-md-6 text-center">
                <div class="btn-group">
                  <button id="previous"class="btn btn-primary" mwl-date-modifier date="calendarDay" decrement="calendarView">Previous</button>
                  <button id="today" class="btn btn-default" mwl-date-modifier date="calendarDay"  set-to-today> Today </button>
                  <button id="next" class="btn btn-primary" mwl-date-modifier date="calendarDay" increment="calendarView"> Next </button>
                </div>
              </div>

              <br class="visible-xs visible-sm">

              <div class="col-md-6 text-center">
                <div class="btn-group">
                  <label id="year" class="btn btn-primary" ng-model="calendarView" btn-radio="'year'">Year</label>
                  <label id="month" class="btn btn-primary" ng-model="calendarView" btn-radio="'month'">Month</label>
                  <label id="week" class="btn btn-primary" ng-model="calendarView" btn-radio="'week'">Week</label>
                  <label id="day" class="btn btn-primary" ng-model="calendarView" btn-radio="'day'">Day</label>
                </div>
              </div>

            </div>

            <br>

            <mwl-calendar
            events="events"
            view="calendarView"
            view-title="calendarTitle"
            current-day="calendarDay"
            on-event-click="eventClicked(calendarEvent)"
            on-event-drop="eventDropped(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" 
            on-edit-event-click="eventEdited(calendarEvent)"
            on-delete-event-click="eventDeleted(calendarEvent)"
            auto-open="true"
            day-view-start="06:00"
            day-view-end="22:00"
            day-view-split="30">
          </mwl-calendar>

          <br><br><br>

          
        </div>
      </div>

    </div>         


  </div>

  <!-- begin list view -->
  <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
    <div class="wrapper">
      <div ng-repeat="data in dataCalendar" class="bg-white-only r r-2x m-b-xs wrapper-sm b-l b-2x b-primary">          
        <input id="title" ng-model="data.title" class="form-control m-t-n-xs no-border no-padder no-bg">
        <a class="pull-right text-xs text-muted" ng-click="deleteactivity(data.eventid)"><i class="fa fa-trash-o"></i></a>
        <a class="pull-right text-xs text-muted">&nbsp;</a>
        <a class="pull-right text-xs text-muted">&nbsp;</a>

        <a class="pull-right text-xs text-muted" ng-click="editEventModal('lg','<?=$username['centerid']?>',data.eventid)"><i class="fa fa-edit"></i></a>
        <div class="text-xs text-muted">{[{data.datefrom}]} - {[{data.dateto}]}</div>
      </div>
    </div>
  </div>
  <!-- end list view -->

</div>

</div>
</div>
