{{ content() }}
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Center Map Location</h1>
  <a id="top"></a>
</div>
  <script type="text/ng-template" id="searchbox.tpl.html">
        <input id="search" type="text" placeholder="Search Box">
    </script>
<div id="map_canvas">
  <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
   <ui-gmap-search-box template="searchbox.template" events="searchbox.events"></ui-gmap-search-box>


   
  <ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
</ui-gmap-marker>
</ui-gmap-google-map>

<div ng-cloak>
  <ul>
    <li>coords update ctr: {[{coordsUpdates}]}</li>
    <li>dynamic move ctr: {[{dynamicMoveCtr}]}</li>
  </ul>
</div>
</div>
<style type="text/css">
  html, body, #map_canvas {
    height: 500px;
    width: 100%;
    margin: 0px;
  }

  #map_canvas {
    position: relative;
  }

  .angular-google-map-container {
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
  }
  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    margin-top: :20px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }
</style>






