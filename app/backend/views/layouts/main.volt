<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  {{ stylesheet_link('be/css/custom.css') }}
  {{ stylesheet_link('fe/css/powerbrain_inner.css') }}
  {{ stylesheet_link('vendors/ng-responsive-calendar/dist/css/calendar.min.css') }}
  {{ stylesheet_link('vendors/angular-ui-select/dist/select.css') }}

   <!-- Color Picker -->
  {{ stylesheet_link('vendors/angular-bootstrap-colorpicker/css/colorpicker.css') }}

  <!-- WORKING -->
  {{ stylesheet_link('globaljs/angular-xeditable/dist/css/xeditable.css') }}

  <!-- include the css and sprite -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css">
  <link rel="image_src" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen-sprite.png">

{{ stylesheet_link('vendors/fullcalendar/dist/fullcalendar.css') }}

  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]<link rel="shortcut icon" href="img/frontend/logo.png" type='image/x-icon'/>-->
  <script>
    var usertask = "<?=$username['task']?>";
  </script>
  <script type="text/ng-template" id="requestReview.html">
    "<div ng-include="'/be/tpl/requestReview.html'"></div>"
  </script>
  <script type="text/ng-template" id="appointmentReview.html">
    "<div ng-include="'/be/tpl/appointmentReview.html'"></div>"
  </script>

  <?php 
  if($username['task'] == "CS"){
    ?>
    <script>
      var centerid = "<?php echo $username['centerid']; ?>";
      var userid = "<?php echo $username['pi_id']; ?>";
      var centeridz = "<?php echo $username['centerid']; ?>";
      var centertitle ="<?php echo $username['centertitle']; ?>";
    </script>
    <?php }else{ ?>
    <script>
      var centerid = undefined;
      var centeridz =undefined;
      var centertitle=undefined;
      var userid = "<?php echo $username['pi_id']; ?>";
    </script>
    <?php } ?>
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  </head>
  <body ng-controller="AppCtrl">
    {{ content() }}
    <div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
      <div class="app-header navbar">
        <!-- navbar header -->
        <div class="navbar-header bg-white">
          <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
            <i class="glyphicon glyphicon-cog"></i>
          </button>
          <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
            <i class="glyphicon glyphicon-align-justify"></i>
          </button>
          <!-- brand -->
          <a href="#/" class="navbar-brand text-lt">
           <img src="/img/logo.jpg" alt=".">
           <span class="hidden-folded m-l-xs">PBE</span>
         </a>
         <!-- / brand -->
       </div>
       <!-- / navbar header -->

       <!-- navbar collapse -->
       <div class="collapse navbar-collapse box-shadow {[{app.settings.navbarCollapseColor}]}">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <!-- <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a> -->
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
        <!-- <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="#">
                  <span class="badge bg-info pull-right">5</span>
                  <span translate="header.navbar.new.NEWS">Task</span>
                </a>
              </li>
              <li><a href="#" translate="header.navbar.new.BLOG">User</a></li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.PAGE">Email</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.USER">Email</span>
                </a>
              </li>
            </ul>
          </li>
        </ul> -->
        <!-- / link and dropdown -->

        <!-- search form -->
        <!-- <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form> -->
        <!-- / search form -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown" ng-controller="NewmessageCtrl" >
            <a href class="dropdown-toggle" ng-click="removeclass()">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span id="notif" class="badge badge-sm up bg-danger pull-right-xs" ng-show="hideme" ng-bind="count"></span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>You have <span>{[{ count }]}</span> Unread notifications</strong>
                </div>
                <div ui-jq="slimScroll" ui-options="{height:'300px', size:'7px'}">
                  <div class="list-group">
                    <div ng-repeat="list in notiflist | limitTo : limit">

                    <a ng-if="list.type==1 && list.status==0" ui-sref="managerequestinfo" class="media list-group-item d8fbf8" ng-click="reviewmodal(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>
                      <a ng-if="list.type==0 && list.status==0" ui-sref="manageappointment" class="media list-group-item d8fbf8" ng-click="reviewmodal1(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>

                      <a ng-if="list.type==1 && list.status==1" ui-sref="managerequestinfo" class="media list-group-item" ng-click="reviewmodal(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>
                      <a ng-if="list.type==0 && list.status==1" ui-sref="manageappointment" class="media list-group-item" ng-click="reviewmodal1(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>

                      <a ng-if="list.type==1 && list.status==2" ui-sref="managerequestinfo" class="media list-group-item" ng-click="reviewmodal(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>
                      <a ng-if="list.type==0 && list.status==2" ui-sref="manageappointment" class="media list-group-item" ng-click="reviewmodal1(list.id)">
                        <span class="pull-left thumb-sm">
                          <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                        </span>
                        <span class="media-body block m-b-none">
                          {[{ list.fname }]} {[{ list.lname }]}<br>
                          <small class="text-muted">{[{ list.timeago }]}</small>
                        </span>
                      </a>

                    </div>
                    <a class="media list-group-item text-muted" ng-click="incrementLimit()" ng-show="showmore">
                        See more
                    </a>
                  </div>
                </div>
                <div class="panel-footer text-sm">
                  <a ui-sref="notifications" class="pull-right"><i class="fa fa-list-ol"></i></a>
                  <a ui-sref="notifications" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm  ">
                <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $username['profile']; ?>" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['pi_username']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
              </li>
              <?php  if($username['task'] != "CS"){ ?>
              <li>
                <a ui-sref="settings">
                  <span class="badge bg-danger pull-right">30%</span>
                  <span>Settings</span>
                </a>
              </li>
              <?php } ?>
              <li>
                <a ui-sref="editprofile"><span translate="header.navbar.profile.UPDATE"></span></a>
              </li>
              <li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="/pbeadmin/admin/logout">Logout</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
         <?php 
        if($username['task'] == "CS"){
          ?>
        <div class="nav navbar-nav m-l-sm navbar-right">
          <a href class="btn no-shadow navbar-btn">
            <span class="hidden-folded m-l-xs font-bold"><?php echo $username['centertitle']; ?></span>
          </a>
        </div>
        <?php } ?>

      </div>
      <!-- / navbar collapse -->
    </div>

    <!-- menu -->
    <div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
      <div class="aside-wrap">
        <div class="navi-wrap">
          <!-- user -->
          <div class="clearfix hidden-xs text-center hide" id="aside-user">
            <div class="dropdown wrapper">
              <a ui-sref="app.page.profile">
                <span class="thumb-lg w-auto-folded avatar m-t-sm">
                  <img src="/img/a0.jpg" class="img-full" alt="...">
                </span>
              </a>
              <a href class="dropdown-toggle hidden-folded">
                <span class="clear">
                  <span class="block m-t-sm">
                    <strong class="font-bold text-lt">John.Smith</strong> 
                    <b class="caret"></b>
                  </span>
                  <span class="text-muted text-xs block">Art Director</span>
                </span>
              </a>
              <!-- dropdown -->
              <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
                  <span class="arrow top hidden-folded arrow-info"></span>
                  <div>
                    <p>300mb of 500mb used</p>
                  </div>
                  <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
                </li>
                <li>
                  <a href>Settings</a>
                </li>
                <li>
                  <a ui-sref="app.page.profile">Profile</a>
                </li>
                <li>
                  <a href>
                    <span class="badge bg-danger pull-right">3</span>
                    Notifications
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a ui-sref="logout"> Logout</a>
                </li>
              </ul>
              <!-- / dropdown -->
            </div>
            <div class="line dk hidden-folded"></div>
          </div>
          <!-- / user -->

          <!-- nav -->
          <nav ui-nav class="navi">
            <!-- first -->
            <ul class="nav">
              <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                <span translate="aside.nav.HEADER">Navigation</span>
              </li>
              <li>
                <a ui-sref="dashboard">
                  <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                  <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
                </a>
              </li>

              <?php 
              if($username['task']!="CS"){
                ?>
                <li ng-class="{active:$state.includes('app.users')}">
                  <a href class="auto">
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="glyphicon glyphicon-user icon"></i>
                    <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
                  </a>
                  <ul class="nav nav-sub dk">
                    <li ui-sref-active="active">
                      <a ui-sref="userlist">
                        <span translate="aside.nav.users.USER_LIST">User List</span>
                      </a>
                    </li>
                    <li ui-sref-active="active">
                      <a ui-sref="userscreate">
                        <span translate="aside.nav.users.CREATE_USER">Create User</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li ng-class="{active:$state.includes('app.pages')}">
                  <a href class="auto">
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="glyphicon glyphicon-th-large icon text-success"></i>
                    <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
                  </a>
                  <ul class="nav nav-sub dk">
                    <li ui-sref-active="active">
                      <a ui-sref="createpage">
                        <span translate="aside.nav.pages.CREATE_PAGE">Create Page</span>
                      </a>
                    </li>
                    <li ui-sref-active="active">
                      <a ui-sref="managepage">
                        <span translate="aside.nav.pages.MANAGE_PAGE">Manage Page</span>
                      </a>
                    </li>
                     <li ui-sref-active="active">
                      <a ui-sref="pagebanner">
                        <span translate="aside.nav.pages.PAGEBANNER">Static Page</span>
                      </a>
                    </li>
                     <li ui-sref-active="active">
                      <a ui-sref="statpage.home">
                        <span translate="aside.nav.pages.STAT_PAGE">Manage Static Pages</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <?php
              }   
              ?>
              <?php 
              if($username['task']!="CS"){
                ?>
                <li>
                  <a ui-sref="addimageslider">
                    <i class="icon-control-forward"></i>
                    <span translate="aside.nav.mainpageslider.ADDIMAGESLIDER"></span>
                  </a>
                </li>
                 <li ng-class="{active:$state.includes('app.pages')}">
                  <a href class="auto">
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="glyphicon glyphicon-th-large icon text-success"></i>
                    <span class="font-bold" translate="aside.nav.blogpage.BLOG">BLOG</span>
                  </a>
                  <ul class="nav nav-sub dk">
                    <li ui-sref-active="active">
                      <a ui-sref="blog">
                        <span translate="aside.nav.blogpage.CREATEBLOG">Create Blog</span>
                      </a>
                    </li>
                    <li ui-sref-active="active">
                      <a ui-sref="manageblog">
                        <span translate="aside.nav.blogpage.MANAGEBLOG">Manage Blog</span>
                      </a>
                    </li>
                    <li ui-sref-active="active">
                      <a ui-sref="blogcategories">
                        <span translate="aside.nav.blogpage.BLOGCATEG">Blog Category</span>
                      </a>
                    </li>
                    <li ui-sref-active="active">
                      <a ui-sref="blogtags">
                        <span translate="aside.nav.blogpage.BLOGTAG">Blog Tags</span>
                      </a>
                    </li>
                  </ul>
                </li>
                  <li>
                <a ui-sref="elearning">
                  <i class="glyphicon glyphicon-book"></i>
                  <span translate="aside.nav.blogpage.ELEARNING">ELEARNING</span>
                </a>
              </li>
                <?php
              }   
              ?>



  <!-- <li ng-class="{active:$state.includes('app.newsletter')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-envelope icon"></i>
      <span class="font-bold" translate="aside.nav.newsletter.NEWSLETTER">Newsletter</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.newsletter.CREATE_NEWSLETTER">Create Newsletter</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.MANAGE_NEWSLETTER">Manage Newsletter</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.ADD_SUBSCRIBERS">Add Subscribers</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.SUBSCRIBERS_LIST">Subscribers List</span>
        </a>
      </li>
    </ul>
  </li> -->
  <!-- <li ng-class="{active:$state.includes('app.story')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-bullhorn icon"></i>
      <span class="font-bold" translate="aside.nav.story.STORY">Success Stories</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.story.MANAGE_STORY">Manage Stories</span>
        </a>
      </li>
    </ul>
  </li>
  <li class="line dk"></li> -->

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.centerpage.CENTER">Center</span>
  </li>
  <?php 
  if($username['task']!="CS"){
    ?>
    <li ng-class="{active:$state.includes('app.slider')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="glyphicon glyphicon-home"></i>
        <span translate="aside.nav.centerpage.centerslider.CENTER">Center</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="createcenter">
            <span translate="aside.nav.centerpage.centerslider.CREATESCENTER">Create Center</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="managecenter">
            <span translate="aside.nav.centerpage.centerslider.MANAGECENTER">Manage CENTER</span>
          </a>
        </li>
      </ul>
    </li>
    <?php
  }   
  ?>

  <li ng-class="{active:$state.includes('app.income')}">
    <a href class="auto">      
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <!-- <b class="label bg-primary pull-right">2</b> -->
      <i class="glyphicon glyphicon-th-list"></i>
      <span translate="aside.nav.centerpage.centernews.CENTERNEWS">Center NEWs</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centernews">
          <span translate="aside.nav.centerpage.centernews.CREATECENTERNEWS">Create Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managecenternews">
          <span translate="aside.nav.centerpage.centernews.MANAGECENTERNEWS">Manage Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="newstags">
          <span translate="aside.nav.centerpage.centernews.NEWSTAGS">News Tags</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="newscategory">
          <span translate="aside.nav.centerpage.centernews.NEWSCATEGORY">News Category</span>
        </a>
      </li>
    </ul>
  </li>
  <li>
    <a ui-sref="centerslider">
      <i class="icon-control-forward"></i>
      <span translate="aside.nav.centerslider.ADDIMAGESLIDER">Manage Slider</span>
    </a>
  </li>

  <?php 
  if($username['task']=="CS"){
    ?>
    <li>
      <a ui-sref="directormsg">
        <i class="fa fa-user"></i>
        <!-- <b class="badge bg-success dk pull-right">30%</b> -->
        <span translate="aside.nav.dirctors_msg.DRCTORS_MSG">Directors Message</span>
      </a>
    </li>
    <?php
  }   
  ?>
  <!--  <li>
    <a ui-sref="centerdetails">
      <i class="icon-list"></i>
      <span translate="aside.nav.centerdetail.DETAILS">Center Details/span>
    </a>
  </li> -->
    <?php 
      if($username['task']=="CS"){
        ?>
  <li ng-class="{active:$state.includes('app.income')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="icon-list"></i>
      <span translate="aside.nav.centerdetails.CENTERDETAILS">Center Details</span>
    </a>
    <ul class="nav nav-sub dk">
      <li >
        <a ui-sref="centerschedule">
          <span translate="aside.nav.centerdetails.HOURS">Class Schedule</span>
        </a>
      </li>
      <li >
        <a ui-sref="centerevents">
          <span translate="aside.nav.centerdetails.EVENTCALLENDAR">Event Calendar</span>
        </a>
      </li>
      <li >
          <a ui-sref="centermap">
            <span translate="aside.nav.centerdetails.LOCATION">Map & Direction</span>
          </a>
        </li>
    </ul>
  </li>
      <?php
  } else{
  ?>
    <li>
      <a ui-sref="centerevents">
        <i class="glyphicon glyphicon-tasks icon"></i>
        <span translate="aside.nav.centerdetails.EVENTCALLENDAR">Event Calendar</span>
      </a>
    </li>
  <?php
    }   
  ?>


  <?php 
  if($username['task']!="CS"){
    ?>
    <li ng-class="{active:$state.includes('app.income')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="fa fa-file-text-o"></i>
        <!-- <b class="badge bg-success dk pull-right">30%</b> -->
        <span translate="aside.nav.testimonials.TESTIMONIALS">Testimonial</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref="createtestimonials">
          <a href>
            <span translate="aside.nav.testimonials.CREATETESTIMONIALS"></span>
          </a>
        </li>
        <li ui-sref="managetestimonials">
          <a href>
            <span translate="aside.nav.testimonials.MANAGETESTIMONIALS"></span>
          </a>
        </li>
      </ul>
    </li>
    <?php
  }   
  ?>
    <li>
      <a ui-sref="managerequestinfo">
        <i class="icon-envelope-letter"></i>
        <span translate="aside.nav.requestinfo.MANAGEREQUESTINFO"></span>
      </a>
    </li>

    <li>
      <a ui-sref="manageappointment">
        <i class="icon-bubble"></i>
        <span translate="aside.nav.appointment.MANAGEAPPOINTMENT"></span>
      </a>
    </li>
  <li class="line dk hidden-folded"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">          
    <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
  </li> 
  <li ng-class="{active:$state.includes('app.income')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="icon-users"></i>
      <span translate="aside.nav.author.AUTHOR">Author</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref="createauthor">
        <a href>
          <span translate="aside.nav.author.CREATEAUTHOR"></span>
        </a>
      </li>
      <li ui-sref="manageauthor">
        <a href>
          <span translate="aside.nav.author.MANAGEAUTHOR"></span>
        </a>
      </li>
    </ul>
  </li> 
  <?php 
  if($username['task']=="CS"){
    ?>
  <li>
    <a ui-sref="managemycenter">
      <i class="glyphicon glyphicon-tasks icon"></i>
      <span translate="aside.nav.your_stuff.MANAGE">Manage Center</span>
    </a>
  </li>
  <?php
  }   
  ?>
<!--   <li>
    <a ui-sref="elearning">
      <i class="glyphicon glyphicon-book"></i>
      <span translate="aside.nav.your_stuff.ELEARNING">ELEARNING</span>
    </a>
  </li> -->
  <?php 
  if($username['task']!="CS"){
    ?>
  <li>
    <a ui-sref="auditlogs">
      <i class="glyphicon glyphicon-tasks icon"></i>
      <span translate="aside.nav.your_stuff.LOGS">LOGS</span>
    </a>
  </li>
    <li>
      <a ui-sref="settings">
        <i class="glyphicon glyphicon-cog icon"></i>
        <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
      </a>
    </li>
    <?php
  }   
  ?>
  

</ul>
<!-- / third -->

</nav>
<!-- nav -->

<!-- aside footer -->
<!-- <div class="wrapper m-t">
  <div class="text-center-folded">
    <span class="pull-right pull-none-folded">60%</span>
    <span class="hidden-folded" translate="aside.MEMORY">Milestone</span>
  </div>
  <progressbar value="60" class="progress-xxs m-t-sm dk" type="info"></progressbar>
  <div class="text-center-folded">
    <span class="pull-right pull-none-folded">35%</span>
    <span class="hidden-folded" translate="aside.DISK">Release</span>
  </div>
  <progressbar value="35" class="progress-xxs m-t-sm dk" type="primary"></progressbar>
</div> -->
<!-- / aside footer -->
</div>
</div>
</div>
<!-- / menu -->
<!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>    
  </div>
<!-- /content -->
<!-- aside right -->
<!-- <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
  <div class="vbox">
    <div class="wrapper b-b b-light m-b">
      <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
      Chat
    </div>
    <div class="row-row">
      <div class="cell">
        <div class="cell-inner padder">
          <!-- chat list -->
          <!-- <div class="m-b">
            <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
            <div class="clear">
              <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                <span class="arrow left pull-up"></span>
                <p class="m-b-none">Hi John, What's up...</p>
              </div>
              <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
            </div>
          </div>
          <div class="m-b">
            <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
            <div class="clear">
              <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                <span class="arrow right pull-up arrow-light"></span>
                <p class="m-b-none">Lorem ipsum dolor :)</p>
              </div>
              <small class="text-muted">1 minutes ago</small>
            </div>
          </div>
          <div class="m-b">
            <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
            <div class="clear">
              <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                <span class="arrow left pull-up"></span>
                <p class="m-b-none">Great!</p>
              </div>
              <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
            </div>
          </div>
          <!-- / chat list -->
        <!-- </div>
      </div>
    </div>
    <div class="wrapper m-t b-t b-light">
      <form class="m-b-none">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Say something">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">SEND</button>
          </span>
        </div>
      </form>
    </div>
  </div>
</div> --> 
<!-- / aside right -->

<!-- footer -->
<div class="app-footer wrapper b-t bg-light">
  <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
  &copy; 2015 Copyright.
</div>
<!-- / footer -->

</div>
<!-- JS -->
{{ javascript_include('vendors/jquery/dist/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('vendors/angular/angular.min.js') }}
{{ javascript_include('vendors/angular-cookies/angular-cookies.min.js') }}
{{ javascript_include('vendors/angular-animate/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}

{{ javascript_include('vendors/angular-sanitize/angular-sanitize.min.js') }}





<!-- map -->
{{ javascript_include('vendors/angular-google-maps/dist/angular-google-maps.min.js') }}
{{ javascript_include('vendors/angular-google-maps/dist/angular-google-maps_dev_mapped.js') }}
{{ javascript_include('vendors/lodash/lodash.min.js') }}

<script src='http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&v=3.19'></script>
<!-- <script src='//maps.googleapis.com/maps/api/js?sensor=false'></script> -->

<script src="/vendors/moment/moment.js"></script>
<script src="/vendors/angular-moment/angular-moment.js"></script>
{{ javascript_include('be/js/jquery/jquery.maskedinput.js') }}




<!-- APP -->
{{ javascript_include('be/js/scripts/app.js') }}
{{ javascript_include('be/js/scripts/factory/factory.js') }}
{{ javascript_include('be/js/scripts/controllers/controllers.js') }}
{{ javascript_include('be/js/scripts/directives/directives.js') }}
{{ javascript_include('be/js/scripts/config.js') }}

<!-- CKeditor -->
{{ javascript_include('be/js/ckeditor/ckeditor.js') }}
{{ javascript_include('be/js/ckeditor/styles.js') }}

<!-- Page Controllers -->
{{ javascript_include('be/js/scripts/controllers/page/createpage.js') }}
{{ javascript_include('be/js/scripts/controllers/page/managepage.js') }}
{{ javascript_include('be/js/scripts/controllers/page/editpage.js') }}
{{ javascript_include('be/js/scripts/factory/page/managepage.js') }}


{{ javascript_include('be/js/scripts/controllers/scheduleappointment/manageappointment.js') }}
{{ javascript_include('be/js/scripts/factory/scheduleappointment/manageappointment.js') }}

{{ javascript_include('be/js/scripts/controllers/requestinformation/managerequestinfo.js') }}
{{ javascript_include('be/js/scripts/factory/requestinformation/managerequestinfo.js') }}


<!-- TIME -->
{{ javascript_include('globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js') }}

<!-- CALENDAR -->
{{ javascript_include('vendors/angular-jquery-timepicker/src/timepickerdirective.js') }}


<!-- ui-calendar-->
{{ javascript_include('vendors/angular-ui-calendar/src/calendar.js') }}
{{ javascript_include('vendors/fullcalendar/dist/fullcalendar.min.js') }}
{{ javascript_include('vendors/fullcalendar/dist/gcal.js') }}


<!-- ui-select-->
{{ javascript_include('vendors/angular-ui-select/dist/select.min.js') }}


<!-- xeditable -->
{{ javascript_include('globaljs/angular-xeditable/dist/js/xeditable.js') }}

<!-- include the js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
{{ javascript_include('be/js/jquery/angular-chosen.min.js') }} 



<!-- News Controllers -->
{{ javascript_include('be/js/scripts/controllers/news/createnews.js') }}
{{ javascript_include('be/js/scripts/factory/news/createnews.js') }}


<!-- other plugins -->
{{ javascript_include('vendors/ng-file-upload/ng-file-upload.min.js') }}
{{ javascript_include('vendors/ng-file-upload/ng-file-upload-shim.js') }}

{{ javascript_include('//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js') }}

<!-- Color Picker -->
{{ javascript_include('vendors/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js') }}
</body>
</html>