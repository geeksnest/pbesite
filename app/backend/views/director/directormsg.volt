{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Directors Message</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(msg)" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information 
            </div>
            <div class="panel-body">
              <input id="centerid"  type="hidden"  ng-init="msg.centerid='<?php echo $username['centerid']; ?>'" ng-model="msg.centerid" >
              <div class="form-group col-sm-12">
                Directors Information (Optional)
                <textarea id="info"  class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="msg.info">                  
                </textarea>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
               <div class="form-group col-sm-12">
                 Message
                <textarea id="message"  class="ck-editor" ng-model="msg.message" ></textarea>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="panel-body">
        <footer class="panel-footer  bg-light lter">
         
          <div class="pull-right">
           <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
           <button  id="submit"type="submit" class="btn btn-success" ng-disabled="formpage.$invalid" scroll-to="Scrollup">Save</button>
         </div>
         <div style="clear:both;"></div>
       </footer>
     </div>
   </div>
 </div>
</fieldset>
</form>