{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Update News</h1>
	<a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(news)" name="formpage">
	<fieldset ng-disabled="isSaving">
		<div class="wrapper-md">
			<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
			<div class="row">
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Information
						</div>
						<div class="panel-body ">
							<input  type="hidden" ng-value="news.newsid = newsid"   ng-model="news.newsid" >
							<div class="form-group col-sm-12">
								<label>News Title</label> <span class="label bg-danger" ng-show="notification">Ooopss! News Title already Taken <br/></span>
								<input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title"  ng-keyup="onpagetitle(news.title)">
							</div>
							<b>Page Slugs: </b>
							<input type="hidden" ng-model="news.slugs"><span ng-bind="news.slugs"></span>
							<div class="line line-dashed b-b line-lg"></div>
							<?php 
							if($username['task']!="CS"){
								?>
								<div class="form-group col-sm-4">
									<div class="col-sm-12">Select Center
										<div class="input-group">
											<select name="centerid" ng-model="news.centerid" class="form-control" ng-change="slctcenterid(news.centerid)">
												<option value="" style="display:none">Please choose </option>
												<option ng-repeat="centerlist in centerlist" value="{[{ centerlist.centerid }]}">{[{ centerlist.title }]}</option>
											</select>
										</div>
									</div>
								</div>
								<?php
							}?>
							<div class="form-group col-sm-4">
								<div class="col-sm-12">Date Publish
									<div class="input-group w-md">
										<input type="hidden" required="required" ng-value="news.hiddendate = news.publish" ng-model="news.publish" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">	
										<span class="input-group-btn">
											<input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="news.publish" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
											<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group col-sm-4">
								<div class="col-sm-12">Author
									<div class="input-group">
										<select ng-model="news.authorid" class="form-control" required>
											<option value="" style="display:none">Please choose </option>
											<option ng-repeat="authorlist in authorlist" value="{[{ authorlist.id }]}">{[{ authorlist.name }]}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div> 
							<div class="form-group col-sm-6">
								<label>Category</label>
								<select chosen allow-single-deselect class="form-control w-md" ng-model="news.category" options="category" ng-options="cat.categoryid as cat.categoryname for cat in category" required="required">
								</select>
								
							</div>

							<div class="form-group col-sm-6">
								<label>Tags/Keywords</label>
								<select chosen multiple options="tag" class="form-control m-b" ng-options="item as item.tags for item in tag track by item.id" ng-model="news.tag" required="required"></select>

							</div>


							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Short Description
								<textarea class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="news.description">                  
								</textarea>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Body
								<textarea class="ck-editor" ng-model="news.body" ></textarea>
							</div>

						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Featured
						</div>
						<div class="panel-body">
							<label class="i-checks">
								<input type="radio" name="submain" value="video" ng-model="news.featuredoption" ng-click="video()">
								<i></i>
								Video
							</label>
							<label class="i-checks">
								<input type="radio" name="submain" value="banner" ng-model="news.featuredoption" ng-click="banner()">
								<i></i>
								Picture Banner
							</label>
							<div class="line line-dashed b-b line-lg"></div>
							<div ng-show="showvideo">
								<style type="text/css">
									iframe {
										width: 100%;
									} 
								</style>
								<div class="input-group m-b">
									<span class="input-group-btn">
										<a class="btn btn-default"  ng-click="featuredvideo('lg')">  Embed Video  </a>
									</span>
									
									<input type="text" ng-value="news.video=datafeat" ng-model="news.video" class="form-control" ng-change="paste(news.video)" placeholder="">
								</div>

								<div ng-bind-html="watchvideo"></div>
							</div>
							<div ng-show="showbanner">
								<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
								<div class="panel-body">
									<div class="input-group m-b">
										<span class="input-group-btn">
											<a class="btn btn-default"  ng-click="featuredbanner('lg')">Select Image</a>
										</span>
										<input type="text" class="form-control" ng-value="news.featbanner = featpath " ng-model="news.featbanner" placeholder="{[{featpath}]}" required>
									</div>
								</div>
								<div class="panel-body" style="margin-top:-30px;">
									<img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{news.featbanner}]}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Banner
						</div>
						<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
						<div class="panel-body">
							<div class="input-group m-b">
								<span class="input-group-btn">
									<a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
								</span>
								<input type="text" class="form-control" ng-value="news.banner = amazonpath " ng-model="news.banner" placeholder="{[{amazonpath}]}" >

							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}">
						</div>

					</div>
				</div> -->
			</div>
			<div class="row">
				<div class="panel-body">
					<footer class="panel-footer  bg-light lter">
						<div class="pull-left">
							<div class="form-group form-inline">
								<label class="control-label col-sm-12">Status</label>
								<div class="col-sm-12">
									<div class="radio">	
										<label class="i-checks">
											<input type="radio" name="a" ng-model="news.status" required="required" value="true" ng-value="news.status =stat "><i></i>Active
										</label>
									</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="radio">
										<label class="i-checks">
											<input type="radio" name="a" ng-model="news.status" required="required" value="false" ng-value="news.status = stat"><i></i>Inactive
										</label>
									</div>
								</div>
							</div>  
						</div>
						<div class="pull-right">
							<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
							<button ng-show="bt1" type="submit" class="btn btn-success" ng-disabled="formpage.$invalid" scroll-to="Scrollup" >Submit</button>
							<button ng-show="bt2" type="submit" class="btn btn-success" disabled>Submit</button>
						</div>
						<div style="clear:both;"></div>
					</footer>
				</div>
			</div>
		</div>
	</fieldset>
</form>
