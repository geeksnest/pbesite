{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Update Center</h1>
	<a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(center)" name="formpage">
	<fieldset ng-disabled="isSaving">
		<div class="wrapper-md">
			<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
			<div class="row">
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Information
						</div>
						<div class="panel-body ">
							Center Name
							<input id="centerid" type="hidden" ng-value="center.centerid = centerid" ng-model="center.centerid">
							<input id="title" type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.title" required="required" ng-keyup="onpagetitle(center.title)">
							<br>
							<b>Page Slugs: </b>
							<input id="slugs" type="hidden" ng-model="center.slugs"><span ng-bind="center.slugs"></span>

							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								<label>Select Manager</label>
								<select id="manager" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="center.manager" required>
									<option ng-repeat="data in centerManager" value="{[{ data.id }]}" ng-selected="center.manager == data.id">{[{data.fname}]} {[{data.lname}]}</option>
								</select>
							</div>
							<div class="form-group col-sm-6">
								<label>Region</label>
								<input id="region" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.region" required>
							</div>
							<div class="form-group col-sm-6">
								<label>District</label>
								<input id="district" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.district" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-4">
								<label>State Abbrevations</label>
								<input id="state" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.state" required>
								<!-- <div ui-module="select2">
									<select ui-select2 ng-model="center.state" class="form-control w-md g-pristine ng-invalid ng-invalid-required " ng-change="selectstate(center.state)" required>
										<option value=""></option>
										<option ng-value="data.statecode" ng-repeat="data in statelist">{[{data.state}]} </option>
									</select>
								</div> -->
							</div>
							<div class="form-group col-sm-4">
								<label>City</label>
								<input id="city" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.city" required>
								<!-- <div ui-module="select2">
									<select ui-select2 ng-model="center.city" class="form-control w-md g-pristine ng-invalid ng-invalid-required" ng-change="selectcity(center.city)" required>
										<option value=""></option>
										<option ng-value="data.zip" ng-repeat="data in citylist">{[{data.city}]} </option>
									</select>
								</div> -->
							</div>
							<div class="form-group col-sm-4">
								<label>Zip Code</label>
								<input id="zipcode" type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required" ng-model="center.zipcode" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Address
								<textarea id="address" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.address" required>									
								</textarea>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								<label>Meta Keywords(Note: Separate Keywords with a comma)</label>
								<input id="metatitle" type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required"  ng-model="center.metatitle" required>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group col-sm-12">
								Meta Description
								<textarea id="desc" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="center.desc" required>									
								</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">
							Center Banner
						</div>

						<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
						<div class="panel-body">
							<div class="input-group m-b">
								<span class="input-group-btn">
									<a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
								</span>
								<input id="banner" type="text" class="form-control" ng-value="center.banner = amazonpath " ng-model="center.banner" placeholder="{[{amazonpath}]}" required>

							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}">
							<div class="form-group">
								<label class="col-sm-12 control-label">MOnday to Friday Time</label>
									<div class="col-sm-12">
											From :<select id="mffrom" ng-model="center.mffrom" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
											<option value="07:00 AM">07:00 AM</option>
				                            <option value="07:30 AM">07:30 AM</option>
				                            <option value="08:00 AM">08:00 AM</option>
				                            <option value="08:30 AM">08:30 AM</option>
				                            <option value="09:00 AM">09:00 AM</option>
				                            <option value="09:30 AM">09:30 AM</option>
				                            <option value="10:00 AM">10:00 AM</option>
				                            <option value="10:30 AM">10:30 AM</option>
				                            <option value="11:00 AM">11:00 AM</option>
				                            <option value="11:30 AM">11:30 AM</option>
				                            <option value="12:00 AM">12:00 AM</option>
				                            <option value="12:30 AM">12:30 AM</option>
				                            <option value="01:00 PM">01:00 PM</option>
				                            <option value="01:30 PM">01:30 PM</option>
				                            <option value="02:00 PM">02:00 PM</option>
				                            <option value="02:30 PM">02:30 PM</option>
				                            <option value="03:00 PM">03:00 PM</option>
				                            <option value="03:30 PM">03:30 PM</option>
				                            <option value="04:00 PM">04:00 PM</option>
				                            <option value="04:30 PM">04:30 PM</option>
				                            <option value="05:00 PM">05:00 PM</option>
				                            <option value="05:30 PM">05:30 PM</option>
				                            <option value="06:00 PM">06:00 PM</option>
				                            <option value="06:30 PM">06:30 PM</option>
				                            <option value="07:00 PM">07:00 PM</option>
				                            <option value="07:30 PM">07:30 PM</option>
				                            <option value="08:00 PM">08:00 PM</option>
				                            <option value="08:30 PM">08:30 PM</option>
				                            <option value="09:00 PM">09:00 PM</option>
				                            <option value="09:30 PM">09:30 PM</option>
				                            <option value="10:00 PM">10:00 PM</option>
				                            <option value="10:30 PM">10:30 PM</option>
				                            <option value="11:00 PM">11:00 PM</option>
				                            <option value="11:30 PM">11:30 PM</option>
				                            <option value="12:00 PM">12:00 PM</option>
				                            <option value="12:30 PM">12:30 PM</option>
											</select>
									</div>
									<div class="col-sm-12">
										<div class="m-b-sm">
											To :
											<select id="mfto"  ng-model="center.mfto" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
											<option value="07:00 AM">07:00 AM</option>
				                            <option value="07:30 AM">07:30 AM</option>
				                            <option value="08:00 AM">08:00 AM</option>
				                            <option value="08:30 AM">08:30 AM</option>
				                            <option value="09:00 AM">09:00 AM</option>
				                            <option value="09:30 AM">09:30 AM</option>
				                            <option value="10:00 AM">10:00 AM</option>
				                            <option value="10:30 AM">10:30 AM</option>
				                            <option value="11:00 AM">11:00 AM</option>
				                            <option value="11:30 AM">11:30 AM</option>
				                            <option value="12:00 AM">12:00 AM</option>
				                            <option value="12:30 AM">12:30 AM</option>
				                            <option value="01:00 PM">01:00 PM</option>
				                            <option value="01:30 PM">01:30 PM</option>
				                            <option value="02:00 PM">02:00 PM</option>
				                            <option value="02:30 PM">02:30 PM</option>
				                            <option value="03:00 PM">03:00 PM</option>
				                            <option value="03:30 PM">03:30 PM</option>
				                            <option value="04:00 PM">04:00 PM</option>
				                            <option value="04:30 PM">04:30 PM</option>
				                            <option value="05:00 PM">05:00 PM</option>
				                            <option value="05:30 PM">05:30 PM</option>
				                            <option value="06:00 PM">06:00 PM</option>
				                            <option value="06:30 PM">06:30 PM</option>
				                            <option value="07:00 PM">07:00 PM</option>
				                            <option value="07:30 PM">07:30 PM</option>
				                            <option value="08:00 PM">08:00 PM</option>
				                            <option value="08:30 PM">08:30 PM</option>
				                            <option value="09:00 PM">09:00 PM</option>
				                            <option value="09:30 PM">09:30 PM</option>
				                            <option value="10:00 PM">10:00 PM</option>
				                            <option value="10:30 PM">10:30 PM</option>
				                            <option value="11:00 PM">11:00 PM</option>
				                            <option value="11:30 PM">11:30 PM</option>
				                            <option value="12:00 PM">12:00 PM</option>
				                            <option value="12:30 PM">12:30 PM</option>
											</select>
										</div>
									</div>
							</div>
							<div class="form-group">
								<label class="col-sm-12 control-label">Saturday Time</label>
									<div class="col-sm-12">
											From :
											<select id="satfrom" ng-model="center.satfrom" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
											<option value="07:00 AM">07:00 AM</option>
				                            <option value="07:30 AM">07:30 AM</option>
				                            <option value="08:00 AM">08:00 AM</option>
				                            <option value="08:30 AM">08:30 AM</option>
				                            <option value="09:00 AM">09:00 AM</option>
				                            <option value="09:30 AM">09:30 AM</option>
				                            <option value="10:00 AM">10:00 AM</option>
				                            <option value="10:30 AM">10:30 AM</option>
				                            <option value="11:00 AM">11:00 AM</option>
				                            <option value="11:30 AM">11:30 AM</option>
				                            <option value="12:00 AM">12:00 AM</option>
				                            <option value="12:30 AM">12:30 AM</option>
				                            <option value="01:00 PM">01:00 PM</option>
				                            <option value="01:30 PM">01:30 PM</option>
				                            <option value="02:00 PM">02:00 PM</option>
				                            <option value="02:30 PM">02:30 PM</option>
				                            <option value="03:00 PM">03:00 PM</option>
				                            <option value="03:30 PM">03:30 PM</option>
				                            <option value="04:00 PM">04:00 PM</option>
				                            <option value="04:30 PM">04:30 PM</option>
				                            <option value="05:00 PM">05:00 PM</option>
				                            <option value="05:30 PM">05:30 PM</option>
				                            <option value="06:00 PM">06:00 PM</option>
				                            <option value="06:30 PM">06:30 PM</option>
				                            <option value="07:00 PM">07:00 PM</option>
				                            <option value="07:30 PM">07:30 PM</option>
				                            <option value="08:00 PM">08:00 PM</option>
				                            <option value="08:30 PM">08:30 PM</option>
				                            <option value="09:00 PM">09:00 PM</option>
				                            <option value="09:30 PM">09:30 PM</option>
				                            <option value="10:00 PM">10:00 PM</option>
				                            <option value="10:30 PM">10:30 PM</option>
				                            <option value="11:00 PM">11:00 PM</option>
				                            <option value="11:30 PM">11:30 PM</option>
				                            <option value="12:00 PM">12:00 PM</option>
				                            <option value="12:30 PM">12:30 PM</option>
											</select>
									</div>
									<div class="col-sm-12">
											To :
											<select id="satto" ng-model="center.satto" class="form-control w-md g-pristine ng-invalid ng-invalid-required " required>
											<option value="07:00 AM">07:00 AM</option>
				                            <option value="07:30 AM">07:30 AM</option>
				                            <option value="08:00 AM">08:00 AM</option>
				                            <option value="08:30 AM">08:30 AM</option>
				                            <option value="09:00 AM">09:00 AM</option>
				                            <option value="09:30 AM">09:30 AM</option>
				                            <option value="10:00 AM">10:00 AM</option>
				                            <option value="10:30 AM">10:30 AM</option>
				                            <option value="11:00 AM">11:00 AM</option>
				                            <option value="11:30 AM">11:30 AM</option>
				                            <option value="12:00 AM">12:00 AM</option>
				                            <option value="12:30 AM">12:30 AM</option>
				                            <option value="01:00 PM">01:00 PM</option>
				                            <option value="01:30 PM">01:30 PM</option>
				                            <option value="02:00 PM">02:00 PM</option>
				                            <option value="02:30 PM">02:30 PM</option>
				                            <option value="03:00 PM">03:00 PM</option>
				                            <option value="03:30 PM">03:30 PM</option>
				                            <option value="04:00 PM">04:00 PM</option>
				                            <option value="04:30 PM">04:30 PM</option>
				                            <option value="05:00 PM">05:00 PM</option>
				                            <option value="05:30 PM">05:30 PM</option>
				                            <option value="06:00 PM">06:00 PM</option>
				                            <option value="06:30 PM">06:30 PM</option>
				                            <option value="07:00 PM">07:00 PM</option>
				                            <option value="07:30 PM">07:30 PM</option>
				                            <option value="08:00 PM">08:00 PM</option>
				                            <option value="08:30 PM">08:30 PM</option>
				                            <option value="09:00 PM">09:00 PM</option>
				                            <option value="09:30 PM">09:30 PM</option>
				                            <option value="10:00 PM">10:00 PM</option>
				                            <option value="10:30 PM">10:30 PM</option>
				                            <option value="11:00 PM">11:00 PM</option>
				                            <option value="11:30 PM">11:30 PM</option>
				                            <option value="12:00 PM">12:00 PM</option>
				                            <option value="12:30 PM">12:30 PM</option>
											</select>
									</div>
							<div class="form-group">
									<div class="col-sm-12">
										Contact Number(ex. (999) 999-9999)<input id="contactnumber" type="text" class="form-control  ng-pristine ng-invalid ng-invalid-required"  ng-model="center.contactnumber" required id="phone">
									</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="panel-body">
					<footer class="panel-footer  bg-light lter">
						<div class="pull-left">
							<div class="form-group">
			  	            <label class="control-label col-sm-12">Status</label>
			  	              <div class="col-sm-12">	
			  	            	<label class="checkbox-inline i-checks control-label">
			                			<input id="status" type="radio" name="a" ng-model="center.status" required="required" class="form-control ng-dirty ng-valid ng-valid-required" value="true"><i></i>Active
			              		</label>
			              		<label class="checkbox-inline i-checks control-label">
			                			<input id="status" type="radio" name="a" ng-model="center.status" required="required" class="form-control ng-dirty ng-valid ng-valid-required" value="false"><i></i>Inactive
			              		</label>
			              	  </div>
			              	</div>  
						</div>
						<div class="pull-right">
							<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
							<button id="submit" type="submit" class="btn btn-success" ng-disabled="formpage.$invalid" scroll-to="Scrollup">Submit</button>
						</div>
						<div style="clear:both;"></div>
					</footer>
				</div>
			</div>
		</div>
	</fieldset>
</form>



<script type="text/ng-template" id="scheduleedit.html">
  <div ng-include="'/be/tpl/scheduleedit.html'"></div>
</script>
<script type="text/ng-template" id="schedule.html">
  <div ng-include="'/be/tpl/schedule.html'"></div>
</script>

<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Center Class Schedule</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">

  <div class="panel panel-default">
    <div class="panel-heading">
      Responsive Table
    </div>
    <div class="col-md-3">
     <div class="input-group m-b">
      <span class="input-group-addon"><i class="fa fa-plus"></i> Select Day</span>
      <select ng-model="selectday" class="form-control w-md g-pristine ng-invalid ng-invalid-required"  ng-change="schedmodal(selectday)">
        <option value="monday">Monday</option>
        <option value="tuesday">Tuesday</option>
        <option value="wednesday">Wednesday</option>
        <option value="thursday">Thursday</option>
        <option value="friday">Friday</option>
        <option value="saturday">Saturday</option>
        <option value="sunday">Sunday</option>
        
      </select>
    </div>

  </div>
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="table-responsive">
    <table class="table table-striped b-t b-light ">
      <thead class="bg-info" >
        <tr>
          <th style="text-align:left;">Monday</th>
          <th style="text-align:left;">Tuesday</th>
          <th style="text-align:left;">Wednesday</th>
          <th style="text-align:left;">Thursday</th>
          <th style="text-align:left;">Friday</th>
          <th style="text-align:left;">Saturday</th>
          <th style="text-align:left;">Sunday</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="margin-right:0px !important;">
           <!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('monday')"><i class="fa fa-plus"></i>Add Schedule</button> -->
           <ul class="list-group no-borders pull-in auto" style="padding:2px;">
            <li class="list-group-item" ng-repeat="data in sched | filter: 'monday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
              <div>
                <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
                <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
                <div class="clear">
                  <div ng-if="data.type=='kids'">Kids Class</div>
                  <div ng-if="data.type=='teen'">Teen Class</div>
                  <div ng-if="data.type=='adult'">Adult Class</div>
                  <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
                </div>
              </li>
            </ul>
          </td>
          <td>
            <!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('tuesday')"><i class="fa fa-plus"></i>Add Schedule</button> -->
            <ul class="list-group no-borders pull-in auto" style="padding:2px;">
              <li class="list-group-item" ng-repeat="data in sched | filter: 'tuesday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
                <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
                <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
                <div class="clear">
                  <div ng-if="data.type=='kids'">Kids Class</div>
                  <div ng-if="data.type=='teen'">Teen Class</div>
                  <div ng-if="data.type=='adult'">Adult Class</div>
                  <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
                </div>             
              </li>

            </ul>
          </td>
          <td>
            <!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('wednesday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
            <ul class="list-group no-borders pull-in auto" style="padding:2px;">
              <li class="list-group-item" ng-repeat="data in sched | filter: 'wednesday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
                <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
                <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
                <div class="clear">
                  <div ng-if="data.type=='kids'">Kids Class</div>
                  <div ng-if="data.type=='teen'">Teen Class</div>
                  <div ng-if="data.type=='adult'">Adult Class</div>
                  <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
                </div>
              </li>
            </ul>
          </td>
          <td>
           <!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('thursday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
           <ul class="list-group no-borders pull-in auto" style="padding:2px;">
            <li class="list-group-item" ng-repeat="data in sched | filter: 'thursday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
              <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
              <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
              <div class="clear">
                <div ng-if="data.type=='kids'">Kids Class</div>
                <div ng-if="data.type=='teen'">Teen Class</div>
                <div ng-if="data.type=='adult'">Adult Class</div>
                <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
              </div>
            </li>
          </ul>
        </td>
        <td>
          <!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('friday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
          <ul class="list-group no-borders pull-in auto" style="padding:2px;">
            <li class="list-group-item" ng-repeat="data in sched | filter: 'friday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
              <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
              <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
              <div class="clear">
                <div ng-if="data.type=='kids'">Kids Class</div>
                <div ng-if="data.type=='teen'">Teen Class</div>
                <div ng-if="data.type=='adult'">Adult Class</div>
                <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
              </div>
            </li>
          </ul>
        </td>
        <td>
         <!--  <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('saturday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
         <ul class="list-group no-borders pull-in auto" style="padding:2px;">
          <li class="list-group-item" ng-repeat="data in sched | filter: 'saturday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
            <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
            <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
            <div class="clear">
              <div ng-if="data.type=='kids'">Kids Class</div>
              <div ng-if="data.type=='teen'">Teen Class</div>
              <div ng-if="data.type=='adult'">Adult Class</div>
              <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
            </div>
          </li>
        </ul>
      </td>
      <td>
        <!-- <button class="btn m-b-xs btn-sm btn-default btn-addon" ng-click="sched('sunday')"><i class="fa fa-plus"></i>Add  Schedule</button> -->
        <ul class="list-group no-borders pull-in auto" style="padding:2px;">
          <li class="list-group-item" ng-repeat="data in sched | filter: 'sunday'" style="background:{[{data.bg}]}; color:#fff;margin-bottom:2px;">
            <a href="" class="text-muted" ng-click="delete(data.classid)" ><i class="fa fa-trash-o pull-right"></i></a>
            <a href="" class="text-muted" ng-click="update(data.classid, data.day)" ><i class="icon-pencil pull-right "></i></a>
            <div class="clear">
              <div ng-if="data.type=='kids'">Kids Class</div>
              <div ng-if="data.type=='teen'">Teen Class</div>
              <div ng-if="data.type=='adult'">Adult Class</div>
              <small class="text-muted" style="color:#fff;">{[{data.from}]} ~ {[{data.to}]}</small>
            </div>
          </li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>
</div>
<footer class="panel-footer">
  <!-- chat form -->
  <div class="pull-left inline">
    <p>
      <button class="btn btn-sm btn-icon btn-info">A</button> Adult &nbsp;&nbsp;&nbsp;
      <button class="btn btn-sm btn-icon btn-danger">K</button> Kids &nbsp;&nbsp;&nbsp;
      <button class="btn btn-sm btn-icon btn-success">T</button> Teens 
    </p>
  </div>
  <div style="clear:both;"></div>
</footer>
</div>

</div>

