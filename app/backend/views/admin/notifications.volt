<script type="text/ng-template" id="requestReview.html">
  <div ng-include="'/be/tpl/requestReview.html'"></div>
</script>
<script type="text/ng-template" id="appointmentReview.html">
  <div ng-include="'/be/tpl/appointmentReview.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <h1 class="m-n font-thin h3 text-black">Notifications</h1>
        </div>
      </div>
    </div>


    <div class="panel-body">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row wrapper">
        <div class="col-sm-12">
          <div class="col-sm-6">
            <form name="rEquired">
              <div class="input-group">
                 <select id="search" ng-model="searchtext" class="form-control input-sm" ng-change="search(searchtext)">
                  <option value="" style="display:none">Filter</option>
                  <option value="undefined">ALL</option>             
                  <option value="0">NEW</option>
                  <option value="1">REVIEWED</option>
                  <option value="2">REPLIED</option>
                 </select>
                <span class="input-group-btn">
                  <button id="reset" type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="row wrapper">
        <div class="col-md-12">
          <div class="panel no-border">
            <div class="panel-heading wrapper b-b b-light">
              <span class="text-xs text-muted pull-right">
                <i class="fa fa-circle text-warning m-r-xs"></i>New: {[{ countnew }]}
                <i class="fa fa-circle text-primary m-r-xs m-l-sm"></i>Reviewed: {[{ countreviewed }]}
                <i class="fa fa-circle text-success m-r-xs m-l-sm"></i>Replied: {[{ countreplied }]}
              </span>             
            </div>
            <ul class="list-group list-group-lg m-b-none">
              <li class="list-group-item" ng-repeat="list in notiflist">
            
                <a ng-if="list.type==1 && list.status==0" ui-sref="managerequestinfo" ng-click="reviewmodal(list.id)">
                  <span class="thumb-sm m-r">
                    <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-warning inline m-t-sm">New</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                 <a ng-if="list.type==1 && list.status==1" ui-sref="managerequestinfo" ng-click="reviewmodal(list.id)">
                  <span class="thumb-sm m-r">
                    <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-primary inline m-t-sm">Reviewed</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                <a ng-if="list.type==1 && list.status==2" ui-sref="managerequestinfo" ng-click="reviewmodal(list.id)">
                  <span class="thumb-sm m-r">
                    <i class="glyphicon icon-envelope-letter text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-success inline m-t-sm">Replied</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                <a ng-if="list.type==0 && list.status==0" ui-sref="manageappointment" ng-click="reviewmodal1(list.id)">
                  <span class="thumb-sm m-r">
                     <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-warning inline m-t-sm">New</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                <a ng-if="list.type==0 && list.status==1" ui-sref="manageappointment" ng-click="reviewmodal1(list.id)">
                  <span class="thumb-sm m-r">
                     <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-primary inline m-t-sm">Reviewed</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                <a ng-if="list.type==0 && list.status==2" ui-sref="manageappointment" ng-click="reviewmodal1(list.id)">
                  <span class="thumb-sm m-r">
                     <i class="glyphicon icon-bubble text-md text-muted wrapper-sm"></i>
                  </span>
                  <span class="label bg-success inline m-t-sm">Replied</span>
                  <span>{[{ list.fname }]} {[{ list.lname }]} | <small class="text-muted">{[{ list.timeago }]}</small></span>
                </a>

                <a class="pull-right thumb-sm m-r" title="Remove" ng-click="delete(list.id)">
                  <i class="glyphicon glyphicon-remove text-md wrapper-sm"></i>
                </a>

                <a class="pull-right thumb-sm m-r" title="READ" tooltip="READ" ng-click="read(list.id,list.status)" ng-if="list.status==0">
                  <i class="fa fa-circle text-md wrapper-sm"></i>
                </a>

                <a class="pull-right thumb-sm m-r" title="UNREAD" tooltip="UNREAD" ng-click="read(list.id,list.status)" ng-if="list.status==1">
                  <i class="fa fa-circle-o text-md wrapper-sm"></i>
                </a>

              </li>
            </ul>
            <div class="panel-footer">
              <footer class="panel-footer">
                <div class="row">
                  <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                      <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                    </footer>
                  </div>
                </div>
              </footer>
            </div>
          </div>
        </div>
      </div>
    </div>




  </div>
</div>