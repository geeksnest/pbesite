<div id="Scrollup"></div>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="addmedia.html">
	<div ng-include="'/be/tpl/addmedia.html'"></div>
</script>
<script type="text/ng-template" id="editmedia.html">
	<div ng-include="'/be/tpl/editmedia.html'"></div>
</script>
<div>
	<tabset class="tab-container">
		<tab>
			<tab-heading><i class="glyphicon glyphicon-th-list"></i>Media</tab-heading>
			<div class="media" ng-repeat='data in traininglist | filter:{idno:idno}' style=" background-color:{[{data.theme}]}; padding:10px;color:#fff">
				<span class="pull-left thumbnail"><img src="{[{data.logo}]}" alt="..."></span>
				<div class="media-body">
					<!-- <div class="pull-right text-center text-muted">
						<strong class="h4">12:18</strong><br>
						<small class="label bg-light">pm</small>
					</div> -->
					<a href class="h4">{[{data.title}]}</a>
					<small class="block m-t-sm"><p ng-bind-html="data.description">></p></small>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="panel panel-default b-a">
				<div class="panel-heading bg-info ">
					<button class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="addmedia('lg')"><i class="fa fa-plus"></i>Add Media</button>
					Activity Lists
				</div>
				
				<div class="panel-body b-b b-light">
					Search Activity: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
				</div>
				<div>
					<style type="text/css">
						.m_title:first-letter {
							text-transform: uppercase;
						}
					</style>
					<table class="table m-b-none" ui-jq="footable" data-filter="#filter" data-page-size="20">
						<thead>
							<tr>
								<th data-hide="phone,tablet">
									Title
								</th>
								<th data-hide="phone,tablet">
									Class For
								</th>
								<td>Action</td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="data in rmedia  | filter:{trainingid:idno}">
								<td>{[{data.title}]}</td>
								<td class="m_title">{[{data.classfor}]}</td>
								<td data-value="1">
									<p>
										<a class="btn btn-rounded btn-sm btn-icon btn-warning" title="Edit"  ng-click="edit(data.idno,'lg')"><i class="glyphicon glyphicon-pencil"></i></a>
										<a class="btn btn-rounded btn-sm btn-icon btn-danger" title="Delete" ng-click="delete(data.idno)" ><i class="glyphicon glyphicon-trash"></i></a>
									</p>
								</td>
							</tr>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="10" class="text-center">
									<ul class="pagination"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</tab>
	</tabset>
</div>