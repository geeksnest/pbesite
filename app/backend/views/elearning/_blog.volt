<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="categoryAdd.html">
	<div ng-include="'/be/tpl/categoryAdd1.html'"></div>
</script>

<script type="text/ng-template" id="tagsAdd.html">
	<div ng-include="'/be/tpl/tagsAdd.html'"></div>
</script>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Blog</h1>
  <a id="top"></a>
</div>
<div >
	<tabset class="tab-container">
		<tab>
			<tab-heading><i class="glyphicon glyphicon-th-list"></i> Manage Blogs</tab-heading>
			<div class="panel panel-default">
				<div class="panel-heading">
					Blog Lists
				</div>
				<div class="panel-body b-b b-light">
					Search: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
				</div>
				<div>
					<table class="table m-b-none" ui-jq="footable" data-filter="#filter" data-page-size="20">
						<thead>
							<tr>
								<th data-hide="phone,tablet">
									News Title
								</th>
								<th data-hide="phone,tablet">
									Author
								</th>
								<th data-hide="phone,tablet" >
									Date Publish
								</th>
								<th data-hide="phone">
									Status
								</th>
								<td >
								</td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="data in blog">
								<td>{[{data.title}]}</td>
								<td>{[{data.name}]}</a></td>
								<td>{[{data.publish}]}</td>
								<td data-value="1"><span class="label bg-success" title="Active">{[{data.status}]}</span></td>
								<td data-value="1">
									<p>
										<a class="btn btn-sm btn-icon btn-warning" title="Edit" ui-sref="editblog({id:data.blogid})"><i class="glyphicon glyphicon-pencil"></i></a>
										<a class="btn btn-sm btn-icon btn-danger" ng-click="_delete(data.blogid)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
									</p>
								</td>
							</tr>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="10" class="text-center">
									<ul class="pagination"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</tab>
		<tab>
			<tab-heading><i class="glyphicon glyphicon-plus"></i> Add New Blog</tab-heading>
			<form id="formblog" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="save(news)" name="formblog"  >
				<fieldset ng-disabled="isSaving">
					<div class="wrapper-md">
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{alert.msg}]}</alert>
						<div class="row">
							
							<div class="col-sm-8">
								<div class="panel panel-default">
									<div class="panel-heading font-bold">
										Info
									</div>
									<div class="panel-body">
										<div class="form-group">
											<label class="col-lg-2 control-label">News Title</label>
											<div class="col-lg-10">
												<span class="label bg-danger" ng-show="notification">Ooopss! News Title already Taken <br/></span>
												<input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title"  ng-keyup="ontitle(news.title)">
												<span class="help-block m-b-none"><b>Page Slugs:</b> <i ng-bind="slugs"></i></span>
												<input type="hidden" ng-model="news.slugs" ng-value="news.slugs = slugs">
											</div>
										</div>
										<div class="line line-dashed b-b line-lg"></div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Author</label>
											<div class="col-lg-10">
												<select ng-model="news.author" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">
													<option value="" style="display:none">Please choose </option>
													<option ng-repeat="authorlist in authorlist" value="{[{ authorlist.id }]}">{[{ authorlist.name }]}</option>
												</select>
											</div>
										</div>
										<div class="line line-dashed b-b line-lg"></div>
										<div class="form-group" ng-controller="CalendarCTRL">
											<label class="col-lg-2 control-label">Date Publish</label>
											<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-btn">
														<input id="date" name="date" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" datepicker-popup="dd-MMMM-yyyy" ng-model="news.publish" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
														<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
													</span>
												</div>
											</div>
										</div>
										<div class="line line-dashed b-b line-lg"></div>
										<div class="form-group col-sm-12">
											Short Description
											<textarea id="txtarea" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" rows="5"  ng-model="news.description" required>                  
											</textarea>
										</div>
										<div class="line line-dashed b-b line-lg"></div>
										<div class="col-sm-12">
											<textarea id="txtarea" class="ck-editor form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.body" required></textarea>
											<div class="line line-dashed b-b line-lg"></div>
										</div>
										<div class="line line-dashed b-b line-lg"></div>
									</div>

								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-heading font-bold">
										Featured
									</div>
									<div class="panel-body">
										<label class="i-checks">
											<input type="radio" name="submain" ng-model="news.featuredoption" value="video"  ng-click="video()" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern">
											<i></i>
											Video
										</label>
										<label class="i-checks">
											<input type="radio" name="submain" ng-model="news.featuredoption" value="banner" ng-click="banner()" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern">
											<i></i>
											Picture Banner
										</label>
										<div class="line line-dashed b-b line-lg"></div>
										<alert ng-repeat="alert in msg" type="{[{alert.type }]}" close="closemsg($index)">{[{ alert.msg }]}</alert>
										<div ng-show="showvideo">
											<style type="text/css">
												iframe {
													width: 100%;
												} 
											</style>
											<div class="input-group m-b">
												<span class="input-group-btn">
													<a class="btn btn-default"  ng-click="featuredvideo('lg')">Embed Video</a>
												</span>
												<input type="text" ng-value="news.video=utube" ng-model="news.video" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" placeholder="">
											</div>
											<div ng-bind-html="watchvideo"></div>
											
										</div>
										<div ng-show="showbanner">

											<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
											
											<div class="input-group m-b">
												<span class="input-group-btn">
													<a class="btn btn-default"  ng-click="featuredbanner('lg')">Select Image</a>
												</span>
												<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-value="news.featbanner = featpath " ng-model="news.featbanner" placeholder="{[{featpath}]}" >
											</div>
											
											<div  style="margin-top:5px;">
												<img style="width:100%" src="{[{news.featbanner}]}">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-heading font-bold">
										Blog Status
									</div>
									<div class="panel-body">
										<div class="form-group form-inline">
											<div class="col-sm-12">
												<div class="radio">	
													<label class="i-checks">
														<input type="radio" name="a" ng-model="news.status" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern"  value="true" ng-init="news.status =stat "><i></i>Active
													</label>
												</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<div class="radio">
													<label class="i-checks">
														<input type="radio" name="a" ng-model="news.status" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern"    value="false" ng-init="news.status = stat"><i></i>Inactive
													</label>
												</div>
											</div>
										</div>  
									</div>
								</div>

								<div class="panel-body">
									<footer class="panel-footer  bg-light lter">

										<div class="pull-right">
											<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
											<button type="submit" class="btn btn-success" ng-disabled="formblog.$invalid" disabled="disabled" scroll-to="Scrollup">Submit</button>
										</div>
										<div style="clear:both;"></div>
									</footer>
								</div>
							</div>
							
							
							
							<!-- <div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading font-bold">
										Tags/Keyword
									</div>
								</div>
							</div>
							<div style="clear:both;"></div> -->
						</div>	
					</div>



				</div>
			</fieldset>
		</form>











	</tab>
</tabset>
</div>