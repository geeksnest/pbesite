<div id="Scrollup"></div>
<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="addmedia.html">
	<div ng-include="'/be/tpl/addmedia.html'"></div>
</script>
<script type="text/ng-template" id="editmedia.html">
	<div ng-include="'/be/tpl/editmedia.html'"></div>
</script>
<div>
	<style type="text/css">
		iframe {
			width: 83% !important;
			margin:0px !important;
		}
	</style>
	<tabset class="tab-container" style="padding:0px !important;">
		<tab>
			<!-- <div class="bg-light lter b-b wrapper-md ng-scope">
				<button class="btn m-b-xs btn-sm btn-info btn-addon pull-right" ng-click="slctfeatured('lg')"><i class="icon-control-play"></i>Select Featured Video</button>
				<h1 class="m-n font-thin h3">Featured Video</h1>
			</div>
			<div style="margin-top:10px;" ng-bind-html="utube">
				
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div> -->
			<tab-heading><i class="glyphicon glyphicon-th-list"></i> Leaders</tab-heading>
			<div class="panel panel-default">
				<div class="panel-heading">
					<button  id="addmedia" class="btn m-b-xs btn-sm btn-primary btn-addon pull-right"  style="margin-right:20px;" ng-click="addmedia('lg')"><i class="fa fa-plus"></i>Add Media</button>
					Activity Lists
				</div>
				
				<div class="panel-body b-b b-light">
					Search Activity: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
				</div>
				<div>
					<table class="table m-b-none" ui-jq="footable" data-filter="#filter" data-page-size="20">
						<thead>
							<tr>
								<th data-hide="phone,tablet">
									Title
								</th>
								<td>Action</td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="data in rmedia  | filter:{trainingid:idno}">
								<td>{[{data.title}]}</td>
								<td data-value="1">
									<p>
										<a class="btn btn-rounded btn-sm btn-icon btn-warning" title="Edit"  ng-click="edit(data.idno,'lg')"><i class="glyphicon glyphicon-pencil"></i></a>
										<a class="btn btn-rounded btn-sm btn-icon btn-danger" title="Delete" ng-click="delete(data.idno)" ><i class="glyphicon glyphicon-trash"></i></a>
									</p>
								</td>
							</tr>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="10" class="text-center">
									<ul class="pagination"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</tab>
	</tabset>
</div>