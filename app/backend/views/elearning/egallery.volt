<script type="text/ng-template" id="featvideo.html">
	<div ng-include="'/be/tpl/featvideo.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="egallerysave.html">
	<div ng-include="'/be/tpl/egallerysave.html'"></div>
</script>
<script type="text/ng-template" id="egalleryupdate.html">
	"<div ng-include="'/be/tpl/egalleryupdate.html'"></div>"
</script>

<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">E-Gallery Image</h1>
  <a id="top"></a>
</div>



<div class="panel-body">
	<!-- <button class="btn m-b-xs w-xs btn-primary" ng-click="saveimg()">Primary</button> -->

	<alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
	<div class="loader" ng-show="imageloader">
		<div class="loadercontainer">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
			Uploading your images please wait...
		</div>
	</div>
	<div ng-show="imagecontent">
		<div class="col-sml-12">
			<div class="dragdropcenter">
				<div ngf-drop ngf-select class="drop-box" 
				ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
				accept="image/*" ngf-change="upload($files)">Drop images here or click to upload</div>
			</div>
		</div>
		<div style=" height:500px; overflow-y:scroll;">
			<div ng-show="noimage"><center>IMAGE GALLERY IS EMPTY</div>
			<div class="line line-dashed b-b line-lg"></div>
			<div class="col-sm-3" ng-repeat="data in imagelist" style="margin-bottom:15px;" ng-show="imggallery">
				<button id="delete" class=" btn btn-rounded btn btn-icon btn-danger" ng-click="delete(data.id)" style="margin:10px 5px -50px 5px"><i class="fa fa-trash-o"></i></button>
				<button id="edit" class=" btn btn-rounded btn btn-icon btn-warning" ng-click="edit(data.id, data.filename, data.title, data.description)" style="margin:10px 5px -50px -5px"><i class="icon-pencil"></i></button>

				<input id="pathimage" type="hidden" id="title" name="title" class="form-control" ng-model="imgamazonpath"  ng-init="imgamazonpath= amazonpath+'/uploads/banner/'+ data.filename" onClick="this.setSelectionRange(0, this.value.length)" >
				<input id="imagegallerystyle" type="button" class="imagegallerystyle" style="background-image: url('{[{imgamazonpath}]}');" >
			</div>
		</div>
	</div>
</div>