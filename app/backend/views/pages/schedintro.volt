{{ content() }}
<style type="text/css">
 .label_profile_pic {
   display: block;
   margin:10px 0;
  }
  .create-proj-thumb{
    text-align: center;
  }
  .create-proj-thumb label{
    font-size: 12px;
  }
  .create-proj-thumb img{
    max-width: 100%;
  }
  .propic {
  position: relative;
   }
</style>
<script type="text/ng-template" id="message.html">
 "<div ng-include="'/be/tpl/message.html'"></div>"
</script>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md" >
  <h1 class="m-n font-thin h3">Schedule an Introductory Session</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md ng-scope">
  <div class="row">
    <div class="col-sm-8">
      <form name="form" class="form-validation ng-pristine ng-invalid ng-invalid-required">
        <div class="panel panel-info">
          <div class="panel-heading">
            <span class="h4">Information</span>
          </div>
          <div class="panel-body">
            <!-- <p class="text-muted">Please fill the information to continue</p> -->
            <div class="form-group">
              <label> <em class="text-muted">Body</em></label>
              <textarea id="body" class="ck-editor" ng-model="body" ></textarea>
            </div>
          </div>
          <footer class="panel-footer text-right bg-light lter">
            <button id="submit"  type="submit" class="btn btn-success" ng-click="save({id:did,body:body,img:img,files:files})">Update Details</button>
          </footer>
        </div>
      </form>
    </div>
    <div class="col-sm-4">
      <div class="panel panel-info">
        <div class="panel-heading">
          <span class="h4">Image</span>
        </div>
        <div class="panel-body">
          <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
            <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
          </div>
          <div class="col-sm-12 create-proj-thumb">
            <div class="line line-dashed b-b line-lg"></div>                    
            <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/{[{img}]}"  ng-if="imageselected == false">
            <img ngf-src="projImg[0]" ng-if="imageselected == true">
          </div>
          <div class="col-sm-12 propic create-proj-thumb">
          <br><br>
            <div class=" border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">
              <a href="">Choose an image from your computer</a><br>
              <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
              <label>At least 1024x768 pixels</label>

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


