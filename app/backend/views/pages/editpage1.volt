{{ content() }}
<script>
  CKEDITOR.dtd.$removeEmpty['i'] = false
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-9">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
              <div class="panel-body">
              <input type="hidden" id="title" name="title" ng-model="page.pageid" >
                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
                <br>
                <b>Page Slugs: </b>
                <input type="hidden" ng-model="page.slugs"><span ng-bind="page.slugs"></span>
                <div class="line line-dashed b-b line-lg"></div>
                Body Content
                <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
              </div>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar Switch
            </div>

              <div class="panel-body">

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="a" ng-model="page.layout" value="1" ng-click="radio('1')">
                    <i></i>
                    Full Screen Page Layout
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="a" ng-model="page.layout" value="2" ng-click="radio('2')">
                    <i></i>
                    Page with Sidebar Layout
                  </label>
                </div>

               
              
              </div>

          </div>
          
        </div>

        <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="1" ng-model="page.leftbar">
                    <i></i>
                    About
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="2" ng-model="page.leftbar">
                    <i></i>
                    Classes
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="3" ng-model="page.leftbar">
                    <i></i>
                    Workshop
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="4" ng-model="page.leftbar">
                    <i></i>
                    Wellness at Work
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="5" ng-model="page.leftbar">
                    <i></i>
                    Communication Action
                  </label>
                </div>
               
              </div>


          </div>
        </div>


        <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Right Sidebar
            </div>


              <div class="panel-body">
                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r1" ng-click="rightbar(r1)" ng-model = "r1">
                    <i></i>
                    Option two checked
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r2" ng-click="rightbar(r2)" ng-model = "r2">
                    <i></i>
                    Option two checked
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r3" ng-click="rightbar(r3)" ng-model = "r3">
                    <i></i>
                    Option two checked
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r4" ng-click="rightbar(r4)" ng-model = "r4">
                    <i></i>
                    Option two checked
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r5" ng-click="rightbar(r5)" ng-model = "r5">
                    <i></i>
                    Option two checked
                  </label>
                </div>
              
              </div>


          </div>
        </div>


      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="managepage" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>


      
  </div>
</fieldset>
</form>