{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
 <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script>
  CKEDITOR.dtd.$removeEmpty['i'] = false
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-9">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
            <div class="panel-body">
              <div class="form-group col-sm-12">
               Title
               <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
             </div>
             <br>
             <b>Page Slugs: </b>
             <input id="slugs" type="hidden" ng-model="page.slugs"><span ng-bind="page.slugs"></span>

             <div class="line line-dashed b-b line-lg"></div>
             <div class="form-group col-sm-12">
              Short Description
              <textarea id="desc" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="page.desc">                  
              </textarea>
            </div>
            <div class="line line-dashed b-b line-lg"></div>
             <div class="form-group col-sm-12">
              Meta Tags (Note : Separate Keywords with a comma.)
              <textarea  id="metatags" class="form-control ng-pristine ng-invalid ng-invalid-required"  ng-model="page.metatags" required>                  
              </textarea>
            </div>
             <div class="line line-dashed b-b line-lg"></div>
            <div class="form-group col-sm-12">
             Body Content
             <textarea id="body"  class="ck-editor" ng-model="page.body" ></textarea>
           </div>
         </div>
       </div>
     </div>
     <div class="col-sm-3">
      <div class="panel panel-default">
        <div class="panel-heading no-border bg-primary">
          Menu Option
        </div>
        <div class="panel-body">
          <div class="radio">
            <label class="i-checks">
              <input id="parent(1)" type="radio" name="type" ng-model="page.menutyped" value="parentsub" ng-click="radio('1')" required="required">
              <i></i>
              Sub-Menu of Parent Page
            </label>
          </div>
          <div class="radio">
            <label class="i-checks">
              <input id="paren(2)" type="radio" name="type" ng-model="page.menutyped" value="childsub" ng-click="radio('2')" required="required">
              <i></i>
              Menu of Sub-Menu of Parent Page
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3" ng-show="sidebar">
      <div class="{[{bghead1}]}">
        <div class="{[{bghead}]}">
          Select Parent Menu
        </div>
        <div class="panel-body">
          <div class="radio" ng-repeat="pages in data">
            <label class="i-checks">
              <input type="radio" name="main" value="{[{pages.id}]}" ng-model="page.parentmenu" ng-click="showsub(pages.id)" required>
              <i></i>
              {[{pages.parentmenu}]}
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3" ng-show="parentsub">
      <div class="{[{bghead1}]}">
        <div class="{[{bghead}]}">
          Select Parent Sub-Menu
        </div>

        <div class="panel-body">
          <div class="radio" ng-repeat="paged in listsub">
            <label class="i-checks">
              <input type="radio" name="submain" value="{[{paged.slug}]}" ng-model="page.menussub" ng-click="gitid(paged.pageid)">
              <i></i>
              {[{paged.title}]}
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3" ng-show="parentsub">
      <div class="{[{bghead1}]}">
        <div class="{[{bghead}]}">
          Select Banner
        </div>
        <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
        <div class="panel-body">
         <div class="input-group m-b">
          <span class="input-group-btn">
            <a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
          </span>
          <input id="banner" type="text" class="form-control" ng-value="page.banner = amazonpath " ng-model="page.banner" placeholder="{[{amazonpath}]}" >
          
        </div>
      </div>
      <div class="panel-body" style="margin-top:-30px;">
        <img style="width:100%" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}">
      </div>
      
    </div>
  </div>
</div>
<div class="row">
  <div class="panel-body">
    <footer class="panel-footer  bg-light lter">
      <div class="col-sm-10">
        Status
        <div class="radio">
          <label class="i-checks">
            <input  id="status" type="radio" name="status" value="true" ng-model="page.status" required="required" ng-value="page.status=stat">
            <i></i>
            Active Page
          </label>
        </div>
        <div class="radio">
          <label class="i-checks">
            <input  id="status" type="radio" name="status" value="false" ng-model="page.status" required="required" ng-value="page.status=stat">
            <i></i>
            Deactivate Page
          </label>
        </div>
      </div>   
      <div class="pull-right">
       <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
       <button id="submit" type="submit" class="btn btn-success" ng-disabled="formpage.$invalid"  scroll-to="Scrollup">Submit</button>
     </div>
     <div style="clear:both;"></div>
   </footer>
 </div>
</div>
</div>
</fieldset>
</form>