{{ content() }}
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">System Logs</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md ng-scope">
	  	<div class="panel panel-default">
		    <div class="panel-heading font-bold">LOGS</div>
			    <div class="panel-body">
	           		<div class="row wrapper">
	           		<div class="col-sm-12">
	           			<div class="col-sm-6">
	           				<form name="rEquired">
	           				<div class="input-group">
				              <input id="search" class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext" ng-change="search(searchtext)" required>
				              <span class="input-group-btn">
				                <button id="reset" type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
				              </span>
				            </div>
				            </form>
	           			</div>
	           			<div class="col-sm-6">
	           				<div class="btn-toolbar">
						      <div class="btn-group dropdown">
						      </div>
						       <div class="btn-group">
						        <button class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title=" DELETE ALL LOGS | VISIBLE ONLY IN TEST SITE" ng-click="deletealllogs()"><i class="glyphicon glyphicon-trash"></i></button>
						      </div>
						    </div>
	           			</div>
	           		</div>
	           		</div>
	           		<div class="col-sm-12">
      					<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    				</div>
	           		<div class="col-sm-12">
	           		<div class="table-responsive">
	           			<table class="table table-striped b-t b-light">
				        <thead style="background:#00cdcd;color:white">
				          <tr>
				            <th width="15%">Date Time</th>
				            <th width="10%">User Name</th>
				            <th width="10%">Module</th>
				            <th width="10%">Event</th>
				            <th width="45%">Title</th>
				          </tr>
				        </thead>
				        <style>
				        	.even {
				        		background: #eee;
				        	}
				        </style>
				        <tbody>
				        	<tr ng-repeat="list in data.data" ng-class-even="'even'">
				                  <td>{[{ list.datetime}]}</td>
				                  <td>{[{ list.username}]}</td>
				                  <td>{[{ list.module}]}</td>
				                  <td>{[{ list.event}]}</td>
				                  <td>{[{ list.title}]}</td>
				            </tr>
				        </tbody>
				        </table>
	           		</div>
	           		</div>



	           </div>
	           <footer class="panel-footer">
			      <div class="row">
				        <div class="panel-body">
				            <footer class="panel-footer text-center bg-light lter">
				              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
				            </footer>
				        </div>
				       </div>
    		    </footer>
		</div>
</div>