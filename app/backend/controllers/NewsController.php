<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class NewsController extends ControllerBase
{

    public function indexAction()
    {
        

    }

    public function createnewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function managenewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editpageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
   
}

