<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class CentersController extends ControllerBase
{

    public function indexAction()
    {
        

    }

    public function createcenterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function managecenterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function updatecenterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function updatemycenterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function centerinfoAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function centerschedAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function centermapAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    

   
   
}

