<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as PI;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }   
    public function indexAction()
    {   
        $auth = $this->session->get('auth');
        if ($auth){
            $this->response->redirect('pbeadmin/admin');
        }   
        $this->view->error = null;
        if ($this->request->isPost()) {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $shapass='';
            //$hashpass = $this->security->hash($password);
            $response= $this->curl('user/login/'.$username.'/'.$password);
            if(@$response[0]->success){
                @$this->_registerSession(@$response[0]->success,  @$this->curl('center/info/'.@$response[0]->success->id)[0]->centerid,$this->curl('center/info/'.@$response[0]->success->id)[0]->centertitle);
                @$this->response->redirect('pbeadmin/admin');
            }else if(@$response[0]->error){
                echo @$response[0]->error;
            }
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }
    private function _registerSession($user, $centerid, $centertitle)
    {
        @$this->session->set('auth', array(
            'pi_id' => $user->id,
            'pi_fullname' => $user->firstname .' '.$user->lastname,
            'pi_username' => $user->username,
            'profile' => $user->profile,
            'task' => $user->task,
            'centerid' => $centerid,
            'centertitle' => $centertitle
            ));
        //Set SuperAdmin
        if(@$user->userLevel){
            @$this->session->set('SuperAgent', true );
        }
    }
   


}

