<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PagesController extends ControllerBase
{

    public function indexAction()
    {
        

    }

    public function createpageAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function managepageAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editpageAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function statpageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function bannerAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function homeAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function sociallinkAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function reqinfoAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function schedintroAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    
   
}

