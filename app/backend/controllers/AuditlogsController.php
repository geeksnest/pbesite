<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class AuditlogsController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function auditlogsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

