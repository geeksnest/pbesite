<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class AuthorController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function createauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function manageauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

