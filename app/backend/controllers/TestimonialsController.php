<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class TestimonialsController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function createtestimonialAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managetestimonialAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function edittestimonialsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

