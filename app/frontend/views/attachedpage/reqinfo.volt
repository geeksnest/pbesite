 <div class="entry-content" ng-controller="ContactCtrl">
    <p class="subphoto"><img alt="school-p1" class="alignnone size-full wp-image-307" src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/<?php echo $reqinfoimg ?>" width="150" /></p>
    <?php echo $reqinfo?>
    <div class="contact">
        <div role="form" class="wpcf7" id="wpcf7-f313-p309-o1" lang="en-US" dir="ltr">
            <div class="screen-reader-response"></div>
            <form ng-submit="sendrequestinfo(jb)" class="wpcf7-form" name="message">
                <p class="t1">First Name</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap first-name">
                        <input type="text" name="fname" ng-model="jb.fname" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t1">Last Name</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap last-name">
                        <input type="text" name="lname" ng-model="jb.lname" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t1">Email</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap your-email">
                        <input type="email" name="email" ng-model="jb.email" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t1">Interest Center</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap interest-center">
                        <select name="center" ng-model="jb.center" required class="wpcf7-form-control wpcf7-select input" id="interest-center" aria-invalid="false">
                            <option value="" style="display:none"></option>
                            <option ng-repeat="data in optioncenter" value="{[{data.title}]}">{[{data.title}]}</option>
                            <option value="Other Location">Other Location</option>
                        </select>
                    </span>
                </p>
                <p class="t1">Please choose a category for your request:</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap your-category">
                        <select name="category" ng-model="jb.category" required class="wpcf7-form-control wpcf7-select input" id="your-category" aria-invalid="false">
                            <option value="" style="display:none"></option>
                            <option value="Children’s BE Class">Children’s BE Class</option>
                            <option value="Adult BE Class">Adult BE Class</option>
                            <option value="BE in Schools">BE in Schools</option>
                            <option value="Others">Others</option>
                        </select>
                    </span>
                </p>
                <p class="t1 red">Please tell us what information you need</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap your-message">
                        <textarea name="content" ng-model="jb.content" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea input" aria-invalid="false" required></textarea>
                    </span>
                </p>
                <p class="t2">
                    <input type="submit" value="Submit" name="sendrequestinfo" class="wpcf7-form-control wpcf7-submit btn1"> 
                    <input type="reset" ng-click="reset()" value="Reset" class="btn2"></p><div class="wpcf7-response-output wpcf7-display-none"></div></form>
                </div>
                <div ng-show="notify" class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" role="alert" style="display: block;">Your message was sent successfully. Thanks.</div>
            </div>
        </div>