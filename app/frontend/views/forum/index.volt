<?php 

if(!@$_SERVER['HTTP_REFERER']){return header("/../elearning/login");}?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-controller="UserinfoCtrl"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	
	<link href="/fe/css/forum.css" rel="stylesheet">
	<!--  <link rel="stylesheet" type="text/css" href="/fe/font-awesome/css/font-awesome.min.css" />	 -->
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<link href="/be/css/font-awesome.min.css" rel="stylesheet">
	<style>
		.panel-heading span {
			margin-top: -20px;
			font-size: 15px;
		}
		.row {
			margin-top: 40px;
			padding: 0 10px;
		}
		.clickable {
			cursor: pointer;
		}    
	</style>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow">

			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<!--POWER BRAIN EDUCATION MAIN MENU -->
					<?=$this->view->getRender('etemplates', 'mMenu');?> 
				</div>
			</header>
			<div style="height:58px;"></div>
			<div class="clearboth"></div>
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eBanner');?> 
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eMenu');?> 
			<div class="container" >
				<div class="wrapper text-center" >
					<h1 class="welc">POWER BRAIN LEARNING COMMUNITY FORUM</h1>
				</div>
			</div>
			<div class="forum-cont">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div style="background:#5bc0de;" class="panel-heading ">
									<h3 class="panel-title">Main Category</h3>
								</div>
								<div class = "list-group">
									<a href = "#" class = "list-group-item">
										<h4 class = "list-group-item-heading">Sub Category 1</h4>
										<p class = "list-group-item-text">You will get a free domain registration with website pages.</p>
									</a>
									<a href = "#" class = "list-group-item">
										<h4 class = "list-group-item-heading">Sub Category 2</h4>
										<p class = "list-group-item-text">You will get a free domain registration with website pages.</p>
									</a>

								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="container " >
			<!-- SUBFOOTER -->
			<?=$this->view->getRender('etemplates', 'eFooter');?> 
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">
		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>


	<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
	<script src="/fe/scripts/others/bootstrap.min.js"></script>
	<script src="/fe/scripts/others/resposive-menu.js"></script>
	<!-- ANGULAR-->
	<?=$scritps ?>
	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
	<script type="text/javascript">
		jQuery(function ($) {
			$('.panel-heading span.clickable').on("click", function (e) {
				if ($(this).hasClass('panel-collapsed')) {
                // expand the panel
                $(this).parents('.panel').find('.panel-body').slideDown();
                $(this).removeClass('panel-collapsed');
                $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
            else {
                // collapse the panel
                $(this).parents('.panel').find('.panel-body').slideUp();
                $(this).addClass('panel-collapsed');
                $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        });
		});
	</script>

	

	
</body>
</html>