<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>CENTER</title>
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link rel="stylesheet" href="/vendors/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css">

</head>
<body data-ng-app="app">
	<?php
	if($logoimage->value1 == 1){
		header('Location: /../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-cloak>
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $logoimage->logo; ?>">
						</a>

					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<!-- ////PHP MENU -->
					<?php 
					?>
					<nav id="mainMenu" >
						<ul class="menuMain">
							 <?php
					             $bilang = count($parentMenu);
					             foreach ($parentMenu as $key => $value){
					              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
					              if($parentMenu[$key]->parentmenu =="Learning Community"){
					                ?><li><a  href="/../elearning/verify"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
					              }elseif($parentMenu[$key]->parentmenu =="Blog"){
					                ?><li><a  href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
					              }
					              else{
					                $page_link = '/../view/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
					                ?><li><a  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
					              }
					            }
					        ?>
						</ul>
					</nav>
					<!-- END NAVIGATION  FULL WIDTH-->
					<div id="subMenu" >
						<nav class="sub">
							<div class="sub-title">
								<a href="#" id="nav-close" class="pull-right"><img src="/../img/close.png" class="closemenu"></a>
								<img src="/../img/frontend/logo.png">
								<div class="clearBoth"></div>
							</div>
							<ul>
								<?php
					             $bilang = count($parentMenu);
					             foreach ($parentMenu as $key => $value){
					              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
					              if($parentMenu[$key]->parentmenu =="Learning Community"){
					                ?><li><a  href="/../elearning/verify"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
					              }elseif($parentMenu[$key]->parentmenu =="Blog"){
					                ?><li><a  href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
					              }
					              else{
					                $page_link = '/../view/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
					                ?><li><a  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
					              }
					            }
					        ?>
							</ul>
						</nav>
						<div class=" pull-right">
							<a id="nav-expander" class="nav-expander fixed">
								<img  src="/../img/responsivemenu.png" class="responsivemenu"><i class="fa fa-bars fa-lg white"></i>
							</a>
						</div>
					</div>
				</article>
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<!-- BANNER @Ryanjeric -->
		<?php 
		if($mainMenu=="what is brain education"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
					<div class="ttl">
						<h1 class="grn2"><span>What is Brain Education?</span></h1>
						<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
					</div>
				</article>
			</div>
			<?php
		}
		?>
		<?php 
		if($mainMenu=="benefits"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub21.jpg">
					<div class="ttl">
						<h1 class="grn2"><span>Benefits</span></h1>
						<p class="t1">Through mindfulness training and “Brain Versatilizing” exercises that promote neuroplasticity, Power Brain literally helps our kids build their focus and attention.</p>
					</div>
				</article>
			</div>
			<?php
		}
		?>
		<?php 
		if($mainMenu=="power brain training"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub31311-1.jpg">
					<?php
					if($subMenu == "for kids"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Kids</span></h1>
							<p class="t1">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
						</div>
						<?php  
					}
					?>
					<?php
					if($subMenu == "for adults"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Adults</span></h1>
							<p class="t1">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
						</div>
						<?php  
					}
					?>
					<?php
					if($subMenu == "for families"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Families</span></h1>
							<p class="t1">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
						</div>
						<?php  
					}
					?>

					<?php
					if($subMenu == "find center"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>Find a center</span></h1>
							<p class="t1">BE classes and leadership training for
								kids and adults</p>
							</div>
							<?php  
						}
						?>


					</article>
				</div>
				<?php
			}
			?>
			<?php 
			if($mainMenu=="power brain school"){
				?>
				<div class="container">
					<article style="slider">
						<img class="imgslider" src="/../img/frontend/from/img-sub31311-1.jpg">
						<div class="ttl">
							<h1 class="grn2"><span>Power Brain Schools</span></h1>
							<p class="t1">Power Brain Education has worked with 10,000 teachers and parents, and 30,000 students in over 350 schools nationwide.</p>
						</div>
					</article>
				</div>
				<?php
			}
			?>
			<?php 
			if($mainMenu=="get started"){
				?>
				<div class="container">
					<article style="slider">
						<img class="imgslider" src="/../img/frontend/from/img-sub312.jpg">
						<div class="ttl">
							<h1 class="grn2"><span>Get Started</span></h1>
							<p class="t1">The first step to starting Power Brain Training is scheduling an Introductory Session.</p>
						</div>
					</article>
				</div>
				<?php
			}
			?>
			<!--END OF Banner-->
			<div class="container" id="full-sub">
				<ul class="subnav subnav-3 grn2">
					<?php
					foreach ($subMenus as $key => $value) {
						if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
							$current = 'current_page_item';
						}else{
							$current = '';
						}

						?>
						<li class="page_item  <?=$current?>"><a href="/../<?=$subMenus[$key]->parentmenu?>/<?=$subMenus[$key]->submenu?>"><?=$subMenus[$key]->title?></a></li>
						<?php
					}
					?>
				</ul>
			</div>

			<div class="container" id="min-sub">
				<ul class="">
					<?php
					foreach ($subMenus as $key => $value) {
						?>
						<li ><a href="/../<?=$subMenus[$key]->parentmenu?>/<?=$subMenus[$key]->submenu?>"><?=$subMenus[$key]->title?></a></li>
						<?php
					}
					?>
				</ul>
			</div>

			<div class="container">
				<article class="content">

					<?php

					foreach ($subMenus as $key => $value) {
						if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
							$pageTitle=$subMenus[$key]->title;
						}
					}
					?>
					<p class="subpath"><a href="/../">Home</a><em>|</em><?=$mainMenu?><i class="arr1"></i><span class="grn2"><?=@$pageTitle?></span></p>
					<div class="entry-header">
						<h1 class="sec-ttl2 entry-title"><?=$centerinfo->title?></h1>
					</div>





					<div class="subbox7">
						<!-- SLIDER HERE -->
						<div class="col">
							<div id="wrapper">
								<div id="carousel-wrapper">
									<div id="carousel">
										<?php
										foreach ($centerslider as $value) {
											?>
											<span id="<?=$value->filename?>"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?=$value->filename?>" /></span>
											<?php						
										}		
										?>
									</div>
								</div>
								<div id="thumbs-wrapper">
									<div id="thumbs">
										<?php
										foreach ($centerslider as $value) {
											?>
											<a href="#<?=$value->filename?>"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?=$value->filename?>" /></a>
											<?php						
										}		
										?>
									</div>
									<a id="prev" href="#"></a>
									<a id="next" href="#"></a>
								</div>
							</div>
						</div>

						<div class="col2">
							<div class="box">
								<div class="link"><a class="fancybox" data-fancybox-type="iframe" href="http://powerbraineducation.com/fairfax-va/"><i class="ic-schedule"></i><br><span class="red">Class<br>Schedule</span></a></div>
								<div class="link"><a href="#centermap"><i class="ic-map"></i><br><span class="red">Map &amp;<br>Directions</span></a></div>
								<div class="link"><a href="#centercalendar"><i class="ic-calendar"></i><br><span class="red">Event<br>Calendar</span></a></div>
							</div>
							<div class="box2">
								<h4 class="t1">Hours</h4>
								<p class="t2"><span class="red">Monday - Friday:</span><?=$centerinfo->mffrom?>-<?=$centerinfo->mfto?><br>
									<span class="red">Saturday:</span> <?=$centerinfo->satfrom?>-<?=$centerinfo->satto?><br>
									<span class="red">Sunday:</span> by appointment</p>
									<h4 class="t1">ADDRESS</h4>
									<p class="t2">3903 FAIR RIDGE DR, STE 217<br>FAIRFAX, VA 22033</p>
									<p class="t3 red"><i class="ic-tell"></i><?=$centerinfo->contactnumber?></p>
								</div>
							</div>

							<div style="clear:both;"></div>
							<h3 class="sec-ttl3" style="margin-top:20px;">Meet our Director</h3>
							<p class="subphoto2">
								<img class="alignnone wp-image-515 size-full" src="<?=$centermanager->profile?>" alt="" width="150" height="150">
							</p>
							<?=$directormessage->message;?>


							<h3 class="sec-ttl3">Info for <?=$centermanager->fname?> <?=$centermanager->lname?> :</h3>
							<?=$directormessage->info;?>


							<h4 class="sec-ttl6" style="margin-top:25px;">News</h4>
							<br>
							<div ng-controller="NewsCtrl">
								<div ng-repeat="data in data.data">
									<!-- /../<?=$parentslug?>/<?=$dubslug?>/<?=$data->slugs?> <<LINK-->
									<a href="#" class="subbox">
										<p class="photo"> 
											<img width="225" height="141" src="{[{data.banner}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
										</p>
										<h3>{[{data.title}]}</h3>
										<span class="red">Date:</span> 9am-5pm / <span class="red">Author:</span> 9am-5pm <br><br>
										{[{data.desc}]}
									</a>
								</div>
								
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer text-center bg-light lter">
											<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
										</footer>
									</div>
								</div>
							</div>



							<h4 class="center-calendar" id="centercalendar">Event Calendar</h4>

							<div ng-app="demo" >
								<div ng-controller="MainCtrl" id="demo">
									<div class="col-md-12 ">
										<h2 class="text-center">{[{ calendarTitle }]}</h2>

										<div class="row">

											<div class="col-md-6 text-center">
												<div class="btn-group">

													<button
													class="btn btn-primary"
													mwl-date-modifier
													date="calendarDay"
													decrement="calendarView">
													Previous
												</button>
												<button
												class="btn btn-default"
												mwl-date-modifier
												date="calendarDay"
												set-to-today>
												Today
											</button>
											<button
											class="btn btn-primary"
											mwl-date-modifier
											date="calendarDay"
											increment="calendarView">
											Next
										</button>
									</div>
								</div>

								<br class="visible-xs visible-sm">

								<div class="col-md-6 text-center">
									<div class="btn-group">
										<label class="btn btn-primary" ng-model="calendarView" btn-radio="'year'">Year</label>
										<label class="btn btn-primary" ng-model="calendarView" btn-radio="'month'">Month</label>
										<label class="btn btn-primary" ng-model="calendarView" btn-radio="'week'">Week</label>
										<label class="btn btn-primary" ng-model="calendarView" btn-radio="'day'">Day</label>
									</div>
								</div>

							</div>

							<br>

							<mwl-calendar
							events="events"
							view="calendarView"
							view-title="calendarTitle"
							current-day="calendarDay"
							on-event-click="eventClicked(calendarEvent)"
							on-event-drop="eventDropped(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd"
							edit-event-html="'<i class=\'glyphicon glyphicon-pencil\'></i>'"
							delete-event-html="'<i class=\'glyphicon glyphicon-remove\'></i>'"
							on-edit-event-click="eventEdited(calendarEvent)"
							on-delete-event-click="eventDeleted(calendarEvent)"
							auto-open="true"
							day-view-start="06:00"
							day-view-end="22:00"
							day-view-split="30">
						</mwl-calendar>


					</div>
				</div>

			</div>
























		</div>
	</article>

	<aside class="aside">
		<p class="subpath"><i class="arr1"></i><a href="javascript:history.back();">Return to <span class="grn2">Previous Page</span></a></p>
		<a href="/../get-started/schedule-an-introductory-session" class="nofloat"><div class="sbn2"><img src="/../img/frontend/from/bn-side1.jpg"></div></a>
		<a href="/../get-started/request-information" class="nofloat"><div class="sbn2"><img src="/../img/frontend/from/bn-side2.jpg"></div></a>
		<div class="sbox3">
			<h4 class="sec-ttl6">Other locations <i class="ic-location"></i></h4>
			<ul class="box">
								<!-- <li><a href="http://powerbraineducation.com/locations/syosset-ny/"><span class="photo"><img width="74" height="40" src="http://powerbraineducation.com/wp-content/uploads/2014/09/img-center-ny2-150x81.jpg" class="attachment-74x40 wp-post-image" alt="img-center-ny2"></span>Syosset, NY</a></li>
								<li><a href="http://powerbraineducation.com/locations/bayside-ny/"><span class="photo"><img width="74" height="37" src="http://powerbraineducation.com/wp-content/uploads/2014/09/IMG_2380-150x75.jpg" class="attachment-74x40 wp-post-image" alt="IMG_2380 NY-Bayside training center"></span>Bayside, NY</a></li>
								<li><a href="http://powerbraineducation.com/locations/mesa-az/"><span class="photo"><img width="74" height="40" src="http://powerbraineducation.com/wp-content/uploads/2014/09/img-center-az-150x81.jpg" class="attachment-74x40 wp-post-image" alt="img-center-az"></span>Mesa, AZ</a></li>
								<li><a href="http://powerbraineducation.com/locations/fairfax-va/"><span class="photo"><img width="74" height="40" src="http://powerbraineducation.com/wp-content/uploads/2014/09/img-center-va-150x81.jpg" class="attachment-74x40 wp-post-image" alt="img-center-va"></span>Fairfax, VA</a></li>

								<div style="clear:both"></div> -->
							</ul>
						</div>




						<div class="sbox2">
							<h4>Testimonial</h4>

							<div class="listCont">
								<p class="photo"><a href=""><img src="<?php echo $featuredforAdultTestimonial->image; ?>"></a></p>
								<p class="prgrp"><?php echo $featuredforAdultTestimonial->content;?></p>
								<p class="morez"><a href="/../power-brain-training/for-adults/testimonials/">See Testimonials About Adults' Progress</a></p>
							</div>
							<br>
							<br>
							<div class="listCont">
								<p class="photo "><a href=""><img src="<?php echo $featuredforFamiliesTestimonial->image; ?>"></a></p>
								<p class="prgrp"><?php echo $featuredforFamiliesTestimonial->content;?></p>
								<p class="morez"><a href="/../power-brain-training/for-families/testimonials#/">See Testimonials About Families' Progress</a></p>
							</div>
						</div>
					</aside>
				</div>


				<div class="container border-top" >
					<!-- FOUR COL -->
					<article class="wrap-cont">
						<p class="title">Power Brain Training Center</p>
						<section class="clmn-4">
							<p class="round-img"><img src="/img/frontend/from/m-center1.jpg"></p>
							<div class="listCont">
								<p class="t1">For Kids</p>
								<p class="t2">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
								<p class="more"><a href="/../power-brain-training/for-kids">Read More</a></p>
							</div>
						</section>
						<section class="clmn-4">
							<p class="round-img"><img src="/img/frontend/from/m-center2.jpg"></p>
							<div class="listCont">
								<p class="t1">For Schools</p>
								<p class="t2">We've worked with thousands of students (pre-k ~12), teachers and parents in over 350 schools in the US.</p>
								<p class="more"><a href="/../power-brain-school/program">Read More</a></p>
							</div>
						</section>
						<section class="clmn-4 "></p>
							<p class="round-img"><img src="/img/frontend/from/m-center3.jpg"></p>
							<div class="listCont">
								<p class="t1">For Adults</p>
								<p class="t2">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
								<p class="more"><a href="/../power-brain-training/for-adults">Read More</a></p>
							</div>
						</section>
						<section class="clmn-4 "></p>
							<p class="round-img"><img src="/img/frontend/from/m-center4.jpg"></p>
							<div class="listCont">          
								<p class="t1">For Families</p>
								<p class="t2">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
								<p class="more"><a href="/../power-brain-training/for-families">Read More</a></p>
							</div>
						</section>

					</article>
					<!-- END FOUR COL -->
				</div>

				<div class="container " >
					<!-- SUBFOOTER -->
					<?=$this->view->getRender('etemplates', 'eFooter');?> 
					<!-- SUBFOOTER -->
				</div>
			</div>
			<div class="container">

				<!-- FOOTER -->
				<footer class="fcopy">
					Copyright 2015 Power Brain Training Center. All rights reserved.
				</footer>
				<!-- END FOOTER -->
			</div>
			<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
			<script src="/fe/scripts/others/bootstrap.min.js"></script>
			<script src="/fe/scripts/others/resposive-menu.js"></script>


			<!-- ANGULAR-->
			<script src="/fe/scripts/others/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>
			<script type="text/javascript">
				$(function() {
					$('#carousel').carouFredSel({
						responsive: true,
						circular: false,
						auto: true,
						items: {
							visible: 1,
							width: 100,
							height: '70%'
						},
						scroll: {
							pauseOnHover  : true,
							fx: 'crossfade',
							duration: 900
						}
					});

					$('#thumbs').carouFredSel({
						responsive: true,
						circular: false,
						infinite: true,
						auto: false,
						prev: '#prev',
						next: '#next',
						items: {
							visible: {
								min: 2,
								max: 4
							},
							width: 50,
							height: '50%'
						}
					});

					$('#thumbs a').click(function() {
						$('#carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
						$('#thumbs a').removeClass('selected');
						$(this).addClass('selected');
						return false;
					});

				});
			</script>



			<script type="text/javascript" src="/vendors/angular/angular.js"></script>
			<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
			<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
			<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>>
			<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
			<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
			<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
			<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
			<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
			<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
			<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
			<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>

			<script type="text/javascript" src="/fe/scripts/app.js"></script>
			<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
			<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
			<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
			<script type="text/javascript" src="/fe/scripts/config.js"></script>

			<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>


			<script src="/vendors/moment/moment.js"></script>
			<script src="/vendors/angular-moment/angular-moment.js"></script>

			<!-- ///UPLOADIMAGE -->
			<script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
			<script src="/vendors/ng-file-upload/ng-file-upload-shim.js"></script>


			

			
			<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.0/moment.min.js"></script>
			<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.4/interact.min.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/module.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendar.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/filters/calendarTruncateEventTitle.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/filters/calendarLimitTo.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/filters/calendarDate.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/calendarConfig.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/calendarTitle.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/calendarHelper.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/calendarDebounce.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/moment.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/services/interact.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCollapseFallback.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDateModifier.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarYear.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarMonth.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarWeek.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarDay.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarSlideBox.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarHourList.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDraggable.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDroppable.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/src/directives/mwlElementDimensions.js"></script>
			<script src="/vendors/angular-bootstrap-calendar/docs/scripts/demo.js"></script>

			

			<!-- Google Analytics-->
			<?php 
			echo $script_google->script;
			?>
			<!--End  Google Analytics-->



		</body>
		</html>