<!doctype html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title>Angular bootstrap calendar demo</title>
    <meta name="description" content="Angular bootstrap calendar demo">
    <meta name="viewport" content="width=device-width">
    <style type="text/css">
      [ng-cloak] {
        display: none;
      }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css">
    <link rel="stylesheet" href="/vendors/angular-bootstrap-calendar/docs/styles/main.css">
  </head>
  <body ng-app="demo" ng-cloak>

    
    <div class="container-fluid">
       <div class="row" ng-controller="MainCtrl" id="demo">
        <div class="col-md-8 col-md-offset-2">
          <h2 class="text-center">{[{ calendarTitle }]}</h2>

          <div class="row">

            <div class="col-md-6 text-center">
              <div class="btn-group">

                <button
                  class="btn btn-primary"
                  mwl-date-modifier
                  date="calendarDay"
                  decrement="calendarView">
                  Previous
                </button>
                <button
                  class="btn btn-default"
                  mwl-date-modifier
                  date="calendarDay"
                  set-to-today>
                  Today
                </button>
                <button
                  class="btn btn-primary"
                  mwl-date-modifier
                  date="calendarDay"
                  increment="calendarView">
                  Next
                </button>
              </div>
            </div>

            <br class="visible-xs visible-sm">

            <div class="col-md-6 text-center">
              <div class="btn-group">
                <label class="btn btn-primary" ng-model="calendarView" btn-radio="'year'">Year</label>
                <label class="btn btn-primary" ng-model="calendarView" btn-radio="'month'">Month</label>
                <label class="btn btn-primary" ng-model="calendarView" btn-radio="'week'">Week</label>
                <label class="btn btn-primary" ng-model="calendarView" btn-radio="'day'">Day</label>
              </div>
            </div>

          </div>

          <br>

          <mwl-calendar
            events="events"
            view="calendarView"
            view-title="calendarTitle"
            current-day="calendarDay"
            on-event-click="eventClicked(calendarEvent)"
            on-event-drop="eventDropped(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd"
            edit-event-html="'<i class=\'glyphicon glyphicon-pencil\'></i>'"
            delete-event-html="'<i class=\'glyphicon glyphicon-remove\'></i>'"
            on-edit-event-click="eventEdited(calendarEvent)"
            on-delete-event-click="eventDeleted(calendarEvent)"
            auto-open="true"
            day-view-start="06:00"
            day-view-end="22:00"
            day-view-split="30">
          </mwl-calendar>
      

        </div>
      </div>

    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.0/moment.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.min.js"></script>
    <script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.4/interact.min.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/module.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendar.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/filters/calendarTruncateEventTitle.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/filters/calendarLimitTo.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/filters/calendarDate.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/calendarConfig.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/calendarTitle.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/calendarHelper.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/calendarDebounce.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/moment.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/services/interact.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCollapseFallback.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDateModifier.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarYear.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarMonth.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarWeek.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarDay.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarSlideBox.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlCalendarHourList.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDraggable.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlDroppable.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/src/directives/mwlElementDimensions.js"></script>
    <script src="/vendors/angular-bootstrap-calendar/docs/scripts/demo.js"></script>
    <!-- Google Analytics-->
    <?php 
     echo $script_google->script;
     ?>
<!--End  Google Analytics-->
</body>
</html>
