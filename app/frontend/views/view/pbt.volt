<?php echo $this->getContent()?>

<?php
foreach ($subMenus as $key => $value) {
	// echo $subMenus[$key]->submenu.'/'.$subMenu.'<br>';
	if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
		$pageTitle=$subMenus[$key]->title;
	}
}
?>
<p class="subpath"><a href="/../">Home</a><em>|</em><?=$mainMenu?><i class="arr1"></i><span class="grn2"><?=$pageTitle?></span></p>
<div class="entry-header">
	<h1 class="sec-ttl2 entry-title"><?=$pageTitle?></h1>
</div>

<!-- <?=$contentPage?>--> 



<a href="#" class="subbox">
	<p class="photo"> <img width="225" height="141" src="http://powerbraineducation.com/wp-content/uploads/2014/09/bn-forkids1.jpg" class="attachment-full wp-post-image" alt="bn-forkids1"></p>
	<h3>Classes</h3>
	<p>Brain Education for Enhanced Learning (BEEL) is our introductory children’s program for students age 4-16. This 30-session program, typically delivered in weekly 90 minute classes, is designed to develop the “whole child” through hundreds of physical, emotional and cognitive exercises and activities.</p>
</a>


<a href="#" class="subbox">
	<p class="photo"> <img width="225" height="141" src="http://powerbraineducation.com/wp-content/uploads/2014/09/bn-forkids2.jpg" class="attachment-full wp-post-image" alt="bn-forkids2"></p>
	<h3>PB Student Leadership</h3>
	<p>Our BE Student Leadership Program offers an amazing opportunity for students to develop their character and leadership skills. Over the course of two full days, students participate in advanced Brain Education training (physical exercise, social/emotional regulation activities and brain exercises designed to improve confidence, focus, memory and imagination) with an emphasis on leadership skills.</p>
</a>

<a href="#" class="subbox">
	<p class="photo"> <img width="225" height="141" src="http://powerbraineducation.com/wp-content/uploads/2014/09/bn-forkids3.jpg" class="attachment-full wp-post-image" alt="bn-forkids3"></p>
	<h3>Global Leadership Camp</h3>
	<p>Through this 6-day transformational course, children, 3rd~12th grade, will have fun while learning to awaken their true potential as principled leaders in their community.  Children will deeply experience Brain Education (BE) through an innovative curriculum composed of: Ki Gong, BE principle study, physical training, brain games, brain screen study method, alpha brainwave focus training, and daily living application.</p>
</a>

<a href="#" class="subbox">
	<p class="photo"> <img width="225" height="141" src="http://powerbraineducation.com/wp-content/uploads/2014/09/bn-forkids5.jpg" class="attachment-full wp-post-image" alt="bn-forkids5"></p>
	<h3>Summer Program</h3>
	<p>Our interactive and engaging local summer program empowers students to improve social relationships, confidence, physical health, creativity, concentration and academic success.  Campers participate in an enriching variety of activities including: Mind-body coordination exercises, Brain Art creativity projects, Brain Power Study Enhancement Sessions and fun team building endeavors.</p>
</a>
