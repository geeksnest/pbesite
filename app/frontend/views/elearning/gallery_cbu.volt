<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Power Brain Education Learning Community Gallery</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
			width: 500px;
		}
		.fancybox-custom .fancybox-skin img{
			width: 100% !important;
		}
	</style>

	<!-- MODAL GALLERY -->
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/simplelightbox.min.css" media="screen" />
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/gcontainer.css" media="screen" />

	
</head>
<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="GalleryCtrl" ng-cloak>
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eBanner');?> 
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eMenu');?> 


		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">GALLERY</h1>
			</div>
		</div>
		<style type="text/css">

			#photos {
				/* Prevent vertical gaps */
				line-height: 0;

				-webkit-column-count: 5;
				-webkit-column-gap:   0px;
				-moz-column-count:    5;
				-moz-column-gap:      0px;
				column-count:         5;
				column-gap:           0px;
			}

			#photos img {
				/* Just in case there are inline attributes */
				width: 100% !important;
				height: auto !important;
			}

			@media (max-width: 1200px) {
				#photos {
					-moz-column-count:    4;
					-webkit-column-count: 4;
					column-count:         4;
				}
			}
			@media (max-width: 1000px) {
				#photos {
					-moz-column-count:    3;
					-webkit-column-count: 3;
					column-count:         3;
				}
			}
			@media (max-width: 800px) {
				#photos {
					-moz-column-count:    2;
					-webkit-column-count: 2;
					column-count:         2;
				}
			}
			@media (max-width: 400px) {
				#photos {
					-moz-column-count:    1;
					-webkit-column-count: 1;
					column-count:         1;
				}
			}


		</style>
		<div >
			<div class="gcontainer ">
				<div class="gallery " id="photos">	
					<a ng-repeat="data in imagelist" href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" >
						<img class="img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
					</a>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

</div>




<div class="container " >
	<!-- SUBFOOTER -->
	<?=$this->view->getRender('etemplates', 'eFooter');?> 
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>


<?=$scritps ?>

<!-- GRID GALLERY -->


<script type="text/javascript" src="/fe/gallery/simple-lightbox.min.js"></script>
<script>
	$(function(){
		var gallery = $('.gallery a').simpleLightbox();
	});


	$(document).ready(function() {
		function getRandomSize(min, max) {
			return Math.round(Math.random() * (max - min) + min);
		}
		for (var i = 0; i < 25; i++) {
			var width = getRandomSize(200, 400);
			var height =  getRandomSize(200, 400);
		}
	});
</script>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/gallery.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>

</body>
</html>