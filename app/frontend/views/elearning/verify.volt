<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">

	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->

	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">

	<link href="/be/css/animate.css" rel="stylesheet">
</head>

<body>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>
		<div class="container" ng-controller="LoginCtrl">
			<div class="wrapper text-center" style="margin-bottom:10%;margin-top:50px;">
				<img src='/img/logo.jpg' style="width:8%;height:auto;">
				<h2>Power Brain Education</h2>
				<h3>Learning Community</h3>

				<div class="loader" ng-show="imageloader">
					<div class="loadercontainer">
						<div class="spinner">
							<div class="rect1"></div>
							<div class="rect2"></div>
							<div class="rect3"></div>
							<div class="rect4"></div>
							<div class="rect5"></div>
						</div>
						Loading please wait...
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>


<!-- ANGULAR-->



<script type="text/javascript" src="/vendors/angular/angular.js"></script>
<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>

<script type="text/javascript" src="/fe/scripts/app.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
<script type="text/javascript" src="/fe/scripts/config.js"></script>

<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>



<script src="/vendors/moment/moment.js"></script>
<script src="/vendors/angular-moment/angular-moment.js"></script>
<script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>

<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>



<script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/gcal.js"></script>



<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src="/vendors/lodash/lodash.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false'></script>

<!-- Auth0 Lock script and AngularJS module -->
<script src="/fe/scripts/others/lock-7.5.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-storage/master/dist/angular-storage.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js"></script>
<script src="/fe/scripts/others/auth0-angular-4.js"> </script>


<!-- CONTROLLLERS -->
<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/verify.js"></script>

<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>
<!-- ///UPLOADIMAGE -->
<script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
<script src="/vendors/ng-file-upload/ng-file-upload-shim.js"></script>




</body>
</html>