<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
</head>

<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
				
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eBanner');?> 
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eMenu');?> 


		

		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">AN INTRODUCTION TO BRAIN EDUCATION</h1>
			</div>
		</div>
		<div class="elear-content el-frame">
			<iframe src="https://player.vimeo.com/video/141194525" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/141194525">Director Intro</a> from <a href="https://vimeo.com/powerbraineducation">PowerBrain Education</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
		</div>
		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">SHORT ACTIVITIES</h1>
			</div>
		</div>
		<div class="elear-content">
			<table class="d-table">
				<tbody>
					<tr>
						<td style="text-align:center;"><input type="checkbox" class="chk"></td>
						<td style="padding-left:10px;"><h3 style="margin-top:10px;">Message from the Program Director</h3></td>
						<td style="text-align:center;"><img class="img-icon" src="/../img/frontend/yticon.png"><img class="img-icon" src="/../img/frontend/pdficon.png"></td>
					</tr>

				</tbody>
			</table>
		</div>




	</div>




	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>


<!-- ANGULAR-->



<script type="text/javascript" src="/vendors/angular/angular.js"></script>
<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>

<script type="text/javascript" src="/fe/scripts/app.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
<script type="text/javascript" src="/fe/scripts/config.js"></script>

<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>



<script src="/vendors/moment/moment.js"></script>
<script src="/vendors/angular-moment/angular-moment.js"></script>
<script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>

<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>



<script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/gcal.js"></script>



<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src="/vendors/lodash/lodash.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false'></script>


<!-- Auth0 Lock script and AngularJS module -->
<script src="//cdn.auth0.com/js/lock-7.5.min.js"></script>

<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-storage/master/dist/angular-storage.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js"></script>

<script src="//cdn.auth0.com/w2/auth0-angular-4.js"> </script>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>

<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>



</body>
</html>