<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Power Brain Education Learning Community Gallery</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
			width: 500px;
		}
		.fancybox-custom .fancybox-skin img{
			width: 100% !important;
		}
	</style>

	<!-- MODAL GALLERY -->
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
</head>
<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="GalleryCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>
		<div class="container" id="full-sub">
			<ul class="subnav subnav-3 grn2 resize-menu" >
				<?php
				$subMenus = $subs;
				foreach ($subMenus as $keys => $values) {
					foreach ($subMenus[$keys] as $key => $value) {
						if($key==$cur_state){
							$current = '';
						}else{
							$current = '';
						}
						if($value == "Mentors"){
							$ngshow = 'ng-show="_mentors"';
						}elseif($value == "Leaders"){
							$ngshow = 'ng-show="_leades"';
						}else{
							$ngshow = '';
						}
						if($value == 'Gallery'){
							$current = 'current_page_item';
						}
						?>
						<li <?=$ngshow?> class="page_item  <?=$current?>"><a href="/../elearning/<?=$key?>"><?=$value?></a></li>
						<?php
					}
				}?>
			</ul>
			<div class="w-msg">
				<p>Hi! {[{usrname}]}</p>
				<p><a href="">My Account</a> | <a href="" ng-click="logout()">Logout</a></p>
			</div>
		</div>


		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">GALLERY</h1>
			</div>
		</div>
		<div class="elear-content">

		<!-- <div class="demo-gallery">
			<ul id="lightgallery" class="list-unstyled row">
				<li ng-repeat="data in imagelist" class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/1-375.jpg 375, img/1-480.jpg 480, img/1.jpg 800" 
				data-src="https://powerbrain.s3.amazonaws.com/uploads/banner/l5ots79o1or1444025071747.png">
				<a href="">
					<img class="img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
				</a>
			</li>
		</ul> -->
		<div class="col-sm-3"ng-repeat="data in imagelist">
			<a  class="fancybox-effects-b" href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" title="">
				<img class="img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
			</a>
		</div>

		<div style="clear:both"></div>	
	</div>
</div>

</div>




<div class="container " >
	<!-- SUBFOOTER -->
	<?=$footer ?>
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>


<?=$scritps ?>

<!-- GRID GALLERY -->

<script type="text/javascript">
	$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */
			 $('.fancybox').fancybox();
			/*
			 *  Different effects
			 */
			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});
			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});
			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,
				openEffect : 'none',
				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});
			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,
				openEffect : 'elastic',
				openSpeed  : 150,
				closeEffect : 'elastic',
				closeSpeed  : 150,
				closeClick : true,
				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			 $('.fancybox-buttons').fancybox({
			 	openEffect  : 'none',
			 	closeEffect : 'none',
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	closeBtn  : false,
			 	helpers : {
			 		title : {
			 			type : 'inside'
			 		},
			 		buttons	: {}
			 	},
			 	afterLoad : function() {
			 		this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 	}
			 });

			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			 $('.fancybox-thumbs').fancybox({
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	closeBtn  : false,
			 	arrows    : false,
			 	nextClick : true,
			 	helpers : {
			 		thumbs : {
			 			width  : 50,
			 			height : 50
			 		}
			 	}
			 });

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			 */
			 $('.fancybox-media')
			 .attr('rel', 'media-gallery')
			 .fancybox({
			 	openEffect : 'none',
			 	closeEffect : 'none',
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	arrows : false,
			 	helpers : {
			 		media : {},
			 		buttons : {}
			 	}
			 });
			/*
			 *  Open manually
			 */
			 $("#fancybox-manual-a").click(function() {
			 	$.fancybox.open('1_b.jpg');
			 });

			 $("#fancybox-manual-b").click(function() {
			 	$.fancybox.open({
			 		href : 'iframe.html',
			 		type : 'iframe',
			 		padding : 5
			 	});
			 });
			});
</script>

<script type="text/javascript" src="/fe/gallery/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/fe/gallery/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/fe/gallery/jquery.fancybox.js?v=2.1.5"></script>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/gallery.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>

</body>
</html><!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Power Brain Education Learning Community Gallery</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
			width: 500px;
		}
		.fancybox-custom .fancybox-skin img{
			width: 100% !important;
		}
	</style>

	<!-- MODAL GALLERY -->
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link   rel="stylesheet" type="text/css" href="/fe/gallery/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
</head>
<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="GalleryCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>
		<div class="container" id="full-sub">
			<ul class="subnav subnav-3 grn2 resize-menu" >
				<?php
				$subMenus = $subs;
				foreach ($subMenus as $keys => $values) {
					foreach ($subMenus[$keys] as $key => $value) {
						if($key==$cur_state){
							$current = '';
						}else{
							$current = '';
						}
						if($value == "Mentors"){
							$ngshow = 'ng-show="_mentors"';
						}elseif($value == "Leaders"){
							$ngshow = 'ng-show="_leades"';
						}else{
							$ngshow = '';
						}
						if($value == 'Gallery'){
							$current = 'current_page_item';
						}
						?>
						<li <?=$ngshow?> class="page_item  <?=$current?>"><a href="/../elearning/<?=$key?>"><?=$value?></a></li>
						<?php
					}
				}?>
			</ul>
			<div class="w-msg">
				<p>Hi! {[{usrname}]}</p>
				<p><a href="">My Account</a> | <a href="" ng-click="logout()">Logout</a></p>
			</div>
		</div>


		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">GALLERY</h1>
			</div>
		</div>
		<div class="elear-content">

		<!-- <div class="demo-gallery">
			<ul id="lightgallery" class="list-unstyled row">
				<li ng-repeat="data in imagelist" class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/1-375.jpg 375, img/1-480.jpg 480, img/1.jpg 800" 
				data-src="https://powerbrain.s3.amazonaws.com/uploads/banner/l5ots79o1or1444025071747.png">
				<a href="">
					<img class="img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
				</a>
			</li>
		</ul> -->
		<div class="col-sm-3"ng-repeat="data in imagelist">
			<a  class="fancybox-effects-b" href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" title="">
				<img class="img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
			</a>
		</div>

		<div style="clear:both"></div>	
	</div>
</div>

</div>




<div class="container " >
	<!-- SUBFOOTER -->
	<?=$footer ?>
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>


<?=$scritps ?>

<!-- GRID GALLERY -->

<script type="text/javascript">
	$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */
			 $('.fancybox').fancybox();
			/*
			 *  Different effects
			 */
			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});
			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});
			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,
				openEffect : 'none',
				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});
			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,
				openEffect : 'elastic',
				openSpeed  : 150,
				closeEffect : 'elastic',
				closeSpeed  : 150,
				closeClick : true,
				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			 $('.fancybox-buttons').fancybox({
			 	openEffect  : 'none',
			 	closeEffect : 'none',
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	closeBtn  : false,
			 	helpers : {
			 		title : {
			 			type : 'inside'
			 		},
			 		buttons	: {}
			 	},
			 	afterLoad : function() {
			 		this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 	}
			 });

			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			 $('.fancybox-thumbs').fancybox({
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	closeBtn  : false,
			 	arrows    : false,
			 	nextClick : true,
			 	helpers : {
			 		thumbs : {
			 			width  : 50,
			 			height : 50
			 		}
			 	}
			 });

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			 */
			 $('.fancybox-media')
			 .attr('rel', 'media-gallery')
			 .fancybox({
			 	openEffect : 'none',
			 	closeEffect : 'none',
			 	prevEffect : 'none',
			 	nextEffect : 'none',
			 	arrows : false,
			 	helpers : {
			 		media : {},
			 		buttons : {}
			 	}
			 });
			/*
			 *  Open manually
			 */
			 $("#fancybox-manual-a").click(function() {
			 	$.fancybox.open('1_b.jpg');
			 });

			 $("#fancybox-manual-b").click(function() {
			 	$.fancybox.open({
			 		href : 'iframe.html',
			 		type : 'iframe',
			 		padding : 5
			 	});
			 });
			});
</script>

<script type="text/javascript" src="/fe/gallery/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/fe/gallery/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/fe/gallery/jquery.fancybox.js?v=2.1.5"></script>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/gallery.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>

</body>
</html>