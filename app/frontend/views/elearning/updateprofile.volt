<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link href="/fe/css/eprof.css" rel="stylesheet">
	<link href="/fe/css/app.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
	<script>var _prof = null;</script>
</head>
<body ng-controller="UserinfoCtrl">
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow" ng-controller="EuserinfoCtrl" ng-cloak>
			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<!--POWER BRAIN EDUCATION MAIN MENU -->
					<?=$this->view->getRender('etemplates', 'mMenu');?> 
				</div>
			</header>
			<div style="height:58px;"></div>
			<div class="clearboth"></div>

			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eBanner');?> 
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eMenu');?> 

			<div class="container" >
				<div class="prof-wrapper">
					<h1 class="welc">Profile Info</h1>
				</div>
				<div class="prof-menu ">
					<ol style="font-size:15px !important;">
						<li><a href="/elearning/profile">&raquo; Profile</a></li>
						<li><a href="/elearning/media"> Media Gallery</a></li>
						<li><a href="/elearning/mediavid"> Media Embed</a></li>
						<li><a href="/elearning/profile/password"> Change Password</a></li>
					</ol>
				</div>
				<div class="prof-content">
					<div>
						<h3>Update Profile</h3>
						<form name="form" class="form-validation ">
							<input type="hidden" class="form-control" name="password" ng-model="user.userid" ng-value="user.userid = usrid" required>   

							<input type="hidden" class="form-control" name="password" ng-model="user.task" required>  
							<input type="hidden" class="form-control" name="password" ng-model="user.status" required>  
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<span class="h4">Personal Information</span>
								</div>
								<div class="panel-body">
									<p class="text-muted">Please fill the information to continue</p>
									<div class="form-group">
										<label>First Name</label>
										<input type="text" class="form-control " ng-model="user.fname" ng-pattern="/^[a-zA-Z0-9]{4,10}$/" required>
									</div>
									<div class="form-group">
										<label>Last Name</label>
										<input type="text" class="form-control " ng-model="user.lname" ng-pattern="/^[a-zA-Z0-9]{4,10}$/" required>
									</div>
									<div class="form-group pull-in clearfix">
										<label>Birth Date :</label><br>
										<div class="col-sm-3">
											<label>Year</label>
											<select name="account" class="form-control m-b" ng-model="user.year" required>
												<option value=""></option>
												<?=$_yropt ?>
											</select>
										</div>
										<div class="col-sm-4">
											<label>Month</label>
											<select name="account" class="form-control m-b" ng-model="user.month" required>
												<option value=""></option>
												<option value="01">January</option>
												<option value="02">Febuary</option>
												<option value="03">March</option>
												<option value="04">April</option>
												<option value="05">May</option>
												<option value="06">June</option>
												<option value="07">July</option>
												<option value="08">August</option>
												<option value="09">September</option>
												<option value="10">October</option>
												<option value="11">November</option>
												<option value="12">December</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label>Day</label>
											<select name="account" class="form-control m-b" ng-model="user.day"   required>
												<option value=""></option>
												<?=$_dayopt ?>
											</select>
										</div>
									</div>
									<div class="form-group pull-in clearfix">
										<label>Gender</label><br>
										<div class="col-sm-3">
											<select  name="account" class="form-control m-b" ng-model="user.gender"   required>
												<option value=""></option>
												<option>Male</option>
												<option>Female</option>
											</select>
										</div>
									</div>
								</div>

							</div>
							
							<div class="col-sm-6">
								<div class="panel-heading">
									<span class="h4">Upload Profile</span>
								</div>
								<div class="col-sm-6">
									<div class="panel-body">
										<div class="col-sm-12" ng-show="imagecontent">
											<div class="">
												<div ngf-drop ngf-select ng-model="files" ngf-change="upload(files)" class="drop-box"
												ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
												accept="image/*,application/pdf" style="line-height:30px;">Drop images here or click to upload</div>
											</div>
										</div>
									</div>
									<div class="loader" ng-show="imageloader">
										<div class="loadercontainer">
											<div class="spinner">
												<div class="rect1"></div>
												<div class="rect2"></div>
												<div class="rect3"></div>
												<div class="rect4"></div>
												<div class="rect5"></div>
											</div>
											Uploading your images please wait...
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<img src="https://powerbrain.s3.amazonaws.com/uploads/banner/{[{user.banner}]}" style="width:220px;height:210px;padding:10px;">
								</div>
								<div style="clear:both"></div>
							</div>
							
							<div class="col-sm-6" class="panel panel-default">
								<div class="panel-heading">
									<span class="h4">Account Information</span>
								</div>
								<div class="panel-body">
									<p class="text-muted">Please fill the information to continue</p>
									<div class="form-group">
										<label>Username <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em></label>


										<input type="text" class="form-control " ng-model="user.username"  ng-pattern="/^[a-zA-Z0-9]{4,10}$/" required> 
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="text" class="form-control" name="password" ng-model="user.email"  required>   
									</div>
								</div>
								<hr>
								<footer class="panel-footer text-right bg-light lter">
									<button type="submit" class="btn btn-success" ng-disabled=" user.username == null ||  user.fname == null || user.lname =='' || user.year == '' || user.month == '' || user.day == '' || user.gender == ''" ng-click="submit(user)">Update</button>
								</footer>
							</div>

						</form>

						<div style="clear:both;"></div>
					</div>

				</div>
			</div>
		</div>




		<div class="container " >
			<!-- SUBFOOTER -->
			<?=$this->view->getRender('etemplates', 'eFooter');?> 
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">

		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>

	<?=$scritps ?>

	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/euser.js"></script>

	<!-- /// FACTORY -->
	<script type="text/javascript" src="/fe/scripts/factory/user/elcuser.js"></script>

</body>
</html>