<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<link href="/be/css/animate.css" rel="stylesheet">
	<!-- Loading Bar -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<style>
		.ui-progressbar {
			position: relative;
		}
		.progress-label {
			position: absolute;
			left: 50%;
			top: 4px;
			font-weight: bold;
			text-shadow: 1px 1px 0 #fff;
		}
	</style>
	<script type="text/javascript">
		var userid = "<?=$userid;?>";
		var code = "<?=$code;?>";
	</script>
</head>
<body >
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>

		<div class="container">
			<div class="wrapper text-center" style="margin-bottom:10%;margin-top:50px;">
				<img src='/img/logo.jpg' style="width:8%;height:auto;">
				<h2>Power Brain Eduction</h2>
				<h3>Thank You!!! Your account is now for review</h3>
				<h4>But you can browse our website.</h4>
			</div>
		</div>
	</div>

	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<?=$scritps ?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	
</script>
<!-- <script type="text/javascript" src="/fe/scripts/controllers/elearning/verify.js"></script> -->

<!-- /// FACTORY -->
<!-- <script type="text/javascript" src="/fe/scripts/factory/user/elcuser.js"></script> -->

</body>
</html>