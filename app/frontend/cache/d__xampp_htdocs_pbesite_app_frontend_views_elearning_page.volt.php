<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Login Power Brain Education Learning Community</title>
	<meta name="description" content="<?=$centerinfo->desc?>">
	<meta name="keywords" content="<?=$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">

	<link href="/fe/css/elearning.css" rel="stylesheet">

	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->

	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">

	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<script>
		var centerid = "<?=$centerid;?>";
		console.log(centerid);
	</script>
</head>
<body>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>

					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<!-- ////PHP MENU -->
					<?php 
					?>
					<nav id="mainMenu" >
						<ul class="menuMain">
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value) {
             // ARROW
								--$bilang;
								if($bilang==0){
									$arrow= '<i class="arr"></i>';
								}else{
									$arrow="";
								}
            // arrow
								//echo $parentMenu[$key]->slug.' | '.$mainMenu;
								if($parentMenu[$key]->parentmenu=="Learning Community"){
									$curparent = 'class="active';
								}else{
									$curparent = '';
								}

								?>
								<li><a <?=$curparent?>  href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>/page"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
								<?php
							}
							?>
						</ul>
					</nav>
					<!-- END NAVIGATION  FULL WIDTH-->
					<div id="subMenu" >
						<nav class="sub">
							<div class="sub-title">
								<a href="#" id="nav-close" class="pull-right">close</a>
								<img src="/../img/frontend/logo.png">
								<div class="clearBoth"></div>
							</div>
							<ul>
								<?php
								$bilang = count($parentMenu);
								foreach ($parentMenu as $key => $value) {
          // ARROW
									--$bilang;
									if($bilang==0){
										$arrow= '<i class="arr"></i>';
									}else{
										$arrow= "";
									}
          // ARROW
									?>
									<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>/page"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
									<?php
								}
								?>
							</ul>
						</nav>
						<div class=" pull-right">
							<a id="nav-expander" class="nav-expander fixed">
								MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
							</a>
						</div>
					</div>
				</article>
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		
		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>



		<div class="container">
			<div class="wrapper text-center" style="margin-bottom:10%;">
				<img src='/img/hammer.gif' style="width:8%;height:auto;">

				<h2>Power Brain Eduction</h2>
				<h3>Learning Community</h3>


		
					<div class="form-group frm-input ">
						<input type="text" class="form-control" placeholder="Username" >
					</div>
					<div class="form-group frm-input ">
						<input type="password" class="form-control" placeholder="Password">
					</div>
					<div class="form-group frm-input" style="margin-top:-15px;">
						<p style="float:right;"><a href="#">Forgot Your Password?</a></p>
					</div>
					<div style="clear:both"></div>
					<div class="form-group frm-input ">
						<button type="submit" class="btn btn-sm btn-info login-btn" >Login</button>

					</div>		
		
				
			</div>
			
		</div>


	</div>




	<div class="container " >
		<!-- SUBFOOTER -->
		<article class="fcon">
			<section class="fcon-box1">
				<p class="tt1 ">Click Here To</p>
				<a href="/../view/page/get-started/schedule-an-introductory-session/page" class="btn1"><b>Get Started</b><i class="arr"></i></a>
			</section>
			<section class="fcon-box2 ">
				<ul class="fcon-li box-font">
					<li><a href="/../view/page/what-is-brain-education/background/page">What is Brain Education?</a></li>
					<li><a href="/../view/page/benefits/focus/page">Benefits</a></li>
					<li><a href="/../view/page/power-brain-training/for-kids/page">Power Brain Training Center</a></li>
					<li><a href="/../view/page/power-brain-school/program/page">Power Brain Schools</a></li>
				</ul>
			</section>
			<section class="fcon-box2-1">
				<ul class="fcon-li box-font">
					<li><a href="/../view/page/get-started/introductory-session/page">BE Around the World</a></li>
					<li><a href="/../view/page/get-started/introductory-session/page">Terms and Conditions</a></li>
					<li><a href="/../view/page/get-started/introductory-session/page">Contact Us</a></li>
				</ul>
			</section>
			<section class="fcon-box2-2">
				<ul class="fcon-li box-font" ng-controller="CenterCtrl">
					<li class="t4">Find a Location</li>
					<li ng-repeat="data in data.data | limitTo:3">
						<a href="/../view/center/power-brain-training/find-center/{[{data.slugs}]}/">{[{ data.state }]}: {[{ data.title | limitTo: 17 }]}{[{ data.title.length > 17 ? '...' : '' }]}</a>
					</li>
				</ul>
			</section>
			<section class="fcon-box3 ">
				<ul class="soc-mid">
					<li id="desyrel">Stay Connected</li>
					<li><a href="https://www.facebook.com/powerbraineducation" class="fb" target="_blank"><span>fb</span></a></li>
					<li><a href="https://twitter.com/powerbrainedu" class="tw"  target="_blank"><span>tw</span></a></li>
					<li><a href="https://www.youtube.com/powerbraineducation" class="yt"  target="_blank"><span>yt</span></a></li>
					<li><a href="/../index/rss" class="rss"  target="_blank"><span>rss</span></a></li>
				</ul>
			</section>
		</article>
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>


<!-- ANGULAR-->



<script type="text/javascript" src="/vendors/angular/angular.js"></script>
<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>

<script type="text/javascript" src="/fe/scripts/app.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
<script type="text/javascript" src="/fe/scripts/config.js"></script>

<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>



<script src="/vendors/moment/moment.js"></script>
<script src="/vendors/angular-moment/angular-moment.js"></script>
<script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>

<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>



<script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/gcal.js"></script>



<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src="/vendors/lodash/lodash.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false'></script>



<script type="text/javascript" src="/fe/scripts/controllers/centermap/centermap.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/centermap/mapfactory.js"></script>


<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>


</body>
</html>