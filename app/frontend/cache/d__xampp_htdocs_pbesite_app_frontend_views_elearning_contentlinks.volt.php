<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
	<script>var _state = "cl";</script>
</head>
<body ng-controller="UserinfoCtrl">
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow" ng-controller="trainingCtrl" ng-cloak>
			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<!--POWER BRAIN EDUCATION MAIN MENU -->
					<?=$this->view->getRender('etemplates', 'mMenu');?> 
				</div>
			</header>
			<div style="height:58px;"></div>
			<div class="clearboth"></div>

			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eBanner');?> 
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eMenu');?> 
			

			<div class="container" >
				<div class="wrapper text-center" >
					<h1 class="welc">Content Links</h1>
				</div>
			</div>
			<div class="elear-content el-frame" >
				<iframe src="https://player.vimeo.com/video/142986113" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/142986113">21_Content Link Intro</a> from <a href="https://vimeo.com/powerbraineducation">PowerBrain Education</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
			</div>


			<div class="elear-content">
				<div class="e-cont" ng-repeat="data in traininglist | filter:{page:'contlinks'}">
					<a href="/../elearning/contentpage/{[{data.slug}]}">
						<div class="ebanner">
							<img src="{[{data.logo}]}">
						</div>
						<div class="econtent">
							<div class="econtent-title">
								<h1>{[{data.title}]}</h1>
							</div >
							<div class="econtent-desc"><div ng-bind-html="data.description"></div></div>
						</div>
						<div class="econtent" style="width:100% !important">
							<br>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {[{data.percent}]};">
									{[{data.percent}]}
								</div>
							</div>
						</div>
					</a>
					<div style="clear:both;" ></div>
				</div>
				<div style="margin-bottom:100px !important;"></div>
			</div>	
		</div>




		<div class="container " >
			<!-- SUBFOOTER -->
			<?=$this->view->getRender('etemplates', 'eFooter');?> 
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">

		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>

	<?=$scritps ?>

	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>


	<!-- ///ELEARNING -->
	<script type="text/javascript" src="/fe/scripts/controllers/elearning/training.js"></script>

	<!-- /// FACTORY -->
	<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
	<script type="text/javascript" src="/fe/scripts/factory/elearning/tmedia.js"></script>


</body>
</html>