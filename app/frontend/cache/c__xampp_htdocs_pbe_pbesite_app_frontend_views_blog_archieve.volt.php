<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Blog</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
		<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->

	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">

	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<script>
		var publish = "<?php echo $publish; ?>";
	</script>
</head>
<body>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="BlogarchieveCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>
					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<nav id="mainMenu" >
						<ul class="menuMain">
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value){
				              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
				              if($parentMenu[$key]->slug==str_replace(' ','-',$mainMenu)){$curparent = 'class="active"';}else{$curparent = '';}// CHECK IF ACTIVE

				            if($parentMenu[$key]->parentmenu =="Learning Community"){
				                ?><!-- <li><a <?=$curparent?>  href="/../elearning/login"><?=$parentMenu[$key]->parentmenu?></a></li> --><?php // IF PAGE IS LEARNING COMMUNITY
				            }elseif($parentMenu[$key]->parentmenu =="Blog"){?><li><a class="active" href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
				            }
				            else{
				            	$page_link = '/../view/page/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'/page';
				            	?><li><a <?=$curparent?>  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
				            }
				        }
				        ?>
				    </ul>
				</nav>
				<!-- END NAVIGATION  FULL WIDTH-->
				<div id="subMenu" >
					<nav class="sub">
						<div class="sub-title">
							<a href="#" id="nav-close" class="pull-right">close</a>
							<img src="/../img/frontend/logo.png">
							<div class="clearBoth"></div>
						</div>
						<ul>
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value) {
          							// ARROW
								--$bilang;
								if($bilang==0){
									$arrow= '<i class="arr"></i>';
								}else{
									$arrow= "";
								}
          							// ARROW
								?>
								<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>/page"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
								<?php
							}
							?>
						</ul>
					</nav>
					<div class=" pull-right">
						<a id="nav-expander" class="nav-expander fixed">
							MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
						</a>
					</div>
				</div>
			</article>
		</div>
	</header>
	<div style="height:58px;"></div>
	<div class="clearboth"></div>
	<!-- BANNER @Ryanjeric -->
	<div class="container">
		<article style="slider">
			<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
			<div class="ttl">
				<h1 class="grn2"><span>What is Brain Education?</span></h1>
				<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
			</div>
		</article>
	</div>

	<div class="container" ng-cloak>
		<article class="content">
			<p class="subpath"><a href="/../">Home</a><em>|</em><a href="/blog/page">Blog</a><i class="arr1"></i><span class="grn2"><?=@strtoupper($publish)?></span></p>
			<table class="table m-b-none" style="margin-bottom:0px;">
				<tbody>
					<tr ng-repeat="data in blog | limitTo : limit">
						<td style="border-style:none !important">
							<div class="vidcontainer">
								<!-- /../view/page/<?=$parentslug?>/<?=$dubslug?>/<?=$data->slugs?> <<LINK-->
								<a href="/../blog/read/{[{data.blogid}]}" class="subbox">
									<p class="photo"> 
										<!-- <img width="225" height="141" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.banner}]}" class="attachment-full wp-post-image" alt="bn-forkids1"> -->
										<div class="blog-fetured hide-p">
											<div ng-if="data.newtype == 'youtube'" >
												<img width="225" height="141" src="{[{data.videourl}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
												<div class="youtube-play"><img src="/img/youtubeplay.png" ></div>
											</div>
											<div ng-if="data.newtype == 'vimeo'" >
												<img class="vimeo-{[{  vimeoid}]}" width="225" height="141" src="" class="attachment-full wp-post-image" alt="bn-forkids1">
												<div class="vimeo-play"><img src="/img/vimeo.png" ></div>
											</div>
											<div  ng-if="data.newtype == 'banner'" >
												<img width="225" height="141" src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{data.featured}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
											</div>
										</div>
										
									</p>
									<h3>{[{data.title}]}</h3>
									<span class="red">Date:</span> {[{data.publish}]} / <span class="red">Author:</span> {[{data.name}]}<br><br>
									{[{data.description}]}
								</a>
							</div>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5" class="text-center">
							<button ng-hide="hideloadmore" class="show_more" ng-click="showmoreblogs()" ng-disabled="loading">
								<span ng-show="loading">Loading...</span><span ng-hide="loading">Show More...</span>
							</button>
						</td>
					</tr>
				</tfoot>
			</table>

		</article>

		<aside class="aside">
			<p class="subpath"><i class="arr2"></i><a href="javascript:history.back();">Return to <span class="grn2">Previous Page</span></a></p>

			<span class="sbn">Blog Index</span>
			<div class="sbox2">
				<h4>Categories</h4>
				<div class="listCont">
					<ul>
						<?php
						$getginfo = $newscategory;
						foreach ($getginfo as $key => $value) {
							?>
							<li><a href="/blog/category/<?php echo $getginfo[$key]->categoryslugs; ?>"><?php echo $getginfo[$key]->categoryname; ?></a></li>
							<?php } ?>
					</ul>					
				</div>
				<hr>
				<h4>Tags</h4>
				<div class="listCont">
					<div class="tagsL">
						<?php
						$gettags = $newstagslist;
						$count = 1;
						foreach ($gettags as $key => $value) { ?>
						<a href="/blog/tag/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
						<?php
						$count > 5 ? $count =1 : $count++;
						} ?>
					</div>
				</div>
				<hr>
				<h4> Archives</h4>
				<div class="listCont">
					<ul class="nf">
						<?php
						$getarchives = $archivelist;
						foreach ($getarchives as $key => $value) { ?>
						<li><a href="/blog/archieve/<?php echo $getarchives[$key]->datepublished; ?>"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a></li>
						<?php } ?>
					</ul>					
				</div>
				<br>
				<br>				
			</div>			
		</aside>
	</div>
	<!-- ////// -->

	<div class="container border-top" >
		<!-- FOUR COL -->
		<article class="wrap-cont">
			<p class="title">Power Brain Training Center</p>
			<section class="clmn-4">
				<p class="round-img"><img src="/img/frontend/from/m-center1.jpg"></p>
				<div class="listCont">
					<p class="t1">For Kids</p>
					<p class="t2">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
					<p class="more"><a href="/../view/page/power-brain-training/for-kids/page">Read More</a></p>
				</div>
			</section>
			<section class="clmn-4">
				<p class="round-img"><img src="/img/frontend/from/m-center2.jpg"></p>
				<div class="listCont">
					<p class="t1">For Schools</p>
					<p class="t2">We've worked with thousands of students (pre-k ~12), teachers and parents in over 350 schools in the US.</p>
					<p class="more"><a href="/../view/page/power-brain-school/program/page">Read More</a></p>
				</div>
			</section>
			<section class="clmn-4 "></p>
				<p class="round-img"><img src="/img/frontend/from/m-center3.jpg"></p>
				<div class="listCont">
					<p class="t1">For Adults</p>
					<p class="t2">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
					<p class="more"><a href="/../view/page/power-brain-training/for-adults/page">Read More</a></p>
				</div>
			</section>
			<section class="clmn-4 "></p>
				<p class="round-img"><img src="/img/frontend/from/m-center4.jpg"></p>
				<div class="listCont">          
					<p class="t1">For Families</p>
					<p class="t2">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
					<p class="more"><a href="/../view/page/power-brain-training/for-families/page">Read More</a></p>
				</div>
			</section>

		</article>
		<!-- END FOUR COL -->
	</div>

	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$footer ?>
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/archieve.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/eblog.js"></script>



</body>
</html>