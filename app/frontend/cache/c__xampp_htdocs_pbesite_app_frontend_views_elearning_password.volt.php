<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link href="/fe/css/eprof.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
	<script>var _state = "cl";</script>
	<script>var _prof = null;</script>
</head>
<body ng-controller="UserinfoCtrl">
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow" ng-controller="EuserinfoCtrl" ng-cloak>
			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<!--POWER BRAIN EDUCATION MAIN MENU -->
					<?=$this->view->getRender('etemplates', 'mMenu');?> 
				</div>
			</header>
			<div style="height:58px;"></div>
			<div class="clearboth"></div>
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eBanner');?> 
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eMenu');?> 

			<div class="container" >

				<div class="prof-wrapper">
					<h1 class="welc">Profile Info</h1>
				</div>
				<div class="prof-menu ">
					<ol style="font-size:15px !important;">
						<li><a href="/elearning/profile">&raquo; Profile</a></li>
						<li><a href="/elearning/media"> Media Gallery</a></li>
						<li><a href="/elearning/profile/password">Change Password</a></li>
					</ol>
				</div>
				<div class="prof-content">




					<div>
						<h3>Change Password</h3>
						<form name="form" class="form-validation " ng-submit="chngpass(chnge)">
							<input type="hidden" class="form-control " ng-model="chnge.userid" ng-value="chnge.userid = usrid" required>   
							<div class="panel panel-default">
								<div class="panel-heading">
									<span class="h4">Account Information</span>
								</div>
								<div class="panel-body">
									<p class="text-muted">Please fill the information to continue</p>
									<div class="form-group">
										<label>Old password</label>
										<input type="password" class="form-control " ng-model="chnge.oldpass" required> 
									</div>
									<p class="text-muted">Enter New Password</p>
									<div class="form-group">
										<label>New Password</label>
										<input type="password" class="form-control" name="password" ng-model="chnge.password" required>   
									</div>
									<div class="form-group">
										<label>Confirm New Password</label>
										<input type="password" class="form-control " name="confirm_password" required ng-model="chnge.confirm_password" ui-validate=" '$value==chnge.password' " ui-validate-watch=" 'password' " required>
										<!-- <span ng-show="form.confirm_password.$error.validator" class="ng-hide">Passwords do not match!</span> -->
									</div>
								</div>

								<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
								<footer class="panel-footer text-right bg-light lter">
									<button type="submit" class="btn btn-success"  >Submit</button>
								</footer>
							</div>
						</form>
						<div style="clear:both;"></div>
					</div>



				</div>
			</div>
		</div>




		<div class="container " >
			<!-- SUBFOOTER -->
			<?=$this->view->getRender('etemplates', 'eFooter');?> 
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">

		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>

	<?=$scritps ?>

	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/euser.js"></script>

	<!-- /// FACTORY -->
	<script type="text/javascript" src="/fe/scripts/factory/user/elcuser.js"></script>

</body>
</html>