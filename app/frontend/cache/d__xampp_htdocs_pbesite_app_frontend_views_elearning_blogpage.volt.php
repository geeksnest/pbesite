<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Login Power Brain Education Learning Community</title>
	<meta name="description" content="<?=$centerinfo->desc?>">
	<meta name="keywords" content="<?=$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
</head>
<script>
	var idno = "<?=$idno;?>";
</script>
<body ng-controller="UserinfoCtrl">
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="BlogCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>
					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<!-- ////PHP MENU -->
					<?php 
					?>
					<nav id="mainMenu" >
						<ul class="menuMain">
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value) {
             // ARROW
								--$bilang;
								if($bilang==0){
									$arrow= '<i class="arr"></i>';
								}else{
									$arrow="";
								}
            // arrow
								//echo $parentMenu[$key]->slug.' | '.$mainMenu;
								if($parentMenu[$key]->parentmenu=="Learning Community"){
									$curparent = 'class="active';
								}else{
									$curparent = '';
								}

								?>
								<li><a <?=$curparent?>  href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>/page"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
								<?php
							}
							?>
						</ul>
					</nav>
					<!-- END NAVIGATION  FULL WIDTH-->
					<div id="subMenu" >
						<nav class="sub">
							<div class="sub-title">
								<a href="#" id="nav-close" class="pull-right">close</a>
								<img src="/../img/frontend/logo.png">
								<div class="clearBoth"></div>
							</div>
							<ul>
								<?php
								$bilang = count($parentMenu);
								foreach ($parentMenu as $key => $value) {
          // ARROW
									--$bilang;
									if($bilang==0){
										$arrow= '<i class="arr"></i>';
									}else{
										$arrow= "";
									}
          // ARROW
									?>
									<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>/page"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
									<?php
								}
								?>
							</ul>
						</nav>
						<div class=" pull-right">
							<a id="nav-expander" class="nav-expander fixed">
								MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
							</a>
						</div>
					</div>
				</article>
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>
		<div class="container" id="full-sub">
			<ul class="subnav subnav-3 grn2 resize-menu" >
				<?php
				$subMenus = $subs;
				foreach ($subMenus as $keys => $values) {

					foreach ($subMenus[$keys] as $key => $value) {
						if($key==$cur_state){
							$current = 'current_page_item';
						}else{
							$current = '';
						}
						?>
						<li class="page_item  <?=$current?>"><a href="/../elearning/view/learning-community/<?=$key?>/page"><?=$value?></a></li>
						<?php
					}
				}?>
			</ul>
			<div class="w-msg">
				<p>Hi! {[{usrname}]}</p>
				<p><a href="">My Account</a> | <a href="" ng-click="logout()">Logout</a></p>
			</div>
		</div>
		<div class="container" id="min-sub">
			<ul class="">

			</ul>
		</div>

		<div ng-repeat="data in blog | filter:{blogid:_idno}">
			<div class="elear-content container" >
				<div class="wrapper text-center" >
					<h1 class="welc">{[{data.title}]}</h1>
				</div>
			</div>
			<div class="elear-content">
				<div ng-if="data.featuredtype == 'video'" >
					<div ng-bind-html="data.featured"></div>
				</div>
				<div ng-if="data.featuredtype == 'banner'">
					<img src="{[{data.featured}]}">
				</div>
			</div>
			<div class="elear-content" >
				{[{data.description}]}
			</div>
			<div class="elear-content" >
				<div ng-bind-html="data.body"></div>
				<div style="margin-bottom:100px !important;"></div>
			</div>
		</div>
		<div style="clear:both;" ></div>
	</div>
	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$footer ?>
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">
	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>

<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>

<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/viewblog.js"></script>

<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/eblog.js"></script>
</body>
</html>