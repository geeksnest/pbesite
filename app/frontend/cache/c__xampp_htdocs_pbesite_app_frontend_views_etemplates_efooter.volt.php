<article class="fcon">
	<section class="fcon-box1">
		<p class="tt1 ">Click Here To</p>
		<a href="/../get-started/schedule-an-introductory-session" class="btn1"><b>Get Started</b><i class="arr"></i></a>
	</section>
	<section class="fcon-box2 ">
		<ul class="fcon-li box-font">
			<li><a href="/../what-is-brain-education/background">What is Brain Education?</a></li>
			<li><a href="/../benefits/focus">Benefits</a></li>
			<li><a href="/../power-brain-training/for-kids">Power Brain Training Center</a></li>
			<li><a href="/../power-brain-school/program">Power Brain Schools</a></li>
		</ul>
	</section>
	<section class="fcon-box2-1">
		<ul class="fcon-li box-font">
			<li><a href="/../get-started/introductory-session">BE Around the World</a></li>
			<li><a href="/../get-started/introductory-session">Terms and Conditions</a></li>
			<li><a href="/../get-started/introductory-session">Contact Us</a></li>
		</ul>
	</section>
	<section class="fcon-box2-2">
		<ul class="fcon-li box-font" ng-controller="CenterCtrl">
			<li class="t4">Find a Location</li>
        <!--
        <li ng-repeat="data in data.data | limitTo:3">
          <a href="/../center/power-brain-training/find-center/{[{data.slugs}]}/">{[{ data.state }]}: {[{ data.title | limitTo: 17 }]}{[{ data.title.length > 17 ? \'...\' : \'\' }]}</a>
        </li> 
    -->
    <li>
    	<a href="/../center/mesa">AZ: Gilbert/Mesa</a>
    </li>
    <li>
    	NY:
    	<a href="/../center/bayside"> Bayside</a>,
    	<a href="/../center/syosset">Syosset</a>
    </li>
    <li>
    	<a href="/../center/fairfax">VA: Fairfax</a>
    </li>
</ul>
</section>
<section class="fcon-box3 ">
	<ul class="soc-mid">
		<li id="desyrel">Stay Connected</li>
		<li><a href="<?php echo $social[0]->url;?>" class="fb" target="_blank"><span>fb</span></a></li>
		<li><a href="<?php echo $social[1]->url;?>" class="tw"  target="_blank"><span>tw</span></a></li>
		<li><a href="<?php echo $social[2]->url;?>" class="yt"  target="_blank"><span>yt</span></a></li>
		<li><a href="/../index/rss" class="rss"  target="_blank"><span>rss</span></a></li>
	</ul>
</section>