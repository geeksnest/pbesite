<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
</head>
<script>
	var classid = "<?=$classid;?>";
</script>

<body ng-controller="UserinfoCtrl">
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="ClassCtrl" ng-cloak>
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eBanner');?> 
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eMenu');?> 


		
		<div class="elear-content container" >
			<div class="wrapper text-center" >
				<h1 class="welc">{[{emedia[0].title}]}</h1>
			</div>
		</div>
		<div class="elear-content el-frame">
			<div ng-bind-html="preview"></div>
		</div>
		<div class="elearning" >
			<div class="wrapper text-center" >

			</div>
		</div>
		<div ng-show="files" class="elear-content" >
			<div style="background:#ddd;padding:10px;">
				<h3 style="float:left" class="welc">Download:</h3>
				<div class="pdf-media" style="text-align:center;" ng-repeat="data in dbpdf">
					<a href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" target="blank">
						<img class="img-pdf" src="/../img/frontend/pdficon.png">
					</a>
					<a href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" target="blank">
						<p>{[{data.filename}]}</p>
					</a>
				</div>
				<div style="clear:both;" ></div>
			</div>
		</div>
		<br>
		<div class="elear-content">
			<div ng-bind-html="body"></div>

			<!-- <textarea class="ck-editor" ng-model="msg" required="required"></textarea> -->
			<div class="text-center">
				<input type="hidden" ng-model="usrid">
				<input type="hidden" ng-model="classid" ng-value="classid = classid">
				<button class="submit-class" ng-click="submitlesson({usrid:usrid,classid:classid,msg:'test'})">I HAVE COMPLETED THIS LESSON</button>
			</div>

		</div>

	</div>
	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">
	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>

<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>

<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/class.js"></script>

<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/elearning/tmedia.js"></script>
<script type="text/javascript" src="/be/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/be/js/ckeditor/styles.js"></script>
</body>
</html>