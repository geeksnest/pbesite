<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
</head>

<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>
					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<nav id="mainMenu" >
						<ul class="menuMain">
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value){
				              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
				              if($parentMenu[$key]->slug==str_replace(' ','-',$mainMenu)){$curparent = 'class="active"';}else{$curparent = '';}// CHECK IF ACTIVE

				              if($parentMenu[$key]->parentmenu =="Learning Community"){
				                ?><li><a class="active"  href="/../elearning/verify"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
				            }elseif($parentMenu[$key]->parentmenu =="Blog"){
				                ?><li><a  href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
				            }
				            else{
				            	$page_link = '/../view/page/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
				            	?><li><a   href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
				            }
				        }
				        ?>
				    </ul>
				</nav>
				<!-- END NAVIGATION  FULL WIDTH-->
				<div id="subMenu" >
					<nav class="sub">
						<div class="sub-title">
							<a href="#" id="nav-close" class="pull-right">close</a>
							<img src="/../img/frontend/logo.png">
							<div class="clearBoth"></div>
						</div>
						<ul>
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value) {
          // ARROW
								--$bilang;
								if($bilang==0){
									$arrow= '<i class="arr"></i>';
								}else{
									$arrow= "";
								}
          // ARROW
								?>
								<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
								<?php
							}
							?>
						</ul>
					</nav>
					<div class=" pull-right">
						<a id="nav-expander" class="nav-expander fixed">
							MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
						</a>
					</div>
				</div>
			</article>
		</div>
	</header>
	<div style="height:58px;"></div>
	<div class="clearboth"></div>
	
	<div class="container">
		<article style="slider">
			<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
			<div class="ttl">
				<h1 class="grn2"><span>What is Brain Education?</span></h1>
				<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
			</div>
		</article>
	</div>
	<div class="container" id="full-sub">
		<ul class="subnav subnav-3 grn2 resize-menu drop" >
			<?php
			$subMenus = $subs;
			foreach ($subMenus as $keys => $values) {
				foreach ($subMenus[$keys] as $key => $value) {
					if($key==$cur_state){
						$current = '';
					}else{
						$current = '';
					}
					if($value == "Mentors"){
						$ngshow = 'ng-show="_mentors"';
					}elseif($value == "Leaders"){
						$ngshow = 'ng-show="_leades"';
					}else{
						$ngshow = '';
					}

					if($value == 'Leaders'){
						$current = 'current_page_item';
					}


					if($value == 'Teacher Resources'){$_sub_menu = $teachersub;}else{$_sub_menu='';}
					if($value == 'Training Reinforcement'){$_sub_menu_t = $triningsub;}else{$_sub_menu_t='';}
					if($value == 'Content Links'){$_contlink = $contentsub;}else{$_contlink='';}
					?>
					<li<?=$ngshow?> class="page_item  <?=$current?>"><a href="/../elearning/<?=$key?>"><?=$value?></a><?=@$_sub_menu?><?=@$_sub_menu_t?><?=@$_contlink?></li>
					<?php
				}
			}?>
		</ul>
		<div class="w-msg">
			<p>Hi! {[{usrname}]}</p>
			<p><a href="">My Account</a> | <a href="" ng-click="logout()">Logout</a></p>
		</div>
	</div>
	<div class="container" id="min-sub">
		<ul class="">
			<?php
			$subMenus = $subs;
			foreach ($subMenus as $keys => $values) {
				foreach ($subMenus[$keys] as $key => $value) {
					?>
					<li><a href="/../elearning/<?=$key?>"><?=$value?></a></li>
					<?php
				}
			}?>
		</ul>
	</div>
	<div class="container" >
		<div class="wrapper text-center" >
			<h1 class="welc">LEADERS</h1>
		</div>
	</div>
	<div class="elear-content el-frame">
		<iframe src="https://player.vimeo.com/video/142986112" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/142986112">25_Leader intro</a> from <a href="https://vimeo.com/powerbraineducation">PowerBrain Education</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
	</div>
	<div class="container" >
		<div class="wrapper text-center" >
			<h1 class="welc">LEADERS VIDEO</h1>
		</div>
	</div>
	<div class="elear-content" ng-controller="LeadersCtrl">
		<table class="d-table">
			<tbody>
				<tr ng-repeat="data in rmedia ">
					<td style="text-align:center;width:50px;"><input type="checkbox" class="chk" ng-checked="data.checked" disabled></td>
					<td style="padding-left:10px;"><h3 style="margin-top:10px;"><a href="/../elearning/class/{[{data.idno}]}">{[{data.title}]}</a></h3></td>
						<!-- <td style="text-align:center;width:90px;">
							<a href="/../elearning/class/{[{data.idno}]}" ng-click="lastview({userid:uid, title:data.title,link:'/../elearning/class/'+data.idno} )">
								<img class="img-icon" src="/../img/frontend/yticon.png"><img ng-show="data.pdficon" class="img-icon" src="/../img/frontend/pdficon.png">
							</a>
						</td> -->
						<td style="text-align:center;width:50px;">
							<a href="/../elearning/class/{[{data.idno}]}">
								<img class="img-icon" src="/../img/frontend/yticon.png">
							</a>
						</td>
						<td style="text-align:center;" ng-show="data.pdficon">
							<a href="/../elearning/class/{[{data.idno}]}">
								<img  class="img-icon" src="/../img/frontend/pdficon.png">
							</a>
						</td>

					</tr>
				</tbody>
			</table>
		</div>
	</div>




	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$footer ?>
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>


<?=$scritps ?>



<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>

<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/leaders.js"></script>

<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/elearning/tmedia.js"></script>

</body>
</html>