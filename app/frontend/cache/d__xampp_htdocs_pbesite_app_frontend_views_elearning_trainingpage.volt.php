<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Login Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
<meta Http-Equiv="Pragma" Content="no-cache">
<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
	<script>var _state = "tr";</script>
</head>
<script>
	_idno = "<?=$idno?>";
</script>

<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="TrainigpageCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>

		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eBanner');?> 
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eMenu');?> 

		
		<div ng-repeat="data in traininglist | filter:{page:'training', title:slug}" style="background:{[{data.theme}]};">
			<div class="elear-content" style="border:0px !important" >
				<div class="e-cont" style="border:0px !important; color:#fff;" >
					<div class="ebanner"><img src="{[{data.logo}]}"></div>
					<div class="econtent">
						<div class="econtent-title">
							<h1>{[{data.title}]}</h1>
						</div >
						<div class="econtent-desc"><div ng-bind-html="data.description"></div></div>
					</div>
					<div style="clear:both;" ></div>
				</div>
			</div>
		</div>

		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">SHORT ACTIVITIES</h1>
			</div>
		</div>
		<div class="elear-content">
			<table class="d-table">
				<tbody>
					<tr ng-repeat="data in rmedia | orderBy:'-title'">
						<td style="text-align:center;width:50px;"><input type="checkbox" class="chk" ng-checked="data.checked" disabled></td>
						<td style="padding-left:10px;"><h3 style="margin-top:10px;"><a ng-click="lastview({userid:uid, title:data.title,link:'/../elearning/class/'+data.slug})" href="/../elearning/class/{[{data.slug}]}">{[{data.title}]}</a></h3></td>
						<td style="text-align:center;width:50px;">
							<a href="/../elearning/class/{[{data.slug}]}" ng-click="lastview({userid:uid, title:data.title,link:'/../elearning/class/'+data.slug})">
								<img class="img-icon" src="/../img/frontend/yticon.png">
							</a>
						</td>
						<td ng-show="data.pdficon" style="text-align:center;width:50px;">
							<a href="/../elearning/class/{[{data.slug}]}" ng-click="lastview({userid:uid, title:data.title,link:'/../elearning/class/'+data.slug})">
								<img  class="img-icon" src="/../img/frontend/pdficon.png">
							</a>
						</td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>




	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>


<!-- ANGULAR-->


<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>


<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/trainingpage.js"></script>

<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/elearning/tmedia.js"></script>





</body>
</html>