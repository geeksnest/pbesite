<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title><?=$centerinfo->title?></title>
	<meta name="description" content="<?=$centerinfo->desc?>">
	<meta name="keywords" content="<?=$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->

	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">

	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<script>
		var centerid = "<?=$centerid;?>";
	</script>
</head>
<body>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>

					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
				
					 <nav id="mainMenu" >
			            <ul class="menuMain">
			              <?php
					             $bilang = count($parentMenu);
					             foreach ($parentMenu as $key => $value){
					              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
					              if($parentMenu[$key]->parentmenu =="Learning Community"){
					                ?><li><a  href="/../elearning/verify"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
					              }elseif($parentMenu[$key]->parentmenu =="Blog"){
					                ?><li><a  href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
					              }
					              else{
					                $page_link = '/../view/page/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
					                ?><li><a  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
					              }
					            }
					        ?>
			          </ul>
			        </nav>
					<!-- END NAVIGATION  FULL WIDTH-->
					<div id="subMenu" >
						<nav class="sub">
							<div class="sub-title">
								<a href="#" id="nav-close" class="pull-right">close</a>
								<img src="/../img/frontend/logo.png">
								<div class="clearBoth"></div>
							</div>
							<ul>
								<?php
								$bilang = count($parentMenu);
								foreach ($parentMenu as $key => $value) {
          // ARROW
									--$bilang;
									if($bilang==0){
										$arrow= '<i class="arr"></i>';
									}else{
										$arrow= "";
									}
          // ARROW
									?>
									<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
									<?php
								}
								?>
							</ul>
						</nav>
						<div class=" pull-right">
							<a id="nav-expander" class="nav-expander fixed">
								MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
							</a>
						</div>
					</div>
				</article>
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<!-- BANNER @Ryanjeric -->
		<?php 
		if($mainMenu=="what is brain education"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
					<div class="ttl">
						<h1 class="grn2"><span>What is Brain Education?</span></h1>
						<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
					</div>
				</article>
			</div>
			<?php
		}
		?>
		<?php 
		if($mainMenu=="benefits"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub21.jpg">
					<div class="ttl">
						<h1 class="grn2"><span>Benefits</span></h1>
						<p class="t1">Through mindfulness training and “Brain Versatilizing” exercises that promote neuroplasticity, Power Brain literally helps our kids build their focus and attention.</p>
					</div>
				</article>
			</div>
			<?php
		}
		?>
		<?php 
		if($mainMenu=="power brain training"){
			?>
			<div class="container">
				<article style="slider">
					<img class="imgslider" src="/../img/frontend/from/img-sub31311-1.jpg">
					<?php
					if($subMenu == "for kids"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Kids</span></h1>
							<p class="t1">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
						</div>
						<?php  
					}
					?>
					<?php
					if($subMenu == "for adults"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Adults</span></h1>
							<p class="t1">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
						</div>
						<?php  
					}
					?>
					<?php
					if($subMenu == "for families"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>For Families</span></h1>
							<p class="t1">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
						</div>
						<?php  
					}
					?>

					<?php
					if($subMenu == "find center"){
						?>
						<div class="ttl">
							<h1 class="grn2"><span>Find a center</span></h1>
							<p class="t1">BE classes and leadership training for
								kids and adults</p>
							</div>
							<?php  
						}
						?>


					</article>
				</div>
				<?php
			}
			?>
			<?php 
			if($mainMenu=="power brain school"){
				?>
				<div class="container">
					<article style="slider">
						<img class="imgslider" src="/../img/frontend/from/img-sub31311-1.jpg">
						<div class="ttl">
							<h1 class="grn2"><span>Power Brain Schools</span></h1>
							<p class="t1">Power Brain Education has worked with 10,000 teachers and parents, and 30,000 students in over 350 schools nationwide.</p>
						</div>
					</article>
				</div>
				<?php
			}
			?>
			<?php 
			if($mainMenu=="get started"){
				?>
				<div class="container">
					<article style="slider">
						<img class="imgslider" src="/../img/frontend/from/img-sub312.jpg">
						<div class="ttl">
							<h1 class="grn2"><span>Get Started</span></h1>
							<p class="t1">The first step to starting Power Brain Training is scheduling an Introductory Session.</p>
						</div>
					</article>
				</div>
				<?php
			}
			?>
			<!--END OF Banner-->
			<div class="container" id="full-sub">
				<ul class="subnav subnav-3 grn2">
					<?php
					foreach ($subMenus as $key => $value) {
						if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
							$current = 'current_page_item';
						}else{
							$current = '';
						}

						?>
						<li class="page_item  <?=$current?>"><a href="/../view/page/<?=$subMenus[$key]->parentmenu?>/<?=$subMenus[$key]->submenu?>"><?=$subMenus[$key]->title?></a></li>
						<?php
					}
					?>
				</ul>
			</div>

			<div class="container" id="min-sub">
				<ul class="">
					<?php
					foreach ($subMenus as $key => $value) {
						?>
						<li ><a href="/../view/page/<?=$subMenus[$key]->parentmenu?>/<?=$subMenus[$key]->submenu?>"><?=$subMenus[$key]->title?></a></li>
						<?php
					}
					?>
				</ul>
			</div>

			<div class="container">
				<article class="content">
					<?php

					foreach ($subMenus as $key => $value) {
						if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
							$pageTitle=$subMenus[$key]->title;
						}
					}
					?>
					<p class="subpath"><a href="/../">Home</a><em>|</em><?=$mainMenu?><i class="arr1"></i><span class="grn2"><?=@$pageTitle?></span></p>
					<div class="entry-header">
						<h1 class="sec-ttl2 entry-title"><?=$centerinfo->title?></h1>
					</div>
					<div class="subbox7">
					
						<!-- SLIDER HERE -->
						<div class="col">
							<div id="wrapper">
								<div id="carousel-wrapper">
									<div id="carousel">
										<?php


										foreach ($centerslider as $value) {
											?>
											<span id="<?=$value->id?>"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?=$value->filename?>" /></span>
											<?php						
										}		
										?>
									</div>
								</div>
								<div id="thumbs-wrapper">
									<div id="thumbs">
										<?php
										foreach ($centerslider as $value) {
											?>
											<a href="#<?=$value->id?>" ><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?=$value->filename?>" /></a>
											<?php						
										}		
										?>
									</div>
									<a id="prev" href="#"></a>
									<a id="next" href="#"></a>
								</div>
							</div>
						</div>

						<div class="col2" ng-controller="ScheduleCtrl">
							<div class="box">
								<div class="link">
									<script type="text/ng-template" id="feschedule.html">
										<div ng-include="'/be/tpl/feschedule.html'"></div>
									</script>
									<a class="fancybox" data-fancybox-type="iframe" href="#" ng-click="modal('lg')"><i class="ic-schedule"></i><br><span class="red">Class<br>Schedule</span></a>
								</div>
								<div class="link" ng-click="mapscroll()"><a href=""><i class="ic-map"></i><br><span class="red">Map &amp;<br>Directions</span></a></div>
								<div class="link"><a href="#centercalendar"><i class="ic-calendar"></i><br><span class="red">Event<br>Calendar</span></a></div>
							</div>
							<div class="box2">
								<h4 class="t1">Hours</h4>
								<p class="t2"><span class="red">Monday-Friday:</span><?=$centerinfo->mffrom?>-<?=$centerinfo->mfto?><br>
									<span class="red">Saturday:</span> <?=$centerinfo->satfrom?>-<?=$centerinfo->satto?><br>
									<span class="red">Sunday:</span> by appointment</p>
									<h4 class="t1">ADDRESS</h4>
									<p class="t2"><?=$centerinfo->address?></p>
									<p class="t3 red"><i class="ic-tell"></i><?=$centerinfo->contactnumber?></p>
								</div>
							</div>

							<div style="clear:both;"></div>
							<h3 class="sec-ttl3" style="margin-top:20px;">Meet our Director</h3>
							<p class="subphoto2">
								<img class="alignnone wp-image-515 size-full" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?=$centermanager->profile?>" alt="" width="150" height="150">
							</p>
							<?=$directormessage->message;?>
							<?php 
							if( trim($directormessage->info)!=""){
								?>
								<div>
									<h3 class="sec-ttl3">Info for <?=$centermanager->fname?> <?=$centermanager->lname?> :</h3>
									<?=$directormessage->info;?>
								</div>
								<?php
							}
							?>
							<h4 class="sec-ttl6" style="margin-top:25px;">News</h4>
							<br>
							<div ng-controller="NewsCtrl">
								<div ng-repeat="data in data">
									<a href="/../view/centernews/power-brain-training/find-center/{[{data.slug}]}" class="subbox">
										
										<p class="photo" ng-if="data.featuredoption == 'video'"> 
											<img width="225" height="141" src="{[{data.banner}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
										</p>

										<p class="photo" ng-if="data.featuredoption == 'banner'"> 
											<img width="225" height="141" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.banner}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
										</p>

										<h3>{[{data.title}]}</h3>
										<span class="red">Date:</span> {[{data.publish}]} / <span class="red">Author:</span> {[{data.authorname}]} <br><br>
										{[{data.description}]}
									</a>
								</div>
								
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer text-center bg-light lter">
											<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
										</footer>
									</div>
								</div>
							</div>
							<h4 class="centermap" ng-controller="ScheduleCtrl">Map &amp; Directions</h4>
							<div ng-controller="Centermapfe" style="margin-bottom:50px;">

								<div id="map_canvas">
									<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
									<ui-gmap-search-box template="searchbox.template" events="searchbox.events"></ui-gmap-search-box>
									<ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
								</ui-gmap-marker>
							</ui-gmap-google-map>
							<div ng-cloak>
								<ul>
									<li>coords update ctr: {[{coordsUpdates}]}</li>
									<li>dynamic move ctr: {[{dynamicMoveCtr}]}</li>
								</ul>
							</div>
						</div>
						<style type="text/css">
							html, body, #map_canvas {
								height: 500px;
								width: 100%;
								margin: 0px;
							}
							#map_canvas {
								position: relative;
							}
							.angular-google-map-container {
								position: absolute;
								top: 0;
								bottom: 0;
								right: 0;
								left: 0;
							}
							#pac-input {
								background-color: #fff;
								font-family: Roboto;
								font-size: 15px;
								font-weight: 300;
								margin-left: 12px;
								margin-top: :20px;
								padding: 0 11px 0 13px;
								text-overflow: ellipsis;
								width: 300px;
							}
						</style>
					</div>

					<h4 class="center-calendar">Event Calendar</h4>
					<div ng-controller="viewcalendarCtrl" id="demo">
						<div class="">
							<h2 class="text-center">{[{ calendarTitle }]}</h2>
								<div class="clearfix m-b">
									<!-- <div class="pull-right">
										<button type="button" class="btn btn-sm btn-default" ng-click="today(calendar1)">today</button>
										<div class="btn-group m-l-xs">
											<button class="btn btn-sm btn-default" ng-click="changeView('agendaDay', 'calendar1')">Day</button>
											<button class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', 'calendar1')">Week</button>
											<button class="btn btn-sm btn-default" ng-click="changeView('month', 'calendar1')">Month</button>
										</div>
									</div> -->
								</div>
								<div class="pos-rlt" >
									<div class="fc-overlay" >
										<div class="panel bg-white b-a pos-rlt" style="border:2px solid #aaa !important; color:#555 !important ;">
											<span class="arrow"></span>
											<div class="bg font-thin m-b-sm" style="color:#fff !important;background:#ed6459;padding:10px;">{[{event.title}]}</div>
											<div class=" b-b b-light" style="padding:10px;">
												{[{event.title}]}
											</div>
											<!-- <div><i class="icon-calendar text-muted m-r-xs"></i> {[{event.start | date:'medium'}]}</div>
											<div class="ng-hide" ng-show='event.end'><i class="icon-clock text-muted m-r-xs"></i> {[{event.end | date:'medium'}]}</div>
											<div class="ng-hide" ng-show='event.location'><i class="icon-pointer text-muted m-r-xs"></i> {[{event.location}]}</div>
											<div class="m-t-sm">{[{event.info}]}</div>
											<div class="m-t-sm">{[{event.url}]}</div> -->
										</div>
									</div>
									<style type="text/css">
										td {
											/*//border: 1px solid #aaa !important;*/
										}
									</style>

									<div style="color:#ed6459 !important;text-align:left !important;" class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
								</div>
							
							


						</div>
					</div>
				</div>
			</article>

			<aside class="aside">
				<p class="subpath"><i class="arr1"></i><a href="javascript:history.back();">Return to <span class="grn2">Previous Page</span></a></p>
				<a href="/../view/page/get-started/schedule-an-introductory-session" class="nofloat"><div class="sbn2"><img src="/../img/frontend/from/bn-side1.jpg"></div></a>
				<a href="/../view/page/get-started/request-information" class="nofloat"><div class="sbn2"><img src="/../img/frontend/from/bn-side2.jpg"></a></div></a>
				<div class="sbox3">
					<h4 class="sec-ttl6">Other locations <i class="ic-location"></i></h4>
					<ul class="box" ng-controller="CenterCtrl">
						<li ng-repeat="data in data.data | limitTo:5">
							<a href="/../view/center/<?=$parentslug.'/'.$dubslug.'/{[{data.slugs}]}'?>"><span class="photo"><img width="74" height="40" src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.banner}]}" class="attachment-74x40 wp-post-image" alt="img-center-ny2"></span>{[{data.title}]}</a>
						</li>

						<div style="clear:both"></div>
					</ul>
				</div>




				<div class="sbox2">
					<h4>Testimonial</h4>

					<div class="listCont">
						<p class="photo"><a href=""><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/testimonialimage/<?php echo $featuredforAdultTestimonial->image; ?>"></a></p>
						<p class="prgrp"><?php echo @$featuredforAdultTestimonial->content;?></p>
						<p class="morez"><a href="/../view/page/power-brain-training/for-adults/testimonials/">See Testimonials About Adults' Progress</a></p>
					</div>
					<br>
					<br>
					<div class="listCont">
						<p class="photo "><a href=""><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/testimonialimage/<?php echo $featuredforFamiliesTestimonial->image; ?>"></a></p>
						<p class="prgrp"><?php echo @$featuredforFamiliesTestimonial->content;?></p>
						<p class="morez"><a href="/../view/page/power-brain-training/for-families/testimonials#/">See Testimonials About Families' Progress</a></p>
					</div>
				</div>
			</aside>
		</div>


		<div class="container border-top" >
			<!-- FOUR COL -->
			<article class="wrap-cont">
				<p class="title">Power Brain Training Center</p>
				<section class="clmn-4">
					<p class="round-img"><img src="/img/frontend/from/m-center1.jpg"></p>
					<div class="listCont">
						<p class="t1">For Kids</p>
						<p class="t2">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
						<p class="more"><a href="/../view/page/power-brain-training/for-kids">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4">
					<p class="round-img"><img src="/img/frontend/from/m-center2.jpg"></p>
					<div class="listCont">
						<p class="t1">For Schools</p>
						<p class="t2">We've worked with thousands of students (pre-k ~12), teachers and parents in over 350 schools in the US.</p>
						<p class="more"><a href="/../view/page/power-brain-school/program">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4 "></p>
					<p class="round-img"><img src="/img/frontend/from/m-center3.jpg"></p>
					<div class="listCont">
						<p class="t1">For Adults</p>
						<p class="t2">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
						<p class="more"><a href="/../view/page/power-brain-training/for-adults">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4 "></p>
					<p class="round-img"><img src="/img/frontend/from/m-center4.jpg"></p>
					<div class="listCont">          
						<p class="t1">For Families</p>
						<p class="t2">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
						<p class="more"><a href="/../view/page/power-brain-training/for-families">Read More</a></p>
					</div>
				</section>

			</article>
			<!-- END FOUR COL -->
		</div>

		<div class="container " >
			<!-- SUBFOOTER -->
			<article class="fcon">
				<section class="fcon-box1">
					<p class="tt1 ">Click Here To</p>
					<a href="/../view/page/get-started/schedule-an-introductory-session" class="btn1"><b>Get Started</b><i class="arr"></i></a>
				</section>
				<section class="fcon-box2 ">
					<ul class="fcon-li box-font">
						<li><a href="/../view/page/what-is-brain-education/background">What is Brain Education?</a></li>
						<li><a href="/../view/page/benefits/focus">Benefits</a></li>
						<li><a href="/../view/page/power-brain-training/for-kids">Power Brain Training Center</a></li>
						<li><a href="/../view/page/power-brain-school/program">Power Brain Schools</a></li>
					</ul>
				</section>
				<section class="fcon-box2-1">
					<ul class="fcon-li box-font">
						<li><a href="/../view/page/get-started/introductory-session">BE Around the World</a></li>
						<li><a href="/../view/page/get-started/introductory-session">Terms and Conditions</a></li>
						<li><a href="/../view/page/get-started/introductory-session">Contact Us</a></li>
					</ul>
				</section>
				<section class="fcon-box2-2">
					<ul class="fcon-li box-font">
						<li class="t4">Find a Location</li>
						<!-- <li ng-repeat="data in data.data | limitTo:3">ng-controller="CenterCtrl"
							<a href="/../view/center/power-brain-training/find-center/{[{data.slugs}]}/">{[{ data.state }]}: {[{ data.title | limitTo: 17 }]}{[{ data.title.length > 17 ? '...' : '' }]}</a>
						</li> -->
						 <li>
					        <a href="/../view/center/power-brain-training/find-center/mesa">AZ: Gilbert/Mesa</a>
					      </li>
					      <li>
					       
					        <a href="/../view/center/power-brain-training/find-center/bayside">NY: Bayside</a>,
					         <a href="/../view/center/power-brain-training/find-center/syosset">Syosset</a>
					      </li>
					        <li>
					        <a href="/../view/center/power-brain-training/find-center/fairfax">VA: Fairfax</a>
					      </li>
					</ul>
				</section>
				<section class="fcon-box3 ">
					<ul class="soc-mid">
						<li id="desyrel">Stay Connected</li>
						<li><a href="https://www.facebook.com/powerbraineducation" class="fb" target="_blank"><span>fb</span></a></li>
						<li><a href="https://twitter.com/powerbrainedu" class="tw"  target="_blank"><span>tw</span></a></li>
						<li><a href="https://www.youtube.com/powerbraineducation" class="yt"  target="_blank"><span>yt</span></a></li>
						<li><a href="/../index/rss" class="rss"  target="_blank"><span>rss</span></a></li>
					</ul>
				</section>
			</article>
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">

		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>
	<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
	<script src="/fe/scripts/others/bootstrap.min.js"></script>
	<script src="/fe/scripts/others/resposive-menu.js"></script>


	<!-- ANGULAR-->
	<script src="/fe/scripts/others/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {
			$('#carousel').carouFredSel({
				responsive: true,
				circular: false,
				auto: true,
				items: {
					visible: 1,
					width: 100,
					height: '70%'
				},
				scroll: {
					pauseOnHover  : true,
					fx: 'crossfade',
					duration: 900
				}
			});

			$('#thumbs').carouFredSel({
				responsive: true,
				circular: false,
				infinite: true,
				auto: false,
				prev: '#prev',
				next: '#next',
				items: {
					visible: {
						min: 2,
						max: 4
					},
					width: 50,
					height: '50%'
				}
			});

			$('#thumbs a').click(function() {
				$('#carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
				$('#thumbs a').removeClass('selected');
				$(this).addClass('selected');
				return false;
			});

		});
	</script>



	<script type="text/javascript" src="/vendors/angular/angular.js"></script>
	<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
	<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
	<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
	<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
	<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
	<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
	<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
	<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
	<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
	<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
	<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
	<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>

	<script type="text/javascript" src="/fe/scripts/app.js"></script>
	<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
	<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
	<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
	<script type="text/javascript" src="/fe/scripts/config.js"></script>

	<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
	<script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>



	<script src="/vendors/moment/moment.js"></script>
	<script src="/vendors/angular-moment/angular-moment.js"></script>
	<script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>

	<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>



<script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/vendors/fullcalendar/dist/gcal.js"></script>



<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src="/vendors/lodash/lodash.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false'></script>



<script type="text/javascript" src="/fe/scripts/controllers/centermap/centermap.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/centermap/mapfactory.js"></script>


<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>


 <!-- Auth0 Lock script and AngularJS module -->
    <script src="/fe/scripts/others/lock-7.5.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-storage/master/dist/angular-storage.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js"></script>
    <script src="/fe/scripts/others/auth0-angular-4.js"> </script>


</body>
</html>