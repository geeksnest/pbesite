<div class="entry-content" ng-controller="ContactCtrl">
<p class="subphoto"><img alt="img-appoinment" class="alignnone size-full wp-image-442" height="165" src="<?php echo $this->config->application->amazonlink; ?>/uploads/bannerimage/<?php echo $schedintroimg ?>" width="157" /></p>
    <?php echo $schedintro?>

    <div class="contact">
        <div role="form" class="wpcf7" id="wpcf7-f444-p311-o1" lang="en-US" dir="ltr">
            <div class="screen-reader-response"></div>
            <form ng-submit="sendsched(jb)" method="post" class="wpcf7-form" name="message">
                <p class="t2">
                    <span class="wpcf7-form-control-wrap interest-center">
                        <select name="center" ng-model="jb.center" class="wpcf7-form-control wpcf7-select input" id="interest-center" aria-invalid="false" required>
                            <option value="" style="display:none"></option>
                            <option ng-repeat="data in optioncenter" value="{[{data.title}]}">{[{data.title}]}</option>
                        </select>
                    </span>
                </p>
                <p class="t1">First Name</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap first-name">
                        <input type="text" name="fname" ng-model="jb.fname" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" id="first-name" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t1">Last Name</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap last-name">
                        <input type="text" name="lname" ng-model="jb.lname" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" id="last-name" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t1">Email</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap email">
                        <input type="email" name="email" ng-model="jb.email" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email input" id="email" aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p class="t3">Schedule your intro session</p>
                <div class="tt">
                    <div class="adj">Date &nbsp;
                        <span class="wpcf7-form-control-wrap sch-date">
                            <input type="date" required name="datesched" ng-model="jb.datesched" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-date input" id="sch-date" aria-invalid="false">
                        </span>
                        &nbsp;(Ex: 10-31-2015) &nbsp; &nbsp; &nbsp; &nbsp;
                    </div>
                    <div class="adj2">Time &nbsp;
                        <span class="wpcf7-form-control-wrap sch-time">
                            <select name="timesched" ng-model="jb.timesched" class="select-box" class="wpcf7-form-control wpcf7-select" id="sch-time" aria-invalid="false" required>
                                <option value="" style="display:none"></option>
                                <option value="07:00 AM">07:00 AM</option>
                                <option value="07:30 AM">07:30 AM</option>
                                <option value="08:00 AM">08:00 AM</option>
                                <option value="08:30 AM">08:30 AM</option>
                                <option value="09:00 AM">09:00 AM</option>
                                <option value="09:30 AM">09:30 AM</option>
                                <option value="10:00 AM">10:00 AM</option>
                                <option value="10:30 AM">10:30 AM</option>
                                <option value="11:00 AM">11:00 AM</option>
                                <option value="11:30 AM">11:30 AM</option>
                                <option value="12:00 AM">12:00 AM</option>
                                <option value="12:30 AM">12:30 AM</option>
                                <option value="01:00 PM">01:00 PM</option>
                                <option value="01:30 PM">01:30 PM</option>
                                <option value="02:00 PM">02:00 PM</option>
                                <option value="02:30 PM">02:30 PM</option>
                                <option value="03:00 PM">03:00 PM</option>
                                <option value="03:30 PM">03:30 PM</option>
                                <option value="04:00 PM">04:00 PM</option>
                                <option value="04:30 PM">04:30 PM</option>
                                <option value="05:00 PM">05:00 PM</option>
                                <option value="05:30 PM">05:30 PM</option>
                                <option value="06:00 PM">06:00 PM</option>
                                <option value="06:30 PM">06:30 PM</option>
                                <option value="07:00 PM">07:00 PM</option>
                            </select>
                        </span>
                    </div>
                </div>
                <p class="t3">Is there anything you\'d like us to know?</p>
                <p class="t2">
                    <span class="wpcf7-form-control-wrap any-like">
                        <textarea name="content" ng-model="jb.content" cols="20" rows="5" required class="wpcf7-form-control wpcf7-textarea input" id="any-like" aria-invalid="false"></textarea>
                    </span>
                </p>
                <p class="t2">
                    <input type="submit" name="sendsched" value="Send" class="wpcf7-form-control wpcf7-submit btn1" id="submit">
                    <input type="reset" ng-click="reset()" value="Reset" class="btn2"></p><div class="wpcf7-response-output wpcf7-display-none">
                </p>
                <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
                <div ng-show="notify" class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" role="alert" style="display: block;">Your message was sent successfully. Thanks.</div>
            </form>
        </div>
    </div>
</div>



