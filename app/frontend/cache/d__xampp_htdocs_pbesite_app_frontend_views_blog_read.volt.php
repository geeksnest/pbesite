<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>


	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title><?=@$centerinfo->title?></title>

	<!-- SAMPLE CONTENT -->
	<meta property="og:url"           content="<?=$_SERVER['REQUEST_URI']?>" />
	<meta property="og:type"          content="News Blog" />
	<meta property="og:title"         content="<?php echo $title?>" />
	<meta property="og:description"   content="<?php echo $description?>" />
	<meta property="og:image"         content="https://powerbrain.s3.amazonaws.com/uploads/blogimages/<?php echo $featured?>" />



	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<script>var idno = "<?=$idno;?>";</script>
	<style type="text/css">
		.pinterest-img {
			/* opacity: 0; */
			visibility: collapse;
			position: absolute;
			top:0;
			left:0;
			height: 0px;
		}
	</style>
</head>
<body>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="BlogCtrl">
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<article class="innerwrap ">
					<!-- LOGO -->
					<div class="logo">
						<a href="/../">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
						</a>
					</div>
					<!-- END LOGO -->
					<!-- NAVIGATION  FULL WIDTH-->
					<nav id="mainMenu" >
						<ul class="menuMain">
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value){
					              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
					              if($parentMenu[$key]->parentmenu =="Learning Community"){
					                ?><li><a  href="/../elearning"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
					            }elseif($parentMenu[$key]->parentmenu =="Blog"){
					                ?><li><a  href="/../blogs"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
					            }
					            else{
					            	$page_link = '/../'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
					            	?><li><a  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
					            }
					        }
					        ?>
					    </ul>
					</nav>
					<!-- END NAVIGATION  FULL WIDTH-->
					<div id="subMenu" >
						<nav class="sub">
							<div class="sub-title">
								<a href="#" id="nav-close" class="pull-right">close</a>
								<img src="/../img/frontend/logo.png">
								<div class="clearBoth"></div>
							</div>
							<ul>
								<?php
								$bilang = count($parentMenu);
								foreach ($parentMenu as $key => $value){
					              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
					              if($parentMenu[$key]->parentmenu =="Learning Community"){
					                ?><li><a  href="/../elearning"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
					            }elseif($parentMenu[$key]->parentmenu =="Blog"){
					                ?><li><a  href="/../blogs"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
					            }
					            else{
					            	$page_link = '/../'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
					            	?><li><a  href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
					            }
					        }
					        ?>
					    </ul>
					</nav>
					<div class=" pull-right">
						<a id="nav-expander" class="nav-expander fixed">
							MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
						</a>
					</div>
				</div>
			</article>
		</div>
	</header>
	<div style="height:58px;"></div>
	<div class="clearboth"></div>
	<!-- BANNER @Ryanjeric -->
	<div class="container">
		<article style="slider">
			<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
			<div class="ttl">
				<h1 class="grn2"><span>What is Brain Education?</span></h1>
				<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
			</div>
		</article>
	</div>
	<div class="container" ng-cloak>
		<article class="content" ng-repeat="data in readblog " >
			<p class="subpath"><a href="/../">Home</a><em>|</em><a href="/blogs">Blog</a><i class="arr1"></i><span class="grn2">{[{data.title}]}</span></p>
			<div class="subbox7">
				<!-- <h4 class="sec-ttl6" style="margin-top:25px;">News</h4> -->
				<div>
					<h1>{[{data.title}]}</h1>
					<h3>{[{data.description}]}</h3><br>
					<div>
						<span class="red">Date:</span> {[{data.publish}]}   <span class="red">Author:</span> {[{data.name}]}  <br><br>

						<hr style="margin-top:0px;">

						<div class="elear-content">
							<div ng-if="data.featuredtype == 'video'" >
								<div ng-bind-html="data.featured"></div>
							</div>
							<div ng-if="data.featuredtype == 'banner'">
								<div class="" alt="{[{data.title}]}" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{data.featured}]}');background-size:cover;min-height:400px;
									background-repeat:no-repeat;
									background-position:center center;"></div>
								</div>

							</div>

							<br><br>
							<style type="text/css">
								.read p{
									font-size: 16px !important;
									line-height: 30px !important;
								}
							</style>

							<div class="read">
								<?= $blogbody ?>
							</div>


							<hr style="margin-top:0px;">
							<p>Category: <a ng-repeat="categ in dbcateg" href="/../blogs/category/{[{categ.categoryslugs}]}"><label ng-if="$index>0"> ,&nbsp;</label>{[{categ.categoryname}]}</a></p>
							<p>Tags:  <a ng-repeat="tag in dbtag" href="/../blogs/tag/{[{tag.slugs}]}"><label ng-if="$index>0"> ,&nbsp;</label>{[{tag.tags}]}</a></p>
							<hr style="margin-top:0px;">
							<div class="sbox2">
								<h4>About the Author</h4>
								<div >
									<p class="authorphoto"><a href=""><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/authorimages/{[{data.image}]} "></a></p>
									<h2>{[{data.name}]}</h2>
									<p class="prgrp"><div ng-bind-html="data.about"></div></p>
									<p class="more"><a href="/../aboutauthor/{[{data.authorid}]}">See Full Author Bio</a></p>
								</div>
							</div>
							<hr style="margin-top:0px;">
							<style type="text/css">
								.float-left{
									float: left;
									margin-right: 10px;
								}
							</style>
							<div class="col-sm-12">
								<div class="blog-social" style="line-height:20px ">
									<div class="blog-social-item blog-fb-like float-left">
										
										<div class="fb-like" data-href="https://powerbraineducation.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
										<div id="fb-root"></div>
										<script>
											(function(d, s, id) {
												var js, fjs = d.getElementsByTagName(s)[0];
												if (d.getElementById(id)) return;
												js = d.createElement(s); js.id = id;
												js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=217644384961866";
												fjs.parentNode.insertBefore(js, fjs);
											}(document, 'script', 'facebook-jssdk'));

											window.fbAsyncInit = function(){ 
								            FB.XFBML.parse(); // now we can safely call parse method
								        };
								    </script>
								</div>


								<div class="blog-social-item float-left margin-left">
									
									<iframe allowtransparency="true" frameborder="0" scrolling="no"
									src="https://platform.twitter.com/widgets/tweet_button.html?size=medium"
									style="width:60px; height:20px;"></iframe>
								</div>



								<div class="blog-social-item float-left margin-left">
									<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>
									<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
								</div>
								<div style="clear:both"></div>
								<div id="disqus_thread"></div>
								<script>

							    (function() {  // DON'T EDIT BELOW THIS LINE
							    	var d = document, s = d.createElement('script');

							    	s.src = '//powerbraineducation.disqus.com/embed.js';

							    	s.setAttribute('data-timestamp', +new Date());
							    	(d.head || d.body).appendChild(s);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
						</div>
					</div>
				</div>
			</article>
			<aside class="aside">
				<p class="subpath"><i class="arr2"></i><a href="javascript:history.back();">Return to <span class="grn2">Previous Page</span></a></p>

				<span class="sbn">Blog Index</span>
				<div class="sbox2">
					<h4>Categories</h4>
					<div class="listCont">
						<ul>
							<?php
							$getginfo = $newscategory;
							foreach ($getginfo as $key => $value) {
								?>
								<li><a href="/blogs/category/<?php echo $getginfo[$key]->categoryslugs; ?>"><?php echo $getginfo[$key]->categoryname; ?></a></li>
								<?php } ?>
							</ul>					
						</div>
						<hr>
						<h4>Tags</h4>
						<div class="listCont">
							<div class="tagsL">
								<?php
								$gettags = $newstagslist;
								$count = 1;
								foreach ($gettags as $key => $value) { ?>
								<a href="/blogs/tag/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
								<?php
								$count > 5 ? $count =1 : $count++;
							} ?>
						</div>
					</div>
					<hr>
					<h4> Archives</h4>
					<div class="listCont">
						<ul class="nf">
							<?php
							$getarchives = $archivelist;
							foreach ($getarchives as $key => $value) { ?>
							<li><a href="/blogs/archieve/<?=substr($getarchives[$key]->datepublished,0 ,7)?>"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a></li>
							<?php } ?>
						</ul>					
					</div>
					<br>
					<br>				
				</div>			
			</aside>
		</div>
		<!-- ////// -->
		<div class="container border-top" >
			<!-- FOUR COL -->
			<article class="wrap-cont">
				<p class="title">Power Brain Training Center</p>
				<section class="clmn-4">
					<p class="round-img"><img src="/img/frontend/from/m-center1.jpg"></p>
					<div class="listCont">
						<p class="t1">For Kids</p>
						<p class="t2">Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential.</p>
						<p class="more"><a href="/../power-brain-training/for-kids">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4">
					<p class="round-img"><img src="/img/frontend/from/m-center2.jpg"></p>
					<div class="listCont">
						<p class="t1">For Schools</p>
						<p class="t2">We've worked with thousands of students (pre-k ~12), teachers and parents in over 350 schools in the US.</p>
						<p class="more"><a href="/../power-brain-school/program">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4 "></p>
					<p class="round-img"><img src="/img/frontend/from/m-center3.jpg"></p>
					<div class="listCont">
						<p class="t1">For Adults</p>
						<p class="t2">Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance.</p>
						<p class="more"><a href="/../power-brain-training/for-adults">Read More</a></p>
					</div>
				</section>
				<section class="clmn-4 "></p>
					<p class="round-img"><img src="/img/frontend/from/m-center4.jpg"></p>
					<div class="listCont">          
						<p class="t1">For Families</p>
						<p class="t2">Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family.</p>
						<p class="more"><a href="/../power-brain-training/for-families">Read More</a></p>
					</div>
				</section>

			</article>
			<!-- END FOUR COL -->
		</div>
		<div class="container " >
			<!-- SUBFOOTER -->
			<?=$this->view->getRender('etemplates', 'eFooter');?> 
			<!-- SUBFOOTER -->
		</div>
	</div>
	<div class="container">
		<!-- FOOTER -->
		<footer class="fcopy">
			Copyright 2015 Power Brain Training Center. All rights reserved.
		</footer>
		<!-- END FOOTER -->
	</div>
	<?=$scritps ?>
	<!-- ///USER -->
	<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
	<!-- ///ELEARNING -->
	<script type="text/javascript" src="/fe/scripts/controllers/elearning/readblog.js"></script>
	<!-- /// FACTORY -->
	<script type="text/javascript" src="/fe/scripts/factory/elearning/eblog.js"></script>
</body>
