<?php echo $this->getContent()?>
<?php if($pagelayout == 1)
		{
?>




	<?php echo $title; ?>

	<?php echo $body; ?>



<?php
		}
		else
		{



?>
<section id="content">

      	<div id="sidebarmenu">
	      <ul>
	      	<?php 
				$getginfo = $leftsidebar;
				foreach ($getginfo as $key => $value) {
			?>
	        <li><a href="#"> <label class="dot"> ▪</label> <?php echo $getginfo[$key]->item;  ?></a></li>

	        <?php } ?>  
	      </ul> 
	    </div>

	      <div id="newsletter">
	        <div class="frmtitle">e-Newsletter</div>
	        <form>
	          <div >
	            <input type="text" class="text" placeholder="E-mail Address" >
	          </div>
	          <input type="button" class="send" value="Send" />
	        </form>
	        <div class="clearboth"></div>
	      </div>

	    </section>

<section id="middle">
	<h5 class="h5title">About</h5>
	<h3 class="h3title"><?php echo $title; ?></h3>

	<?php echo $body; ?>

</section>

<div class="clearboth"></div>
<aside id="sidebar">
	<?php if($item1 == true){?>
	<a href="" class="add"><img src="/img/frontend/banner_getstarted_small.jpg"></a>
	<?php }else{}?>
	<?php if($item2 == true){?>
	<div class="success_stories">
		<div class="frmtitle marg_lft suc_stor" >Success Stories</div>



		<div class="story_cont">
			<img class="thumb_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<div class="story_info">
				<div class="story_title">A More Confident Person</div>
				<div class="story_add">Jane Ansin, Arlington center, (MA)</div>
				<div class="story_msg">Dahn yoga helps me so much with my anxiety, trust in myself and others.</div>
			</div>
		</div>


		<div class="story_cont">
			<img class="thumb_pic" src="/img/frontend/thumbnailTM_1738.jpg" />
			<div class="story_info">
				<div class="story_title">Seeing With Bright Eyes!</div>
				<div class="story_add">Jane Ansin, Arlington center, (MA)</div>
				<div class="story_msg">I became more positive and during meditation, I could feel so much warmth inside my body and renewed...</div>
			</div>
		</div>
		<div class="clearboth"></div>
	</div>

	<div class="clearboth"></div>
	<?php }else{}?>
	<?php if($item3 == true){?>
	<div class="popularpost">
		<div class="frmtitle marg_lft pop_feat" >Popular Features</div>


		<div class="post">
			<img class="post_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<span>The Power of Mindful Eating</span>
			<div class="clearboth"></div>
		</div>
		<div class="post">
			<img class="post_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<span>Chronic Stress and the Brain: Meditation for Fighting Anxiety</span>
			<div class="clearboth"></div>
		</div>
		<div class="post">
			<img class="post_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<span>Wellness at Work: Jung-Choong Breathing for Stress Reduction</span>
			<div class="clearboth"></div>
		</div>
		<div class="post">
			<img class="post_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<span>Dahn Yoga and Multiple Sclerosis: Exercises for Healing</span>
			<div class="clearboth"></div>
		</div>
		<div class="post">
			<img class="post_pic" src="/img/frontend/thumbnailTM_1598.jpg" />
			<span>Brain Education Creator, Ilchi Lee Inspires with Solar Energy Training in Mago Garden</span>
			<div class="clearboth"></div>
		</div>

		<div class="clearboth"> </div>

	</div>
	<?php }else{}?>

</aside>


<?php } ?>
