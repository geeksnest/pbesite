<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Power Brain Education Learning Community Gallery</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
		<link rel="stylesheet" type="text/css" href="/fe/scripts/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- MODAL GALLERY -->

	<style type="text/css">

		#pinBoot {
			position: relative;
			max-width: 100%;
			width: 100%;
		}
		img {
			width: 100%;
			max-width: 100%;
			height: auto;
		}
		.white-panel {
			position: absolute;
			background: white;
			box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);
			padding: 10px;
		}


		.white-panel h1 {
			font-size: 1em;
		}
		.white-panel h1 a {
			color: #A92733;
		}
		.white-panel:hover {
			box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.5);
			margin-top: -5px;
			-webkit-transition: all 0.3s ease-in-out;
			-moz-transition: all 0.3s ease-in-out;
			-o-transition: all 0.3s ease-in-out;
			transition: all 0.3s ease-in-out;
		}

		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

	</style>

</head>
<body ng-controller="UserinfoCtrl" ng-cloak>
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance/');
	}
	?>
	<div class="box-shadow" ng-controller="GalleryCtrl" ng-cloak>
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eBanner');?> 
		<!-- E-Learning Community Sub MEnu -->
		<?=$this->view->getRender('etemplates', 'eMenu');?> 


		<div class="container" >
			<div class="wrapper text-center" >
				<h1 class="welc">POWER BRAIN GALLERY</h1>
			</div>
		</div>
		
		<div >
			<div class="gcontainer ">
				<section id="pinBoot">
			      <article class="white-panel" ng-repeat="data in imagelist"> 
			      	<a class="fancybox-effects-c" href="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" title="
						<h4><a href=''>{[{data.title}]}</a></h4>
			        	<p>{[{data.description}]}</p>
							">
							<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}" alt="">
						</a> 
			        <h4><a href="#">{[{data.title}]}</a></h4>
			        <p>{[{data.description}]}</p>
			      </article>
			    </section>
			</div>
		</div>
		<div class="clear"></div>
	</div>

</div>




<div class="container " >
	<!-- SUBFOOTER -->
	<?=$this->view->getRender('etemplates', 'eFooter');?> 
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">

	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>


<?=$scritps ?>

<!-- GRID GALLERY -->

<script type="text/javascript" src="/fe/scripts/fancybox/pinterestlayout.js"></script>


<script type="text/javascript" src="/fe/scripts/fancybox/jquery.fancybox.js?v=2.1.5"></script>



	<script type="text/javascript">
		$(document).ready(function() {

			$('.fancybox').fancybox();

			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});


			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>



<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/gallery.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>

</body>
</html>