<!DOCTYPE html>
<html lang="en" data-ng-app="app"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
<meta Http-Equiv="Pragma" Content="no-cache">
<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<link href="/fe/css/app.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/slider/flexslider.css" rel="stylesheet">
</head>
<script>
</script>
<body ng-controller="UserinfoCtrl">
	<?php
	if($logoimage->value1 == 1){
		header('Location: ../../../maintenance');
	}
	?>
	<div class="box-shadow" ng-controller="AccountCtrl"  ng-cloak>
		<header id="menuSlide" class="navbar  navbar-fixed-top" >
			<div class="container">
				<!--POWER BRAIN EDUCATION MAIN MENU -->
				<?=$this->view->getRender('etemplates', 'mMenu');?> 
			</div>
		</header>
		<div style="height:58px;"></div>
		<div class="clearboth"></div>
		<div class="container">
			<article style="slider">
				<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
				<div class="ttl">
					<h1 class="grn2"><span>What is Brain Education?</span></h1>
					<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
				</div>
			</article>
		</div>
		<div>
			<div class="wrapper text-center" style="margin-bottom:10px;margin-top:50px;">
				<img src='/img/logo.jpg' style="width:8%;height:auto;">
				<h2>Complete Account Profile</h2>
			</div>
			<form name="form" class="form-validation ">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="h4">Personal Information</span>
						</div>
						<div class="panel-body">
							<p class="text-muted">Please fill the information to continue. All Fields Are Required</p>
							<div class="form-group">
								<label>First Name <span ng-show="user.fname == '' || user.fname == null" class="badge bg-danger">Required</span></label>
								<input type="text" class="form-control " ng-model="user.fname" ng-init="user.fname = fname"  required>
							</div>
						<!-- <div class="form-group">
							<label>Middle Name</label>
							<input type="text" class="form-control " ng-model="user.mname" ng-pattern="/^[a-zA-Z0-9]{4,10}$/" required>
						</div> -->
						<div class="form-group">
							<label>Last Name <span ng-show="user.lname == '' || user.lname == null" class="badge bg-danger">Required</span></label>
							<input type="text" class="form-control " ng-model="user.lname" ng-init="user.lname = lname"  required>
						</div>
						<div class="form-group pull-in clearfix ">
							<label>Gender <span ng-show="user.gender == '' || user.gender == null" class="badge bg-danger">Required</span></label>
							<select  style="width:100px !important;" name="account" class="form-control m-b" ng-model="user.gender" ng-init="user.gender = gender"   required>
								<option value=""></option>
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
						<div class="form-group pull-in clearfix">
							<label>Birth Date : <span ng-show="user.year == '' || user.year == null || user.month == '' || user.month == null || user.day == '' || user.day == null" class="badge bg-danger">Required</span></label><br>
							<div class="col-sm-3">
								<label>Year</label>
								<select name="account" class="form-control m-b" ng-model="user.year" ng-init="user.year = byear" required>
									<option value=""></option>
									<?=$_yropt ?>
								</select>
							</div>
							<div class="col-sm-4">
								<label>Month</label>
								<select name="account" class="form-control m-b" ng-model="user.month"  ng-init="user.month = bmonth" required>
									<option value=""></option>
									<option value="01">January</option>
									<option value="02">Febuary</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</div>
							<div class="col-sm-2">
								<label>Day</label>
								<select name="account" class="form-control m-b" ng-model="user.day" ng-init="user.day = bday"  required>
									<option value=""></option>
									<?=$_dayopt ?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label>Select School <span ng-show="user.skul == '' || user.skul == null" class="badge bg-danger">Required</span></label>
							<select   name="account" class="form-control m-b" ng-model="user.skul" ng-init="user.skul = school" required>
								<option value="0" disabled>Select</option>
								<option ng-repeat="data in schoolList" value="{[{data.id}]}">{[{data.name}]}</option>
							</select>
						</div>
						<div class="form-group">
							<label>Grade <span ng-show="user.grade == '' || user.grade == null" class="badge bg-danger">Required</span></label>
							<input type="text" class="form-control " ng-model="user.grade" ng-init="user.grade = grade"   required>
						</div>
					</div>
					
				</div>

			</div>
			<div>
				<div class="col-sm-6">
					<input type="hidden" class="form-control" name="password" ng-model="user.userid" ng-value="user.userid = usrid" required>   
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="h4">Upload Profile</span>
						</div>
						<div class="col-sm-6">
							<div class="panel-body">
								<div class="col-sm-12" ng-show="imagecontent">
									<div class="">
										<div ngf-drop ngf-select ng-model="files" ngf-change="upload(files)" class="drop-box"
										ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
										accept="image/*,application/pdf" style="line-height:30px;">Drop images here or click to upload</div>
									</div>
								</div>
							</div>
							<div class="loader" ng-show="imageloader">
								<div class="loadercontainer">
									<div class="spinner">
										<div class="rect1"></div>
										<div class="rect2"></div>
										<div class="rect3"></div>
										<div class="rect4"></div>
										<div class="rect5"></div>
									</div>
									Uploading your images please wait...
								</div>
							</div>
							<input type="hidden" class="form-control" name="profile" ng-model="user.profile" ng-init="user.profile=userprof" required>  

						</div>
						<div class="col-sm-6">
							<img src="https://powerbrain.s3.amazonaws.com/uploads/banner/{[{user.profile}]}" style="width:250px;height:210px;padding:10px;">
						</div>
						<div style="clear:both"></div>
					</div>
				</div>
				<div class="col-sm-6">
					<input type="hidden" class="form-control" name="password" ng-model="user.userid" ng-value="user.userid = usrid" required>   
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="h4">Account Information</span>
						</div>
						<div class="panel-body">
							<p class="text-muted">Please fill the information to continue</p>
							<div class="form-group">
								<label>Username <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em></label><span ng-show="user.username == '' || user.username == null"  class="badge bg-danger">Required</span><br> <span style="padding:10px;color:#f00;" ng-show="usrname">Username already been used !!!!<br/></span> 
								<input type="text" class="form-control " ng-model="user.username" ng-init="user.username = usrname" ng-change="chkusername(user.username)" required> 
							</div>
							<div class="form-group">
								<label>Enter password <span ng-show="user.password == '' || user.password == null" class="badge bg-danger">Required</span></label>
								<input type="password" class="form-control" name="password" ng-model="user.password" required>   
							</div>
							<div class="form-group">
								<label>Confirm password <span ng-show="confirm_password == '' || confirm_password == null" class="badge bg-danger">Required</span></label>
								
								<input type="password" class="form-control " name="confirm_password" required ng-model="confirm_password" ui-validate=" '$value==user.password' " ui-validate-watch=" 'password' ">
								<span ng-show="form.confirm_password.$error.validator" class="ng-hide">Passwords do not match!</span>
							</div>
							<hr>
							<footer class="panel-footer text-right bg-light lter">
								<button type="submit" class="btn btn-success" ng-disabled=" user.username == null ||  user.password == null || user.fname == null || user.lname =='' || user.year == '' || user.month == '' || user.day == '' || user.gender == '' || user.skul == '' || user.skul == 0 || user.grade == '' || usrname || confirm_password == null || form.confirm_password.$error.validator" ng-click="submit(user)" >Submit</button>
							</footer>
						</div>
					</div>
				</div>
			</div>

		</form>
		<div style="clear:both"></div>
	</div>



</div>
<div class="container " >
	<!-- SUBFOOTER -->
	<?=$this->view->getRender('etemplates', 'eFooter');?> 
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">
	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>

<?=$scritps ?>




<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<script type="text/javascript" src="/fe/scripts/controllers/elearning/user.js"></script>
<!-- /// FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/user/elcuser.js"></script>

</body>
</html>