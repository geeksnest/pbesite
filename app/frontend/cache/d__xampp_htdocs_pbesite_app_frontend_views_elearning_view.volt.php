<?php 
if(!@$_SERVER['HTTP_REFERER']){
	echo "page redirect";
	return header("/../elearning/login");
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-controller="UserinfoCtrl"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/e/slider.css" rel="stylesheet">
</head>
<body >
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow">
			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<article class="innerwrap ">
						<!-- LOGO -->
						<div class="logo">
							<a href="/../">
								<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/<?php echo $logoimage->logo; ?>">
							</a>
						</div>
						<!-- END LOGO -->
						<!-- NAVIGATION  FULL WIDTH-->
						<nav id="mainMenu" >
							<ul class="menuMain">
								<?php
								$bilang = count($parentMenu);
								foreach ($parentMenu as $key => $value){
				              --$bilang;if($bilang==0){$arrow= '<i class="arr"></i>';}else{$arrow="";} // ARROW
				              if($parentMenu[$key]->slug==str_replace(' ','-',$mainMenu)){$curparent = 'class="active"';}else{$curparent = '';}// CHECK IF ACTIVE

				              if($parentMenu[$key]->parentmenu =="Learning Community"){
				                ?><li><a class="active"  href="/../elearning/verify"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS LEARNING COMMUNITY
				            }elseif($parentMenu[$key]->parentmenu =="Blog"){
				                ?><li><a  href="/../blog/page"><?=$parentMenu[$key]->parentmenu?></a></li><?php // IF PAGE IS BLOG
				            }
				            else{
				            	$page_link = '/../view/page/'.$parentMenu[$key]->slug.'/'.$parentMenu[$key]->subslug.'';
				            	?><li><a   href="<?=$page_link?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li><?php
				            }
				        }
				        ?>
				    </ul>
				</nav>
				<!-- END NAVIGATION  FULL WIDTH-->
				<div id="subMenu" >
					<nav class="sub">
						<div class="sub-title">
							<a href="#" id="nav-close" class="pull-right">close</a>
							<img src="/../img/frontend/logo.png">
							<div class="clearBoth"></div>
						</div>
						<ul>
							<?php
							$bilang = count($parentMenu);
							foreach ($parentMenu as $key => $value) {
          					// ARROW
								--$bilang;
								if($bilang==0){
									$arrow= '<i class="arr"></i>';
								}else{
									$arrow= "";
								}
          					// ARROW
								?>
								<li><a href="/../view/page/<?=$parentMenu[$key]->slug?>/<?=$parentMenu[$key]->subslug?>"><?=$parentMenu[$key]->parentmenu?><?=$arrow?></a></li>
								<?php
							}
							?>
						</ul>
					</nav>
					<div class=" pull-right">
						<a id="nav-expander" class="nav-expander fixed">
							MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
						</a>
					</div>
				</div>
			</article>
		</div>
	</header>
	<div style="height:58px;"></div>
	<div class="clearboth"></div>
	<div class="container">
		<article style="slider">
			<img class="imgslider" src="/../img/frontend/from/img-sub121.jpg">
			<div class="ttl">
				<h1 class="grn2"><span>What is Brain Education?</span></h1>
				<p class="t1">Brain Education is an innovative educational program designed to develop the full potential of the human brain.</p>
			</div>
		</article>
	</div>
	<div class="container" id="full-sub">
		<ul class="subnav subnav-3 grn2 resize-menu drop" >
			<?php
			$subMenus = $subs;
			foreach ($subMenus as $keys => $values) {
				foreach ($subMenus[$keys] as $key => $value) {
					if($key==$cur_state){
						$current = '';
					}else{
						$current = '';
					}
					if($value == "Mentors"){
						$ngshow = 'ng-show="_mentors"';
					}elseif($value == "Leaders"){
						$ngshow = 'ng-show="_leades"';
					}else{
						$ngshow = '';
					}
					if($value == 'Teacher Resources'){$_sub_menu = $teachersub;}else{$_sub_menu='';}
					if($value == 'Training Reinforcement'){$_sub_menu_t = $triningsub;}else{$_sub_menu_t='';}
					if($value == 'Content Links'){$_contlink = $contentsub;}else{$_contlink='';}
					?>
					<li<?=$ngshow?> class="page_item  <?=$current?>"><a href="/../elearning/<?=$key?>"><?=$value?></a><?=@$_sub_menu?><?=@$_sub_menu_t?><?=@$_contlink?></li>
					<?php
				}
			}?>
		</ul>
		<div class="w-msg">
			<p>Hi! {[{usrname}]}</p>
			<p><a href="">My Account</a> | <a href="" ng-click="logout()">Logout</a></p>
		</div>
	</div>

	<div class="container" id="min-sub">
		<ul class="">
			<?php
			$subMenus = $subs;
			foreach ($subMenus as $keys => $values) {
				foreach ($subMenus[$keys] as $key => $value) {
					?>
					<li><a href="/../elearning/<?=$key?>"><?=$value?></a></li>
					<?php
				}
			}?>
		</ul>
	</div>

	<div class="container" >
		<div class="wrapper text-center" >
			<h1 class="welc">WELCOME TO THE LEARNING COMMUNITY</h1>
		</div>
	</div>
	<style type="text/css">
		.cover {
			object-fit: cover;
		}
	</style>
	<div class="elear-content-v">
		<div class="vid-cont" ng-controller="BlogCtrl">
			<div class="scontainer">
				<div class="slider-container" id="caption-slide"  >
					<div class="slider" ng-cloak>
						<div>
							<img  class="cover" src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[0].featured}]}" alt="">
							<span class="caption">
								<a href="../blog/read/{[{eblog[0].slug}]}">
									<p class="sec-ttl nm">{[{eblog[0].title}]}</p>
									<p>{[{eblog[0].description}]} </p>
								</a>
							</span>
						</div>
						<div ng-cloak>
							<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[1].featured}]}" alt="">
							<span class="caption">
								<a href="../blog/read/{[{eblog[1].slug}]}">
									<p class="sec-ttl nm">{[{eblog[1].title}]}</p>
									<p>{[{eblog[1].description}]} </p>
								</a>
							</span>
						</div>
						<div ng-cloak>
							<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[2].featured}]}" alt="">
							<span class="caption">
								<a href="../blog/read/{[{eblog[2].slug}]}">
									<p class="sec-ttl nm">{[{eblog[2].title}]}</p>
									<p>{[{eblog[2].description}]} </p>
								</a>
							</span>
						</div>
						<div ng-cloak>
							<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[3].featured}]}" alt="">
							<span class="caption">
								<a href="../blog/read/{[{eblog[3].slug}]}">
									<p class="sec-ttl nm">{[{eblog[3].title}]}</p>
									<p>{[{eblog[3].description}]} </p>
								</a>
							</span>
						</div ng-cloak>
						<div ng-cloak>
							<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[4].featured}]}" alt="">
							<span class="caption">
								<a href="../blog/read/{[{eblog[4].slug}]}">
									<p class="sec-ttl nm">{[{eblog[4].title}]}</p>
									<p>{[{eblog[4].description}]} </p>
								</a>
							</span>
						</div>
					</div>
					<div class="switch" id="prev"><span></span></div>
					<div class="switch" id="next"><span></span></div>
				</div>
			</div>
		</div>
		<style type="text/css">
			.fadein {
				position:relative;
				height:320px;
				width:320px;
			}
			.fadein img {
				position:absolute;
				left:0;
				top:0;
			}
		</style>
		<div class="gal-cont" ng-controller="BlogCtrl">
			<div class="fadein">
				<img ng-repeat="data in imagelist"  src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{data.filename}]}">
			</div>
		</div>
		<div style="clear:both;"></div>
		<div style="marg-bottom">
			<div class="w3-box">
				<p class="sec-ttl nm">TEACHER RESOURCES</p>
				<div class="c-box" style="text-align:center;">
					<img src="https://powerbrain.s3.amazonaws.com/uploads/banner/fjk0bgwrk91444024937783.png">
				</div>	
			</div>
			<div class="w3-box" ng-controller="BlogCtrl">
				<p class="sec-ttl nm">TEACHER REINFORCEMENT</p>
				<div class="c-box">
					<h4>Your current Course</h4>
					<div  class="lastvisit">
						<a ng-show="lv" href="{[{pagevisit[0].link}]}"><h5>&raquo; {[{pagevisit[0].classtitle}]}</h5></a>
					</div>
				</div>	
			</div>
			<div class="w3-box">
				<p class="sec-ttl nm">CONTENT LINKS</p>
				<div class="c-box" style="text-align:center;">
					<img src="https://powerbrain.s3.amazonaws.com/uploads/banner/l5ots79o1or1444025071747.png">
					<!-- IMAGE -->
				</div>	
			</div>
		</div>
		<div style="clear:both;" ></div>
		<div style="margin-bottom:100px;"></div>
	</div>
</div>
<div class="container " >
	<!-- SUBFOOTER -->
	<?=$footer ?>
	<!-- SUBFOOTER -->
</div>
</div>
<div class="container">
	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>
<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>
<!-- ANGULAR-->
<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/viewblog.js"></script> <!-- CONTROLLER -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/eblog.js"></script> <!-- FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>
<!-- SLIDER -->
<script type="text/javascript" src="/fe/css/e/slider.js"></script>
<script>
	$("#slider-container").sliderUi({
		speed: 700,
		cssEasing: "cubic-bezier(0.285, 1.015, 0.165, 1.000)"
	});
	$("#caption-slide").sliderUi({
		caption: true
	});
	$('.fadein img:gt(0)').hide();

	setInterval(function () {
		$('.fadein :first-child').fadeOut()
		.next('img')
		.fadeIn()
		.end()
		.appendTo('.fadein');
	}, 4000); 
</script>
</body>
</html>