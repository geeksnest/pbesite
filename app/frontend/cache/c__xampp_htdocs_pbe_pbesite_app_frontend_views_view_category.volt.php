<?php echo $this->getContent()?>
<?php
foreach ($subMenus as $key => $value) {
	// echo $subMenus[$key]->submenu.'/'.$subMenu.'<br>';
	if(str_replace('-',' ',$subMenus[$key]->submenu)==$subMenu){
		$pageTitle=$subMenus[$key]->title;
	}
}
?>
<script type="text/javascript">
	var category = "<?=$category;?>";
</script>
<p class="subpath"><a href="http://powerbraineducation.com">Home</a><em>|</em><?=$mainMenu?><i class="arr1"></i><span class="grn2"><?=$pageTitle?></span></p>
<div ng-controller="ListNewsbyCategory">

	<div ng-repeat="data in data.data">
		<a href="/../view/centernews/power-brain-training/find-center/{[{data.slug}]}" class="subbox">
			<p class="photo"> 
				<img width="225" height="141" src="{[{data.banner}]}" class="attachment-full wp-post-image" alt="bn-forkids1">
			</p>
			<h3>{[{data.title}]}</h3>
			<span class="red">Date:</span> {[{data.publish}]} / <span class="red">Author:</span> {[{data.authorname}]} <br><br>
			{[{data.description}]}
		</a>
	</div>

	<div class="row">
		<div class="panel-body">
			<footer class="panel-footer text-center bg-light lter">
				<pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
			</footer>
		</div>
	</div>
	
</div>
