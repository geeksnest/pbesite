<?php 

if(!@$_SERVER['HTTP_REFERER']){return header("/../elearning/login");}?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-controller="UserinfoCtrl"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title> Power Brain Education Learning Community</title>
	<meta name="description" content="<?=@$centerinfo->desc?>">
	<meta name="keywords" content="<?=@$centerinfo->metatitle?>">
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0"> 
	<link href="/fe/css/bootstrap.css" rel="stylesheet">
	<link href="/fe/css/powerbrain.css" rel="stylesheet">
	<link href="/fe/css/powerbrain_inner.css" rel="stylesheet">
	<link href="/fe/css/powerbrainpages.css" rel="stylesheet">
	<link href="/fe/css/slider.css" rel="stylesheet">
	<link href="/fe/css/elearning.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/globaljs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css"> -->
	<link href="/vendors/fullcalendar/dist/fefullcalendar.css" rel="stylesheet">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<!-- SLIDER -->
	<link href="/fe/css/e/slider.css" rel="stylesheet">

	<SCRIPT type="text/javascript">
		window.history.forward();
		function noBack() { window.history.forward(); }
		var eventid = "";
	</SCRIPT>

</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
	<?php if($logoimage->value1 == 1){header('Location: ../../../maintenance/');}?>
		<div class="box-shadow">

			<header id="menuSlide" class="navbar  navbar-fixed-top" >
				<div class="container">
					<!--POWER BRAIN EDUCATION MAIN MENU -->
					<?=$this->view->getRender('etemplates', 'mMenu');?> 
				</div>
			</header>
			<div style="height:58px;"></div>
			<div class="clearboth"></div>
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eBanner');?> 
			<!-- E-Learning Community Sub MEnu -->
			<?=$this->view->getRender('etemplates', 'eMenu');?> 


			<div class="container" >
				<div class="wrapper text-center" >
					<h1 class="welc">WELCOME TO THE LEARNING COMMUNITY</h1>
				</div>
			</div>
			<style type="text/css">
				.cover {
					object-fit: cover;
				}
			</style>
			<div class="elear-content-v">
				<div class="vid-cont" ng-controller="BlogCtrl">
					<div class="scontainer">
						<div class="slider-container" id="caption-slide"  >
							<div class="slider" ng-cloak>
								<div>
									<img  class="cover" src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[0].featured}]}" alt="">
									<span class="caption">
										<a href="../blog/{[{eblog[0].slug}]}" target="_blank">
											<p class="sec-ttl nm">{[{eblog[0].title}]}</p>
											<p>{[{eblog[0].description}]} </p>
										</a>
									</span>
								</div>
								<div ng-cloak>
									<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[1].featured}]}" alt="">
									<span class="caption">
										<a href="../blog/{[{eblog[1].slug}]}" target="_blank">
											<p class="sec-ttl nm">{[{eblog[1].title}]}</p>
											<p>{[{eblog[1].description}]} </p>
										</a>
									</span>
								</div>
								<div ng-cloak>
									<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[2].featured}]}" alt="">
									<span class="caption">
										<a href="../blog/{[{eblog[2].slug}]}" target="_blank">
											<p class="sec-ttl nm">{[{eblog[2].title}]}</p>
											<p>{[{eblog[2].description}]} </p>
										</a>
									</span>
								</div>
								<div ng-cloak>
									<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[3].featured}]}" alt="">
									<span class="caption">
										<a href="../blog/{[{eblog[3].slug}]}" target="_blank">
											<p class="sec-ttl nm">{[{eblog[3].title}]}</p>
											<p>{[{eblog[3].description}]} </p>
										</a>
									</span>
								</div ng-cloak>
								<div ng-cloak>
									<img class="cover"  style="object-fit: cover; " src="<?php echo $this->config->application->amazonlink; ?>/uploads/blogimages/{[{eblog[4].featured}]}" alt="">
									<span class="caption">
										<a href="../blog/{[{eblog[4].slug}]}" target="_blank">
											<p class="sec-ttl nm">{[{eblog[4].title}]}</p>
											<p>{[{eblog[4].description}]} </p>
										</a>
									</span>
								</div>
							</div>
							<div class="switch" id="prev"><span></span></div>
							<div class="switch" id="next"><span></span></div>
						</div>
					</div>
				</div>
				<style type="text/css">
					.fadein {
						position:relative;
						height:320px;
						width:320px;
					}
					.fadein img {
						position:absolute;
						left:0;
						top:0;
					}
				</style>
				<div class="gal-cont" ng-controller="BlogCtrl">
					<div class="fadein">
						<?php  
						foreach ($img_gal as $key => $value) {
							foreach($value as $k=>$val){
								if($k == 'filename'){
									?>
									<img src="<?=$this->config->application->amazonlink;?>/uploads/banner/<?=$val?>"> 
									<?php
								}
							}	
						}
						?>
						
					</div>
				</div>
				<div style="clear:both;"></div>
				<div style="marg-bottom">
					<div class="w3-box" ng-controller="BlogCtrl">
						<a href="../elearning/trainingreinforcement" >
							<p class="sec-ttl nm">Classroom Activities</p>
						</a>

						<style type="text/css">
							.object-fit_contain { 
								object-fit: contain;
								height:200px; overflow:hidden; 
							}
							.thumbvimeo {
								width: 270px;
							}
						</style>
						<div class=" object-fit_contain" style="text-align:center;padding:0px!important;border:0px !important;height:270px;">
							<a href="../elearning/trainingreinforcement" >
								<img src="/../img/frontend/PBE_PowerBrain10Diagram.png" class="thumbvimeo">
							</a>
						</div>	
					</div>
					<div class="w3-box" ng-controller="BlogCtrl">
						<p class="sec-ttl nm">Activity Progress</p>

						<div  class="lastvisit" ng-show="currentProgress">
							<div style="text-align:center;">	
							</div>
							<img src="{[{pagevisit[0].pagewdsub}]}">
							<h4>Your current Course</h4>
							<a ng-show="lv" href="{[{pagevisit[0].link}]}"><h5>&raquo; {[{pagevisit[0].classtitle}]}</h5></a>
						</div>

						<div class="c-box object-fit_contain" style="text-align:center;padding:0px!important;border:0px !important" ng-show="currentProg">

							<a href="../elearning/trainingreinforcement" >
								<img src="{[{noProg}]}" class="thumbvimeo">
							</a>

							<p class="more"><a href="../elearning/trainingreinforcement" style="float:left !important;"  target="_blank" ng-show="currentProg">Classroom Activities Introduction</a></p>
						</div>	




					</div>
					<div class="w3-box">
						<p class="sec-ttl nm">Upcoming Events</p>
						<div class="up-events" ng-controller="EventglobalCtrl">
							<ul style="background:#fff;">
								<li ng-repeat="data in data.data | limitTo:5" class="upcoming_list" >
								<a href="/../event/{[{data.eventid}]}/" target="_blank">
									{[{data.title}]}</a> <br>
								<span class="up_dates">From:{[{data.datefrom.replace("-", "/").replace("-", "/").substring(2, 10)}]} - To: {[{data.dateto.replace("-", "/").replace("-", "/").substring(2, 10)}]}</span><span class="up_local">
								</span>
							</li>
						</ul>
						<p class="more"><a href="/../power-brain-training/event"  target="_blank">See More Upcoming Events</a></p>

					</div>	
				</div>
			</div>
			<div style="clear:both;" ></div>
			<div style="margin-bottom:100px;"></div>
		</div>
	</div>





	<div class="container " >
		<!-- SUBFOOTER -->
		<?=$this->view->getRender('etemplates', 'eFooter');?> 
		<!-- SUBFOOTER -->
	</div>
</div>
<div class="container">
	<!-- FOOTER -->
	<footer class="fcopy">
		Copyright 2015 Power Brain Training Center. All rights reserved.
	</footer>
	<!-- END FOOTER -->
</div>






<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
<script src="/fe/scripts/others/bootstrap.min.js"></script>
<script src="/fe/scripts/others/resposive-menu.js"></script>
<!-- ANGULAR-->
<?=$scritps ?>

<!-- ///USER -->
<script type="text/javascript" src="/fe/scripts/controllers/user/userinfo.js"></script>
<!-- ///CENTER -->
<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>
<!-- ///ELEARNING -->
<script type="text/javascript" src="/fe/scripts/controllers/elearning/viewblog.js"></script> <!-- CONTROLLER -->

<script type="text/javascript" src="/fe/scripts/factory/elearning/eblog.js"></script> <!-- FACTORY -->
<script type="text/javascript" src="/fe/scripts/factory/elearning/training.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/elearning/egallery.js"></script>
<!-- SLIDER -->
<script type="text/javascript" src="/fe/css/e/slider.js"></script>

<!-- ///Event -->
<script type="text/javascript" src="/fe/scripts/controllers/globalevent/global.js"></script>
<script type="text/javascript" src="/fe/scripts/factory/globalevent/globalfactory.js"></script>
<script>
	$("#slider-container").sliderUi({
		speed: 700,
		cssEasing: "cubic-bezier(0.285, 1.015, 0.165, 1.000)"
	});
	$("#caption-slide").sliderUi({
		caption: true
	});
	$('.fadein img:gt(0)').hide();

	setInterval(function () {
		$('.fadein :first-child').fadeOut()
		.next('img')
		.fadeIn()
		.end()
		.appendTo('.fadein');
	}, 4000); 
</script>



</body>
</html>