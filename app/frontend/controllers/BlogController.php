<?php

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Controllers\ControllerBase as CB;
use \Phalcon\Mvc\View;

class BlogController extends ControllerBase
{
  public function page404($slug, $page){
    if(@$this->curl('check/page404/'.$slug.'/'.$page)->main[0]->error){return $this->return404();}
    if(@$this->curl('check/page404/'.$slug.'/'.$page)->sub[0]->error){return $this->return404();}
  }
  public function _settings(){
    $this->view->logoimage    = $this->curl('/index/route/slug/page/others')->settings_managesettings;
    $this->view->script_google  = $this->curl('/index/route/slug/page/others')->settings_script;
  }

  public function _titlepage($slug, $page, $others){
    $this->view->slugtitle = @$this->curl('/index/route/'.$slug.'/page/others')->slugtitle->title;
    $this->view->titlepage = @$this->curl('/index/route/slug/page/'.$others)->pagetitle->title;
  }

  public function _testimonial_pagecntent($slug, $page, $others){
    foreach($this->pageslugs($slug,$page, $others) as $menus => $curl){$this->view->$menus = $curl;}
    foreach($this->curltetimonial() as $title => $curl){$this->view->$title = $curl;}
  }

  public function _sfcm(){
    $this->view->scritps= $this->_scripts();
    $this->view->footer= $this->_footer();
    $this->view->cur_state = @$page;
    $this->mainmenus();
  }


  function mainmenus(){
    $this->_settings(); 
    $this->page404($slug, $page);   
    $this->_testimonial_pagecntent($slug, $page, $others); //*Function
    
    foreach($this->centerinfo(@$slug,@$page, @$others) as $center => $curl){$this->view->$center = $curl;}
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function pageAction(){
    $this->_sfcm();
  }
  public function readAction($idno){ 
    
   $this->view->title= @$this->curl('/read/blog/'.$idno)->blog[0]->title;
   $this->view->description= @$this->curl('/read/blog/'.$idno)->blog[0]->description; 
   $this->view->featured= @$this->curl('/read/blog/'.$idno)->blog[0]->featured;   
   $this->view->blogbody= @$this->curl('/read/blog/'.$idno)->blog[0]->body;
   $this->_sfcm();
   $this->view->idno= $idno;
 }

 public function tagAction($tagsslugs){  
  $this->_sfcm();
  $this->view->tagsslugs= $tagsslugs;  
}

public function categoryAction($cat){   
  $this->_sfcm();
  $this->view->cat= $cat;
}

public function archieveAction($publish){    
  $this->_sfcm();
  $this->view->publish= $publish;
}


public function previewAction(){    


  $authorinfo = @$this->curl('/manage/authoredit/'.$_GET['author']);
  $this->view->fullname= $authorinfo->name;
  $this->view->profilepic= $authorinfo->image;   
  $this->view->autorid= $authorinfo->id; 
  $this->view->about= $authorinfo->about; 

  $this->_sfcm();
  $this->view->publish= $publish;
}



function _footer(){
  return '
  <article class="fcon">
    <section class="fcon-box1">
      <p class="tt1 ">Click Here To</p>
      <a href="/../page/get-started/schedule-an-introductory-session" class="btn1"><b>Get Started</b><i class="arr"></i></a>
    </section>
    <section class="fcon-box2 ">
      <ul class="fcon-li box-font">
        <li><a href="/../what-is-brain-education/background">What is Brain Education?</a></li>
        <li><a href="/../benefits/focus">Benefits</a></li>
        <li><a href="/../power-brain-training/for-kids">Power Brain Training Center</a></li>
        <li><a href="/../power-brain-school/program">Power Brain Schools</a></li>
      </ul>
    </section>
    <section class="fcon-box2-1">
      <ul class="fcon-li box-font">
        <li><a href="/../get-started/introductory-session">BE Around the World</a></li>
        <li><a href="/../get-started/introductory-session">Terms and Conditions</a></li>
        <li><a href="/../get-started/introductory-session">Contact Us</a></li>
      </ul>
    </section>
    <section class="fcon-box2-2">
      <ul class="fcon-li box-font">
        <li class="t4">Find a Location</li>
        <!-- <li ng-repeat="data in data.data | limitTo:3"> ng-controller="CenterCtrl"
        <a href="/../center/power-brain-training/find-center/{[{data.slugs}]}/">{[{ data.state }]}: {[{ data.title | limitTo: 17 }]}{[{ data.title.length > 17 ? \'...\' : \'\' }]}</a>
      </li> -->
      <li>
        <a href="/../center/mesa">AZ: Gilbert/Mesa</a>
      </li>
      <li>
        NY:
        <a href="/../center/bayside"> Bayside</a>,
        <a href="/../center/syosset">Syosset</a>
      </li>
      <li>
        <a href="/../center/fairfax">VA: Fairfax</a>
      </li>
    </ul>
  </section>
  <section class="fcon-box3 ">
    <ul class="soc-mid">
      <li id="desyrel">Stay Connected</li>
      <li><a href="https://www.facebook.com/powerbraineducation" class="fb" target="_blank"><span>fb</span></a></li>
      <li><a href="https://twitter.com/powerbrainedu" class="tw"  target="_blank"><span>tw</span></a></li>
      <li><a href="https://www.youtube.com/powerbraineducation" class="yt"  target="_blank"><span>yt</span></a></li>
      <li><a href="/../index/rss" class="rss"  target="_blank"><span>rss</span></a></li>
    </ul>
  </section>
</article>
';
}

function _scripts(){
  return '
  <script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
  <script src="/fe/scripts/others/bootstrap.min.js"></script>
  <script src="/fe/scripts/others/resposive-menu.js"></script>
  <script type="text/javascript" src="/vendors/angular/angular.js"></script>
  <script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
  <script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
  <script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>
  <script type="text/javascript" src="/fe/scripts/app.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
  <script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
  <script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
  <script type="text/javascript" src="/fe/scripts/config.js"></script>
  <script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
  <script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>
  <script src="/vendors/moment/moment.js"></script>
  <script src="/vendors/angular-moment/angular-moment.js"></script>
  <script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>
  <script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>
  <script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
  <script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src="/vendors/fullcalendar/dist/gcal.js"></script>
  <script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
  <script src="/vendors/lodash/lodash.min.js"></script>
  <script src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
  <!-- Auth0 Lock script and AngularJS module -->
  <script src="/fe/scripts/others/lock-7.5.min.js"></script>
  <script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-storage/master/dist/angular-storage.js"></script>
  <script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js"></script>
  <script src="/fe/scripts/others/auth0-angular-4.js"> </script>
  <!-- ///CENTER -->
  <script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>
  <!-- ///UPLOADIMAGE -->
  <script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
  <script src="/vendors/ng-file-upload/ng-file-upload-shim.js"></script>

  ';
}

}

