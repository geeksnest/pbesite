<?php

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Models\Settings as Settings;
use \Phalcon\Mvc\View;


class MaintenanceController extends ControllerBase
{
	public function indexAction()
	{
		$this->view->logoimage 	= $this->curl('/index/route/slug/page/others')->settings_managesettings;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function errorAction() { 
        return $this->response->redirect($this->config->application->baseURL . '../view/page404/what-is-brain-education/background/page');

     }

     public function maintenanceAction() { 
        return $this->response->redirect($this->config->application->baseURL . '../maintenance');

     }
	
}

