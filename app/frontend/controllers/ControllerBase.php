<?php

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Models\Center as Center;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{


    function getStatpage($page){
        foreach ($this->curl("/pagestatic/page/".$page) as $key => $value){
           foreach ($value->body as $k => $objdata) {

            if($page=="banner"){
                $data[] = array(
                    "".$k."" => array(
                        "title"         => $objdata->title,
                        "desc"          => $objdata->desc,
                        "banner"        => $objdata->banner,
                        "position"      => $objdata->position,
                        "titlecolor"    => $objdata->titleStyle->color,
                        "titlealign"    => $objdata->titleStyle->textalign,
                        "desccolor"     => $objdata->descStyle->color,
                        "descalign"     => $objdata->descStyle->textalign,
                        "bgcolor"       => $objdata->bgcolor
                        ),
                    );
            }else{
                $data[] = array(
                    "".$k."" => array(
                        "title"     => $objdata->title,
                        "desc"      => $objdata->desc,
                        "banner"    => $objdata->banner,
                        ),
                    );
            }


        }
    } 
    return  $data ;    
} 

protected function initialize(){
    $this->view->pageBanner = @$this->getStatpage("banner");
    $this->view->pageHome = @$this->getStatpage("home");
    $this->view->social = @$this->curl("/pagestatic/page/social")[0]->body->_socialLinks;
    $this->view->schedintro = @$this->curl("/pagestatic/page/schedintro")[0]->body;
    $this->view->reqinfo = @$this->curl("/pagestatic/page/reqinfo")[0]->body;
    $this->view->reqinfoimg = @$this->curl("/pagestatic/page/reqinfo")[0]->img;
    $this->view->schedintro = @$this->curl("/pagestatic/page/schedintro")[0]->body;
    $this->view->schedintroimg = @$this->curl("/pagestatic/page/schedintro")[0]->img;
    
}

public function referer(){
    if(!@$_SERVER['HTTP_REFERER']){
        echo '<script>window.location.href = "/../elearning/login";</script>';
        exit();
    }
}
public function onConstruct(){
    $curl = curl_init($this->config->application->ApiURL. '/blog/categories');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false){
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->newscategory = $decoded;

    $curl = curl_init($this->config->application->ApiURL.'/blog/tags');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);

    if($curl_response == false){
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }

    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->newstagslist = $decoded;

    $curl = curl_init($this->config->application->ApiURL.'/blog/archives');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if($curl_response == false){
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Addition info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->archivelist = $decoded;

}

public function angularLoader($ang){
    $modules = array();
    $scripts = '';
    foreach($ang as $key => $val){
        $scripts .= $this->tag->javascriptInclude($val);
        $modules[] = $key;
    }
    $this->view->modules = (!empty($modules) ? $modules : array());
    $this->view->otherjvascript = $scripts;
}

public function httpPost($url,$params)
{
    $postData = '';
        //create name value pairs seperated by &
    foreach($params as $k => $v){
        $postData .= $k . '='.$v.'&';
    }
    rtrim($postData, '&');
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $output=curl_exec($ch);
    curl_close($ch);
    return $output;
}

public function curl($url){
    $service_url = $this->config->application->ApiURL.'/'.$url;
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    return $decoded = json_decode($curl_response);
}

public function return404(){
    return $this->response->redirect($this->config->application->baseURL . '../view/page404/what-is-brain-education/background/page');
}

public function pageslugs($slug, $page, $others){
    return array(
        "parentslug"    =>$slug,
        "dubslug"       =>$page,
        "mainMenu"      =>str_replace('-',' ',$slug),
        "subMenu"       =>str_replace('-',' ',$page),
        "subpage"       =>$others,
        "orderpages"    =>@$this->orderSubpage($page),
        "bannerinfo"    =>@$this->pbt(),
            "parentMenu"    =>@$this->curl('/index/route/slug/page/others')->parent_menu,                   //REQUEST
            "subMenus"      =>@$this->curl('/index/route/'.$slug.'/page/others')->submenu_menu,             //REQUEST
            "contentPage"   =>@$this->curl('/index/route/'.$slug.'/'.$page.'/'.$others)->getpages->datapage,//REQUEST
            "listsubpages"  =>@$this->curl('/index/route/'.$slug.'/'.$page.'/'.$others)->subpages,          //REQUEST
            );
}

public function centerinfo($slug, $page, $others){
    return array(
            "centerinfo"        =>@$this->curl('/getpage/centers/'.$others)->centerinfo,            //REQUEST
            "centerid"          =>@$this->curl('/getpage/centers/'.$others)->centerinfo->centerid,  //REQUEST
            "centerslider"      =>@$this->curl('/getpage/centers/'.$others)->slider,                //REQUEST
            "centermanager"     =>@$this->curl('/getpage/centers/'.$others)->centermanager,         //REQUEST         
            "directormessage"   =>@$this->curl('/getpage/centers/'.$others)->directormessage,       //REQUEST 
            );
}

public function pbt(){
    return array(
        "for kids"      =>array("title"=>"For Kids",    "info"=>"Our Brain Education classes and leadership programs empower kids age 4~16 to maximize their brain potential."),
        "for adults"    =>array("title"=>"For Adults",  "info"=>"Classes and empowerment workshops teach relaxation and mindfulness to create optimal life balance."),
        "for families"  =>array("title"=>"For Families","info"=>"Our monthly Family Classes and annual Family Retreats bring health and happiness to the whole family."),
        "find center"   =>array("title"=>"find center", "info"=>"BE classes and leadership training for kids and adults"),
        );
}

public function inputform(){
    return array(
        "inputform" => @$this->reqinfo(),
        "inputform2"=> @$this->sisinfo(),
        );
}

public function orderSubpage($subslug){
    $forkids     = array( "classes", "pb-student-leadership", "global-leadership-camp", "hsp-leadership-course", "summer-program");
    $foradults   = array("classes", "workshops", "successful-aging", "be-leadership-course", "testimonials");
    $forfamilies = array("family-class", "pb-family-retreat", "testimonials");
    $orderarray  = array("for-kids"=>$forkids,"for-adults"=>$foradults,"for-families"=>$forfamilies);
    return @$orderarray[$subslug]; 
}

public function curltetimonial(){
    $_testimonial = $this->curl('getpage/testimonial');
    return array(
            "pbsTestimonial"                =>@$this->curl('getpage/testimonial')->pbsTestimonial,                  //REQUEST
            "featuredforAdultTestimonial"   =>@$this->curl('getpage/testimonial')->featuredforAdultTestimonial,     //REQUEST
            "featuredforFamiliesTestimonial"=>@$this->curl('getpage/testimonial')->featuredforFamiliesTestimonial,  //REQUEST
            "forAdultsTestimonial"          =>@$this->curl('getpage/testimonial')->forAdultsTestimonial,            //REQUEST
            "forFamiliesTestimonial"        =>@$this->curl('getpage/testimonial')->forFamiliesTestimonial,          //REQUEST
            );
}

public function reqinfo(){
    return '';
}
public function sisinfo(){
    return '';
}

}