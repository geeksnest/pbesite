<?php
namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;
class ViewController extends ControllerBase
{
	public function page404($slug, $page){
		if(@$this->curl('check/page404/'.$slug.'/'.$page)->main[0]->error){return $this->return404();}
		if(@$this->curl('check/page404/'.$slug.'/'.$page)->sub[0]->error){return $this->return404();}
	}

	public function _settings(){
		$this->view->logoimage 		= $this->curl('/index/route/slug/page/others')->settings_managesettings;
		$this->view->script_google 	= $this->curl('/index/route/slug/page/others')->settings_script;
	}

	public function _titlepage($slug, $page, $others){
		$this->view->slugtitle = @$this->curl('/index/route/'.$slug.'/page/others')->slugtitle->title;
		$this->view->titlepage = @$this->curl('/index/route/slug/page/'.$others)->pagetitle->title;
	}

	public function _testimonial_pagecntent($slug, $page, $others){
		foreach($this->pageslugs($slug,$page, $others) as $menus => $curl){$this->view->$menus = $curl;}
		foreach($this->curltetimonial() as $title => $curl){$this->view->$title = $curl;}
	}
	//@@@@@@@@@
	//@@@@@@@@
	//@@@@@@@
	//@@@@@@
	//@@@@@
	//@@@
	//@@
	//@
	public function pageAction($page, $others = 'page'){
		$slug = $this->dispatcher->getParam("slug");
		if($slug==$others){
			$others ='page';
		}
		$this->view->_url= $others;
		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 	//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function
		$this->_titlepage($slug, $page, $others); 		//*Function
		foreach($this->inputform() as $form => $inputs){
			$this->view->$form = $inputs;
		}

		
	}
	
	public function centerAction( $others = 'page')
	{

		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");

		

		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function

		foreach($this->centerinfo($slug,$page, $others) as $center => $curl){$this->view->$center = $curl;}
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function centernewsAction($others = 'page'){
		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");
		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function

		$this->view->slugnews = $others;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function centernewspreviewAction($others = 'page'){
		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");
		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function

		$this->view->slugnews = $others;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);



		$this->view->categlist= @$this->curl('/news/listcategory');
		$authorinfo = @$this->curl('/manage/authoredit/'.$_GET['author']);
		$this->view->fullname= $authorinfo->name;
		$this->view->profilepic= $authorinfo->image;   
		$this->view->autorid= $authorinfo->id; 
		$this->view->about= $authorinfo->about; 
	}



	public function categoryAction($others = 'page')
	{
		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");
		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function

		$this->view->category = $others;
	}
	public function tagsAction($slug, $page, $others = 'page')
	{
		$this->_settings(); 		//*Function
		$this->page404(); 			//*Function
		$this->_testimonial_pagecntent(); //*Function
		$this->view->tags = $others;
	}
	public function aboutauthorAction($others = 'page'){

		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");

		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function
		
		$this->view->authorsid = $others;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	

	public function page404Action($slug, $page, $others = 'page'){
		$slug = $this->dispatcher->getParam("slug");
		$page = $this->dispatcher->getParam("page");
		$this->_settings(); 		//*Function
		$this->page404($slug, $page); 			//*Function
		$this->_testimonial_pagecntent($slug, $page, $others); //*Function
	}

}

