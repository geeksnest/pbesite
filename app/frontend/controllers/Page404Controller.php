<?php
namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;
class ViewController extends ControllerBase
{
	public function indexAction(){
	}
	public function page404Action(){
		$this->view->parentMenu 			= $this->curl('/index/route/slug/page/others')->parent_menu;
		$this->view->logoimage 				= $this->curl('/index/route/slug/page/others')->settings_managesettings;
		$this->view->script_google 			= $this->curl('/index/route/slug/page/others')->settings_script;
	}
}

