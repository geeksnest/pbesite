<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class ForumController extends ControllerBase{
	function _submenu(){
		$data[]= array(
			"home"=>"Home",
			"introduction"=>"Introduction",
			"trainingreinforcement"=>"Classroom Activities",
			"contentlinks"=>"Content Links",
			"gallery"=>"Gallery",
			"forum"=>"Forum",
			"mentors"=>"Mentors",
			"leaders"=>"Leaders");
		return json_decode(json_encode($data));
	}
	public function page404($slug, $page){
		if(@$this->curl('check/page404/'.$slug.'/'.$page)->main[0]->error){return $this->return404();}
		if(@$this->curl('check/page404/'.$slug.'/'.$page)->sub[0]->error){return $this->return404();}
	}
	public function _settings(){
		$this->view->logoimage    = $this->curl('/index/route/slug/page/others')->settings_managesettings;
		$this->view->script_google  = $this->curl('/index/route/slug/page/others')->settings_script;
	}
	public function _titlepage($slug, $page, $others){
		$this->view->slugtitle = @$this->curl('/index/route/'.$slug.'/page/others')->slugtitle->title;
		$this->view->titlepage = @$this->curl('/index/route/slug/page/'.$others)->pagetitle->title;
	}

	public function _testimonial_pagecntent($slug, $page, $others){
		foreach($this->pageslugs($slug,$page, $others) as $menus => $curl){$this->view->$menus = $curl;}
		foreach($this->curltetimonial() as $title => $curl){$this->view->$title = $curl;}
	}

	public function _sfmsc(){
		$this->referer();
		$this->view->scritps= $this->_scripts();
		$this->mainmenus();
		$this->view->subs= $this->_submenu();
		$this->view->cur_state = @$page;
		$this->view->teachersub = $this->teacherResources();
		$this->view->triningsub = $this->trainingreinforcement();
		$this->view->contentsub = $this->contentlinks();

		if($this->session->get('login')){
			if(!@$this->curl('/user/log/'.$this->session->get('login'))[0]->sucess){ return $this->response->redirect('/../elearning/login');}
		}else{return $this->response->redirect('/../elearning/login');}
	}

	public function _verify(){
		$this->view->scritps= $this->_scripts();
		$this->mainmenus();
		$this->view->subs= $this->_submenu();
		$this->view->cur_state = @$page;
		$this->view->teachersub = $this->teacherResources();
		$this->view->triningsub = $this->trainingreinforcement();
		$this->view->contentsub = $this->contentlinks();
	}

	public function _scm(){
		$this->view->subs= $this->_submenu();
		$this->view->cur_state = @$page;
		$this->mainmenus();
	}

	
	function mainmenus(){
		$this->_settings(); 
		$this->page404($slug, $page);   
		$this->_testimonial_pagecntent($slug, $page, $others); 

		foreach($this->centerinfo(@$slug, @$page, @$others) as $center => $curl){$this->view->$center = $curl;}


		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}


	public function indexAction($slug=" ", $page ="page", $others = 'page'){
		$this->_sfmsc();
		$this->view->img_gal = @$this->curl('/egallery/banner');
	}



	function teacherResources(){
		return '
		<div class="sub-menu">
			<table>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - The Oxygen Mask Principle</a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - 10 Tips to Boost Your Energy</a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - Creating a Power Brain Culture in the Classroom </a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - BE & Positive Discipline </a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - Brain Breaks</a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - Energy Masters</a></td></tr>
				<tr><td><a href="#" class="sub_link" style="padding:0px;border-style:none;color:#555;"> - How to Integrate BE throughout the School Day</a></td></tr>
			</table>
		</div>
		';
	}

	function trainingreinforcement(){
		return '
		<div class="sub-menu2">
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/TEAM-BUILDING"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/ysdzg9icnmi1444103334015.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/PHYSICAL-HEALTH"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/qx1cvbfn7b91444103383610.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/FOCUS"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/zk3er110pb91444722466663.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/MINDFULNESS"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/0m268v6ajor1444721564945.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/MEMORY"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/ojz6j8xgvi1444722295826.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/EMOTIONAL-WELLNESS"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/m9d9s76tj4i1444723317568.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/CONFIDENCE"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/7mjfavsfw291444724192779.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/CREATIVITY"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/thc9e3lerk91444723639075.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/CHARACTER"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/6da6ckawcdi1444725007362.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/trainingpage/CITIZENSHIP"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/jy73eq4zpvi1444724396123.png"></a>
		</div>
		';
	}

	function contentlinks(){
		return '
		<div class="sub-menu3">
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/contentpage/ELA"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/1zmr3u40a4i1444899446985.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/contentpage/MATH"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/vo7xw53ik91444899611159.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/contentpage/SCIENCE"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/xcadp1fw291444899830515.png"></a>
			<a class="sub_link" style="padding:0px;border-style:none;" href="/../elearning/contentpage/SOCIAL-STUDIES"><img src="https://powerbrain.s3.amazonaws.com/uploads/banner/u167zbedn291444899894647.png"></a> 
		</div>
		';
	}



	function _scripts(){
		return '
		<script type="text/javascript" src="/fe/scripts/others/jquery-1.9.1.min.js"></script>
		<script src="/fe/scripts/others/bootstrap.min.js"></script>
		<script src="/fe/scripts/others/resposive-menu.js"></script>
		<script type="text/javascript" src="/vendors/angular/angular.js"></script>
		<script type="text/javascript" src="/vendors/angular-cookies/angular-cookies.min.js"></script>
		<script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
		<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
		<script type="text/javascript" src="/be/js/angular/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="/be/js/angular/angular-translate.js"></script>
		<script type="text/javascript" src="/be/js/angular/ngStorage.min.js"></script>
		<script type="text/javascript" src="/be/js/angular/ui-load.js"></script>
		<script type="text/javascript" src="/be/js/angular/ui-jq.js"></script>
		<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
		<script type="text/javascript" src="/be/js/angular/ui-validate.js"></script>
		<script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
		<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>
		<script type="text/javascript" src="/fe/scripts/app.js"></script>
		<script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
		<script type="text/javascript" src="/fe/scripts/controllers/controllers.js"></script>
		<script type="text/javascript" src="/fe/scripts/directives/directives.js"></script>
		<script type="text/javascript" src="/fe/scripts/config.js"></script>
		<script type="text/javascript" src="/fe/scripts/controllers/centernews/centernews.js"></script>
		<script type="text/javascript" src="/fe/scripts/controllers/calendar/calendar.js"></script>
		<script src="/vendors/moment/moment.js"></script>
		<script src="/vendors/angular-moment/angular-moment.js"></script>
		<script src="/globaljs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>
		<script src="//cdn.jsdelivr.net/angular.bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>
		<script src="/vendors/angular-ui-calendar/calendar.min.js"></script>
		<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
		<script src="/vendors/fullcalendar/dist/gcal.js"></script>
		<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
		<script src="/vendors/lodash/lodash.min.js"></script>
		<script src="//maps.googleapis.com/maps/api/js?sensor=false"></script>

		<!-- Auth0 Lock script and AngularJS module -->
		<script src="/fe/scripts/others/lock-7.5.min.js"></script>
		<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-storage/master/dist/angular-storage.js"></script>
		<script type="text/javascript" src="//cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js"></script>
		<script src="/fe/scripts/others/auth0-angular-4.js"> </script>
		<!-- ///CENTER -->
		<script type="text/javascript" src="/fe/scripts/controllers/centers/centerctrl.js"></script>
		<script type="text/javascript" src="/fe/scripts/factory/center/centerinfo.js"></script>
		<!-- ///UPLOADIMAGE -->
		<script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
		<script src="/vendors/ng-file-upload/ng-file-upload-shim.js"></script>
		';
	}
}

