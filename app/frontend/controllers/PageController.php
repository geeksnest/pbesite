<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PageController extends ControllerBase
{

  public function page404($slug, $page){
    if(@$this->curl('check/page404/'.$slug.'/'.$page)->main[0]->error){return $this->return404();}
    if(@$this->curl('check/page404/'.$slug.'/'.$page)->sub[0]->error){return $this->return404();}
  }
  public function _settings(){
    $this->view->logoimage    = $this->curl('/index/route/slug/page/others')->settings_managesettings;
    $this->view->script_google  = $this->curl('/index/route/slug/page/others')->settings_script;
  }

  public function _titlepage($slug, $page, $others){
    $this->view->slugtitle = @$this->curl('/index/route/'.$slug.'/page/others')->slugtitle->title;
    $this->view->titlepage = @$this->curl('/index/route/slug/page/'.$others)->pagetitle->title;
  }

  public function _testimonial_pagecntent($slug, $page, $others){
    foreach($this->pageslugs($slug,$page, $others) as $menus => $curl){$this->view->$menus = $curl;}
    foreach($this->curltetimonial() as $title => $curl){$this->view->$title = $curl;}
  }  
  //@@@@@@@@@
  //@@@@@@@@
  //@@@@@@@
  //@@@@@@
  //@@@@@
  //@@@
  //@@
  //@
  public function eventAction($others = 'page'){

    $slug = $this->dispatcher->getParam("slug");
    $page = $this->dispatcher->getParam("page");

    $this->_settings(); 
    $this->page404($slug, $page);  
    $this->_testimonial_pagecntent($slug, $page, $others); //*Function
    $this->view->eventid= $others;
  }

  public function eventsAction($slug, $page, $others = 'page'){ 
   $this->_settings(); 
   $this->page404($slug, $page); 
   $this->_testimonial_pagecntent($slug, $page, $others); //*Function
  }

}

