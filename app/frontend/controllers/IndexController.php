<?php

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Models\Settings as Settings;
use \Phalcon\Mvc\View;
use Phalcon\Http\Request;

class IndexController extends ControllerBase
{
	public function indexAction(){
		$this->view->parentMenu 			= $this->curl('/index/route/slug/page/others')->parent_menu;
		$this->view->featuredTestimonial 	= $this->curl('/index/route/slug/page/others')->testimonial;
		$this->view->mainpageslider			= $this->curl('/index/route/slug/page/others')->slider;
		$this->view->logoimage 				= $this->curl('/index/route/slug/page/others')->settings_managesettings;
		$this->view->script_google 			= $this->curl('/index/route/slug/page/others')->settings_script;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function rssAction(){
		$this->view->datarss = $this->curl('latest/rss') ;
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function errorAction() { 
        return @$this->response->redirect($this->config->application->baseURL . '../view/page404/what-is-brain-education/background/page');
    }

    public function maintenanceAction() { 
        return $this->response->redirect($this->config->application->baseURL . '../maintenance');
    }
	
}

