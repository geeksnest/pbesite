<?php

namespace Modules\Frontend;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use \Phalcon\Mvc\Dispatcher as PhDispatcher;
use \Phalcon\Events\Manager as Eventsmanager;
use  Phalcon\Mvc\View;
class Module
{
	public function registerAutoloaders()
	{
		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Modules\Frontend\Controllers' => __DIR__ . '/controllers/',
			'Modules\Frontend\Models' => __DIR__ . '/models/',
			));

		$loader->register();
	}
	public function registerServices($di)
	{
		/**
		 * Read configuration
		 */
		$config = include __DIR__ . "/config/config.php";

		
		$di['dispatcher'] = function() use ($di) {
			$evManager = $di->getShared('eventsManager');

			$evManager->attach(
				"dispatch:beforeException",
				function($event, $dispatcher, $exception)
				{
					switch ($exception->getCode()) {
						case PhDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
						case PhDispatcher::EXCEPTION_ACTION_NOT_FOUND:
						$dispatcher->forward(
							array(
								'controller' => 'index',
								'action'     => 'error',
								)
							);
						return false;
					}
				}
				);
			$dispatcher = new PhDispatcher();
			$dispatcher->setEventsManager($evManager);
			$dispatcher->setDefaultNamespace("Modules\Frontend\Controllers");
			return $dispatcher;
		};


		/**
		 * Dispatch Controller nad View
		 */

		$di->set('cdispatcher', function(){

			//Create an event manager
			$eventsManager = new Eventsmanager();

			//Attach a listener for type "dispatch"
			$eventsManager->attach("cdispatcher", function($event, $dispatcher){
				// ...
			});

			$dispatcher = new PhDispatcher();
			$dispatcher->setEventsManager($eventsManager);

			return $dispatcher;

		}, true);






		/**
		 * Setting up the view component
		 */
		$di['view'] = function() {

			// $uri = explode("/", $_SERVER['REQUEST_URI']);
			// if($uri[1]=="page"){
			// 	$_layout = "main";
			// }elseif($uri[1]=="blog"){
			// 	$_layout = "blog";
			// }elseif($uri[1]=="blogs"){
			// 	$_layout = "blog";
			// }

			$view = new \Phalcon\Mvc\View();
			$view->setViewsDir(__DIR__ . '/views/');
			$view->setLayoutsDir('layouts/');
			$view->setTemplateAfter('main');

			$view->registerEngines(array(
				'.volt' => function ($view, $di){

					$volt = new VoltEngine($view, $di);

					$volt->setOptions(array(
						'compiledPath' => __DIR__ . '/cache/',
						'compiledSeparator' => '_'
						));

					return $volt;
				},
				'.phtml' => 'Phalcon\Mvc\View\Engine\Php'
				));		    
			return $view;
		};

		/**
		 * Database connection is created based in the parameters defined in the configuration file
		 */
		$di['db'] = function() use ($config) {
			return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name
				));
		};

		//Registering a shared view component
		// $di['view'] = function() {
		// 	$view = new View();
		// 	$view->setViewsDir('../app/views/');
		// 	return $view;
		// };

	}

}